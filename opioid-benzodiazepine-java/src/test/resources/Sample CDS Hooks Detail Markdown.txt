Maximum morphine equivalent daily dose (MEDD) is __210.5__ mg/day (PRN meds assumed to be taken at maximum allowed frequency). Taper to < 50.

<style> 
  table, th, td {  padding: 2px; vertical-align: top; } 
  input { display: none; visibility: hidden; } 
  label { display: block; color: #666; } 
  label::before { font-family: Consolas, monaco, monospace; font-weight: bold; content: "+"; vertical-align: text-center; text-align: center; display: inline-block; width: 18px; height: 18px; background: radial-gradient(circle at center, #CCC 50%,  transparent 50%); } 
   #expand0 { height: 0px; overflow: hidden; transition: height 0.3s; } 
   #toggle0:checked ~ #expand0 { height: auto; }	
   #toggle0:checked ~ label::before { content: "-"; } 
   #expand1 { height: 0px; overflow: hidden; transition: height 0.3s; } 
   #toggle1:checked ~ #expand1 { height: auto; }	
   #toggle1:checked ~ label::before { content: "-"; } 
   #expand2 { height: 0px; overflow: hidden; transition: height 0.3s; } 
   #toggle2:checked ~ #expand2 { height: auto; }	
   #toggle2:checked ~ label::before { content: "-"; } 
   #expand3 { height: 0px; overflow: hidden; transition: height 0.3s; } 
   #toggle3:checked ~ #expand3 { height: auto; }	
   #toggle3:checked ~ label::before { content: "-"; } 
</style> 
<table border> 
<tr> 
  <th>Active Opioid Rx</th> 
  <th>Max MEDD</th> 
</tr> 
<tr> 
  <td><b>[ New ]</b> 72 HR Buprenorphine 0.035 MG/HR Transdermal System<br>> Sig: 1 patch Transdermal Every 72 hours 
   <input id="toggle0" type="checkbox" unchecked> 
   <label for="toggle0">Details</label> 
   <div id="expand0">> Daily dose: Buprenorphine patch: 1 * 35 mcg/hr = 35 mcg/hr. Morhpine equivalence: 1.8x.</div> 
 </td> 
 <td>63 mg</td> 
</tr> 
<tr> 
  <td>12 HR Oxycodone Hydrochloride 15 MG Extended Release Oral Tablet<br>> Sig: Take 1 tablet by mouth Every 12 hours for 30 days. 
   <input id="toggle1" type="checkbox" unchecked> 
   <label for="toggle1">Details</label> 
   <div id="expand1">> Prescriber: HISTORICAL, MEDS. Rx date: 2017-03-01.<br>> Dispense: 60 tablets. Refills: 0. Expected supply duration: through 2017-03-30.<br>> Daily dose: Oxycodone Extended Release Oral Tablet 2/d * 15 mg = 30 mg. Morhpine equivalence: 1.5x.</div> 
 </td> 
 <td>45 mg</td> 
</tr> 
<tr> 
  <td>Acetaminophen 300 MG / Oxycodone Hydrochloride 2.5 MG Oral Tablet<br>> Sig: Take 1 tablet by mouth Every 4 hours as needed for pain. 
   <input id="toggle2" type="checkbox" unchecked> 
   <label for="toggle2">Details</label> 
   <div id="expand2">> Prescriber: Michael Flynn, MD (Internal Medicine/Pediatrics). Rx date: 2017-03-01.<br>> Dispense: 60 tablets. Refills: 1. Expected supply duration: through 2017-03-21.<br>> Daily dose: Oxycodone Oral Tablet 6/d * 1 tablet * 2.5 mg = 15 mg. Morhpine equivalence: 1.5x.</div> 
 </td> 
 <td>22.5 mg</td> 
</tr> 
<tr> 
  <td>Methadone Hydrochloride 10 MG Oral Tablet [Dolophine]<br>> Sig: Take 1 tablet by mouth Every 12 hours for 30 days. 
   <input id="toggle3" type="checkbox" unchecked> 
   <label for="toggle3">Details</label> 
   <div id="expand3">> Prescriber: Michael Flynn, MD (Internal Medicine/Pediatrics). Rx date: 2017-01-01.<br>> Dispense: 180 tablets. Refills: 4. Expected supply duration: through 2017-05-30.<br>> Daily dose: Methadone Oral Tablet 2/d * 1 tablet * 10 mg = 20 mg. Morhpine equivalence: 4x.</div> 
 </td> 
 <td>80 mg</td> 
</tr> 
<tr> 
  <td>Total</td> 
  <td>210.5 mg</td> 
</tr> 
</table>