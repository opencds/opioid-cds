package org.opencds

import org.opencds.config.api.cache.CacheService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration

import spock.lang.Specification

@ContextConfiguration(locations = "/WEB-INF/beans.xml")
class BeansSpec extends Specification {
    
	@Autowired
	CacheService cacheService
	
    def "test beans setup"() {
        expect:
		cacheService
    }
}
