package org.opencds.opioid.service.image

import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response

import spock.lang.Ignore
import spock.lang.Specification

@Ignore
class OpioidImageRestServiceSpec extends Specification {

	def 'test image rest service'() {
		given:
		String imageId = 'ce5398ad-02ff-45bb-80ef-c6fb4d255252.png'
		OpioidImageRestService service = new OpioidImageRestService()

		when:
		Response response = service.getImage(imageId)

		then:
		println response.mediaType.getClass()
		response.mediaType == new MediaType('image', 'png')
		response.status == 200
		println response.entity
		new File('test.png') << response.entity
		true
	}
}
