package org.opencds.opioid.service.image

import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.util.concurrent.TimeUnit

import spock.lang.Specification

class OpioidImageRemovalServiceSpec extends Specification {
	OpioidImageRemovalService service

	def 'test quartz cron service'() {
		given:
		String cronExpression = '* * * * * ?'
		Path location = Files.createTempDirectory('image-cache')
		String filePattern = '[A-Za-z0-9]{8}-[A-Za-z0-9]{4}-[A-Za-z0-9]{4}-[A-Za-z0-9]{4}-[A-Za-z0-9]{12}\\.png'
		long age = 5
		TimeUnit ageUnit = TimeUnit.SECONDS

		and:
		(0..10).each {
			File file = Paths.get(location.toString(), UUID.randomUUID().toString() + ".png").toFile()
			println file
			file << ''
		}

		when:
		service = new OpioidImageRemovalService(cronExpression, location.toString(), filePattern, age, ageUnit)
		sleep 10000
		location.toFile().delete();

		then:
		true
	}
}
