package org.opencds.opioid.service.image;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ImageRemovalService implements Job {
	private static final Log log = LogFactory.getLog(ImageRemovalService.class);

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		JobDataMap jdm = context.getJobDetail().getJobDataMap();
		Pattern filePattern = Pattern.compile((String) jdm.get(OpioidImageRemovalService.PATTERN_KEY));
		String location = (String) jdm.get(OpioidImageRemovalService.LOCATION_KEY);
		long age = (long) jdm.get(OpioidImageRemovalService.AGE_KEY);
		TimeUnit ageTU = (TimeUnit) jdm.get(OpioidImageRemovalService.AGE_TU_KEY);
		Path path = Paths.get(location);
		if (!Files.isDirectory(path)) {
			log.error("File location is not a directory: " + location);
			return;
		}
		try {
			Date nowDT = new Date();
			long now = nowDT.getTime();
			Files.list(path).forEach((Path p) -> {
				File file = p.toFile();
				Matcher matcher = filePattern.matcher(file.getName());
				if (matcher.find() && olderThan(now-file.lastModified(), age, ageTU)) {
					log.info("Deleting cached file: " + p);
					p.toFile().delete();
				}
			});
		} catch (IOException e) {
			log.error("Unable to iterate over file list.", e);
		}
	}

	private boolean olderThan(long lastModified, long age, TimeUnit ageTU) {
		System.err.println("lastModified > limit: " + lastModified + " > " + ageTU.toMillis(age));
		return lastModified > ageTU.toMillis(age);
	}

}
