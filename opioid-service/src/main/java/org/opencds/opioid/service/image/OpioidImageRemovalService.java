package org.opencds.opioid.service.image;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.CronScheduleBuilder;
import org.quartz.CronTrigger;
import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class OpioidImageRemovalService {
	private static final Log log = LogFactory.getLog(OpioidImageRemovalService.class);
	public static final String PATTERN_KEY = "FILEPATTERN";
	public static final String LOCATION_KEY = "FILELOCATION";
	public static final String AGE_KEY = "AGE";
	public static final String AGE_TU_KEY = "AGE_TU";

	private Scheduler sched;

	public OpioidImageRemovalService(String cronExpression, String location, String filePattern, long age, TimeUnit ageUnit) {
	    this(cronExpression, location, filePattern, age, ageUnit, null);
	}

    public OpioidImageRemovalService(String cronExpression, String location, String filePattern, long age, TimeUnit ageUnit, String timerName) {
		Properties props = new Properties();
		props.setProperty("org.quartz.scheduler.skipUpdateCheck","true");
		props.setProperty("org.quartz.jobStore.class", "org.quartz.simpl.RAMJobStore");
		props.setProperty("org.quartz.threadPool.class", "org.quartz.simpl.SimpleThreadPool");
		props.setProperty("org.quartz.threadPool.threadCount", "1");

		try {
			SchedulerFactory sf = new StdSchedulerFactory(props);
			sched = sf.getScheduler();
			JobDataMap jdm = new JobDataMap();
			jdm.put(PATTERN_KEY, filePattern);
			jdm.put(LOCATION_KEY, location);
			jdm.put(AGE_KEY, age);
			jdm.put(AGE_TU_KEY, ageUnit);
			String name = "FileRemoval";
			String cronTriggerName = "FileRemovalCronTrigger";
			if (timerName != null) {
			    name = new StringBuilder(timerName).append(".").append(name).toString();
			    cronTriggerName = new StringBuilder(timerName).append(".").append(cronTriggerName).toString();
			}
			JobDetail job = JobBuilder.newJob(ImageRemovalService.class)
					.withIdentity(name)
					.usingJobData(jdm)
					.build();
			CronTrigger trigger = TriggerBuilder.newTrigger()
					.withIdentity(cronTriggerName)
					.withSchedule(CronScheduleBuilder.cronSchedule(cronExpression)).build();
			sched.scheduleJob(job, trigger);
			sched.start();
		} catch (SchedulerException e) {
		    e.printStackTrace();
			log.warn("Unable to schedule image cache removal; cache will increase disk usage over time.");
		}
	}

	public void shutdown() {
		if (sched != null) {
			try {
				sched.shutdown();
			} catch (SchedulerException e) {
				log.error("Encountered exception during scheduler shutdown.", e);
			}
		}
	}
}
