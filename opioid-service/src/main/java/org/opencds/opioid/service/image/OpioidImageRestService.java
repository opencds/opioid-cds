package org.opencds.opioid.service.image;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opencds.hooks.services.util.Responses;

import javax.ws.rs.GET;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

public class OpioidImageRestService {
	private static final Log log = LogFactory.getLog(OpioidImageRestService.class);

	private static final String CONTENT_TYPE = "Content-Type";
	private static final String IMAGE_PNG = "image/png";
	private static final String IMAGE_SVG = "image/svg+xml";
	private final Path cachePath;

	public OpioidImageRestService() {
		cachePath = getFallbackPath();
	}

	public OpioidImageRestService(String imageCachePath) {
		Path path = null;
		if (StringUtils.isNotEmpty(imageCachePath)) {
			path = Paths.get(imageCachePath);
		}
		if (path == null) {
			path = getFallbackPath();
		}
		cachePath = path;
	}

	private Path getFallbackPath() {
		return Paths.get(System.getProperty("user.home"), ".fhir", "image-cache");
	}

	@GET
//	@Produces({IMAGE_PNG})
	@javax.ws.rs.Path("{image}")
	public Response getImage(@PathParam("image") String imageId) {
		File file = Paths.get(cachePath.toString(), imageId).toFile();
		if (!file.exists()) {
			return Responses.notFound(imageId + " not found.");
		}
		System.err.println("File size: " + file.length());
		try (DataInputStream fis = new DataInputStream(new FileInputStream(file))) {
			byte[] b = new byte[(int) file.length()];
			fis.readFully(b);
			System.err.println("byte length: " + b.length);
			String imageType = "";
			if (file.toString().endsWith("png")) {
				imageType = IMAGE_PNG;
			} else if (file.toString().endsWith("svg")) {
				imageType = IMAGE_SVG;
			}
			Map<String, String> headers = new HashMap<>();
			headers.put(CONTENT_TYPE, imageType);
			return Responses.ok(b, headers);
		} catch (IOException e) {
			log.error(e.getMessage(), e);
			return Responses.internalServerError(e.getMessage());
		}
	}
}
