<p>aximum morphine equivalent daily dose (MEDD) is <strong>210.5</strong> mg/day (PRN meds assumed to be taken at maximum allowed frequency). Taper to &lt; 50.</p>
<style> 
  table, th, td {  padding: 2px; vertical-align: top; } 
  input { display: none; visibility: hidden; } 
  label { display: block; color: #666; } 
  label::before { font-family: Consolas, monaco, monospace; font-weight: bold; content: ; vertical-align: text-center; text-align: center; display: inline-block; width: 18px; height: 18px; background: radial-gradient(circle at center, #CCC 50%,  transparent 50%); } 
   #expand0 { height: 0px; overflow: hidden; transition: height 0.3s; } 
   #toggle0:checked ~ #expand0 { height: auto; }	
   #toggle0:checked ~ labe::before { content: "-"; } 
   #expand1 { height: 0px; overflow: hidden; transition: height 0.3s; } 
   #toggle1:checked ~ #expand1 { height: auto; }	
   #toggle1:checked ~ labe::before { content: "-"; } 
   #expand2 { height: 0px; overflow: hidden; transition: height 0.3s; } 
   #toggle2:checked ~ #expand2 { height: auto; }	
   #toggle2:checked ~ labe::before { content: "-"; } 
   #expand3 { height: 0px; overflow: hidden; transition: height 0.3s; } 
   #toggle3:checked ~ #expand3 { height: auto; }	
   #toggle3:checked ~ labe::before { content: "-"; } 
</style> 
<table border> 
<tr> 
  <th>Active Opioid Rx</th> 
  <th>Max MEDD</th> 
</tr> 
<tr> 
  <td><b>[ New ]</b> 72 HR Buprenorphine 0.035 MG/HR Transdermal System<br>>Sig: 1 patch q72h 
   <input id="toggle0" type="checkbox" unchecked> 
   <label for="toggle0">Details</label> 
   <div id="expand0">Daily dose: Buprenorphine patch: 1 * 35 mcg/hr = 35 mcg/hr. Morhpine equivalence: 1.8x.</div> 
 </td> 
</tr> 
<tr> 
  <td>12 HR Oxycodone Hydrochloride 15 MG Extended Release Oral Tablet<br>>Sig: 15 mg q12h 
   <input id="toggle1" type="checkbox" unchecked> 
   <label for="toggle1">Details</label> 
   <div id="expand1">Daily dose: Oxycodone Extended Release Oral Tablet: 2/d * 15 mg = 30 mg. Morhpine equivalence: 1.5x.</div> 
 </td> 
</tr> 
<tr> 
  <td>Acetaminophen 300 MG / Oxycodone Hydrochloride 2.5 MG Oral Tablet<br>>Sig: 1 tablet q4h PRN 
   <input id="toggle2" type="checkbox" unchecked> 
   <label for="toggle2">Details</label> 
   <div id="expand2">Daily dose: Oxycodone Oral Tablet: 6/d * 1 tablet * 2.5 mg = 15 mg. Morhpine equivalence: 1.5x.</div> 
 </td> 
</tr> 
<tr> 
  <td>Methadone Hydrochloride 10 MG Oral Tablet [Dolophine]<br>>Sig: 1 tablet q12h 
   <input id="toggle3" type="checkbox" unchecked> 
   <label for="toggle3">Details</label> 
   <div id="expand3">Daily dose: Methadone Oral Tablet: 2/d * 1 tablet * 10 mg = 20 mg. Morhpine equivalence: 4x.</div> 
 </td> 
</tr> 
<tr> 
  <td>Total</td> 
  <td>210.5 mg</td> 
</tr> 
</table>