package org.opencds.plugin.opioid;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opencds.opioid.config.OpioidConfig;
import org.opencds.plugin.PluginContext.PreProcessPluginContext;
import org.opencds.plugin.PreProcessPlugin;
import org.opencds.plugin.SupportingData;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;

public class OpioidSupportingDataPlugin implements PreProcessPlugin {
    private static final Log log = LogFactory.getLog(OpioidSupportingDataPlugin.class);

    private static final String PROPERTIES_SD_TYPE = "properties";
    private static final String OPIOID_CONFIG_GLOBAL = "opioidConfig";

    @Override
    public void execute(PreProcessPluginContext context) {
        log.debug(getClass().getSimpleName() + ": Applying Opioid MME Supporting Data.");
        Map<String, SupportingData> sdMap = context.getSupportingData();
        if (sdMap.size() > 1) {
            log.error("Expected only one SupportingData. Unable to proceed with pre-processing.");
            return;
        }
        SupportingData sd = sdMap.entrySet().stream().map(e -> e.getValue()).findFirst().orElse(null);
        log.debug("SD: " + sd);
        if (sd == null) {
            log.error("Did not find expected SupportingData. Unable to proceed with pre-processing.");
            return;
        }
        if (PROPERTIES_SD_TYPE.equalsIgnoreCase(sd.getPackageType())) {
            OpioidConfig opioidConfig = context.getCache().get(sd);
            if (opioidConfig == null) {
                opioidConfig = OpioidConfig.build(getProperties(sd));
                context.getCache().put(sd, opioidConfig);
            }
            context.getGlobals().put(OPIOID_CONFIG_GLOBAL, opioidConfig);
            log.debug(opioidConfig.toString());
        } else {
            log.error("Unexpected packageType for SupportingData: " + sd);
        }
    }

    private Properties getProperties(SupportingData sd) {
            Properties properties = new Properties();
            try {
                log.debug("Loading properties: " + sd.getPackageId());
                properties.load(new FileInputStream(sd.getSupportingDataPackage().getFile()));
            } catch (IOException e) {
                log.error("Unable to load supporting data package: " + sd.getPackageId(), e);
            }
            log.debug("Loaded Properties: ");
            properties.keySet().forEach(k -> log.debug(k + " -> " + properties.getProperty((String)k)));
            return properties;
    }

}
