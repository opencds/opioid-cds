package org.opencds.plugin.opioid

import org.opencds.opioid.config.OpioidConfig
import org.opencds.plugin.PluginContext.PreProcessPluginContext
import org.opencds.plugin.SupportingData
import org.opencds.plugin.SupportingDataPackage
import org.opencds.plugin.SupportingDataPackageSupplier
import org.opencds.plugin.opioid.OpioidSupportingDataPlugin
import org.opencds.plugin.support.PluginDataCacheImpl
import spock.lang.Specification

import java.nio.file.Files
import java.nio.file.Path

class OpioidSupportingDataPluginSpec extends Specification {
    private static final String OPIOID_CONFIG = 'opioidConfig'
    private static String chronicOpioidUseDeterminationMethod = "NONE"
    private static String localGuidelineUrl = "http://utah.edu/guidelines"
    private static String localGuidelineLabel = "Guidelines"
    private static boolean reverseMistakenLabelingOfStartAndEndDatesWithUtcTimeZone = true
    private static boolean useDynamicTableFormat = true
    private static boolean returnCardForDebug = true

    private static final String PROPERTIES = """
chronicOpioidUseDeterminationMethod=${chronicOpioidUseDeterminationMethod}
localGuidelineUrl=${localGuidelineUrl}
localGuidelineLabel=${localGuidelineLabel}
reverseMistakenLabelingOfStartAndEndDatesWithUtcTimeZone=${reverseMistakenLabelingOfStartAndEndDatesWithUtcTimeZone}
useDynamicTableFormat=${useDynamicTableFormat}
returnCardForDebug=${returnCardForDebug}
"""

    OpioidSupportingDataPlugin plugin


    def setup() {
        plugin = new OpioidSupportingDataPlugin()
    }

    def 'test propertiesSupplier'() {
        given:
        SupportingData sd = new SupportingData(
                'identifier',
                'kmid',
                'loadedby',
                'pkgid',
                OpioidSupportingDataPlugin.PROPERTIES_SD_TYPE,
                new Date(),
                new SupportingDataPackageSupplier() {
                    SupportingDataPackage get() {
                        return [
                                getBytes : {PROPERTIES.bytes},
                                getFile : {
                                    Path path = Files.createTempFile('testproperties','.properties')
                                    File file = path.toFile()
                                    file << PROPERTIES
                                    file.deleteOnExit()
                                    return file
                                }
                        ] as SupportingDataPackage
                    }
                })

        and:
        Properties props = new Properties()
        props.load(new ByteArrayInputStream(PROPERTIES.bytes))

        when:
        Properties supplied = plugin.getProperties(sd)

        then:
        props.keySet().size() == supplied.keySet().size()
        props.keySet().containsAll(supplied.keySet())
        supplied.keySet().containsAll(props.keySet())
    }

    def 'test execute'() {
        given:
        SupportingData sd = new SupportingData(
                'identifier',
                'kmid',
                'loadedby',
                'pkgid',
                OpioidSupportingDataPlugin.PROPERTIES_SD_TYPE,
                new Date(),
                new SupportingDataPackageSupplier() {
                    SupportingDataPackage get() {
                        return [
                                getBytes : {PROPERTIES.bytes},
                                getFile : {
                                    Path path = Files.createTempFile('testproperties','.properties')
                                    File file = path.toFile()
                                    file << PROPERTIES
                                    file.deleteOnExit()
                                    return file
                                }
                        ] as SupportingDataPackage
                    }
                })
        PluginDataCacheImpl cache = new PluginDataCacheImpl()
        Map<String, Object> globals = [:]
        PreProcessPluginContext context = PreProcessPluginContext.create(
                [:],
                [:],
                globals,
                ['OPIOID_MME_SUPPORTING_DATA':sd],
                cache)

        and:
        Properties props = new Properties()
        props.load(new ByteArrayInputStream(PROPERTIES.bytes))

        when:
        plugin.execute(context)

        then:
        globals
        globals.keySet().size() == 1
        globals.containsKey(OPIOID_CONFIG)
        OpioidConfig config = globals.get(OPIOID_CONFIG)
        config.chronicOpioidUseDeterminationMethod.toString() == chronicOpioidUseDeterminationMethod
        config.localGuidelineUrl == localGuidelineUrl
        config.localGuidelineLabel == localGuidelineLabel
        config.reverseMistakenLabelingOfStartAndEndDatesWithUtcTimeZone == reverseMistakenLabelingOfStartAndEndDatesWithUtcTimeZone
        config.useDynamicTableFormat == useDynamicTableFormat
        config.returnCardForDebug == returnCardForDebug
        config.toString() == expectedCustom

        cache.get(sd) == config
    }

    def 'test execute with no values to get defaults'() {
        given:
        SupportingData sd = new SupportingData(
                'identifier',
                'kmid',
                'loadedby',
                'pkgid',
                OpioidSupportingDataPlugin.PROPERTIES_SD_TYPE,
                new Date(),
                new SupportingDataPackageSupplier() {
                    SupportingDataPackage get() {
                        return [
                                getBytes : {''.getBytes()},
                                getFile : {
                                    Path path = Files.createTempFile('testproperties','.properties')
                                    File file = path.toFile()
                                    file << ''
                                    file.deleteOnExit()
                                    return file
                                }
                        ] as SupportingDataPackage
                    }
                })
        PluginDataCacheImpl cache = new PluginDataCacheImpl()
        Map<String, Object> globals = [:]
        PreProcessPluginContext context = PreProcessPluginContext.create(
                [:],
                [:],
                globals,
                ['OPIOID_MME_SUPPORTING_DATA':sd],
                cache)

        when:
        plugin.execute(context)
        OpioidConfig config = globals.get(OPIOID_CONFIG)

        then:
        config.toString() == expectedDefaults
    }

    private String expectedDefaults = """OpioidConfig [
chronicOpioidUseDeterminationMethod=THREE_CONSECUTIVE_21D_OF_30D
excludeIfNoDraftOpioidMedOrdersAbusedInAmbulatoryCare=true
excludeIfDraftOrActiveMedOrderIndicatingEndOfLife=false
excludeIfActiveConditionIndicatingEndOfLife=false
excludeIfRecentOpioidUse=false
recentOpioidUseLookbackDays=365
recentOpioidUseIfAtLeastXDaysInLookbackOnOpioids=2
mmeShortLabel=OME
mmeLongLabel=oral morphine equivalence
localGuidelineLabel=CPG opioid use guidelines
localGuidelineUrl=http://tbd.utah.edu
restrictToSpecifiedMedStatementCategories=true
medStatementCategoriesAllowed=[community]
reverseMistakenLabelingOfStartAndEndDatesWithUtcTimeZone=true
returnCardForDebug=false
defaultOpioidRxDaysSupplyEstimate=30
includeAverageMmePerDay=true
calculateCoverageDatesUsingPresumptionOfDefaultDaysSupplyUnlessExplicitlyStatedOtherwise=true
useDynamicTableFormat=true
queryForMedOrders=false
queryForMeds=false
newImage=
expandImage=
warningImage=
imageCache=null
]"""
    private String expectedCustom = """OpioidConfig [
chronicOpioidUseDeterminationMethod=NONE
excludeIfNoDraftOpioidMedOrdersAbusedInAmbulatoryCare=true
excludeIfDraftOrActiveMedOrderIndicatingEndOfLife=false
excludeIfActiveConditionIndicatingEndOfLife=false
excludeIfRecentOpioidUse=false
recentOpioidUseLookbackDays=365
recentOpioidUseIfAtLeastXDaysInLookbackOnOpioids=2
mmeShortLabel=OME
mmeLongLabel=oral morphine equivalence
localGuidelineLabel=Guidelines
localGuidelineUrl=http://utah.edu/guidelines
restrictToSpecifiedMedStatementCategories=true
medStatementCategoriesAllowed=[community]
reverseMistakenLabelingOfStartAndEndDatesWithUtcTimeZone=true
returnCardForDebug=true
defaultOpioidRxDaysSupplyEstimate=30
includeAverageMmePerDay=true
calculateCoverageDatesUsingPresumptionOfDefaultDaysSupplyUnlessExplicitlyStatedOtherwise=true
useDynamicTableFormat=true
queryForMedOrders=false
queryForMeds=false
newImage=
expandImage=
warningImage=
imageCache=null
]"""

}
