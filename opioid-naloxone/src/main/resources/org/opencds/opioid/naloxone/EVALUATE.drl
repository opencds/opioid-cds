package org.opencds.opioid.naloxone;

import java.lang.Double;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

import org.hl7.fhir.instance.model.Bundle;
import org.hl7.fhir.instance.model.Bundle.BundleEntryComponent;
import org.hl7.fhir.instance.model.Condition;
import org.hl7.fhir.instance.model.Medication;
import org.hl7.fhir.instance.model.MedicationOrder;
import org.hl7.fhir.instance.model.Practitioner;
import org.hl7.fhir.instance.model.Resource;
import org.hl7.fhir.instance.model.ResourceType;
import org.opencds.hooks.model.context.HookContext;
import org.opencds.hooks.model.context.MedicationPrescribeContext;
import org.opencds.hooks.model.fhir.CdsHooksFhirClientFactory;
import org.opencds.hooks.model.request.CdsRequest;
import org.opencds.hooks.model.response.CdsResponse;
import org.opencds.opioid.config.OpioidConfig;
import org.opencds.opioid.UtilsOpioid;
import org.opencds.opioid.config.valuesets.ChronicOpioidUseDeterminationMethod;
import org.opencds.tools.terminology.rxnav.client.RxNavTerminologyClient;
import org.opencds.tools.terminology.umls.api.client.UmlsTerminologyClient;
import org.opencds.Utils;
import org.opencds.opioid.util.StringUtil;


rule "EVALUATE do evaluation"
	dialect "java"
	ruleflow-group "EVALUATE"
	when
	 	$medOrders : ArrayList() from collect 
	 	(
			org.hl7.fhir.instance.model.MedicationOrder
			( 							
			)
		)
		
		$meds : ArrayList() from collect 
	 	(
			org.hl7.fhir.instance.model.Medication
			( 							
			)
		)
		
		$conditions : ArrayList() from collect 
	 	(
			org.hl7.fhir.instance.model.Condition
			( 							
			)
		)
		
		$practitioners : ArrayList() from collect 
	 	(
			org.hl7.fhir.instance.model.Practitioner
			( 							
			)
		)
		
        $encounters : ArrayList() from collect 
        (
            org.hl7.fhir.instance.model.Encounter
            (                           
            )
        )
        
		$cdsRequest : CdsRequest()
		
		$fhirClientFactory : CdsHooksFhirClientFactory()
		
		
	then
		UtilsOpioid utilsOpioid = UtilsOpioid.getInstance();
		HookContext cdsRequestContext = $cdsRequest.getContext();
		System.out.println("CdsRequest.context: " + cdsRequestContext);
		
		// OpenCDS cds hooks context is a DSTU2 FHIR Resource
		if (cdsRequestContext != null && cdsRequestContext instanceof MedicationPrescribeContext) {
		    MedicationPrescribeContext medOrderContext = (MedicationPrescribeContext) cdsRequestContext;
		    for (MedicationOrder medOrder : medOrderContext.getMedications()) {
		      $medOrders.add(medOrder);
		    }
		}
		
		System.out.println("$medOrders: " + $medOrders);
		System.out.println("$meds: " + $meds);
		
		CdsResponse cdsResponse = utilsOpioid.getNaloxoneEvaluationResultsAsCdsResponse(
		  evalTime, 
		  focalPersonId,
		  (RxNavTerminologyClient) RxNavGlobals.get("RxNavTerminologyClient"), 
		  (UmlsTerminologyClient) UMLSGlobals.get("UmlsTerminologyClient"), 
		  $medOrders, 
		  $meds, 
		  $conditions, 
		  $practitioners, 
		  $encounters,
		  opioidConfig);
			
		//System.out.println("CdsResponse: " + cdsResponse);
		namedObjects.put("CdsResponse", cdsResponse);
		
		System.out.println("Rule fired : 'do evaluation'");					
end
