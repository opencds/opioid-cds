package org.opencds.opioid.evaluate;

import java.util.ArrayList;
import java.util.List;

import ca.uhn.fhir.context.FhirVersionEnum;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hl7.fhir.dstu2.model.Bundle;
import org.hl7.fhir.dstu2.model.Condition;
import org.hl7.fhir.dstu2.model.Encounter;
import org.hl7.fhir.dstu2.model.Medication;
import org.hl7.fhir.dstu2.model.MedicationOrder;
import org.hl7.fhir.dstu2.model.OperationOutcome;
import org.hl7.fhir.dstu2.model.Practitioner;
import org.hl7.fhir.dstu2.model.Procedure;
import org.hl7.fhir.dstu2.model.Resource;
import org.hl7.fhir.dstu2.model.ResourceType;
import org.opencds.hooks.engine.api.CdsHooksEvaluationContext;
import org.opencds.hooks.engine.api.CdsHooksExecutionEngine;
import org.opencds.hooks.lib.fhir.FhirContextFactory;
import org.opencds.hooks.model.dstu2.request.prefetch.Dstu2PrefetchHelper;
import org.opencds.hooks.model.request.CdsRequest;
import org.opencds.hooks.model.request.prefetch.PrefetchResult;
import org.opencds.hooks.model.response.Card;
import org.opencds.hooks.model.response.CdsResponse;
import org.opencds.opioid.config.OpioidConfig;
import org.opencds.opioid.eval.PeriodicAssessmentEvaluation;
import org.opencds.opioid.results.EvalResults;
import org.opencds.opioid.util.fhir.FhirUtil;
import org.opencds.opioid.util.fhir.MedOrderUtil;
import org.opencds.tools.terminology.rxnav.client.RxNavTerminologyClient;
import org.opencds.tools.terminology.umls.api.client.UmlsTerminologyClient;

import ca.uhn.fhir.rest.client.api.IGenericClient;

public class PeriodicAssessmentEvaluate implements CdsHooksExecutionEngine {
    private static final Log log = LogFactory.getLog(PeriodicAssessmentEvaluate.class);
    private static final Dstu2PrefetchHelper prefetchHelper = new Dstu2PrefetchHelper();

    private static final String OPIOID_CONFIG_GLOBAL = "opioidConfig";
    private static final String RXNAV_CLIENT = "RxNavTerminologyClient";
    private static final String UMLS_CLIENT = "UmlsTerminologyClient";

    private static final String PATIENT_ID = "patientId";

    @Override
    public CdsResponse evaluate(CdsRequest request, CdsHooksEvaluationContext evaluationContext) {
//            RxNavTerminologyClient rxNavClient, UmlsTerminologyClient umlsClient, OpioidConfig config) {

        RxNavTerminologyClient rxNavClient = (RxNavTerminologyClient) evaluationContext.getGlobals().getOrDefault(RXNAV_CLIENT, new RxNavTerminologyClient());
        UmlsTerminologyClient umlsClient = (UmlsTerminologyClient) evaluationContext.getGlobals().getOrDefault(UMLS_CLIENT, new UmlsTerminologyClient());
        OpioidConfig config = (OpioidConfig) evaluationContext.getGlobals().getOrDefault(OPIOID_CONFIG_GLOBAL,
                new OpioidConfig()); // default, if not provided

        PeriodicAssessmentEvaluation evaluation = new PeriodicAssessmentEvaluation(rxNavClient, umlsClient, config);

        IGenericClient fhirClient = null;

        Bundle contextMedOrders = request.getContext().get("medications", Bundle.class);
        List<MedicationOrder> medOrders = new ArrayList<>();
        medOrders.addAll(FhirUtil.findResources(contextMedOrders, ResourceType.MedicationOrder));
        List<Medication> meds = new ArrayList<>();
        meds.addAll(FhirUtil.findResources(contextMedOrders, ResourceType.Medication));

        List<Condition> conditions = new ArrayList<>();
        List<Practitioner> practitioners = new ArrayList<>();
        List<Encounter> encounters = new ArrayList<>();
        List<Procedure> procedures = new ArrayList<>();
        PrefetchResult<OperationOutcome, Resource> prefetchResource = request.getPrefetchResource("medOrders", prefetchHelper);
        if (prefetchResource != null && prefetchResource.hasResourceOfType(Bundle.class)) {
            Bundle medOrdersPrefetchBundle = prefetchResource.getResource(Bundle.class);
            medOrders.addAll(FhirUtil.findResources(medOrdersPrefetchBundle, ResourceType.MedicationOrder));
            meds.addAll(FhirUtil.findResources(medOrdersPrefetchBundle, ResourceType.Medication));
        }
        prefetchResource = request.getPrefetchResource("conditions", prefetchHelper);
        if (prefetchResource != null && prefetchResource.hasResourceOfType(Bundle.class)) {
            Bundle conditionsBundle = prefetchResource.getResource(Bundle.class);
            conditions.addAll(FhirUtil.findResources(conditionsBundle, ResourceType.Condition));
        }
        prefetchResource = request.getPrefetchResource("practitioners", prefetchHelper);
        if (prefetchResource != null && prefetchResource.hasResourceOfType(Bundle.class)) {
            Bundle practitionersBundle = prefetchResource.getResource(Bundle.class);
            practitioners.addAll(FhirUtil.findResources(practitionersBundle, ResourceType.Practitioner));
        }
        prefetchResource = request.getPrefetchResource("encounters", prefetchHelper);
        if (prefetchResource != null && prefetchResource.hasResourceOfType(Bundle.class)) {
            Bundle encountersBundle = prefetchResource.getResource(Bundle.class);
            encounters.addAll(FhirUtil.findResources(encountersBundle, ResourceType.Encounter));

        }
        prefetchResource = request.getPrefetchResource("procedures", prefetchHelper);
        if (prefetchResource != null && prefetchResource.hasResourceOfType(Bundle.class)) {
            Bundle proceduresBundle = prefetchResource.getResource(Bundle.class);
            procedures.addAll(FhirUtil.findResources(proceduresBundle, ResourceType.Procedure));

        }

        // This is a potentially time-consuming step, so only conducting if
        // needed
        // TODO: refactor the FHIR client so it is pushed in if needed, so it
        // can be created once then re-used
        if (config.isQueryForMedOrders() || config.isQueryForMeds()) {
            fhirClient = request.getFhirClient(FhirContextFactory.get(FhirVersionEnum.DSTU2_HL7ORG));
        }

        String focalPersonId = request.getContext().get(PATIENT_ID, String.class);
        if (config.isQueryForMedOrders()) {
            MedOrderUtil.searchAndAddMedOrdersIfNoneOtherThanDraft(fhirClient, focalPersonId, medOrders);
        }

        if (config.isQueryForMeds()) {
            MedOrderUtil.searchAndAddMedsIfReferencedByMedOrdersAndDoesNotExist(fhirClient, focalPersonId, medOrders,
                    meds);
        }

        EvalResults results = evaluation.getPeriodicAssessmentEvaluationResults(evaluationContext.getEvalTime(),
                focalPersonId, evaluationContext.getServerBaseUri(), medOrders, meds, encounters, conditions,
                procedures);

        CdsResponse cdsResponse = results.getCdsResponse();
        if (!results.isReturnCard()) {
            log.info("Not returning card.");
            List<Card> cards = cdsResponse.getCards();
            cards.forEach(card -> {
                log.info("Summary: " + card.getSummary());
                log.info("Detail: " + card.getDetail());
            });
            return new CdsResponse();
        }
        return cdsResponse;
    }

}
