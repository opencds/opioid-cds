package org.opencds.opioid

import java.nio.file.Files

import org.hl7.fhir.instance.model.Condition
import org.hl7.fhir.instance.model.Encounter
import org.hl7.fhir.instance.model.Medication
import org.hl7.fhir.instance.model.MedicationOrder
import org.hl7.fhir.instance.model.Practitioner
import org.opencds.common.utilities.DateUtility
import org.opencds.hooks.model.dstu2.util.Dstu2JsonUtil
import org.opencds.hooks.model.response.Card
import org.opencds.hooks.model.response.CdsResponse
import org.opencds.hooks.model.response.Link
import org.opencds.hooks.model.util.JsonUtil
import org.opencds.opioid.config.valuesets.ChronicOpioidUseDeterminationMethod
import org.opencds.plugin.PluginContext
import org.opencds.plugin.PluginDataCache
import org.opencds.plugin.SupportingData
import org.opencds.plugin.SupportingDataPackage
import org.opencds.plugin.SupportingDataPackageSupplier
import org.opencds.plugin.PluginContext.PreProcessPluginContext
import org.opencds.plugin.opioid.OpioidTerminologyPreProcessPlugin
import org.opencds.plugin.support.PluginDataCacheImpl
import org.opencds.tools.db.api.DbConnection
import org.opencds.tools.terminology.rxnav.client.RxNavTerminologyClient
import org.opencds.tools.terminology.umls.api.client.UmlsTerminologyClient

import ca.uhn.fhir.context.FhirContext
import spock.lang.Ignore
import spock.lang.Specification

@Ignore
class OpioidRuleSpec extends Specification {
	static Map<String, String> globals;

	JsonUtil jsonUtil = new Dstu2JsonUtil()
	
	def setupSpec() {
		try {
		OpioidTerminologyPreProcessPlugin plugin = new OpioidTerminologyPreProcessPlugin()
		Map<Class<?>, List<?>> afl = new HashMap<>();
		Map<String, String> no = new HashMap<>();
		globals = new HashMap<>()
		Map<String, SupportingData> sds = ['OPIOID_DB':
			SupportingData.create(
			'OPIOID_DB',
			'',
			'',
			'src/test/resources/OpioidManagementTerminologyKnowledge.accdb',
			'accdb',
			new Date(),
			[
				get:{
					[
						getFile:{ SupportingData sd ->
							File file = new File('src/test/resources/OpioidManagementTerminologyKnowledge.accdb')
							println "\n\n\t\tFile: " + file.getAbsolutePath() + "\n\n"
							file
						}, getBytes: {null}] as SupportingDataPackage
				}] as SupportingDataPackageSupplier)];
		PluginDataCache cache = new PluginDataCacheImpl()
		PreProcessPluginContext context = PluginContext.createPreProcessPluginContext(afl, no, globals, sds, cache);
		plugin.execute(context)
		assert globals
		} catch (Throwable e) {
			e.printStackTrace()
			throw e
		}
	}

	def "test dummy"() {
		expect:
		println globals.opioidGlobals.keySet()
		true
	}
	
	def "test"() {
		given:
		UtilsOpioid utilsOpioid = UtilsOpioid.getInstance();
		String testFolder = "src/test/resources/";

		FhirContext ctx = FhirContext.forDstu2Hl7Org();
		ca.uhn.fhir.parser.IParser parser = ctx.newJsonParser();

		String focalPersonId = "pat_id_1";
		ArrayList<MedicationOrder> medOrders;
		ArrayList<Medication> meds;
		ArrayList<Condition> conditions;
		ArrayList<Practitioner> practitioners;

		Date evalTime = DateUtility.getInstance().getDateFromStringAs_yyyy_MM_dd("2017_04_01");

		// test 1: draft prescription for
		// 168 HR Buprenorphine 0.015 MG/HR Transdermal System
		medOrders = new ArrayList<MedicationOrder>();
		meds = new ArrayList<Medication>();
		conditions = new ArrayList<Condition>();
		practitioners = new ArrayList<Practitioner>();

		Medication buprenorphinePatch = null;
		Medication methadoneOral = null;
		Medication oxycontinOral = null;
		Medication percocetOral = null;
		MedicationOrder buprenorphinePatchDraftMedOrder = new MedicationOrder();
		MedicationOrder methadoneOralActiveMedOrder = new MedicationOrder();
		MedicationOrder oxycontinOralActiveMedOrder = new MedicationOrder();
		MedicationOrder percocetOralActiveMedOrder = new MedicationOrder();

		String fileName = null;
		try
		{
			fileName = "med_buprenorphine_patch.json";
			buprenorphinePatch = parser.parseResource(Medication.class, new String(Files.readAllBytes((new File(testFolder + fileName)).toPath())));
			meds.add(buprenorphinePatch);

			fileName = "med_methadone_oral.json";
			methadoneOral = parser.parseResource(Medication.class, new String(Files.readAllBytes((new File(testFolder + fileName)).toPath())));
			meds.add(methadoneOral);

			fileName = "med_oxycontin_oral.json";
			oxycontinOral = parser.parseResource(Medication.class, new String(Files.readAllBytes((new File(testFolder + fileName)).toPath())));
			meds.add(oxycontinOral);

			fileName = "med_percocet_oral.json";
			percocetOral = parser.parseResource(Medication.class, new String(Files.readAllBytes((new File(testFolder + fileName)).toPath())));
			meds.add(percocetOral);

			fileName = "medorder_buprenorphine_patch_draft.json";
			buprenorphinePatchDraftMedOrder = parser.parseResource(MedicationOrder.class, new String(Files.readAllBytes((new File(testFolder + fileName)).toPath())));
			medOrders.add(buprenorphinePatchDraftMedOrder);

			fileName = "medorder_methadone_oral_active.json";
			methadoneOralActiveMedOrder = parser.parseResource(MedicationOrder.class, new String(Files.readAllBytes((new File(testFolder + fileName)).toPath())));
			medOrders.add(methadoneOralActiveMedOrder);

			fileName = "medorder_oxycontin_oral_active.json";
			oxycontinOralActiveMedOrder = parser.parseResource(MedicationOrder.class, new String(Files.readAllBytes((new File(testFolder + fileName)).toPath())));
			medOrders.add(oxycontinOralActiveMedOrder);

			fileName = "medorder_percocet_oral_active.json";
			percocetOralActiveMedOrder = parser.parseResource(MedicationOrder.class, new String(Files.readAllBytes((new File(testFolder + fileName)).toPath())));
			medOrders.add(percocetOralActiveMedOrder);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		};
		
		
		when:
        boolean calculateCoverageDatesUsingPresumptionOfDefaultDaysSupplyUnlessExplicitlyStatedOtherwise = true
        RxNavTerminologyClient rxNavClient = new RxNavTerminologyClient(new DbConnection(new File('/Users/phillip/tmp/terminology/LocalDataStore_RxNav_OpioidCds.accdb')))
        UmlsTerminologyClient umlsClient = new UmlsTerminologyClient(new DbConnection(new File('/Users/phillip/tmp/terminology/LocalDataStore_UMLS_OpioidCds.accdb')))
		List<Encounter> encounters = []
		CdsResponse cdsResponse = utilsOpioid.getMmeEvaluationResultsAsCdsResponse(evalTime, focalPersonId, rxNavClient, umlsClient, medOrders, meds, conditions, practitioners, encounters, ChronicOpioidUseDeterminationMethod.THREE_CONSECUTIVE_21D_OF_30D, false, false, false, 60, 60, 'mmeshortlabel', 'mmelonglabel', 'localguidelinelabel-optional', 'localguidelineurl-optional', false, [], true, TimeZone.getDefault(), true, 60, true, calculateCoverageDatesUsingPresumptionOfDefaultDaysSupplyUnlessExplicitlyStatedOtherwise);
		println jsonUtil.toJson(cdsResponse)
		
		then:
		cdsResponse
		System.out.println("> CDS response: ");
		List<Card> cards = cdsResponse.getCards();
		if ((cards != null) && (cards.size() > 0))
		{
			for (int k = 0; k < cards.size(); k++)
			{
				Card card = cards.get(k);
				System.out.println("Card #" + (k+1) + ":");
				System.out.println("- Indicator: " + card.getIndicator());
				System.out.println("- Summary: " + card.getSummary());
				System.out.println("- Detail (markdown): " + card.getDetail());
				System.out.println();
				System.out.println("- Detail (HTML): ");
				String detailHtml = utilsOpioid.getHtmlFromMarkDown(card.getDetail());
				System.out.println(detailHtml);
				System.out.println("- Source: " + card.getSource());
				List<Link> links = card.getLinks();

				String html = "<html>\n<p><strong>" + card.getSummary() + "</strong></p>";

				html += detailHtml.replace("<table>", "<table border=\"1\">");

				html += "<p>";
				if ((links != null) && (links.size() > 0))
				{
					html += "Links: ";
					for (int x = 0; x < links.size(); x++)
					{
						Link link = links.get(x);
						System.out.println("- Link: " + link);
						html += "(<a href=\"" + link.getUrl() + "\" target=\"_blank\">" + link.getLabel() + "</a>)";
						if (x < links.size() - 1)
						{
							html += " ";
						}
						if (x == links.size() -1)
						{
							html += ". ";
						}
					}
				}

				if (card.getSource() != null)
				{
					html += "Source: " + "<a href=\"" + card.getSource().getUrl() + "\" target=\"_blank\">" + card.getSource().getLabel() + "</a>.";
				}

				html += "</p>\n</html>";

				ArrayList<String> outputLines = new ArrayList<String>();
				outputLines.add(html);
				outputLines.each {out ->
					println out
				}
			}
		}
	}
}
