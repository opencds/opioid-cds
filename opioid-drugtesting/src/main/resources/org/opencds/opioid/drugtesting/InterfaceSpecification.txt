Pre-conditions:
- Expected to be used in outpatient, non-ED contexts

Inputs -- Globals
> focalPersonId		String. The FHIR Patient.id of the current patient in context.  Equivalent to "patient" in the CDS Hooks call.
> evalTime			(off spec)  Date.  The specified time of evaluation.  If client didn't specify, then OpenCDS will set to now.

Required Inputs if available -- FHIR Resources (obtained from prefetch, inserted as top-level Resources)
> CDSInput that contains
	> context			ArrayList<Object> of FHIR resources (1..*) representing orders about to be placed, and related resources.
						Includes MedicationOrder(s) being placed, with a status of "draft", as well as referenced Medication(s).  
						Details including dose and frequency required (e.g., as available during time of prescription signing).
	
> MedicationOrder resources (0..*) with status of "active", "completed", or "stopped".  
> Medication resources associated with the orders, either as included resources, contained resources, or as implied resources via medication code.
> Practitioner resources associated with the orders as prescribers.
> Condition resources of active problems.
	
Overall Output:
> namedObject called "CdsResponse" containing the CdsResponse.  This contains an ArrayList<Card>
> If no action needed (not about to prescribe outpatient non-IV opioid or MME < 50), no cards returned (empty list).
> If action needed (about to prescribe outpatient non-IV opioid and MME >= 50), Information Card returned with following attributes, defined in http://cds-hooks.org/#cds-service-response
	summary		String. one-sentence, <140-character summary message for display to the user inside of this card.
	detail		String. optional detailed information to display, represented in Markdown.  Provides guidance + MME table.
	indicator	String. "success" or "info" as above.
	source		Source -- if "info", provide links to further CDC info.

Notes:
> URI for RxNorm: http://www.nlm.nih.gov/research/umls/rxnorm
> URI for SNOMED CT: http://snomed.info/sct
> URI for ICD10CM: http://hl7.org/fhir/sid/icd-10-cm, or urn:oid:2.16.840.1.113883.6.90
> URI for LOINC: http://loinc.org