package org.opencds.opioid.evaluate

import ca.uhn.fhir.context.FhirVersionEnum
import org.opencds.hooks.lib.fhir.FhirContextFactory
import org.opencds.hooks.lib.json.JsonUtil

import java.nio.file.Path
import java.nio.file.Paths
import java.sql.Connection
import java.sql.DriverManager
import java.sql.SQLException
import java.time.LocalDate
import java.time.Month
import java.time.ZoneId
import java.util.function.Supplier

import org.hl7.fhir.dstu2.model.Bundle
import org.hl7.fhir.dstu2.model.Medication
import org.hl7.fhir.dstu2.model.MedicationOrder
import org.hl7.fhir.dstu2.model.Patient
import org.hl7.fhir.dstu2.model.Practitioner
import org.hl7.fhir.dstu2.model.Reference
import org.hl7.fhir.dstu2.model.Bundle.BundleEntryComponent
import org.opencds.hooks.engine.api.CdsHooksEvaluationContext
import org.opencds.hooks.model.context.HookContext
import org.opencds.hooks.model.context.WritableHookContext
import org.opencds.hooks.model.dstu2.util.Dstu2JsonUtil
import org.opencds.hooks.model.request.CdsRequest
import org.opencds.hooks.model.request.WritableCdsRequest
import org.opencds.hooks.model.response.CdsResponse
import org.opencds.opioid.config.OpioidConfig
import org.opencds.opioid.config.valuesets.ChronicOpioidUseDeterminationMethod
import org.opencds.tools.terminology.rxnav.client.RxNavTerminologyClient
import org.opencds.tools.terminology.umls.api.client.UmlsTerminologyClient

import ca.uhn.fhir.context.FhirContext
import spock.lang.Ignore
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll

/**
 * Created by salvador on 5/3/17.
 */
//@Ignore
class NaloxoneInpatientEvaluateFunctionalSpec extends Specification {
    private static RxNavTerminologyClient rxNavClient
    private static UmlsTerminologyClient umlsClient

    // Add base location for folders containing data
    private static Path baseLocation = Paths.get('/Users/phillip/tmp-phi/Test PHI')
    private static Path termBaseLocation = Paths.get('/Users/phillip/documentation/kmm/edmopencdsdev/dist-3.0/2018-07/opioid/deployed-terms')

    private NaloxoneInpatientEvaluate evaluate

    @Shared
    JsonUtil jsonUtil
    @Shared
    OpioidConfig config
    @Shared
    URI uri = Paths.get('/Users/phillip/.fhir/image-cache/').toUri()
    @Shared
    FhirContext ctx = FhirContextFactory.get(FhirVersionEnum.DSTU2_HL7ORG)
    @Shared
    Map<String, Double> timings = [:] as TreeMap
    @Shared
    Map<String, Object> globals = [:]

    /** **************************************************/

    // evalTime: 5/1/17
    private static final Date EVAL_DATETIME = Date.from(LocalDate.of(2018, Month.MARCH, 2).atStartOfDay(ZoneId.systemDefault()).toInstant())

    @Unroll
    def "001"() {

        when:
        println ""
        println "Running test: " + id
        File file = Paths.get(baseLocation.toString(), id).toFile()
        long t0 = System.nanoTime()
        CdsRequest request = buildRequest(file)
        timings.put("Request Build for $id", (System.nanoTime()-t0)/1e6)
        t0 = System.nanoTime()
        CdsHooksEvaluationContext context = CdsHooksEvaluationContext.create(EVAL_DATETIME, uri, null, null, globals);
        CdsResponse response = evaluate.evaluate(request, context);
        timings.put("Rule exec for $id", (System.nanoTime()-t0)/1e6)
        println response
        println ""

        then:
        response instanceof CdsResponse
        response.cards.size() == numCards
        //        response.cards[0].details.contains(someText)

        where:
        num | id | numCards //| someText
        0 | '00' | 1 //| 'this is the text'
        1 | '01' | 1
        2 | '02' | 1
        3 | '03' | 1
        4 | '04' | 1
        5 | '05' | 1
        6 | '06' | 1
        7 | '07' | 1
        8 | '08' | 1
        9 | '09' | 1
        10 | '10' | 1
        11 | '11' | 1
        12 | '12' | 1
        13 | '13' | 1
        14 | '14' | 1
        15 | '15' | 1
        16 | '16' | 1
        17 | '17' | 1
        18 | '18' | 1
        19 | '19' | 1
        20 | '20' | 1
        21 | '21' | 1
        22 | '22' | 1
        23 | '23' | 1
        24 | '24' | 1
        25 | '25' | 1
        26 | '26' | 1
        27 | '27' | 1
        28 | '28' | 1
        29 | '29' | 1
        30 | '30' | 1
        31 | '31' | 1
        32 | '32' | 1
        33 | '33' | 1
        34 | '34' | 1
        35 | '35' | 1
        36 | '36' | 1
    }

    def "004"() {
        when:
        println ""
        println "Running test: " + id
        File file = Paths.get('/Users/phillip/tmp/opioid/', id).toFile()
        long t0 = System.nanoTime()
        CdsRequest request = jsonUtil.fromJson(file.text, CdsRequest)
        timings.put("Request Build for $id", (System.nanoTime()-t0)/1e6)
        t0 = System.nanoTime()
        CdsHooksEvaluationContext context = CdsHooksEvaluationContext.create(EVAL_DATETIME, uri, null, null, globals);
        CdsResponse response = evaluate.evaluate(request, context);
        timings.put("Rule exec for $id", (System.nanoTime()-t0)/1e6)
        println response
        println ""
        File testHtml = new File("test.html")
        testHtml.delete()

        then:
        response instanceof CdsResponse
        testHtml << response.getCards().get(0).getDetail()
        response.cards.size() == numCards

        where:
        num | id | numCards //| someText
        0 | 'problem-2018-04-10.json' | 1 //| 'this is the text'

    }

    private static final String PATIENT = 'Patient.json'
    private static final String MED_ORDER_BUNDLE = 'MedicationOrder_bundle.json'
    private static final String REFERENCED_MEDS = 'referenced_resources' + File.separator + 'Medication'
    private static final String REFERENCED_PRACTITIONER = 'referenced_resources' + File.separator + 'Practitioner'

    private CdsRequest buildRequest(File baseLocation) {
        CdsRequest req = new WritableCdsRequest()
        req.hookInstance = 'd1577c69-dfbe-44ad-ba6d-3e05e953b2ea'
        req.fhirServer = new URL('http://localhost:8080/fhir-wrapper/fhirServer')
        req.hook = 'encounter-view'
        req.user = 'Practitioner/phillip'
        Patient patient = ctx.newJsonParser().parseResource(Patient.class, new File(baseLocation, PATIENT).text)
        req.addPrefetchResource('patient', patient)

        // replace the patient in MedicationOrder below

        MedicationOrder medOrder = ctx.newJsonParser().parseResource(MedicationOrder.class, new File('src/test/resources/medorder_buprenorphine_patch_draft.json').text)
        medOrder.setPatient(new Reference(patient.getId()))
        println("MedOrder Patient: " + medOrder.getPatient().getReference())

        HookContext context = new WritableHookContext()
        context.add('patientId', patient.getIdElement().getIdPart())
        context.add('medications', new Bundle().addEntry(new BundleEntryComponent().setResource(medOrder)))
        req.context = context
        Bundle medOrderBundle = new Bundle();
        new File(baseLocation, REFERENCED_MEDS).eachFile {File medFile ->
            Medication med = ctx.newJsonParser().parseResource(Medication.class, medFile.text)
        }
        new File(baseLocation, REFERENCED_PRACTITIONER).eachFile {File practFile ->
            Practitioner pract = ctx.newJsonParser().parseResource(Practitioner.class, practFile.text)
            medOrderBundle.addEntry(new BundleEntryComponent().setResource(pract))
        }
        Bundle medBundle = ctx.newJsonParser().parseResource(Bundle.class, new File(baseLocation, MED_ORDER_BUNDLE).text)
        medBundle.entry.eachWithIndex {BundleEntryComponent comp, index ->
            medOrderBundle.addEntry(comp)
        }
        req.addPrefetchResource('medOrders', medOrderBundle)
        return req
    }

    /** **************************************************/

    def setupSpec() {
        try {
            rxNavClient = new RxNavTerminologyClient(getConnection(Paths.get(termBaseLocation.toString(), 'LocalDataStore_RxNav_OpioidCds.accdb').toFile()))
            umlsClient = new UmlsTerminologyClient(getConnection(Paths.get(termBaseLocation.toString(), 'LocalDataStore_UMLS_OpioidCds.accdb').toFile()))
            globals.put(NaloxoneInpatientEvaluate.RXNAV_CLIENT, rxNavClient)
            globals.put(NaloxoneInpatientEvaluate.UMLS_CLIENT, umlsClient)
        } catch (Throwable e) {
            e.printStackTrace()
            throw e
        }
    }

    def setup() {
        evaluate = new NaloxoneInpatientEvaluate()
        jsonUtil = new Dstu2JsonUtil()
        config = OpioidConfig.build(new Properties());
        config.setReturnCardForDebug(true)
        config.setNewImage('metaphorDetail - new.svg')
        config.setExpandImage('metaphorDetail - expand.svg')
        config.setWarningImage('caution-sign-svgrepo-com.svg')
        config.setChronicOpioidUseDeterminationMethod(ChronicOpioidUseDeterminationMethod.NONE)
        globals.put(NaloxoneInpatientEvaluate.OPIOID_CONFIG_GLOBAL, config)
    }

    def cleanupSpec() {
        timings.each {k, v ->
            println "$k : $v ms"
        }
    }

    private Supplier<Connection> getConnection(File location) throws ClassNotFoundException {
        Class.forName("net.ucanaccess.jdbc.UcanaccessDriver");
        return [get: {
            try {
                return DriverManager.getConnection("jdbc:ucanaccess://" + location.getAbsolutePath() + ";memory=false");
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }] as Supplier<Connection>;
    }

}
