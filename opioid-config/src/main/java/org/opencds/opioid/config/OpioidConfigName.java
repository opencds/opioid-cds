package org.opencds.opioid.config;

import java.util.Arrays;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public enum OpioidConfigName {
    chronicOpioidUseDeterminationMethod("THREE_CONSECUTIVE_21D_OF_30D") {
        @Override
        public void apply(OpioidConfig opioidConfig, Object value) {
            opioidConfig.setChronicOpioidUseDeterminationMethod(valueOrDefault(value));
        }
    },    
    excludeIfNoDraftOpioidMedOrdersAbusedInAmbulatoryCare("true") {
        @Override
        public void apply(OpioidConfig opioidConfig, Object value) {
            opioidConfig.setExcludeIfNoDraftOpioidMedOrdersAbusedInAmbulatoryCare(Boolean.valueOf(valueOrDefault(value)));
        }
    },
    excludeIfDraftOrActiveMedOrderIndicatingEndOfLife("false") {
        @Override
        public void apply(OpioidConfig opioidConfig, Object value) {
            opioidConfig.setExcludeIfDraftOrActiveMedOrderIndicatingEndOfLife(Boolean.valueOf(valueOrDefault(value)));
        }
    },
    excludeIfActiveConditionIndicatingEndOfLife("false") {
        @Override
        public void apply(OpioidConfig opioidConfig, Object value) {
            opioidConfig.setExcludeIfActiveConditionIndicatingEndOfLife(Boolean.valueOf(valueOrDefault(value)));
        }
    },
    excludeIfRecentOpioidUse("false") {
        @Override
        public void apply(OpioidConfig opioidConfig, Object value) {
            opioidConfig.setExcludeIfRecentOpioidUse(Boolean.valueOf(valueOrDefault(valueOrDefault(value))));
        }
    },
    recentOpioidUseLookbackDays("365") {
        @Override
        public void apply(OpioidConfig opioidConfig, Object value) {
            opioidConfig.setRecentOpioidUseLookbackDays(Integer.valueOf(valueOrDefault(valueOrDefault(value))));
        }
    },
    recentOpioidUseIfAtLeastXDaysInLookbackOnOpioids("2") {
        @Override
        public void apply(OpioidConfig opioidConfig, Object value) {
            opioidConfig.setRecentOpioidUseIfAtLeastXDaysInLookbackOnOpioids(intValue(valueOrDefault(valueOrDefault(value))));
        }
    },
    mmeShortLabel("OME") {
        @Override
        public void apply(OpioidConfig opioidConfig, Object value) {
            opioidConfig.setMmeShortLabel(valueOrDefault(value));
        }
    },
    mmeLongLabel("oral morphine equivalence") {
        @Override
        public void apply(OpioidConfig opioidConfig, Object value) {
            opioidConfig.setMmeLongLabel(valueOrDefault(value));
        }
    },
    localGuidelineLabel("CPG opioid use guidelines") {
        @Override
        public void apply(OpioidConfig opioidConfig, Object value) {
            opioidConfig.setLocalGuidelineLabel(valueOrDefault(value));
        }
    },
    localGuidelineUrl("http://tbd.utah.edu") {
        @Override
        public void apply(OpioidConfig opioidConfig, Object value) {
            opioidConfig.setLocalGuidelineUrl(valueOrDefault(value));
        }
    },
    restrictToSpecifiedMedStatementCategories("true") {
        @Override
        public void apply(OpioidConfig opioidConfig, Object value) {
            opioidConfig.setRestrictToSpecifiedMedStatementCategories(Boolean.valueOf(valueOrDefault(value)));
        }
    },
    medStatementCategoriesAllowed("community") {
        @Override
        public void apply(OpioidConfig opioidConfig, Object value) {
            opioidConfig.setMedStatementCategoriesAllowed(Arrays.asList(valueOrDefault(value).split(",")));
        }
    },
    reverseMistakenLabelingOfStartAndEndDatesWithUtcTimeZone("true"){
        @Override
        public void apply(OpioidConfig opioidConfig, Object value) {
            opioidConfig.setReverseMistakenLabelingOfStartAndEndDatesWithUtcTimeZone(Boolean.valueOf(valueOrDefault(value)));
        }
    },
    returnCardForDebug("false") {
        @Override
        public void apply(OpioidConfig opioidConfig, Object value) {
            opioidConfig.setReturnCardForDebug(Boolean.valueOf(valueOrDefault(value)));
        }
    },
    defaultOpioidRxDaysSupplyEstimate("30") {
        @Override
        public void apply(OpioidConfig opioidConfig, Object value) {
            opioidConfig.setDefaultOpioidRxDaysSupplyEstimate(Integer.valueOf(valueOrDefault(value)));
        }
    },
    includeAverageMmePerDay("true") {
        @Override
        public void apply(OpioidConfig opioidConfig, Object value) {
            opioidConfig.setIncludeAverageMmePerDay(Boolean.valueOf(valueOrDefault(value)));
        }
    },
    calculateCoverageDatesUsingPresumptionOfDefaultDaysSupplyUnlessExplicitlyStatedOtherwise("true") {
        @Override
        public void apply(OpioidConfig opioidConfig, Object value) {
            opioidConfig.setCalculateCoverageDatesUsingPresumptionOfDefaultDaysSupplyUnlessExplicitlyStatedOtherwise(Boolean.valueOf(valueOrDefault(value)));
        }
    },
    useDynamicTableFormat("true") {
        @Override
        public void apply(OpioidConfig opioidConfig, Object value) {
            opioidConfig.setUseDynamicTableFormat(Boolean.valueOf(valueOrDefault(value)));
        }
    },
    queryForMedOrders("false") {
        @Override
        public void apply(OpioidConfig opioidConfig, Object value) {
            opioidConfig.setQueryForMedOrders(Boolean.valueOf(valueOrDefault(value)));
        }
    },
    queryForMeds("false") {
        @Override
        public void apply(OpioidConfig opioidConfig, Object value) {
            opioidConfig.setQueryForMeds(Boolean.valueOf(valueOrDefault(value)));
        }
    },
    newImage("") {
        @Override
        public void apply(OpioidConfig opioidConfig, Object value) {
            opioidConfig.setNewImage(valueOrDefault(value));
        }
    },
    expandImage("") {
        @Override
        public void apply(OpioidConfig opioidConfig, Object value) {
            opioidConfig.setExpandImage(valueOrDefault(value));
        }
    },
    warningImage("") {
        @Override
        public void apply(OpioidConfig opioidConfig, Object value) {
            opioidConfig.setWarningImage(valueOrDefault(value));
        }
    },
    imageCache(null) {
        @Override
        public void apply(OpioidConfig opioidConfig, Object value) {
            opioidConfig.setImageCache(valueOrDefault(value));
        }
    }
    ;
    private static final Log log = LogFactory.getLog(OpioidConfigName.class);
    
    protected String defaultValue;
    
    OpioidConfigName(String defaultValue) {
        this.defaultValue = defaultValue;
    }
    
    protected int intValue(String value) {
        try {
            return Integer.valueOf(value);
        } catch (Exception e) {
            log.warn("Config value is not an integer: " + value);
        }
        return -1;
    }

    protected String valueOrDefault(Object value) {
        return value == null || value.toString().isEmpty() ? defaultValue : value.toString().trim();
    }
    
    public abstract void apply(OpioidConfig opioidConfig, Object value);

}