package org.opencds.opioid.config;

import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.TimeZone;

import org.opencds.opioid.config.valuesets.ChronicOpioidUseDeterminationMethod;

public class OpioidConfig {
    private ChronicOpioidUseDeterminationMethod chronicOpioidUseDeterminationMethod;
    private boolean excludeIfNoDraftOpioidMedOrdersAbusedInAmbulatoryCare;
    private boolean excludeIfDraftOrActiveMedOrderIndicatingEndOfLife;
    private boolean excludeIfActiveConditionIndicatingEndOfLife;
    private boolean excludeIfRecentOpioidUse;
    private int recentOpioidUseLookbackDays;
    private int recentOpioidUseIfAtLeastXDaysInLookbackOnOpioids;
    private String mmeShortLabel;
    private String mmeLongLabel;
    private String localGuidelineLabel;
    private String localGuidelineUrl;
    private boolean restrictToSpecifiedMedStatementCategories;
    private List<String> medStatementCategoriesAllowed;
    private boolean reverseMistakenLabelingOfStartAndEndDatesWithUtcTimeZone;
    private boolean returnCardForDebug;
    private int defaultOpioidRxDaysSupplyEstimate;
    private boolean includeAverageMmePerDay;
    private boolean calculateCoverageDatesUsingPresumptionOfDefaultDaysSupplyUnlessExplicitlyStatedOtherwise;
    private boolean useDynamicTableFormat;
    private boolean queryForMedOrders;
    private boolean queryForMeds;
    private String newImage;
    private String expandImage;
    private String warningImage;
    private String imageCache;

    private TimeZone correctTimeZone = TimeZone.getDefault();

    public static OpioidConfig build(Properties properties) {
        OpioidConfig opioidConfig = new OpioidConfig();
        Arrays.asList(OpioidConfigName.values()).stream()
                .forEach(configName -> configName.apply(opioidConfig, properties.get(configName.toString())));
        return opioidConfig;
    }
    
    public ChronicOpioidUseDeterminationMethod getChronicOpioidUseDeterminationMethod() {
        return chronicOpioidUseDeterminationMethod;
    }

    public void setChronicOpioidUseDeterminationMethod(
            ChronicOpioidUseDeterminationMethod chronicOpioidUseDeterminationMethod) {
        this.chronicOpioidUseDeterminationMethod = chronicOpioidUseDeterminationMethod;
    }

    public void setChronicOpioidUseDeterminationMethod(String chronicOpioidUseDeterminationMethod) {
        this.chronicOpioidUseDeterminationMethod = ChronicOpioidUseDeterminationMethod
                .fromString(chronicOpioidUseDeterminationMethod);
    }

    public boolean isExcludeIfNoDraftOpioidMedOrdersAbusedInAmbulatoryCare() {
        return excludeIfNoDraftOpioidMedOrdersAbusedInAmbulatoryCare;
    }

    public void setExcludeIfNoDraftOpioidMedOrdersAbusedInAmbulatoryCare(
            boolean excludeIfNoDraftOpioidMedOrdersAbusedInAmbulatoryCare) {
        this.excludeIfNoDraftOpioidMedOrdersAbusedInAmbulatoryCare = excludeIfNoDraftOpioidMedOrdersAbusedInAmbulatoryCare;
    }
    
    public boolean isExcludeIfDraftOrActiveMedOrderIndicatingEndOfLife() {
        return excludeIfDraftOrActiveMedOrderIndicatingEndOfLife;
    }

    public void setExcludeIfDraftOrActiveMedOrderIndicatingEndOfLife(
            boolean excludeIfDraftOrActiveMedOrderIndicatingEndOfLife) {
        this.excludeIfDraftOrActiveMedOrderIndicatingEndOfLife = excludeIfDraftOrActiveMedOrderIndicatingEndOfLife;
    }

    public boolean isExcludeIfActiveConditionIndicatingEndOfLife() {
        return excludeIfActiveConditionIndicatingEndOfLife;
    }

    public void setExcludeIfActiveConditionIndicatingEndOfLife(boolean excludeIfActiveConditionIndicatingEndOfLife) {
        this.excludeIfActiveConditionIndicatingEndOfLife = excludeIfActiveConditionIndicatingEndOfLife;
    }

    public boolean isExcludeIfRecentOpioidUse() {
        return excludeIfRecentOpioidUse;
    }

    public void setExcludeIfRecentOpioidUse(boolean excludeIfRecentOpioidUse) {
        this.excludeIfRecentOpioidUse = excludeIfRecentOpioidUse;
    }

    public int getRecentOpioidUseLookbackDays() {
        return recentOpioidUseLookbackDays;
    }

    public void setRecentOpioidUseLookbackDays(int recentOpioidUseLookbackDays) {
        this.recentOpioidUseLookbackDays = recentOpioidUseLookbackDays;
    }

    public int getRecentOpioidUseIfAtLeastXDaysInLookbackOnOpioids() {
        return recentOpioidUseIfAtLeastXDaysInLookbackOnOpioids;
    }

    public void setRecentOpioidUseIfAtLeastXDaysInLookbackOnOpioids(
            int recentOpioidUseIfAtLeastXDaysInLookbackOnOpioids) {
        this.recentOpioidUseIfAtLeastXDaysInLookbackOnOpioids = recentOpioidUseIfAtLeastXDaysInLookbackOnOpioids;
    }

    public String getMmeShortLabel() {
        return mmeShortLabel;
    }

    public void setMmeShortLabel(String mmeShortLabel) {
        this.mmeShortLabel = mmeShortLabel;
    }

    public String getMmeLongLabel() {
        return mmeLongLabel;
    }

    public void setMmeLongLabel(String mmeLongLabel) {
        this.mmeLongLabel = mmeLongLabel;
    }

    public String getLocalGuidelineLabel() {
        return localGuidelineLabel;
    }

    public void setLocalGuidelineLabel(String localGuidelineLabel) {
        this.localGuidelineLabel = localGuidelineLabel;
    }

    public String getLocalGuidelineUrl() {
        return localGuidelineUrl;
    }

    public void setLocalGuidelineUrl(String localGuidelineUrl) {
        this.localGuidelineUrl = localGuidelineUrl;
    }

    public boolean isRestrictToSpecifiedMedStatementCategories() {
        return restrictToSpecifiedMedStatementCategories;
    }

    public void setRestrictToSpecifiedMedStatementCategories(boolean restrictToSpecifiedMedStatementCategories) {
        this.restrictToSpecifiedMedStatementCategories = restrictToSpecifiedMedStatementCategories;
    }

    public List<String> getMedStatementCategoriesAllowed() {
        return medStatementCategoriesAllowed;
    }

    public void setMedStatementCategoriesAllowed(List<String> medStatementCategoriesAllowed) {
        this.medStatementCategoriesAllowed = medStatementCategoriesAllowed;
    }

    public boolean isReverseMistakenLabelingOfStartAndEndDatesWithUtcTimeZone() {
        return reverseMistakenLabelingOfStartAndEndDatesWithUtcTimeZone;
    }

    public void setReverseMistakenLabelingOfStartAndEndDatesWithUtcTimeZone(
            boolean reverseMistakenLabelingOfStartAndEndDatesWithUtcTimeZone) {
        this.reverseMistakenLabelingOfStartAndEndDatesWithUtcTimeZone = reverseMistakenLabelingOfStartAndEndDatesWithUtcTimeZone;
    }

    public boolean isReturnCardForDebug() {
        return returnCardForDebug;
    }

    public void setReturnCardForDebug(boolean returnCardForDebug) {
        this.returnCardForDebug = returnCardForDebug;
    }

    public TimeZone getCorrectTimeZone() {
        return correctTimeZone;
    }

    public void setCorrectTimeZone(TimeZone correctTimeZone) {
        this.correctTimeZone = correctTimeZone;
    }

    public int getDefaultOpioidRxDaysSupplyEstimate() {
        return defaultOpioidRxDaysSupplyEstimate;
    }

    public void setDefaultOpioidRxDaysSupplyEstimate(int defaultOpioidRxDaysSupplyEstimate) {
        this.defaultOpioidRxDaysSupplyEstimate = defaultOpioidRxDaysSupplyEstimate;
    }

    public boolean isIncludeAverageMmePerDay() {
        return includeAverageMmePerDay;
    }

    public void setIncludeAverageMmePerDay(boolean includeAverageMmePerDay) {
        this.includeAverageMmePerDay = includeAverageMmePerDay;
    }

    public boolean isCalculateCoverageDatesUsingPresumptionOfDefaultDaysSupplyUnlessExplicitlyStatedOtherwise() {
        return calculateCoverageDatesUsingPresumptionOfDefaultDaysSupplyUnlessExplicitlyStatedOtherwise;
    }

    public void setCalculateCoverageDatesUsingPresumptionOfDefaultDaysSupplyUnlessExplicitlyStatedOtherwise(
            boolean calculateCoverageDatesUsingPresumptionOfDefaultDaysSupplyUnlessExplicitlyStatedOtherwise) {
        this.calculateCoverageDatesUsingPresumptionOfDefaultDaysSupplyUnlessExplicitlyStatedOtherwise = calculateCoverageDatesUsingPresumptionOfDefaultDaysSupplyUnlessExplicitlyStatedOtherwise;
    }

    public boolean isUseDynamicTableFormat() {
        return useDynamicTableFormat;
    }

    public void setUseDynamicTableFormat(boolean useDynamicTableFormat) {
        this.useDynamicTableFormat = useDynamicTableFormat;
    }

    public boolean isQueryForMedOrders() {
        return queryForMedOrders;
    }

    public void setQueryForMedOrders(boolean queryForMedOrders) {
        this.queryForMedOrders = queryForMedOrders;
    }

    public boolean isQueryForMeds() {
        return queryForMeds;
    }

    public void setQueryForMeds(boolean queryForMeds) {
        this.queryForMeds = queryForMeds;
    }

    public String getNewImage() {
        return newImage;
    }
    
    public void setNewImage(String newImage) {
        this.newImage = newImage;
    }
    
    public String getExpandImage() {
        return expandImage;
    }
    
    public void setExpandImage(String expandImage) {
        this.expandImage = expandImage;
    }

    public String getWarningImage() {
        return warningImage;
    }
    
    public void setWarningImage(String warningImage) {
        this.warningImage = warningImage;
    }
    
    public String getImageCache() {
        return imageCache;
    }
    
    public void setImageCache(String imageCache) {
        this.imageCache = imageCache;
    }

    @Override
    public String toString() {
        return "OpioidConfig [\nchronicOpioidUseDeterminationMethod=" + chronicOpioidUseDeterminationMethod
                + "\nexcludeIfNoDraftOpioidMedOrdersAbusedInAmbulatoryCare="
                + excludeIfNoDraftOpioidMedOrdersAbusedInAmbulatoryCare
                + "\nexcludeIfDraftOrActiveMedOrderIndicatingEndOfLife="
                + excludeIfDraftOrActiveMedOrderIndicatingEndOfLife + "\nexcludeIfActiveConditionIndicatingEndOfLife="
                + excludeIfActiveConditionIndicatingEndOfLife + "\nexcludeIfRecentOpioidUse=" + excludeIfRecentOpioidUse
                + "\nrecentOpioidUseLookbackDays=" + recentOpioidUseLookbackDays
                + "\nrecentOpioidUseIfAtLeastXDaysInLookbackOnOpioids="
                + recentOpioidUseIfAtLeastXDaysInLookbackOnOpioids + "\nmmeShortLabel=" + mmeShortLabel
                + "\nmmeLongLabel=" + mmeLongLabel + "\nlocalGuidelineLabel=" + localGuidelineLabel
                + "\nlocalGuidelineUrl=" + localGuidelineUrl + "\nrestrictToSpecifiedMedStatementCategories="
                + restrictToSpecifiedMedStatementCategories + "\nmedStatementCategoriesAllowed="
                + medStatementCategoriesAllowed + "\nreverseMistakenLabelingOfStartAndEndDatesWithUtcTimeZone="
                + reverseMistakenLabelingOfStartAndEndDatesWithUtcTimeZone + "\nreturnCardForDebug="
                + returnCardForDebug + "\ndefaultOpioidRxDaysSupplyEstimate=" + defaultOpioidRxDaysSupplyEstimate
                + "\nincludeAverageMmePerDay=" + includeAverageMmePerDay
                + "\ncalculateCoverageDatesUsingPresumptionOfDefaultDaysSupplyUnlessExplicitlyStatedOtherwise="
                + calculateCoverageDatesUsingPresumptionOfDefaultDaysSupplyUnlessExplicitlyStatedOtherwise
                + "\nuseDynamicTableFormat=" + useDynamicTableFormat + "\nqueryForMedOrders=" + queryForMedOrders
                + "\nqueryForMeds=" + queryForMeds + "\nnewImage=" + newImage + "\nexpandImage=" + expandImage
                + "\nwarningImage=" + warningImage + "\nimageCache=" + imageCache + "\n]";
    }
    
}
