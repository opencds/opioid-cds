package org.opencds.opioid.config.valuesets;

public enum ChronicOpioidUseDeterminationMethod {
    NONE(0),
    M_80D_OF_90D(1),
    THREE_CONSECUTIVE_21D_OF_30D(2);

    private int id;

    ChronicOpioidUseDeterminationMethod(int id) {
        this.id = id;
    }

    public static ChronicOpioidUseDeterminationMethod fromId(int methodId) {
        ChronicOpioidUseDeterminationMethod method = NONE;
        for (ChronicOpioidUseDeterminationMethod m : values()) {
            if (m.getId() == methodId) {
                method = m;
                break;
            }
        }
        return method;
    }
    
    public static ChronicOpioidUseDeterminationMethod fromString(String methodName) {
        ChronicOpioidUseDeterminationMethod method = NONE;
        for (ChronicOpioidUseDeterminationMethod m : values()) {
            if (m.toString().equalsIgnoreCase(methodName)) {
                method = m;
                break;
            }
        }
        return method;
    }

    public int getId() {
        return id;
    }

}
