package org.opencds.opioid.config

import org.opencds.opioid.config.OpioidConfig
import org.opencds.opioid.config.OpioidConfigName

import spock.lang.Specification

class OpioidConfigAndNameSpec extends Specification {

    def "test toString"() {
        given:
        OpioidConfig config = new OpioidConfig()
        
        when:
        OpioidConfigName.values().each {OpioidConfigName names ->
            names.apply(config, null);
        }
        def toString = config.toString()
        
        then:"correctTimezone isn't printed, as that is not really configurable and the default can vary"
        toString == expectedDefaults
    }
    
    private String expectedDefaults = """OpioidConfig [
chronicOpioidUseDeterminationMethod=THREE_CONSECUTIVE_21D_OF_30D
excludeIfNoDraftOpioidMedOrdersAbusedInAmbulatoryCare=true
excludeIfDraftOrActiveMedOrderIndicatingEndOfLife=false
excludeIfActiveConditionIndicatingEndOfLife=false
excludeIfRecentOpioidUse=false
recentOpioidUseLookbackDays=365
recentOpioidUseIfAtLeastXDaysInLookbackOnOpioids=2
mmeShortLabel=OME
mmeLongLabel=oral morphine equivalence
localGuidelineLabel=CPG opioid use guidelines
localGuidelineUrl=http://tbd.utah.edu
restrictToSpecifiedMedStatementCategories=true
medStatementCategoriesAllowed=[community]
reverseMistakenLabelingOfStartAndEndDatesWithUtcTimeZone=true
returnCardForDebug=false
defaultOpioidRxDaysSupplyEstimate=30
includeAverageMmePerDay=true
calculateCoverageDatesUsingPresumptionOfDefaultDaysSupplyUnlessExplicitlyStatedOtherwise=true
useDynamicTableFormat=true
queryForMedOrders=false
queryForMeds=false
newImage=
expandImage=
warningImage=
imageCache=null
]"""
}
