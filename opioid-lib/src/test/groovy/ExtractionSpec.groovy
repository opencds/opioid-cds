import java.text.DecimalFormat
import java.text.NumberFormat

import org.hl7.fhir.dstu2.model.Bundle
import org.hl7.fhir.dstu2.model.MedicationOrder
import org.hl7.fhir.dstu2.model.Range
import org.hl7.fhir.dstu2.model.Resource
import org.hl7.fhir.dstu2.model.ResourceType
import org.hl7.fhir.dstu2.model.SimpleQuantity
import org.hl7.fhir.dstu2.model.Timing
import org.hl7.fhir.dstu2.model.MedicationOrder.MedicationOrderDosageInstructionComponent
import org.hl7.fhir.dstu2.model.Timing.TimingRepeatComponent
import org.hl7.fhir.dstu2.model.Timing.UnitsOfTime
import org.opencds.Utils

import ca.uhn.fhir.context.FhirContext
import ca.uhn.fhir.parser.JsonParser
import spock.lang.Ignore
import spock.lang.Specification

class ExtractionSpec extends Specification {
    private static NumberFormat myDecimalFormatter = new DecimalFormat("#########.#");

    @Ignore
    def "extract sigs from medication orders"() {
        given:
        FhirContext ctx = FhirContext.forDstu2Hl7Org()
        JsonParser parser = ctx.newJsonParser()
        File folder = new File('/Users/phillip/tmp-phi/opioid')
        File[] folderFiles = folder.listFiles()
        folderFiles.each {File f ->
            if (f.isDirectory() && f.name =~ /^\d+$/) {
                Bundle bundle = parser.parseResource(new File(f, 'MedicationOrder_bundle.json').text)
                bundle.entry*.resource.each {Resource r ->
                    if (r.resourceType == ResourceType.MedicationOrder) {
                        println extractSig((MedicationOrder)r)
                    }
                }
            }
        }

        when:
        def x = 1

        then:
        true

    }

    private String extractSig(MedicationOrder medOrder) {
        MedicationOrderDosageInstructionComponent dosageInstruction = medOrder.getDosageInstruction().get(0);

        String doseStr = "";
        String frequencyStr = "";
        String periodStr = "";
        double doseValue = 0;
        String doseUnit = "";
        boolean isDoseValueFromRange = false;

        // dosage
        try
        {
            SimpleQuantity simpleQuantity = dosageInstruction.getDoseSimpleQuantity();
            doseValue = simpleQuantity.getValue().doubleValue();
            doseUnit = simpleQuantity.getUnit();
            if (doseUnit != null)
            {
                doseUnit = doseUnit.toLowerCase();
            }
            doseStr = myDecimalFormatter.format(doseValue) + " " + doseUnit;
        }
        catch(Exception e)
        {
            // try the range instead
            try
            {
                Range doseRange = dosageInstruction.getDoseRange();
                SimpleQuantity lowQuantity = doseRange.getLow();
                SimpleQuantity highQuantity = doseRange.getHigh();
                double lowDoseValue = lowQuantity.getValue().doubleValue();
                doseUnit = lowQuantity.getUnit();
                if (doseUnit != null)
                {
                    doseUnit = doseUnit.toLowerCase();
                }
                double highDoseValue = highQuantity.getValue().doubleValue();
                if (highDoseValue != lowDoseValue)
                {
                    isDoseValueFromRange = true;
                }

                doseStr = myDecimalFormatter.format(lowDoseValue) + "-" + myDecimalFormatter.format(highDoseValue) + doseUnit;
                doseValue = highDoseValue;
            }
            catch(Exception f)
            {
                // set a default dose of 1 without units
                doseValue = 1.0;
            }
        }

        boolean prn = false;
        try
        {
            if (dosageInstruction.getAsNeededBooleanType().getValue().booleanValue() == true)
            {
                prn = true;
            }
        }
        catch (Exception e)
        {
            // do nothing
        }

        int frequency = 1; // default
        boolean isFrequencyFromRange = false;
        boolean isPeriodFromRange = false;
        boolean isNumTimePerDayFromRange = false;
        double numTimesPerDay = 1; // default

        Timing timing = dosageInstruction.timing
        if (timing != null)
        {
            TimingRepeatComponent repeat = timing.getRepeat();

            if (repeat != null)
            {
                if (repeat.hasFrequency())
                {
                    frequency = repeat.getFrequency();
                    frequencyStr = frequency + "";

                    if (repeat.hasFrequencyMax())
                    {
                        int frequencyMax = repeat.getFrequencyMax();

                        if ((frequencyMax > 0) && (frequencyMax != frequency))
                        {
                            isFrequencyFromRange = true;
                            frequencyStr = frequency + "-" + frequencyMax;
                            frequency = frequencyMax;
                        }
                    }
                }

                BigDecimal periodBigDecimal = null;
                UnitsOfTime periodUnits = null;

                if (repeat.hasPeriod())
                {
                    periodBigDecimal = repeat.getPeriod();
                    periodUnits = repeat.getPeriodUnits();

                    if (repeat.hasPeriodMax())
                    {
                        BigDecimal periodMaxBigDecimal = repeat.getPeriodMax();
                        periodStr = myDecimalFormatter.format(periodBigDecimal.doubleValue()) + "-" + myDecimalFormatter.format(periodMaxBigDecimal.doubleValue()) + periodUnits.toString();
                        if (periodBigDecimal.doubleValue() != periodBigDecimal.doubleValue())
                        {
                            isPeriodFromRange = true;
                        }
                    }
                    else
                    {
                        periodStr = myDecimalFormatter.format(periodBigDecimal.doubleValue());
                        if((periodUnits != null) && (periodUnits.getDisplay() != null))
                        {
                            periodStr += periodUnits.getDisplay();
                        }
                    }
                }

                if ((frequency > 0) && (periodBigDecimal != null) && (periodUnits != null))
                {
                    double period = periodBigDecimal.doubleValue();
                    numTimesPerDay = Utils.getNumTimesPerDay(frequency, period, periodUnits);
                }

                if (isFrequencyFromRange || isPeriodFromRange)
                {
                    isNumTimePerDayFromRange = true;
                }
            }
        }

        String sig = null;
        if (dosageInstruction.text != null) {
            sig = dosageInstruction.text;
        }
        else {
            if (! Utils.isMedicationOrderMissingDoseQuantity(medOrder)) {
                sig = doseStr + " ";
                if (frequency > 1) {
                    sig += frequencyStr + "x ";
                }
                sig += "q" + periodStr;
                if(prn) {
                    sig += " PRN";
                }
            }
        }
        return sig
    }
}
