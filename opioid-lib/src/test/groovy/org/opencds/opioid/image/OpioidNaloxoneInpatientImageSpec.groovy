package org.opencds.opioid.image
import javax.imageio.ImageIO

import spock.lang.Specification

class OpioidNaloxoneInpatientImageSpec extends Specification {
    
    def 'create an image'() {
        given:
        double value = 192.33;
        
        when:
        String image = OpioidNaloxoneInpatientImage.createImage(value)
        
        and:
        File outputfile = new File("saved.svg");
        outputfile.delete()
        outputfile << image
        
        then:
        true
    }

}
