package org.opencds.opioid.image
import java.awt.image.BufferedImage

import javax.imageio.ImageIO

import spock.lang.Specification

class OpioidMmeImageSpec extends Specification {
    
    def 'create an image'() {
        given:
        double value = 173.0;
        
        when:
        String image = OpioidMmeImage.createImage(value)
        
        and:
        File outputfile = new File("saved.svg");
        outputfile.delete()
        outputfile.write(image)
        
        then:
        true
    }

}
