package org.opencds.opioid.util

import java.time.Instant
import java.time.LocalDate
import java.time.ZoneId

import spock.lang.Specification
import spock.lang.Unroll

class AgeUtilSpec extends Specification {
    
    @Unroll
    def 'test underAge'() {
        expect:
        underAge == AgeUtil.underAge(date1, evalTime, age)
        
        where:
        date1 | evalTime | age | underAge
        // exact date 18 years ago, not under age
        date(LocalDate.now().minusYears(18)) | date(LocalDate.now()) | 18 | false
        // one day less than 18 years ago (under age by a day)
        date(LocalDate.now().minusYears(18).plusDays(1)) | date(LocalDate.now()) | 18 | true
        // one day more than 18 years ago (over age by a day)
        date(LocalDate.now().minusYears(18).minusDays(1)) | date(LocalDate.now()) | 18 | false
        // evaluate yesterday, but bday is today (under age by a day)
        date(LocalDate.now().minusYears(18)) | date(LocalDate.now().minusDays(1)) | 18 | true
    }
    
    private Date date(LocalDate date) {
        Instant inst = date?.atStartOfDay(ZoneId.systemDefault())?.toInstant()
        return inst ? Date.from(inst) : null
    }

}
