package org.opencds.opioid.util

import spock.lang.Specification
import spock.lang.Unroll

class StringUtilSpec extends Specification {
    @Unroll
    def "test getting boolean value"() {
        expect:
        truth == StringUtil.toBoolean(value)
        
        where:
        truth | value
        true | "true"
        true | "true      "
        false | null
        false | "false"
        false | ""
    }
}
