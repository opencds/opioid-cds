package org.opencds.opioid.util

import spock.lang.Ignore
import spock.lang.Specification

class FileUtilSpec extends Specification {
    def 'test getTemplate'() {
        expect:
        FileUtil.getTemplateString('common/style.css') == STYLE_TEMPLATE
    }

    String STYLE_TEMPLATE = '.ome-body { width: 670px; font-family: segoe-ui, sans-serif; background-color: #FEFFF1;}.ome-medname { font-weight: 600;}.ome-total { font-weight: 700; {{totalStyle}}}.ome-cdc { width: 670px;}.ome-table { width: 670px; vertical-align: top; padding: 2px; background-color: #FEFFF1;}.ome-td { vertical-align: top; padding: 2px; background-color: #FEFFF1;}.ome-td-qty { vertical-align: top; padding: 2px; background-color: #FEFFF1;}.ome-input { display: none; visibility: hidden;}.ome-expand-text { margin-left: 20px; margin-bottom: 6px; transition: height 0.3s;}{{expandCss}}.ome-image-new { fill: green; color: green;}.ome-bg-yellow { background-color: #FBCB00 !important; margin-top: 6px; margin-bottom: 6px;}.ome-warn { margin-top: 6px; margin-bottom: 6px; width: auto;}.ome-label { display: inline-block; }.ome-img { pointer-events: none; }'
}
