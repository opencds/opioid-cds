package org.opencds.opioid.util

import org.opencds.hooks.lib.json.JsonUtil
import org.opencds.hooks.model.dstu2.util.Dstu2JsonUtil
import org.opencds.hooks.model.response.CdsResponse

import spock.lang.Specification

class MarkdownUtilSpec extends Specification {
    private static final FILE = 'src/test/resources/cds-response.json'

    MarkdownUtil util
    JsonUtil jsonUtil

    def setup() {
        util = new MarkdownUtil()
        jsonUtil = new Dstu2JsonUtil()
    }

    def 'test markdown to html'() {
        given:
        CdsResponse response = jsonUtil.fromJson(new File(FILE).text, CdsResponse.class)

        when:
        String html = util.getHtmlFromMarkDown(response.cards[0].detail)

        then:
        println html
    }
}
