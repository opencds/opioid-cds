package org.opencds.opioid.util
import org.hl7.fhir.dstu2.model.Duration
import org.hl7.fhir.dstu2.model.Medication
import org.hl7.fhir.dstu2.model.MedicationOrder
import org.hl7.fhir.dstu2.model.SimpleQuantity
import org.opencds.RxSig
import org.opencds.Utils
import org.opencds.common.utilities.DateUtility
import org.opencds.opioid.util.fhir.MedOrderUtil
import org.opencds.tools.db.api.DbConnection
import org.opencds.tools.terminology.rxnav.api.model.RxNormSemanticDrug
import org.opencds.tools.terminology.rxnav.client.RxNavTerminologyClient

import ca.uhn.fhir.context.FhirContext
import spock.lang.Ignore
import spock.lang.Specification
import spock.lang.Unroll

@Ignore
class MedOrderUtilFunctionalSpec extends Specification {


	/** **************************************************/
	static RxNavTerminologyClient rxNavClient
	static FhirContext context

	// Add base location for folders containing data
	static File baseLocation


	def setupSpec() {
		rxNavClient = new RxNavTerminologyClient(new DbConnection(new File("C:/Users/u0770443/Box Sync/CDC Opioid CDS Project/Knowledge Artifacts/LocalDataStore_RxNav_OpioidCds.accdb")))
		context = FhirContext.forDstu2Hl7Org()
		baseLocation = new File("//ad.utah.edu/UUHC/hscgroups/KMM ITS/Projects/Opioid CDS/Test Data/Med Orders/")

	}

    @Unroll
    @Ignore
    def "001"() {
		given:
		println "Running test: " + testId
		MedicationOrder medOrder = (MedicationOrder) context.newJsonParser().parseResource(MedicationOrder.class, new File(baseLocation, medorder).text)
		Medication med = context.newJsonParser().parseResource(Medication.class, new File(baseLocation, "Meds\\" + getMedFileName(medOrder.getMedicationReference().getReference())).text)
		Map<String, Map<String, Object>> map = new HashMap<String, Map<String, Object>>()
		if (Utils.isMedicationOrderMissingDoseQuantity(medOrder)) { Utils.updateMedOrderDosageInstructionWithSigInfo(medOrder, false); }

        when:
		MedOrderUtil.setMedOrderAttributes(medOrder, med, map, rxNavClient, 'OME', 30)
		String id = medOrder.getId()
		printAttributes(id, map)

        then:
		isDraft(id, map) == false
		medName(id, map) == 'oxyCODONE 10 mg tablet'

		rxSig("sig", id, map) != null
		rxSig_originalSig("sig", id, map) == 'Take 1 tablet (10 mg) by mouth every 4 hours as needed for pain. Up to 6 per day'
		rxSig_useWithCaution("sig", id, map) == false
		rxSig_command("sig", id, map) == 'take'
		rxSig_doseLow("sig", id, map) == 1
		rxSig_doseHigh("sig", id, map) == 1
		rxSig_doseUnits("sig", id, map) == 'tab'
		rxSig_alternateDoseLow("sig", id, map) == 10
		rxSig_alternateDoseHigh("sig", id, map) == 10
		rxSig_alternateDoseUnits("sig", id, map) == 'mg'
		rxSig_route("sig", id, map) == 'po'
		rxSig_intervalLow("sig", id, map) == 4
		rxSig_intervalHigh("sig", id, map) == 4
		rxSig_intervalTimeUnits("sig", id, map) == 'h'
		rxSig_frequencyLow("sig", id, map) == null
		rxSig_frequencyHigh("sig", id, map) == null
		rxSig_frequencyTimeUnits("sig", id, map) == null
		rxSig_averageDailyDose("sig", id, map) == null
		rxSig_averageDailyDoseUnits("sig", id, map) == null
		rxSig_maxDailyDose("sig", id, map) == 6
		rxSig_maxDailyDoseUnits("sig", id, map) == null
		rxSig_maxDailyFrequency("sig", id, map) == null
		rxSig_duration("sig", id, map) == null
		rxSig_durationTimeUnits("sig", id, map) == null
		rxSig_earliestFillDate("sig", id, map) == null
		rxSig_startDate("sig", id, map) == null
		rxSig_endDate("sig", id, map) == null

		rxSig("note", id, map) == null

		conversionFactor_ave(id, map) == 1.5
		conversionFactor_max(id, map) == 1.5
		singleDoseMmeDescription(id, map) == 'For 1 tablet, OME = 15 mg.'

		prescriber(id, map) == 'Provider XXX'
		rxDate(id, map) == '2018-01-16 00:00'
		dispenseQuantity_value(id, map) == 240
		dispenseQuantity_unit(id, map) == 'tablet'
		numRepeatsAllowed(id, map) == 0

		startDate(id, map) == '2017-11-04 00:00'
		endDate(id, map) == '2017-12-14 00:00'
		endDateCalcMethod(id, map) == 'dispense quantity and max daily dose in sig'
		isEndDateEstimated(id, map) == true
		endDatePreferDefault(id, map) == '2017-12-04 00:00'
		expectedSupplyDuration_value(id, map) == 40
		expectedSupplyDuration_unit(id, map) == 'days'

		dailyDoseStr_ave(id, map) == 'Oxycodone Oral Tablet 240 dispense * 10 mg / 30d supply (**assumed**) = 80 mg.'
		dailyDoseStr_max(id, map) == 'Oxycodone Oral Tablet 6 (daily max per sig) * 10 mg = 60 mg.'
		mmePerDay_ave(id, map) == 120
		mmePerDay_aveCalculatedUsingDefaultDaysSupplyEstimate(id, map) == true
		mmePerDay_max(id, map) == 90

		isPrn(id, map) == true
		isOralBuprenorphine(id, map) == false

        where:
        testId  | medorder 									  | testDescription
        '001' 	| 'medorder_oxycodone_active_structured.json' | 'baseline'
    }

	// prn

	@Ignore
	def "002"() {
		given:
		println "Running test: " + testId
		MedicationOrder medOrder = (MedicationOrder) context.newJsonParser().parseResource(MedicationOrder.class, new File(baseLocation, medorder).text)
		Medication med = context.newJsonParser().parseResource(Medication.class, new File(baseLocation, "Meds\\" + getMedFileName(medOrder.getMedicationReference().getReference())).text)
		Map<String, Map<String, Object>> map = new HashMap<String, Map<String, Object>>()
		if (Utils.isMedicationOrderMissingDoseQuantity(medOrder)) { Utils.updateMedOrderDosageInstructionWithSigInfo(medOrder, false); }

		when:
		MedOrderUtil.setMedOrderAttributes(medOrder, med, map, rxNavClient, 'OME', 30)
		String id = medOrder.getId()
		printAttributes(id, map)

		then:
		isPrn(id, map) == true

		where:
		testId  | medorder 									  			| testDescription
		'002' 	| 'medorder_oxycodone_active_structured_prn_sig.json' 	| 'structured prn false, prn in sig'
	}

	//----------------------------------------------
	// end date from Utils.estimateMedOrderEndDate
	//----------------------------------------------

	@Ignore
	def "003"() {
		given:
		println "Running test: " + testId
		MedicationOrder medOrder = (MedicationOrder) context.newJsonParser().parseResource(MedicationOrder.class, new File(baseLocation, medorder).text)
		Medication med = context.newJsonParser().parseResource(Medication.class, new File(baseLocation, "Meds\\" + getMedFileName(medOrder.getMedicationReference().getReference())).text)
		Map<String, Map<String, Object>> map = new HashMap<String, Map<String, Object>>()
		if (Utils.isMedicationOrderMissingDoseQuantity(medOrder)) { Utils.updateMedOrderDosageInstructionWithSigInfo(medOrder, false); }

		when:
		MedOrderUtil.setMedOrderAttributes(medOrder, med, map, rxNavClient, 'OME', 30)
		String id = medOrder.getId()
		printAttributes(id, map)

		then:
		endDate(id, map) == '2017-12-31 00:00'
		endDateCalcMethod(id, map) == 'end date in sig'
		isEndDateEstimated(id, map) == false
		endDatePreferDefault(id, map) == '2017-12-31 00:00' // when end date in sig, prefer default also sets to this one

		where:
		testId  | medorder 									  						| testDescription
		'003' 	| 'medorder_oxycodone_active_structured_end_date_from_sig.json' 	| 'end date should be from sig'
	}

	@Ignore
	def "004"() {
		given:
		println "Running test: " + testId
		MedicationOrder medOrder = (MedicationOrder) context.newJsonParser().parseResource(MedicationOrder.class, new File(baseLocation, medorder).text)
		Medication med = context.newJsonParser().parseResource(Medication.class, new File(baseLocation, "Meds\\" + getMedFileName(medOrder.getMedicationReference().getReference())).text)
		Map<String, Map<String, Object>> map = new HashMap<String, Map<String, Object>>()
		if (Utils.isMedicationOrderMissingDoseQuantity(medOrder)) { Utils.updateMedOrderDosageInstructionWithSigInfo(medOrder, false); }

		when:
		MedOrderUtil.setMedOrderAttributes(medOrder, med, map, rxNavClient, 'OME', 30)
		String id = medOrder.getId()
		printAttributes(id, map)

		then:
		endDate(id, map) == '2017-12-31 00:00'
		endDateCalcMethod(id, map) == 'end date in note to pharmacy'
		isEndDateEstimated(id, map) == false
		endDatePreferDefault(id, map) == '2017-12-31 00:00' // when end date in note, prefer default also sets to this one

		where:
		testId  | medorder 									  						| testDescription
		'004' 	| 'medorder_oxycodone_active_structured_end_date_from_note.json' 	| 'end date should be from Rx note'
	}

	@Ignore
	def "005"() {
		given:
		println "Running test: " + testId
		MedicationOrder medOrder = (MedicationOrder) context.newJsonParser().parseResource(MedicationOrder.class, new File(baseLocation, medorder).text)
		Medication med = context.newJsonParser().parseResource(Medication.class, new File(baseLocation, "Meds\\" + getMedFileName(medOrder.getMedicationReference().getReference())).text)
		Map<String, Map<String, Object>> map = new HashMap<String, Map<String, Object>>()
		if (Utils.isMedicationOrderMissingDoseQuantity(medOrder)) { Utils.updateMedOrderDosageInstructionWithSigInfo(medOrder, false); }

		when:
		MedOrderUtil.setMedOrderAttributes(medOrder, med, map, rxNavClient, 'OME', 30)
		String id = medOrder.getId()
		printAttributes(id, map)

		then:
		endDate(id, map) == '2018-01-03 00:00'
		endDateCalcMethod(id, map) == 'days supply in sig'
		isEndDateEstimated(id, map) == false
		endDatePreferDefault(id, map) == '2018-01-03 00:00' // when duration in sig, prefer default also sets to this one

		where:
		testId  | medorder 									  								| testDescription
		'005' 	| 'medorder_oxycodone_active_structured_end_date_from_sig_duration.json' 	| 'end date should be from duration in sig'
	}

	@Ignore
	def "006"() {
		given:
		println "Running test: " + testId
		MedicationOrder medOrder = (MedicationOrder) context.newJsonParser().parseResource(MedicationOrder.class, new File(baseLocation, medorder).text)
		Medication med = context.newJsonParser().parseResource(Medication.class, new File(baseLocation, "Meds\\" + getMedFileName(medOrder.getMedicationReference().getReference())).text)
		Map<String, Map<String, Object>> map = new HashMap<String, Map<String, Object>>()
		if (Utils.isMedicationOrderMissingDoseQuantity(medOrder)) { Utils.updateMedOrderDosageInstructionWithSigInfo(medOrder, false); }

		when:
		MedOrderUtil.setMedOrderAttributes(medOrder, med, map, rxNavClient, 'OME', 30)
		String id = medOrder.getId()
		printAttributes(id, map)

		then:
		endDate(id, map) == '2018-01-03 00:00'
		endDateCalcMethod(id, map) == 'days supply in note to pharmacy'
		isEndDateEstimated(id, map) == false
		endDatePreferDefault(id, map) == '2018-01-03 00:00' // when duration in note, prefer default also sets to this one

		where:
		testId  | medorder 									  								| testDescription
		'006' 	| 'medorder_oxycodone_active_structured_end_date_from_note_duration.json' 	| 'end date should be from duration in note'
	}

	@Ignore
	def "007"() {
		given:
		println "Running test: " + testId
		MedicationOrder medOrder = (MedicationOrder) context.newJsonParser().parseResource(MedicationOrder.class, new File(baseLocation, medorder).text)
		Medication med = context.newJsonParser().parseResource(Medication.class, new File(baseLocation, "Meds\\" + getMedFileName(medOrder.getMedicationReference().getReference())).text)
		Map<String, Map<String, Object>> map = new HashMap<String, Map<String, Object>>()
		if (Utils.isMedicationOrderMissingDoseQuantity(medOrder)) { Utils.updateMedOrderDosageInstructionWithSigInfo(medOrder, false); }

		when:
		MedOrderUtil.setMedOrderAttributes(medOrder, med, map, rxNavClient, 'OME', 30)
		String id = medOrder.getId()
		printAttributes(id, map)

		then:
		endDate(id, map) == '2018-03-04 00:00'
		endDateCalcMethod(id, map) == 'dispense quantity and average daily dose in sig'
		isEndDateEstimated(id, map) == true
		endDatePreferDefault(id, map) == '2018-03-04 00:00' // when ave in sig, prefer default also sets to this one

		where:
		testId  | medorder 									  							| testDescription
		'007' 	| 'medorder_oxycodone_active_structured_end_date_from_sig_ave.json' 	| 'end date should be from ave in sig'
	}

	@Ignore
	def "008"() {
		given:
		println "Running test: " + testId
		MedicationOrder medOrder = (MedicationOrder) context.newJsonParser().parseResource(MedicationOrder.class, new File(baseLocation, medorder).text)
		Medication med = context.newJsonParser().parseResource(Medication.class, new File(baseLocation, "Meds\\" + getMedFileName(medOrder.getMedicationReference().getReference())).text)
		Map<String, Map<String, Object>> map = new HashMap<String, Map<String, Object>>()
		if (Utils.isMedicationOrderMissingDoseQuantity(medOrder)) { Utils.updateMedOrderDosageInstructionWithSigInfo(medOrder, false); }

		when:
		MedOrderUtil.setMedOrderAttributes(medOrder, med, map, rxNavClient, 'OME', 30)
		String id = medOrder.getId()
		printAttributes(id, map)

		then:
		endDate(id, map) == '2018-03-04 00:00'
		endDateCalcMethod(id, map) == 'dispense quantity and average daily dose in note to pharmacy'
		isEndDateEstimated(id, map) == true
		endDatePreferDefault(id, map) == '2018-03-04 00:00' // when ave in note, prefer default also sets to this one

		where:
		testId  | medorder 									  							| testDescription
		'008'  	| 'medorder_oxycodone_active_structured_end_date_from_note_ave.json' 	| 'end date should be from ave in note'
	}

	@Ignore
	def "009"() {
		given:
		println "Running test: " + testId
		MedicationOrder medOrder = (MedicationOrder) context.newJsonParser().parseResource(MedicationOrder.class, new File(baseLocation, medorder).text)
		Medication med = context.newJsonParser().parseResource(Medication.class, new File(baseLocation, "Meds\\" + getMedFileName(medOrder.getMedicationReference().getReference())).text)
		Map<String, Map<String, Object>> map = new HashMap<String, Map<String, Object>>()
		if (Utils.isMedicationOrderMissingDoseQuantity(medOrder)) { Utils.updateMedOrderDosageInstructionWithSigInfo(medOrder, false); }

		when:
		MedOrderUtil.setMedOrderAttributes(medOrder, med, map, rxNavClient, 'OME', 30)
		String id = medOrder.getId()
		printAttributes(id, map)

		then:
		endDate(id, map) == '2017-11-28 00:00'
		endDateCalcMethod(id, map) == 'dispense quantity and max daily dose in sig'
		isEndDateEstimated(id, map) == true
		endDatePreferDefault(id, map) == '2017-12-04 00:00'

		where:
		testId  | medorder 									  						| testDescription
		'009'  	| 'medorder_oxycodone_active_structured_end_date_from_sig_max.json' | 'end date should be from max in sig'
	}

	@Ignore
	def "010"() {
		given:
		println "Running test: " + testId
		MedicationOrder medOrder = (MedicationOrder) context.newJsonParser().parseResource(MedicationOrder.class, new File(baseLocation, medorder).text)
		Medication med = context.newJsonParser().parseResource(Medication.class, new File(baseLocation, "Meds\\" + getMedFileName(medOrder.getMedicationReference().getReference())).text)
		Map<String, Map<String, Object>> map = new HashMap<String, Map<String, Object>>()
		if (Utils.isMedicationOrderMissingDoseQuantity(medOrder)) { Utils.updateMedOrderDosageInstructionWithSigInfo(medOrder, false); }

		when:
		MedOrderUtil.setMedOrderAttributes(medOrder, med, map, rxNavClient, 'OME', 30)
		String id = medOrder.getId()
		printAttributes(id, map)

		then:
		endDate(id, map) == '2017-11-28 00:00'
		endDateCalcMethod(id, map) == 'dispense quantity and max daily dose in note to pharmacy'
		isEndDateEstimated(id, map) == true
		endDatePreferDefault(id, map) == '2017-12-04 00:00'

		where:
		testId  | medorder 									  							| testDescription
		'010'  	| 'medorder_oxycodone_active_structured_end_date_from_note_max.json' 	| 'end date should be from max in note'
	}

	@Ignore
	def "011"() {
		given:
		println "Running test: " + testId
		MedicationOrder medOrder = (MedicationOrder) context.newJsonParser().parseResource(MedicationOrder.class, new File(baseLocation, medorder).text)
		Medication med = context.newJsonParser().parseResource(Medication.class, new File(baseLocation, "Meds\\" + getMedFileName(medOrder.getMedicationReference().getReference())).text)
		Map<String, Map<String, Object>> map = new HashMap<String, Map<String, Object>>()
		if (Utils.isMedicationOrderMissingDoseQuantity(medOrder)) { Utils.updateMedOrderDosageInstructionWithSigInfo(medOrder, false); }

		when:
		MedOrderUtil.setMedOrderAttributes(medOrder, med, map, rxNavClient, 'OME', 30)
		String id = medOrder.getId()
		printAttributes(id, map)

		then:
		endDate(id, map) == '2017-12-14 00:00'
		endDateCalcMethod(id, map) == 'dispense quantity and use frequency'
		isEndDateEstimated(id, map) == true
		endDatePreferDefault(id, map) == '2017-12-04 00:00'

		where:
		testId  | medorder 																| testDescription
		'011'  	| 'medorder_oxycodone_active_structured_end_date_from_estimate.json' 	| 'end date should be from estimate in structured data'
	}

	@Ignore
	// TODO: could do more here for this pattern
	def "012a"() {
		given:
		println "Running test: " + testId
		MedicationOrder medOrder = (MedicationOrder) context.newJsonParser().parseResource(MedicationOrder.class, new File(baseLocation, medorder).text)
		Medication med = context.newJsonParser().parseResource(Medication.class, new File(baseLocation, "Meds\\" + getMedFileName(medOrder.getMedicationReference().getReference())).text)
		Map<String, Map<String, Object>> map = new HashMap<String, Map<String, Object>>()
		if (Utils.isMedicationOrderMissingDoseQuantity(medOrder)) { Utils.updateMedOrderDosageInstructionWithSigInfo(medOrder, false); }

		when:
		MedOrderUtil.setMedOrderAttributes(medOrder, med, map, rxNavClient, 'OME', 30)
		String id = medOrder.getId()
		printAttributes(id, map)

		then:
		endDate(id, map) == '2017-12-14 00:00'
		endDateCalcMethod(id, map) == 'dispense quantity and use frequency'
		isEndDateEstimated(id, map) == true
		endDatePreferDefault(id, map) == '2017-12-04 00:00'

		where:
		testId  	| medorder 																	| testDescription
		'012a'  	| 'medorder_oxycodone_active_structured_end_date_from_struct_data_1.json' 	| 'end date should be from structured data'
	}

	@Ignore
	def "013"() {
		given:
		println "Running test: " + testId
		MedicationOrder medOrder = (MedicationOrder) context.newJsonParser().parseResource(MedicationOrder.class, new File(baseLocation, medorder).text)
		Medication med = context.newJsonParser().parseResource(Medication.class, new File(baseLocation, "Meds\\" + getMedFileName(medOrder.getMedicationReference().getReference())).text)
		Map<String, Map<String, Object>> map = new HashMap<String, Map<String, Object>>()
		if (Utils.isMedicationOrderMissingDoseQuantity(medOrder)) { Utils.updateMedOrderDosageInstructionWithSigInfo(medOrder, false); }

		when:
		MedOrderUtil.setMedOrderAttributes(medOrder, med, map, rxNavClient, 'OME', 30)
		String id = medOrder.getId()
		printAttributes(id, map)

		then:
		endDate(id, map) == '2017-12-04 00:00'
		endDateCalcMethod(id, map) == 'presumed 30d supply'
		isEndDateEstimated(id, map) == true
		endDatePreferDefault(id, map) == '2017-12-04 00:00'

		where:
		testId  | medorder 															| testDescription
		'013'  	| 'medorder_oxycodone_active_structured_end_date_from_default.json' | 'end date should be via default, as missing dispense qty'
	}

	//----------------------------------------------
	// Different paths through primary Rx processing
	//----------------------------------------------

	@Ignore
	def "014"() {
		given:
		println "Running test: " + testId
		MedicationOrder medOrder = (MedicationOrder) context.newJsonParser().parseResource(MedicationOrder.class, new File(baseLocation, medorder).text)
		Medication med = context.newJsonParser().parseResource(Medication.class, new File(baseLocation, "Meds\\" + getMedFileName(medOrder.getMedicationReference().getReference())).text)
		Map<String, Map<String, Object>> map = new HashMap<String, Map<String, Object>>()

		when:
		MedOrderUtil.setMedOrderAttributes(medOrder, med, map, rxNavClient, 'OME', 30)
		String id = medOrder.getId()
		printAttributes(id, map)

		then:
		conversionFactor_ave(id, map) == 1.8
		conversionFactor_max(id, map) == 1.8
		singleDoseMmeDescription(id, map) == 'For 1 patch, OME = 27 mg.'
		dailyDoseStr_ave(id, map) == 'Unable to calculate due to free-text sig.'
		dailyDoseStr_max(id, map) == 'Unable to calculate due to free-text sig.'

		where:
		testId  | medorder 									| testDescription
		'014'  	| 'medorder_bup_patch_active_empty.json' 	| 'patch, free text -- empty'
	}

	@Ignore
	def "015"() {
		given:
		println "Running test: " + testId
		MedicationOrder medOrder = (MedicationOrder) context.newJsonParser().parseResource(MedicationOrder.class, new File(baseLocation, medorder).text)
		Medication med = context.newJsonParser().parseResource(Medication.class, new File(baseLocation, "Meds\\" + getMedFileName(medOrder.getMedicationReference().getReference())).text)
		Map<String, Map<String, Object>> map = new HashMap<String, Map<String, Object>>()
		if (Utils.isMedicationOrderMissingDoseQuantity(medOrder)) { Utils.updateMedOrderDosageInstructionWithSigInfo(medOrder, false); }

		when:
		MedOrderUtil.setMedOrderAttributes(medOrder, med, map, rxNavClient, 'OME', 30)
		String id = medOrder.getId()
		printAttributes(id, map)

		then:
		conversionFactor_ave(id, map) == 1.8
		conversionFactor_max(id, map) == 1.8
		singleDoseMmeDescription(id, map) == 'For 1 patch, OME = 36 mg.'

		dailyDoseStr_ave(id, map) == 'Buprenorphine patch: 20 mcg.'
		dailyDoseStr_max(id, map) == 'Buprenorphine patch: 20 mcg.'
		mmePerDay_ave(id, map) == 36.0
		mmePerDay_aveCalculatedUsingDefaultDaysSupplyEstimate(id, map) == false
		mmePerDay_max(id, map) == 36.0

		where:
		testId  | medorder 																				| testDescription
		'015'  	| 'buprenorphine (BUTRANS) 20 mcg_hr Weekly Patch extended release patch__FTEXT_1.json'	| 'patch, structured, dose starts with mg or mcg'
	}

	@Ignore
	def "016"() {
		given:
		println "Running test: " + testId
		MedicationOrder medOrder = (MedicationOrder) context.newJsonParser().parseResource(MedicationOrder.class, new File(baseLocation, medorder).text)
		Medication med = context.newJsonParser().parseResource(Medication.class, new File(baseLocation, "Meds\\" + getMedFileName(medOrder.getMedicationReference().getReference())).text)
		Map<String, Map<String, Object>> map = new HashMap<String, Map<String, Object>>()
		if (Utils.isMedicationOrderMissingDoseQuantity(medOrder)) { Utils.updateMedOrderDosageInstructionWithSigInfo(medOrder, false); }

		when:
		MedOrderUtil.setMedOrderAttributes(medOrder, med, map, rxNavClient, 'OME', 30)
		String id = medOrder.getId()
		printAttributes(id, map)

		then:
		conversionFactor_ave(id, map) == 2.4
		conversionFactor_max(id, map) == 2.4
		singleDoseMmeDescription(id, map) == 'For 1 patch, OME = 28.8 mg.'
		dailyDoseStr_ave(id, map) == 'Fentanyl patch: 1 * 12 mcg/hr = 12 mcg/hr.'
		dailyDoseStr_max(id, map) == 'Fentanyl patch: 1 * 12 mcg/hr = 12 mcg/hr.'
		round(mmePerDay_ave(id, map)) == 28.8
		mmePerDay_aveCalculatedUsingDefaultDaysSupplyEstimate(id, map) == false
		round(mmePerDay_max(id, map)) == 28.8

		where:
		testId  | medorder 																	| testDescription
		'016'  	| 'fentaNYL (DURAGESIC) 12 mcg_hr extended release patch__STRUCT_1.json' 	| 'patch, structured, dose does not start with mc or mcg (patch)'
	}

	@Ignore
	def "017"() {
		given:
		println "Running test: " + testId
		MedicationOrder medOrder = (MedicationOrder) context.newJsonParser().parseResource(MedicationOrder.class, new File(baseLocation, medorder).text)
		Medication med = context.newJsonParser().parseResource(Medication.class, new File(baseLocation, "Meds\\" + getMedFileName(medOrder.getMedicationReference().getReference())).text)
		Map<String, Map<String, Object>> map = new HashMap<String, Map<String, Object>>()
		if (Utils.isMedicationOrderMissingDoseQuantity(medOrder)) { Utils.updateMedOrderDosageInstructionWithSigInfo(medOrder, false); }

		when:
		MedOrderUtil.setMedOrderAttributes(medOrder, med, map, rxNavClient, 'OME', 30)
		String id = medOrder.getId()
		printAttributes(id, map)

		then:
		conversionFactor_ave(id, map) == null
		conversionFactor_max(id, map) == null
		singleDoseMmeDescription(id, map) == null

		dailyDoseStr_ave(id, map) == 'Unable to calculate OME for injectable drug.'
		dailyDoseStr_max(id, map) == 'Unable to calculate OME for injectable drug.'
		round(mmePerDay_ave(id, map)) == null
		mmePerDay_aveCalculatedUsingDefaultDaysSupplyEstimate(id, map) == false
		round(mmePerDay_max(id, map)) == null

		where:
		testId  | medorder 										| testDescription
		'017'  	| 'morphine injection 1-4 mg__STRUCT_1.json' 	| 'IV (cannot calculate)'
	}

	@Ignore
	def "018"() {
		given:
		println "Running test: " + testId
		MedicationOrder medOrder = (MedicationOrder) context.newJsonParser().parseResource(MedicationOrder.class, new File(baseLocation, medorder).text)
		Medication med = context.newJsonParser().parseResource(Medication.class, new File(baseLocation, "Meds\\" + getMedFileName(medOrder.getMedicationReference().getReference())).text)
		Map<String, Map<String, Object>> map = new HashMap<String, Map<String, Object>>()
		if (Utils.isMedicationOrderMissingDoseQuantity(medOrder)) { Utils.updateMedOrderDosageInstructionWithSigInfo(medOrder, false); }

		when:
		MedOrderUtil.setMedOrderAttributes(medOrder, med, map, rxNavClient, 'OME', 30)
		String id = medOrder.getId()
		printAttributes(id, map)

		then:
		conversionFactor_ave(id, map) == 0.15
		conversionFactor_max(id, map) == 0.15
		singleDoseMmeDescription(id, map) == 'For 1 ml, OME = 0.3 mg.'
		dispenseQuantity_value(id, map) == 118.0
		dispenseQuantity_unit(id, map) == 'mL'
		dailyDoseStr_ave(id, map) == 'Codeine Oral Solution 118 mL dispense * 2 mg/ml / 30d supply (**assumed**) = 7.87 mg.'
		dailyDoseStr_max(id, map) == 'Codeine Oral Solution 5 ml * max 3/d * 2 mg/ml = 30 mg.'
		round(mmePerDay_ave(id, map)) == 1.18
		mmePerDay_aveCalculatedUsingDefaultDaysSupplyEstimate(id, map) == true
		round(mmePerDay_max(id, map)) == 4.5

		where:
		testId  | medorder 													| testDescription
		'018'  	| 'Guaifenesin-Codeine 100-10 MG_5ML Syrup__STRUCT_1.json' 	| 'Liquid calculation'
	}

	@Ignore
	def "019"() {
		println "Running test: " + testId
		MedicationOrder medOrder = (MedicationOrder) context.newJsonParser().parseResource(MedicationOrder.class, new File(baseLocation, medorder).text)
		Medication med = context.newJsonParser().parseResource(Medication.class, new File(baseLocation, "Meds\\" + getMedFileName(medOrder.getMedicationReference().getReference())).text)
		Map<String, Map<String, Object>> map = new HashMap<String, Map<String, Object>>()
		if (Utils.isMedicationOrderMissingDoseQuantity(medOrder)) { Utils.updateMedOrderDosageInstructionWithSigInfo(medOrder, false); }

		when:
		MedOrderUtil.setMedOrderAttributes(medOrder, med, map, rxNavClient, 'OME', 30)
		String id = medOrder.getId()
		printAttributes(id, map)

		then:
		conversionFactor_ave(id, map) == 1.5
		conversionFactor_max(id, map) == 1.5
		singleDoseMmeDescription(id, map) == 'For 1 tablet, OME = 15 mg.'
		dispenseQuantity_value(id, map) == 420
		dispenseQuantity_unit(id, map) == 'mg'
		dailyDoseStr_ave(id, map) == 'Oxycodone Oral Tablet 420 mg dispense / 30d supply (**assumed**) = 14 mg.'
		dailyDoseStr_max(id, map) == 'Oxycodone Oral Tablet max 1 tablet * max 6/d * 10 mg = 60 mg.'
		round(mmePerDay_ave(id, map)) == 21.0
		mmePerDay_aveCalculatedUsingDefaultDaysSupplyEstimate(id, map) == true
		round(mmePerDay_max(id, map)) == 90.0

		where:
		testId  | medorder 												| testDescription
		'019'  	| 'oxyCODONE 10 mg tablet__STRUCT_1_mg_dispense.json' 	| 'Mg dispense (may never happen)'
	}

	@Ignore
	def "020"() {
		println "Running test: " + testId
		MedicationOrder medOrder = (MedicationOrder) context.newJsonParser().parseResource(MedicationOrder.class, new File(baseLocation, medorder).text)
		Medication med = context.newJsonParser().parseResource(Medication.class, new File(baseLocation, "Meds\\" + getMedFileName(medOrder.getMedicationReference().getReference())).text)
		Map<String, Map<String, Object>> map = new HashMap<String, Map<String, Object>>()
		if (Utils.isMedicationOrderMissingDoseQuantity(medOrder)) { Utils.updateMedOrderDosageInstructionWithSigInfo(medOrder, false); }

		when:
		MedOrderUtil.setMedOrderAttributes(medOrder, med, map, rxNavClient, 'OME', 30)
		String id = medOrder.getId()
		printAttributes(id, map)

		then:
		conversionFactor_ave(id, map) == 8.0
		conversionFactor_max(id, map) == 8.0
		singleDoseMmeDescription(id, map) == 'For 1 tablet, OME = 40 mg (if 1-20 mg/d), 80 mg (if 21-40 mg/d), 100 mg (if 41-60 mg/d), or 120 mg (if >= 60 mg/d).'

		dispenseQuantity_value(id, map) == 90.0
		dispenseQuantity_unit(id, map) == 'tablet'
		dailyDoseStr_ave(id, map) == 'Methadone Oral Tablet 90 dispense * 10 mg / 30d supply (**assumed**) = 30 mg.'
		dailyDoseStr_max(id, map) == 'Methadone Oral Tablet 1 tablet * max 3/d * 10 mg = 30 mg.'
		round(mmePerDay_ave(id, map)) == 240.0
		mmePerDay_aveCalculatedUsingDefaultDaysSupplyEstimate(id, map) == true
		round(mmePerDay_max(id, map)) == 240.0

		where:
		testId  | medorder 												| testDescription
		'020'  	| 'methadone (DOLOPHINE) 10 mg tablet__STRUCT_1.json' 	| 'tab (non-mg or ml) dispense'
	}

	@Ignore
	def "021"() {
		println "Running test: " + testId
		MedicationOrder medOrder = (MedicationOrder) context.newJsonParser().parseResource(MedicationOrder.class, new File(baseLocation, medorder).text)
		Medication med = context.newJsonParser().parseResource(Medication.class, new File(baseLocation, "Meds\\" + getMedFileName(medOrder.getMedicationReference().getReference())).text)
		Map<String, Map<String, Object>> map = new HashMap<String, Map<String, Object>>()
		if (Utils.isMedicationOrderMissingDoseQuantity(medOrder)) { Utils.updateMedOrderDosageInstructionWithSigInfo(medOrder, false); }

		when:
		MedOrderUtil.setMedOrderAttributes(medOrder, med, map, rxNavClient, 'OME', 30)
		String id = medOrder.getId()
		printAttributes(id, map)

		then:
		rxSig_averageDailyDose("sig", id, map) == 50.0
		conversionFactor_ave(id, map) == 1.5
		conversionFactor_max(id, map) == 1.5
		singleDoseMmeDescription(id, map) == 'For 1 tablet, OME = 15 mg.'
		dailyDoseStr_ave(id, map) == 'Oxycodone Oral Tablet 50 mg (daily ave per sig).'
		dailyDoseStr_max(id, map) == 'Oxycodone Oral Tablet max 1 tablet * max 6/d * 10 mg = 60 mg.'
		round(mmePerDay_ave(id, map)) == 75.0
		mmePerDay_aveCalculatedUsingDefaultDaysSupplyEstimate(id, map) == false
		round(mmePerDay_max(id, map)) == 90.0

		where:
		testId  | medorder 												| testDescription
		'021'  	| 'oxyCODONE 10 mg tablet__STRUCT_1_ave_50_mg.json' 	| 'ave mg per day in sig'
	}

	@Ignore
	def "022"() {
		println "Running test: " + testId
		MedicationOrder medOrder = (MedicationOrder) context.newJsonParser().parseResource(MedicationOrder.class, new File(baseLocation, medorder).text)
		Medication med = context.newJsonParser().parseResource(Medication.class, new File(baseLocation, "Meds\\" + getMedFileName(medOrder.getMedicationReference().getReference())).text)
		Map<String, Map<String, Object>> map = new HashMap<String, Map<String, Object>>()
		if (Utils.isMedicationOrderMissingDoseQuantity(medOrder)) { Utils.updateMedOrderDosageInstructionWithSigInfo(medOrder, false); }

		when:
		MedOrderUtil.setMedOrderAttributes(medOrder, med, map, rxNavClient, 'OME', 30)
		String id = medOrder.getId()
		printAttributes(id, map)

		then:
		rxSig_originalSig("sig", id, map) == 'Take 0.5-1 tablets (5-10 mg) by mouth every 4 hours as needed.'
		conversionFactor_ave(id, map) == 1.5
		conversionFactor_max(id, map) == 1.5
		singleDoseMmeDescription(id, map) == 'For 1 tablet, OME = 15 mg.'
		dailyDoseStr_ave(id, map) == 'Oxycodone Oral Tablet 42 dispense * 10 mg / 30d supply (**assumed**) = 14 mg.'
		dailyDoseStr_max(id, map) == 'Oxycodone Oral Tablet max 6/d * max 10 mg = 60 mg.'
		round(mmePerDay_ave(id, map)) == 21.0
		mmePerDay_aveCalculatedUsingDefaultDaysSupplyEstimate(id, map) == true
		round(mmePerDay_max(id, map)) == 90.0

		where:
		testId  | medorder 												| testDescription
		'022'  	| 'oxyCODONE 10 mg tablet__STRUCT_1_dose_in_mg.json' 	| 'dose in mg'
	}

	@Ignore
	def "023"() {
		println "Running test: " + testId
		MedicationOrder medOrder = (MedicationOrder) context.newJsonParser().parseResource(MedicationOrder.class, new File(baseLocation, medorder).text)
		Medication med = context.newJsonParser().parseResource(Medication.class, new File(baseLocation, "Meds\\" + getMedFileName(medOrder.getMedicationReference().getReference())).text)
		Map<String, Map<String, Object>> map = new HashMap<String, Map<String, Object>>()
		if (Utils.isMedicationOrderMissingDoseQuantity(medOrder)) { Utils.updateMedOrderDosageInstructionWithSigInfo(medOrder, false); }

		when:
		MedOrderUtil.setMedOrderAttributes(medOrder, med, map, rxNavClient, 'OME', 30)
		String id = medOrder.getId()
		printAttributes(id, map)

		then:
		rxSig_originalSig("sig", id, map) == 'Take 0.5-1 tablets (5-10 mg) by mouth every 4 hours as needed.  Average 4 times daily.'
		rxSig_averageDailyFrequency("sig", id, map) == 4

		conversionFactor_ave(id, map) == 1.5
		conversionFactor_max(id, map) == 1.5
		singleDoseMmeDescription(id, map) == 'For 1 tablet, OME = 15 mg.'
		dailyDoseStr_ave(id, map) == 'Oxycodone Oral Tablet ave 4/d * max 10 mg = 40 mg.'
		dailyDoseStr_max(id, map) == 'Oxycodone Oral Tablet max 6/d * max 10 mg = 60 mg.'
		round(mmePerDay_ave(id, map)) == 60.0
		mmePerDay_aveCalculatedUsingDefaultDaysSupplyEstimate(id, map) == false
		round(mmePerDay_max(id, map)) == 90.0

		where:
		testId  | medorder 															| testDescription
		'023'  	| 'oxyCODONE 10 mg tablet__STRUCT_1_dose_in_mg_ave_4x_day.json' 	| 'dose ing mg, ave freq per day in sig'
	}

	@Ignore
	def "024"() {
		println "Running test: " + testId
		MedicationOrder medOrder = (MedicationOrder) context.newJsonParser().parseResource(MedicationOrder.class, new File(baseLocation, medorder).text)
		Medication med = context.newJsonParser().parseResource(Medication.class, new File(baseLocation, "Meds\\" + getMedFileName(medOrder.getMedicationReference().getReference())).text)
		Map<String, Map<String, Object>> map = new HashMap<String, Map<String, Object>>()
		if (Utils.isMedicationOrderMissingDoseQuantity(medOrder)) { Utils.updateMedOrderDosageInstructionWithSigInfo(medOrder, false); }

		when:
		MedOrderUtil.setMedOrderAttributes(medOrder, med, map, rxNavClient, 'OME', 30)
		String id = medOrder.getId()
		printAttributes(id, map)

		then:
		rxSig_maxDailyDose("sig", id, map) == 10.0
		rxSig_maxDailyDoseUnits("sig", id, map) == 'ml'

		conversionFactor_ave(id, map) == 0.15
		conversionFactor_max(id, map) == 0.15
		singleDoseMmeDescription(id, map) == 'For 1 ml, OME = 0.3 mg.'
		dispenseQuantity_value(id, map) == 118.0
		dispenseQuantity_unit(id, map) == 'mL'
		dailyDoseStr_ave(id, map) == 'Codeine Oral Solution 118 mL dispense * 2 mg/ml / 30d supply (**assumed**) = 7.87 mg.'
		dailyDoseStr_max(id, map) == 'Codeine Oral Solution 10 ml (daily max per sig) * 2 mg/ml = 20 mg.'
		round(mmePerDay_ave(id, map)) == 1.18
		mmePerDay_aveCalculatedUsingDefaultDaysSupplyEstimate(id, map) == true
		round(mmePerDay_max(id, map)) == 3.0

		where:
		testId  | medorder 																| testDescription
		'024'  	| 'Guaifenesin-Codeine 100-10 MG_5ML Syrup__STRUCT_1_max_10_mL.json' 	| 'liquid, max per day in sig'
	}

	@Ignore
	def "025"() {
		println "Running test: " + testId
		MedicationOrder medOrder = (MedicationOrder) context.newJsonParser().parseResource(MedicationOrder.class, new File(baseLocation, medorder).text)
		Medication med = context.newJsonParser().parseResource(Medication.class, new File(baseLocation, "Meds\\" + getMedFileName(medOrder.getMedicationReference().getReference())).text)
		Map<String, Map<String, Object>> map = new HashMap<String, Map<String, Object>>()
		if (Utils.isMedicationOrderMissingDoseQuantity(medOrder)) { Utils.updateMedOrderDosageInstructionWithSigInfo(medOrder, false); }

		when:
		MedOrderUtil.setMedOrderAttributes(medOrder, med, map, rxNavClient, 'OME', 30)
		String id = medOrder.getId()
		printAttributes(id, map)

		then:
		rxSig_maxDailyFrequency("sig", id, map) == 2

		conversionFactor_ave(id, map) == 0.15
		conversionFactor_max(id, map) == 0.15
		singleDoseMmeDescription(id, map) == 'For 1 ml, OME = 0.3 mg.'
		dispenseQuantity_value(id, map) == 118.0
		dispenseQuantity_unit(id, map) == 'mL'
		dailyDoseStr_ave(id, map) == 'Codeine Oral Solution 118 mL dispense * 2 mg/ml / 30d supply (**assumed**) = 7.87 mg.'
		dailyDoseStr_max(id, map) == 'Codeine Oral Solution 5 ml * max 2/d * 2 mg/ml = 20 mg.'
		round(mmePerDay_ave(id, map)) == 1.18
		mmePerDay_aveCalculatedUsingDefaultDaysSupplyEstimate(id, map) == true
		round(mmePerDay_max(id, map)) == 3.0

		where:
		testId  | medorder 																| testDescription
		'025'  	| 'Guaifenesin-Codeine 100-10 MG_5ML Syrup__STRUCT_1_max_2x_day.json' 	| 'liquid, max times per day in sig'
	}

	@Ignore
	def "026"() {
		println "Running test: " + testId
		MedicationOrder medOrder = (MedicationOrder) context.newJsonParser().parseResource(MedicationOrder.class, new File(baseLocation, medorder).text)
		Medication med = context.newJsonParser().parseResource(Medication.class, new File(baseLocation, "Meds\\" + getMedFileName(medOrder.getMedicationReference().getReference())).text)
		Map<String, Map<String, Object>> map = new HashMap<String, Map<String, Object>>()
		if (Utils.isMedicationOrderMissingDoseQuantity(medOrder)) { Utils.updateMedOrderDosageInstructionWithSigInfo(medOrder, false); }

		when:
		MedOrderUtil.setMedOrderAttributes(medOrder, med, map, rxNavClient, 'OME', 30)
		String id = medOrder.getId()
		printAttributes(id, map)

		then:
		rxSig_originalSig("sig", id, map) == 'Take 5 mLs by mouth 3 times daily as needed. Average 3 times per day.'
		rxSig_averageDailyFrequency("sig", id, map) == 3
		rxSig_maxDailyFrequency("sig", id, map) == null

		conversionFactor_ave(id, map) == 0.15
		conversionFactor_max(id, map) == 0.15
		singleDoseMmeDescription(id, map) == 'For 1 ml, OME = 0.3 mg.'

		dispenseQuantity_value(id, map) == 118.0
		dispenseQuantity_unit(id, map) == 'mL'
		dailyDoseStr_ave(id, map) == 'Codeine Oral Solution 5 ml * ave 3/d * 2 mg/ml = 30 mg.'
		dailyDoseStr_max(id, map) == 'Codeine Oral Solution 5 ml * max 3/d * 2 mg/ml = 30 mg.'
		round(mmePerDay_ave(id, map)) == 4.5
		mmePerDay_aveCalculatedUsingDefaultDaysSupplyEstimate(id, map) == false
		round(mmePerDay_max(id, map)) == 4.5

		where:
		testId  | medorder 																| testDescription
		'026'  	| 'Guaifenesin-Codeine 100-10 MG_5ML Syrup__STRUCT_1_ave_3x_day.json' 	| 'liquid, max times per day in sig'
	}

	@Ignore
	def "027"() {
		println "Running test: " + testId
		MedicationOrder medOrder = (MedicationOrder) context.newJsonParser().parseResource(MedicationOrder.class, new File(baseLocation, medorder).text)
		Medication med = context.newJsonParser().parseResource(Medication.class, new File(baseLocation, "Meds\\" + getMedFileName(medOrder.getMedicationReference().getReference())).text)
		Map<String, Map<String, Object>> map = new HashMap<String, Map<String, Object>>()
		if (Utils.isMedicationOrderMissingDoseQuantity(medOrder)) { Utils.updateMedOrderDosageInstructionWithSigInfo(medOrder, false); }

		when:
		MedOrderUtil.setMedOrderAttributes(medOrder, med, map, rxNavClient, 'OME', 30)
		String id = medOrder.getId()
		printAttributes(id, map)

		then:
		conversionFactor_ave(id, map) == 7.0
		conversionFactor_max(id, map) == 7.0
		singleDoseMmeDescription(id, map) == 'For 1 ml, OME = 70 mg.'
		dispenseQuantity_value(id, map) == 2.5
		dispenseQuantity_unit(id, map) == 'mL'
		dailyDoseStr_ave(id, map) == 'Unable to calculate as ml per dose not available.'
		dailyDoseStr_max(id, map) == 'Unable to calculate as ml per dose not available.'
		round(mmePerDay_ave(id, map)) == null
		mmePerDay_aveCalculatedUsingDefaultDaysSupplyEstimate(id, map) == false
		round(mmePerDay_max(id, map)) == null

		where:
		testId  | medorder 														| testDescription
		'027'  	| 'Butorphanol Tartrate 10 MG_ML nasal spray__STRUCT_1.json' 	| 'liquid spray, cannot compute max'
	}

	@Ignore
	def "028"() {
		println "Running test: " + testId
		MedicationOrder medOrder = (MedicationOrder) context.newJsonParser().parseResource(MedicationOrder.class, new File(baseLocation, medorder).text)
		Medication med = context.newJsonParser().parseResource(Medication.class, new File(baseLocation, "Meds\\" + getMedFileName(medOrder.getMedicationReference().getReference())).text)
		Map<String, Map<String, Object>> map = new HashMap<String, Map<String, Object>>()
		if (Utils.isMedicationOrderMissingDoseQuantity(medOrder)) { Utils.updateMedOrderDosageInstructionWithSigInfo(medOrder, false); }

		when:
		MedOrderUtil.setMedOrderAttributes(medOrder, med, map, rxNavClient, 'OME', 30)
		String id = medOrder.getId()
		printAttributes(id, map)

		then:
		rxSig_maxDailyDose("sig", id, map) == 4.0
		conversionFactor_ave(id, map) == 1.5
		conversionFactor_max(id, map) == 1.5
		singleDoseMmeDescription(id, map) == 'For 1 tablet, OME = 15 mg.'
		dispenseQuantity_value(id, map) == 42.0
		dispenseQuantity_unit(id, map) == 'tablet'
		dailyDoseStr_ave(id, map) == 'Oxycodone Oral Tablet 42 dispense * 10 mg / 30d supply (**assumed**) = 14 mg.'
		dailyDoseStr_max(id, map) == 'Oxycodone Oral Tablet 4 (daily max per sig) * 10 mg = 40 mg.'
		round(mmePerDay_ave(id, map)) == 21.0
		mmePerDay_aveCalculatedUsingDefaultDaysSupplyEstimate(id, map) == true
		round(mmePerDay_max(id, map)) == 60.0

		where:
		testId  | medorder 												| testDescription
		'028'  	| 'oxyCODONE 10 mg tablet__STRUCT_1_max_4_day.json' 	| 'max dose per day in sig'
	}

	@Ignore
	def "029"() {
		println "Running test: " + testId
		MedicationOrder medOrder = (MedicationOrder) context.newJsonParser().parseResource(MedicationOrder.class, new File(baseLocation, medorder).text)
		Medication med = context.newJsonParser().parseResource(Medication.class, new File(baseLocation, "Meds\\" + getMedFileName(medOrder.getMedicationReference().getReference())).text)
		Map<String, Map<String, Object>> map = new HashMap<String, Map<String, Object>>()
		if (Utils.isMedicationOrderMissingDoseQuantity(medOrder)) { Utils.updateMedOrderDosageInstructionWithSigInfo(medOrder, false); }

		when:
		MedOrderUtil.setMedOrderAttributes(medOrder, med, map, rxNavClient, 'OME', 30)
		String id = medOrder.getId()
		printAttributes(id, map)

		then:
		rxSig_originalSig("sig", id, map) == 'Take 0.5-1 tablets (5-10 mg) by mouth every 4 hours as needed.'
		conversionFactor_ave(id, map) == 1.5
		conversionFactor_max(id, map) == 1.5
		singleDoseMmeDescription(id, map) == 'For 1 tablet, OME = 15 mg.'
		dailyDoseStr_ave(id, map) == 'Oxycodone Oral Tablet 42 dispense * 10 mg / 30d supply (**assumed**) = 14 mg.'
		dailyDoseStr_max(id, map) == 'Oxycodone Oral Tablet max 1 tablet * max 6/d * 10 mg = 60 mg.'
		round(mmePerDay_ave(id, map)) == 21.0
		mmePerDay_aveCalculatedUsingDefaultDaysSupplyEstimate(id, map) == true
		round(mmePerDay_max(id, map)) == 90.0

		where:
		testId  | medorder 									| testDescription
		'029'  	| 'oxyCODONE 10 mg tablet__STRUCT_1.json' 	| 'dose per day from freq'
	}

	@Ignore
	def "030"() {
		println "Running test: " + testId
		MedicationOrder medOrder = (MedicationOrder) context.newJsonParser().parseResource(MedicationOrder.class, new File(baseLocation, medorder).text)
		Medication med = context.newJsonParser().parseResource(Medication.class, new File(baseLocation, "Meds\\" + getMedFileName(medOrder.getMedicationReference().getReference())).text)
		Map<String, Map<String, Object>> map = new HashMap<String, Map<String, Object>>()
		if (Utils.isMedicationOrderMissingDoseQuantity(medOrder)) { Utils.updateMedOrderDosageInstructionWithSigInfo(medOrder, false); }

		when:
		MedOrderUtil.setMedOrderAttributes(medOrder, med, map, rxNavClient, 'OME', 30)
		String id = medOrder.getId()
		printAttributes(id, map)

		then:
		rxSig_originalSig("sig", id, map) == 'Take 0.5-1 tablets (5-10 mg) by mouth every 4 hours as needed.  Average of 3 per day.'
		rxSig_averageDailyDose("sig", id, map) == 3.0
		conversionFactor_ave(id, map) == 1.5
		conversionFactor_max(id, map) == 1.5
		singleDoseMmeDescription(id, map) == 'For 1 tablet, OME = 15 mg.'
		dispenseQuantity_value(id, map) == 42.0
		dispenseQuantity_unit(id, map) == 'tablet'
		dailyDoseStr_ave(id, map) == 'Oxycodone Oral Tablet 3 (daily ave per sig) * 10 mg = 30 mg.'
		dailyDoseStr_max(id, map) == 'Oxycodone Oral Tablet max 1 tablet * max 6/d * 10 mg = 60 mg.'
		round(mmePerDay_ave(id, map)) == 45.0
		mmePerDay_aveCalculatedUsingDefaultDaysSupplyEstimate(id, map) == false
		round(mmePerDay_max(id, map)) == 90.0

		where:
		testId  | medorder 												| testDescription
		'030'  	| 'oxyCODONE 10 mg tablet__STRUCT_1_ave_3_day.json' 	| 'ave freq per day in sig'
	}


	private Boolean isDraft(String medOrderId, Map<String, Map<String, Object>> medOrderIdToNameValuePairMap) {
		return (Boolean) Utils.getAttributeValue(medOrderId, "isDraft", medOrderIdToNameValuePairMap)
	}
	private String medName(String medOrderId, Map<String, Map<String, Object>> medOrderIdToNameValuePairMap) {
		return (String) Utils.getAttributeValue(medOrderId, "medName", medOrderIdToNameValuePairMap)
	}
	private RxSig rxSig(String sigOrNote, String medOrderId, Map<String, Map<String, Object>> medOrderIdToNameValuePairMap) {
		return (RxSig) Utils.getAttributeValue(medOrderId, "rxSig_" + sigOrNote, medOrderIdToNameValuePairMap)
	}
	private String rxSig_originalSig(String sigOrNote, String medOrderId, Map<String, Map<String, Object>> medOrderIdToNameValuePairMap) {
		RxSig rxSig = (RxSig) Utils.getAttributeValue(medOrderId, "rxSig_" + sigOrNote, medOrderIdToNameValuePairMap)
		return rxSig?.getOriginalSig()
	}
	private String rxSig_command(String sigOrNote, String medOrderId, Map<String, Map<String, Object>> medOrderIdToNameValuePairMap) {
		RxSig rxSig = (RxSig) Utils.getAttributeValue(medOrderId, "rxSig_" + sigOrNote, medOrderIdToNameValuePairMap)
		return rxSig?.getCommand()
	}
	private boolean rxSig_useWithCaution(String sigOrNote, String medOrderId, Map<String, Map<String, Object>> medOrderIdToNameValuePairMap) {
		RxSig rxSig = (RxSig) Utils.getAttributeValue(medOrderId, "rxSig_" + sigOrNote, medOrderIdToNameValuePairMap)
		return rxSig?.useWithCaution()
	}
	private Double rxSig_doseLow(String sigOrNote, String medOrderId, Map<String, Map<String, Object>> medOrderIdToNameValuePairMap) {
		RxSig rxSig = (RxSig) Utils.getAttributeValue(medOrderId, "rxSig_" + sigOrNote, medOrderIdToNameValuePairMap)
		return rxSig?.getDoseLow()
	}
	private Double rxSig_doseHigh(String sigOrNote, String medOrderId, Map<String, Map<String, Object>> medOrderIdToNameValuePairMap) {
		RxSig rxSig = (RxSig) Utils.getAttributeValue(medOrderId, "rxSig_" + sigOrNote, medOrderIdToNameValuePairMap)
		return rxSig?.getDoseHigh()
	}
	private String rxSig_doseUnits(String sigOrNote, String medOrderId, Map<String, Map<String, Object>> medOrderIdToNameValuePairMap) {
		RxSig rxSig = (RxSig) Utils.getAttributeValue(medOrderId, "rxSig_" + sigOrNote, medOrderIdToNameValuePairMap)
		return rxSig?.getDoseUnits()
	}
	private Double rxSig_alternateDoseLow(String sigOrNote, String medOrderId, Map<String, Map<String, Object>> medOrderIdToNameValuePairMap) {
		RxSig rxSig = (RxSig) Utils.getAttributeValue(medOrderId, "rxSig_" + sigOrNote, medOrderIdToNameValuePairMap)
		return rxSig?.getAlternateDoseLow()
	}
	private Double rxSig_alternateDoseHigh(String sigOrNote, String medOrderId, Map<String, Map<String, Object>> medOrderIdToNameValuePairMap) {
		RxSig rxSig = (RxSig) Utils.getAttributeValue(medOrderId, "rxSig_" + sigOrNote, medOrderIdToNameValuePairMap)
		return rxSig?.getAlternateDoseHigh()
	}
	private String rxSig_alternateDoseUnits(String sigOrNote, String medOrderId, Map<String, Map<String, Object>> medOrderIdToNameValuePairMap) {
		RxSig rxSig = (RxSig) Utils.getAttributeValue(medOrderId, "rxSig_" + sigOrNote, medOrderIdToNameValuePairMap)
		return rxSig?.getAlternateDoseUnits()
	}
	private String rxSig_route(String sigOrNote, String medOrderId, Map<String, Map<String, Object>> medOrderIdToNameValuePairMap) {
		RxSig rxSig = (RxSig) Utils.getAttributeValue(medOrderId, "rxSig_" + sigOrNote, medOrderIdToNameValuePairMap)
		return rxSig?.getRoute()
	}
	private Double rxSig_intervalLow(String sigOrNote, String medOrderId, Map<String, Map<String, Object>> medOrderIdToNameValuePairMap) {
		RxSig rxSig = (RxSig) Utils.getAttributeValue(medOrderId, "rxSig_" + sigOrNote, medOrderIdToNameValuePairMap)
		return rxSig?.getIntervalLow()
	}
	private Double rxSig_intervalHigh(String sigOrNote, String medOrderId, Map<String, Map<String, Object>> medOrderIdToNameValuePairMap) {
		RxSig rxSig = (RxSig) Utils.getAttributeValue(medOrderId, "rxSig_" + sigOrNote, medOrderIdToNameValuePairMap)
		return rxSig?.getIntervalHigh()
	}
	private String rxSig_intervalTimeUnits(String sigOrNote, String medOrderId, Map<String, Map<String, Object>> medOrderIdToNameValuePairMap) {
		RxSig rxSig = (RxSig) Utils.getAttributeValue(medOrderId, "rxSig_" + sigOrNote, medOrderIdToNameValuePairMap)
		return rxSig?.getIntervalTimeUnits()
	}
	private Integer rxSig_frequencyLow(String sigOrNote, String medOrderId, Map<String, Map<String, Object>> medOrderIdToNameValuePairMap) {
		RxSig rxSig = (RxSig) Utils.getAttributeValue(medOrderId, "rxSig_" + sigOrNote, medOrderIdToNameValuePairMap)
		return rxSig?.getFrequencyLow()
	}
	private Integer rxSig_frequencyHigh(String sigOrNote, String medOrderId, Map<String, Map<String, Object>> medOrderIdToNameValuePairMap) {
		RxSig rxSig = (RxSig) Utils.getAttributeValue(medOrderId, "rxSig_" + sigOrNote, medOrderIdToNameValuePairMap)
		return rxSig?.getFrequencyHigh()
	}
	private String rxSig_frequencyTimeUnits(String sigOrNote, String medOrderId, Map<String, Map<String, Object>> medOrderIdToNameValuePairMap) {
		RxSig rxSig = (RxSig) Utils.getAttributeValue(medOrderId, "rxSig_" + sigOrNote, medOrderIdToNameValuePairMap)
		return rxSig?.getFrequencyTimeUnits()
	}
	private Double rxSig_averageDailyDose(String sigOrNote, String medOrderId, Map<String, Map<String, Object>> medOrderIdToNameValuePairMap) {
		RxSig rxSig = (RxSig) Utils.getAttributeValue(medOrderId, "rxSig_" + sigOrNote, medOrderIdToNameValuePairMap)
		return rxSig?.getAverageDailyDose()
	}
	private String rxSig_averageDailyDoseUnits(String sigOrNote, String medOrderId, Map<String, Map<String, Object>> medOrderIdToNameValuePairMap) {
		RxSig rxSig = (RxSig) Utils.getAttributeValue(medOrderId, "rxSig_" + sigOrNote, medOrderIdToNameValuePairMap)
		return rxSig?.getAverageDailyDoseUnits()
	}
	private Double rxSig_maxDailyDose(String sigOrNote, String medOrderId, Map<String, Map<String, Object>> medOrderIdToNameValuePairMap) {
		RxSig rxSig = (RxSig) Utils.getAttributeValue(medOrderId, "rxSig_" + sigOrNote, medOrderIdToNameValuePairMap)
		return rxSig?.getMaxDailyDose()
	}
	private String rxSig_maxDailyDoseUnits(String sigOrNote, String medOrderId, Map<String, Map<String, Object>> medOrderIdToNameValuePairMap) {
		RxSig rxSig = (RxSig) Utils.getAttributeValue(medOrderId, "rxSig_" + sigOrNote, medOrderIdToNameValuePairMap)
		return rxSig?.getMaxDailyDoseUnits()
	}
	private Integer rxSig_averageDailyFrequency(String sigOrNote, String medOrderId, Map<String, Map<String, Object>> medOrderIdToNameValuePairMap) {
		RxSig rxSig = (RxSig) Utils.getAttributeValue(medOrderId, "rxSig_" + sigOrNote, medOrderIdToNameValuePairMap)
		return rxSig?.getAverageDailyFrequency()
	}
	private Integer rxSig_maxDailyFrequency(String sigOrNote, String medOrderId, Map<String, Map<String, Object>> medOrderIdToNameValuePairMap) {
		RxSig rxSig = (RxSig) Utils.getAttributeValue(medOrderId, "rxSig_" + sigOrNote, medOrderIdToNameValuePairMap)
		return rxSig?.getMaxDailyFrequency()
	}
	private Integer rxSig_duration(String sigOrNote, String medOrderId, Map<String, Map<String, Object>> medOrderIdToNameValuePairMap) {
		RxSig rxSig = (RxSig) Utils.getAttributeValue(medOrderId, "rxSig_" + sigOrNote, medOrderIdToNameValuePairMap)
		return rxSig?.getDuration()
	}
	private String rxSig_durationTimeUnits(String sigOrNote, String medOrderId, Map<String, Map<String, Object>> medOrderIdToNameValuePairMap) {
		RxSig rxSig = (RxSig) Utils.getAttributeValue(medOrderId, "rxSig_" + sigOrNote, medOrderIdToNameValuePairMap)
		return rxSig?.getDurationTimeUnits()
	}
	private String rxSig_earliestFillDate(String sigOrNote, String medOrderId, Map<String, Map<String, Object>> medOrderIdToNameValuePairMap) {
		RxSig rxSig = (RxSig) Utils.getAttributeValue(medOrderId, "rxSig_" + sigOrNote, medOrderIdToNameValuePairMap)
		return getDateStr(rxSig?.getEarliestFillDate())
	}
	private String rxSig_startDate(String sigOrNote, String medOrderId, Map<String, Map<String, Object>> medOrderIdToNameValuePairMap) {
		RxSig rxSig = (RxSig) Utils.getAttributeValue(medOrderId, "rxSig_" + sigOrNote, medOrderIdToNameValuePairMap)
		return getDateStr(rxSig?.getStartDate())
	}
	private String rxSig_endDate(String sigOrNote, String medOrderId, Map<String, Map<String, Object>> medOrderIdToNameValuePairMap) {
		RxSig rxSig = (RxSig) Utils.getAttributeValue(medOrderId, "rxSig_" + sigOrNote, medOrderIdToNameValuePairMap)
		return getDateStr(rxSig?.getEndDate())
	}
	private Double conversionFactor_ave(String medOrderId, Map<String, Map<String, Object>> medOrderIdToNameValuePairMap) {
		return (Double) Utils.getAttributeValue(medOrderId, "conversionFactor_ave", medOrderIdToNameValuePairMap)
	}
	private Double conversionFactor_max(String medOrderId, Map<String, Map<String, Object>> medOrderIdToNameValuePairMap) {
		return (Double) Utils.getAttributeValue(medOrderId, "conversionFactor_max", medOrderIdToNameValuePairMap)
	}
	private String singleDoseMmeDescription(String medOrderId, Map<String, Map<String, Object>> medOrderIdToNameValuePairMap) {
		return (String) Utils.getAttributeValue(medOrderId, "singleDoseMmeDescription", medOrderIdToNameValuePairMap)
	}
	private String prescriber(String medOrderId, Map<String, Map<String, Object>> medOrderIdToNameValuePairMap) {
		return (String) Utils.getAttributeValue(medOrderId, "prescriber", medOrderIdToNameValuePairMap)
	}
	private String rxDate(String medOrderId, Map<String, Map<String, Object>> medOrderIdToNameValuePairMap) {
		Date rxDateObj = (Date) Utils.getAttributeValue(medOrderId, "rxDate", medOrderIdToNameValuePairMap)
		return getDateStr(rxDateObj)
	}
	private BigDecimal dispenseQuantity_value(String medOrderId, Map<String, Map<String, Object>> medOrderIdToNameValuePairMap) {
		SimpleQuantity dispenseQuantity = (SimpleQuantity) Utils.getAttributeValue(medOrderId, "dispenseQuantity", medOrderIdToNameValuePairMap)
		return dispenseQuantity?.getValue()
	}
	private String dispenseQuantity_unit(String medOrderId, Map<String, Map<String, Object>> medOrderIdToNameValuePairMap) {
		SimpleQuantity dispenseQuantity = (SimpleQuantity) Utils.getAttributeValue(medOrderId, "dispenseQuantity", medOrderIdToNameValuePairMap)
		return dispenseQuantity?.getUnit()
	}
	private Integer numRepeatsAllowed(String medOrderId, Map<String, Map<String, Object>> medOrderIdToNameValuePairMap) {
		return (Integer) Utils.getAttributeValue(medOrderId, "numRepeatsAllowed", medOrderIdToNameValuePairMap)
	}
	private String startDate(String medOrderId, Map<String, Map<String, Object>> medOrderIdToNameValuePairMap) {
		Date startDateObj = (Date) Utils.getAttributeValue(medOrderId, "startDate", medOrderIdToNameValuePairMap)
		return getDateStr(startDateObj)
	}
	private String endDate(String medOrderId, Map<String, Map<String, Object>> medOrderIdToNameValuePairMap) {
		Date endDateObj = (Date) Utils.getAttributeValue(medOrderId, "endDate", medOrderIdToNameValuePairMap)
		return getDateStr(endDateObj)
	}
	private String endDateCalcMethod(String medOrderId, Map<String, Map<String, Object>> medOrderIdToNameValuePairMap) {
		return (String) Utils.getAttributeValue(medOrderId, "endDateCalcMethod", medOrderIdToNameValuePairMap)
	}
	private String endDatePreferDefault(String medOrderId, Map<String, Map<String, Object>> medOrderIdToNameValuePairMap) {
		Date endDatePreferDefaultObj = (Date) Utils.getAttributeValue(medOrderId, "endDatePreferDefault", medOrderIdToNameValuePairMap)
		return getDateStr(endDatePreferDefaultObj)
	}
	private Boolean isEndDateEstimated(String medOrderId, Map<String, Map<String, Object>> medOrderIdToNameValuePairMap) {
		return (Boolean) Utils.getAttributeValue(medOrderId, "isEndDateEstimated", medOrderIdToNameValuePairMap)
	}
	private BigDecimal expectedSupplyDuration_value(String medOrderId, Map<String, Map<String, Object>> medOrderIdToNameValuePairMap) {
		Duration expectedSupplyDuration = (Duration) Utils.getAttributeValue(medOrderId, "expectedSupplyDuration", medOrderIdToNameValuePairMap)
		return expectedSupplyDuration?.getValue()
	}
	private String expectedSupplyDuration_unit(String medOrderId, Map<String, Map<String, Object>> medOrderIdToNameValuePairMap) {
		Duration expectedSupplyDuration = (Duration) Utils.getAttributeValue(medOrderId, "expectedSupplyDuration", medOrderIdToNameValuePairMap)
		return expectedSupplyDuration?.getUnit()
	}
	private String dailyDoseStr_ave(String medOrderId, Map<String, Map<String, Object>> medOrderIdToNameValuePairMap) {
		return (String) Utils.getAttributeValue(medOrderId, "dailyDoseStr_ave", medOrderIdToNameValuePairMap)
	}
	private String dailyDoseStr_max(String medOrderId, Map<String, Map<String, Object>> medOrderIdToNameValuePairMap) {
		return (String) Utils.getAttributeValue(medOrderId, "dailyDoseStr_max", medOrderIdToNameValuePairMap)
	}
	private Double mmePerDay_ave(String medOrderId, Map<String, Map<String, Object>> medOrderIdToNameValuePairMap) {
		return (Double) Utils.getAttributeValue(medOrderId, "mmePerDay_ave", medOrderIdToNameValuePairMap)
	}
	private Boolean mmePerDay_aveCalculatedUsingDefaultDaysSupplyEstimate(String medOrderId, Map<String, Map<String, Object>> medOrderIdToNameValuePairMap) {
		return (Boolean) Utils.getAttributeValue(medOrderId, "mmePerDay_aveCalculatedUsingDefaultDaysSupplyEstimate", medOrderIdToNameValuePairMap)
	}
	private Double mmePerDay_max(String medOrderId, Map<String, Map<String, Object>> medOrderIdToNameValuePairMap) {
		return (Double) Utils.getAttributeValue(medOrderId, "mmePerDay_max", medOrderIdToNameValuePairMap)
	}
	private Boolean isPrn(String medOrderId, Map<String, Map<String, Object>> medOrderIdToNameValuePairMap) {
		return (Boolean) Utils.getAttributeValue(medOrderId, "isPrn", medOrderIdToNameValuePairMap)
	}
	private Boolean isOralBuprenorphine(String medOrderId, Map<String, Map<String, Object>> medOrderIdToNameValuePairMap) {
		return (Boolean) Utils.getAttributeValue(medOrderId, "isOralBuprenorphine", medOrderIdToNameValuePairMap)
	}

	private void printAttributes(String medOrderId, Map<String, Map<String, Object>> medOrderIdToNameValuePairMap) {
		Boolean isDraft = (Boolean) Utils.getAttributeValue(medOrderId, "isDraft", medOrderIdToNameValuePairMap)
		String medName = (String) Utils.getAttributeValue(medOrderId, "medName", medOrderIdToNameValuePairMap)
		RxSig rxSig_sig = (RxSig) Utils.getAttributeValue(medOrderId, "rxSig_sig", medOrderIdToNameValuePairMap)
		RxSig rxSig_note = (RxSig) Utils.getAttributeValue(medOrderId, "rxSig_note", medOrderIdToNameValuePairMap)
		Double conversionFactor_ave = (Double) Utils.getAttributeValue(medOrderId, "conversionFactor_ave", medOrderIdToNameValuePairMap)
		Double conversionFactor_max = (Double) Utils.getAttributeValue(medOrderId, "conversionFactor_max", medOrderIdToNameValuePairMap)
		String dailyDoseStr_ave = (String) Utils.getAttributeValue(medOrderId, "dailyDoseStr_ave", medOrderIdToNameValuePairMap)
		String dailyDoseStr_max = (String) Utils.getAttributeValue(medOrderId, "dailyDoseStr_max", medOrderIdToNameValuePairMap)
		SimpleQuantity dispenseQuantity = (SimpleQuantity) Utils.getAttributeValue(medOrderId, "dispenseQuantity", medOrderIdToNameValuePairMap)
		RxNormSemanticDrug drug = (RxNormSemanticDrug) Utils.getAttributeValue(medOrderId, "drug", medOrderIdToNameValuePairMap)
		String prescriber = (String) Utils.getAttributeValue(medOrderId, "prescriber", medOrderIdToNameValuePairMap)
		Date rxDate = (Date) Utils.getAttributeValue(medOrderId, "rxDate", medOrderIdToNameValuePairMap)
		Duration expectedSupplyDuration = (Duration) Utils.getAttributeValue(medOrderId, "expectedSupplyDuration", medOrderIdToNameValuePairMap)

		Date startDate = (Date) Utils.getAttributeValue(medOrderId, "startDate", medOrderIdToNameValuePairMap)
		Date endDate = (Date) Utils.getAttributeValue(medOrderId, "endDate", medOrderIdToNameValuePairMap)
		String endDateCalcMethod = (String) Utils.getAttributeValue(medOrderId, "endDateCalcMethod", medOrderIdToNameValuePairMap)
		Boolean isEndDateEstimated = (Boolean) Utils.getAttributeValue(medOrderId, "isEndDateEstimated", medOrderIdToNameValuePairMap)
		Date endDatePreferDefault = (Date) Utils.getAttributeValue(medOrderId, "endDatePreferDefault", medOrderIdToNameValuePairMap)
		Boolean isOralBuprenorphine = (Boolean) Utils.getAttributeValue(medOrderId, "isOralBuprenorphine", medOrderIdToNameValuePairMap)
		Boolean isPrn = (Boolean) Utils.getAttributeValue(medOrderId, "isPrn", medOrderIdToNameValuePairMap)
		Double mmePerDay_ave = (Double) Utils.getAttributeValue(medOrderId, "mmePerDay_ave", medOrderIdToNameValuePairMap)
		Double mmePerDay_max = (Double) Utils.getAttributeValue(medOrderId, "mmePerDay_max", medOrderIdToNameValuePairMap)
		Boolean mmePerDay_aveCalculatedUsingDefaultDaysSupplyEstimate = (Boolean) Utils.getAttributeValue(medOrderId, "mmePerDay_aveCalculatedUsingDefaultDaysSupplyEstimate", medOrderIdToNameValuePairMap)
		Integer numRepeatsAllowed = (Integer) Utils.getAttributeValue(medOrderId, "numRepeatsAllowed", medOrderIdToNameValuePairMap)
		String singleDoseMmeDescription = (String) Utils.getAttributeValue(medOrderId, "singleDoseMmeDescription", medOrderIdToNameValuePairMap)

		DateUtility dateUtility = DateUtility.getInstance()

		println()
		println("isDraft(id, map) == " + isDraft)
		println("medName(id, map) == " + str(medName))
		println()

		String type = "\"sig\"";
		if (rxSig_sig != null)
		{
			println("rxSig_originalSig(" + type + ", id, map) == " + str(rxSig_sig.getOriginalSig()))
			println("rxSig_useWithCaution(" + type + ", id, map) == " + rxSig_sig.useWithCaution())
			println("rxSig_command(" + type + ", id, map) == " + str(rxSig_sig.getCommand()))
			println("rxSig_doseLow(" + type + ", id, map) == " + rxSig_sig.getDoseLow())
			println("rxSig_doseHigh(" + type + ", id, map) == " + rxSig_sig.getDoseHigh())
			println("rxSig_doseUnits(" + type + ", id, map) == " + str(rxSig_sig.getDoseUnits()))
			println("rxSig_alternateDoseLow(" + type + ", id, map) == " + rxSig_sig.getAlternateDoseLow())
			println("rxSig_alternateDoseHigh(" + type + ", id, map) == " + rxSig_sig.getAlternateDoseHigh())
			println("rxSig_alternateDoseUnits(" + type + ", id, map) == " + str(rxSig_sig.getAlternateDoseUnits()))
			println("rxSig_route(" + type + ", id, map) == " + str(rxSig_sig.getRoute()))
			println("rxSig_intervalLow(" + type + ", id, map) == " + rxSig_sig.getIntervalLow())
			println("rxSig_intervalHigh(" + type + ", id, map) == " + rxSig_sig.getIntervalHigh())
			println("rxSig_intervalUnits(" + type + ", id, map) == " + str(rxSig_sig.getIntervalTimeUnits()))
			println("rxSig_frequencyLow(" + type + ", id, map) == " + rxSig_sig.getFrequencyLow())
			println("rxSig_frequencyHigh(" + type + ", id, map) == " + rxSig_sig.getFrequencyHigh())
			println("rxSig_frequencyUnits(" + type + ", id, map) == " + str(rxSig_sig.getFrequencyTimeUnits()))
			println("rxSig_averageDailyDose(" + type + ", id, map) == " + rxSig_sig.getAverageDailyDose())
			println("rxSig_averageDailyDoseUnits(" + type + ", id, map) == " + str(rxSig_sig.getAverageDailyDoseUnits()))
			println("rxSig_maxDailyDose(" + type + ", id, map) == " + rxSig_sig.getMaxDailyDose())
			println("rxSig_maxDailyDoseUnits(" + type + ", id, map) == " + str(rxSig_sig.getMaxDailyDoseUnits()))
			println("rxSig_averageDailyFrequency(" + type + ", id, map) == " + rxSig_sig.getAverageDailyFrequency())
			println("rxSig_maxDailyFrequency(" + type + ", id, map) == " + rxSig_sig.getMaxDailyFrequency())
			println("rxSig_duration(" + type + ", id, map) == " + rxSig_sig.getDuration())
			println("rxSig_durationTimeUnits(" + type + ", id, map) == " + str(rxSig_sig.getDurationTimeUnits()))
			Date date =  rxSig_sig.getEarliestFillDate();
			if (date == null) { println("rxSig_earliestFillDate(" + type + ", id, map) == null"); }
			else{ println("rxSig_earliestFillDate(" + type + ", id, map) == '" + dateUtility.getDateAsString(date, "yyyy-MM-dd hh:mm") + "'"); }

			date =  rxSig_sig.getStartDate();
			if (date == null) { println("rxSig_startDate(" + type + ", id, map) == null"); }
			else{ println("rxSig_startDate(" + type + ", id, map) == '" + dateUtility.getDateAsString(date, "yyyy-MM-dd hh:mm") + "'"); }

			date =  rxSig_sig.getEndDate();
			if (date == null) { println("rxSig_endDate(" + type + ", id, map) == null"); }
			else{ println("rxSig_endDate(" + type + ", id, map) == '" + dateUtility.getDateAsString(date, "yyyy-MM-dd hh:mm") + "'"); }
		}
		else
		{
			println("rxSig(" + type + ", id, map) == null")
		}

		println()

		type = "\"note\"";
		if (rxSig_note != null)
		{
			println("rxSig_originalSig(" + type + ", id, map) == " + str(rxSig_note.getOriginalSig()))
			println("rxSig_useWithCaution(" + type + ", id, map) == " + rxSig_note.useWithCaution())
			println("rxSig_command(" + type + ", id, map) == " + str(rxSig_note.getCommand()))
			println("rxSig_doseLow(" + type + ", id, map) == " + rxSig_note.getDoseLow())
			println("rxSig_doseHigh(" + type + ", id, map) == " + rxSig_note.getDoseHigh())
			println("rxSig_doseUnits(" + type + ", id, map) == " + str(rxSig_note.getDoseUnits()))
			println("rxSig_alternateDoseLow(" + type + ", id, map) == " + rxSig_note.getAlternateDoseLow())
			println("rxSig_alternateDoseHigh(" + type + ", id, map) == " + rxSig_note.getAlternateDoseHigh())
			println("rxSig_alternateDoseUnits(" + type + ", id, map) == " + str(rxSig_note.getAlternateDoseUnits()))
			println("rxSig_route(" + type + ", id, map) == " + str(rxSig_note.getRoute()))
			println("rxSig_intervalLow(" + type + ", id, map) == " + rxSig_note.getIntervalLow())
			println("rxSig_intervalHigh(" + type + ", id, map) == " + rxSig_note.getIntervalHigh())
			println("rxSig_intervalUnits(" + type + ", id, map) == " + str(rxSig_note.getIntervalTimeUnits()))
			println("rxSig_frequencyLow(" + type + ", id, map) == " + rxSig_note.getFrequencyLow())
			println("rxSig_frequencyHigh(" + type + ", id, map) == " + rxSig_note.getFrequencyHigh())
			println("rxSig_frequencyUnits(" + type + ", id, map) == " + str(rxSig_note.getFrequencyTimeUnits()))
			println("rxSig_averageDailyDose(" + type + ", id, map) == " + rxSig_note.getAverageDailyDose())
			println("rxSig_averageDailyDoseUnits(" + type + ", id, map) == " + str(rxSig_note.getAverageDailyDoseUnits()))
			println("rxSig_maxDailyDose(" + type + ", id, map) == " + rxSig_note.getMaxDailyDose())
			println("rxSig_maxDailyDoseUnits(" + type + ", id, map) == " + str(rxSig_note.getMaxDailyDoseUnits()))
			println("rxSig_averageDailyFrequency(" + type + ", id, map) == " + rxSig_note.getAverageDailyFrequency())
			println("rxSig_maxDailyFrequency(" + type + ", id, map) == " + rxSig_note.getMaxDailyFrequency())
			println("rxSig_duration(" + type + ", id, map) == " + rxSig_note.getDuration())
			println("rxSig_durationTimeUnits(" + type + ", id, map) == " + str(rxSig_note.getDurationTimeUnits()))
			Date date =  rxSig_note.getEarliestFillDate();
			if (date == null) { println("rxSig_earliestFillDate(" + type + ", id, map) == null"); }
			else{ println("rxSig_earliestFillDate(" + type + ", id, map) == '" + dateUtility.getDateAsString(date, "yyyy-MM-dd hh:mm") + "'"); }

			date =  rxSig_note.getStartDate();
			if (date == null) { println("rxSig_startDate(" + type + ", id, map) == null"); }
			else{ println("rxSig_startDate(" + type + ", id, map) == '" + dateUtility.getDateAsString(date, "yyyy-MM-dd hh:mm") + "'"); }

			date =  rxSig_note.getEndDate();
			if (date == null) { println("rxSig_endDate(" + type + ", id, map) == null"); }
			else{ println("rxSig_endDate(" + type + ", id, map) == '" + dateUtility.getDateAsString(date, "yyyy-MM-dd hh:mm") + "'"); }
		}
		else
		{
			println("rxSig(" + type + ", id, map) == null")
		}

		println()

		println("conversionFactor_ave(id, map) == " + conversionFactor_ave)
		println("conversionFactor_max(id, map) == " + conversionFactor_max)
		println("singleDoseMmeDescription(id, map) == " + str(singleDoseMmeDescription))

		println()

		println("prescriber(id, map) == " + str(prescriber))

		Date date =  rxDate;
		if (date == null) { println("rxDate(id, map) == null"); }
		else{ println("rxDate(id, map) == '" + dateUtility.getDateAsString(date, "yyyy-MM-dd hh:mm") + "'"); }

		if (dispenseQuantity != null)
		{
			println("dispenseQuantity_value(id, map) == " + dispenseQuantity.getValue())
			println("dispenseQuantity_unit(id, map) == '" + dispenseQuantity.getUnit() + "'")
		}
		else
		{
			println("dispenseQuantity(id, map) == null")
		}
		println("numRepeatsAllowed(id, map) == " + numRepeatsAllowed)

		println()

		date =  startDate;
		if (date == null) { println("startDate(id, map) == null"); }
		else{ println("startDate(id, map) == '" + dateUtility.getDateAsString(date, "yyyy-MM-dd hh:mm") + "'"); }

		date =  endDate;
		if (date == null) { println("endDate(id, map) == null"); }
		else{ println("endDate(id, map) == '" + dateUtility.getDateAsString(date, "yyyy-MM-dd hh:mm") + "'"); }

		println("endDateCalcMethod(id, map) == " + str(endDateCalcMethod))
		println("endDatePreferDefault(id, map) == " + getDateStr(endDatePreferDefault))
		println("isEndDateEstimated(id, map) == " + isEndDateEstimated)

		if (expectedSupplyDuration != null)
		{
			println("expectedSupplyDuration_value(id, map) == " + expectedSupplyDuration.getValue())
			println("expectedSupplyDuration_unit(id, map) == " + str(expectedSupplyDuration.getUnit()))
		}
		else
		{
			println("expectedSupplyDuration(id, map) == null")
		}

		println()

		println("dailyDoseStr_ave(id, map) == " + str(dailyDoseStr_ave))
		println("dailyDoseStr_max(id, map) == " + str(dailyDoseStr_max))
		println("round(mmePerDay_ave(id, map)) == " + round(mmePerDay_ave))
		println("mmePerDay_aveCalculatedUsingDefaultDaysSupplyEstimate(id, map) == " + mmePerDay_aveCalculatedUsingDefaultDaysSupplyEstimate)
		println("round(mmePerDay_max(id, map)) == " + round(mmePerDay_max))

		println()

		println("isPrn(id, map) == " + isPrn)
		println("isOralBuprenorphine(id, map) == " + isOralBuprenorphine)
	}

	private static Double round (Double value) {
		if (value == null)
		{
			return null;
		}
		int precision = 2;
		int scale = (int) Math.pow(10, precision);
		return new Double((double) Math.round(value * scale) / scale);
	}

	private static String str(String str)
	{
		if (str == null)
		{
			return "null";
		}
		else
		{
			return "'" + str + "'";
		}
	}

	private static String getDateStr(Date date)
	{
		if (date == null)
		{
			return null
		}
		else
		{
			return DateUtility.getInstance().getDateAsString(date, "yyyy-MM-dd HH:mm")
		}
	}

	private static String getMedFileName(String medReference)
	{
		return medReference.substring(medReference.lastIndexOf('/') + 1) + ".json"
	}
}
