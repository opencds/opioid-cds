package org.opencds.opioid.display.mme

import org.opencds.opioid.display.Content
import org.opencds.opioid.mme.MmeEntry

import spock.lang.Ignore
import spock.lang.Specification

@Ignore
class MmeContentSpec extends Specification {

    Content content
    
    def setup() {
        content = new Content()
    }
    
    def "test setStyle"() {
        given:
        List<MmeEntry> entries = [
            new MmeEntry(),
            new MmeEntry()
            ]
            
        when:
        content.setStyle(entries, true)
        
        then:
        content.style == STYLE
        
    }
    
    private String STYLE = '''<style>
body {    font-family: segoe-ui, sans-serif;    background-color: #FEFFF1;}.medname {    font-weight: 600;}.total {    font-weight: 700;    {{totalStyle}}}table, p.cdc {    width: 670px;}table, td {    vertical-align: top;    padding: 2px;    background-color: #FEFFF1;}input {    display: none;    visibility: hidden;}.expand-text {    height: 0px;    margin-left: 20px;    margin-bottom: 6px;    transition: height 0.3s;}#expand0 { overflow: hidden; }#toggle0:checked ~ #expand0 { height: auto; }#toggle0:checked ~ label[for=toggle0] img { transform: rotate(180deg); }#expand1 { overflow: hidden; }#toggle1:checked ~ #expand1 { height: auto; }#toggle1:checked ~ label[for=toggle1] img { transform: rotate(180deg); }.image-new {    fill: green;    color: green;}.bg-yellow {  background-color: #FBCB00 !important;  margin-top: 6px;  margin-bottom: 6px;}p.warn {    margin-top: 6px;    margin-bottom: 6px;    width: auto;}
</style>
'''
}
