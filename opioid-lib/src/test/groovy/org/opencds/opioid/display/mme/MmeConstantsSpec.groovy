package org.opencds.opioid.display.mme

import org.opencds.opioid.CDCMessage
import org.opencds.opioid.display.Content
import org.opencds.opioid.display.MedRow

import spock.lang.Ignore
import spock.lang.Specification

@Ignore
class MmeConstantsSpec extends Specification {

    def 'getStylePrefix returns a beginning style tag'() {
        expect:
        MmeConstants.getStylePrefix() == '<style>\n'
    }

    def 'getStyleSuffix returns an end style tag'() {
        expect:
        MmeConstants.getStyleSuffix() == '\n</style>\n'
    }

    def 'getExpansionToggle returns an expansion toggle for id 1'() {
        expect:
        MmeConstants.getExpansionToggle(1) == EXPANSION_1;
        
        and:
        MmeConstants.getExpansionToggle(2) == EXPANSION_2;
    }

    def 'getStyleContent'() {
        expect:
        MmeConstants.getStyleContent(EXPANSION_1 + EXPANSION_2) == STYLE_CONTENT
    }
    
    def 'getTableRow'() {
        given:
        MedRow medRow = new MedRow()
        medRow.setNewContent(true)
        medRow.setMedName('----medName----')
        medRow.setWarnings(['----warnings----'])
        medRow.setId(1)
        medRow.setExpansionContent(['----expand----'])
        medRow.setMedQuantity('----medQuantity----')
        
        expect:
        MmeConstants.getTableRow(medRow) == TABLE_ROW
    }
    
    def 'getContent'() {
        given:
        Content content = new Content()
        content.setStyle([], true)
        content.setSummaryData('----stat----', 2, 50.0, '----longLabel----', '----shortLabel----')
        content.setCdcTextData(CDCMessage.AT_LEAST_ONE_NO_ABUSE, true, '----shortLabel----', 50.0)
        content.setCurrentValue('----currentValue----')
        content.setMainColumnName('----mainColumnName---')
        content.setMaxAvg('----maxAvg----')
        content.setShortLabel('----shortLabel----')
        content.setMedRows([])
        content.setTotal(99.99)
        content.setOpioidImage('----opioidImage----')
        content.setImageCacheBaseUrl('----imageCacheBaseUrl----')
        
        expect:
        MmeConstants.getContent(content) == CONTENT
    }

    def 'doubleFormat'() {
        expect:
        MmeConstants.doubleFormat(193.3333333333333) == '193.33'
    }
    
    String EXPANSION_1 = '#expand1 { overflow: hidden; }#toggle1:checked ~ #expand1 { height: auto; }#toggle1:checked ~ label[for=toggle1] img { transform: rotate(180deg); }'
    String EXPANSION_2 = '#expand2 { overflow: hidden; }#toggle2:checked ~ #expand2 { height: auto; }#toggle2:checked ~ label[for=toggle2] img { transform: rotate(180deg); }'
    String STYLE_CONTENT = 'body {    font-family: segoe-ui, sans-serif;    background-color: #FEFFF1;}.medname {    font-weight: 600;}.total {    font-weight: 700;    {{totalStyle}}}table, p.cdc {    width: 670px;}table, td {    vertical-align: top;    padding: 2px;    background-color: #FEFFF1;}input {    display: none;    visibility: hidden;}.expand-text {    height: 0px;    margin-left: 20px;    margin-bottom: 6px;    transition: height 0.3s;}#expand1 { overflow: hidden; }#toggle1:checked ~ #expand1 { height: auto; }#toggle1:checked ~ label[for=toggle1] img { transform: rotate(180deg); }#expand2 { overflow: hidden; }#toggle2:checked ~ #expand2 { height: auto; }#toggle2:checked ~ label[for=toggle2] img { transform: rotate(180deg); }.image-new {    fill: green;    color: green;}.bg-yellow {  background-color: #FBCB00 !important;  margin-top: 6px;  margin-bottom: 6px;}p.warn {    margin-top: 6px;    margin-bottom: 6px;    width: auto;}'
    String TABLE_ROW = '<tr> <td style="padding: 2px;" align="left"><span class="image-new" style="font-weight: bold"><img src="{{newImage}}" style="height: 12px; width: auto;" border="0"/> New</span> <b>----medName----</b><input id="toggle1"  type="checkbox" /><label for="toggle1"><img id="toggler1" src="{{expandImage}}" style="height: 15px; width: auto;" /></label>  <p class="warn"><img src="{{warningImage}}" style="height: 12px; width: auto;" border="0"/> ----warnings----</p> <div class="expand-text" id="expand1"><p class="warn">----expand----</p></div></td> <td style="padding: 2px; white-space: nowrap;" align="right"><b>----medQuantity----</b></td></tr>'
    String CONTENT = '''<style>
body {    font-family: segoe-ui, sans-serif;    background-color: #FEFFF1;}.medname {    font-weight: 600;}.total {    font-weight: 700;    }table, p.cdc {    width: 670px;}table, td {    vertical-align: top;    padding: 2px;    background-color: #FEFFF1;}input {    display: none;    visibility: hidden;}.expand-text {    height: 0px;    margin-left: 20px;    margin-bottom: 6px;    transition: height 0.3s;}.image-new {    fill: green;    color: green;}.bg-yellow {  background-color: #FBCB00 !important;  margin-top: 6px;  margin-bottom: 6px;}p.warn {    margin-top: 6px;    margin-bottom: 6px;    width: auto;}
</style>
<div class="bg-yellow" style="width: 670px;"><p>Patient's ----stat---- ----longLabel---- (----shortLabel----) is <b>at least  50.00</b> mg/day not counting 2 Rxs where ----shortLabel---- could not be calculated.</p><p></p></div><div style="width: 670px; text-align: center; font-weight: bold; margin-bottom: 15px;">Daily Average OME (mg/day)</div><img src="----imageCacheBaseUrl----/----opioidImage----" style="position: float; width: 670px;" /><p class="cdc">At least one draft medication order which is an opioid abused in ambulatory care to consider, but ----shortLabel----/day &lt; 50 (50.00 mg). No need for action.</p><div><table border="0" style="padding: 2px;"><tr>  <th style="padding: 2px; text-align: center;">----mainColumnName---</th>  <th style="padding: 2px; text-align: right;">----maxAvg----<br>----shortLabel----/day*</th></tr><tr>  <td style="padding: 2px;" align="left"><b>Total Average OME/Day</b></td>  <td style="padding: 2px;" align="right" nowrap><span class="total">99.99 mg</span></td></tr></table></div><div style="margin-top: 5px; margin-left: 10px; font-size: x-small; width: 670px;">*Avg OME = (qty dispensed)/(days supply). 30d supply assumed unless otherwised noted in Sig or note to pharmacy.<br>*Max OME (see details) = max amount patient may take on a given day according to Sig, even if patient runs out of med early.</div>'''
}
