package org.opencds.opioid.valuesets;

public enum UmlsValueSet {
    CONDITION_INDICATING_END_OF_LIFE(1),
	OPIATE_URINE_DRUG_TEST(2),
	OPIATE_THERAPY_RISK_ASSESSMENT(3),
	SUBSTANCE_ABUSE(4);
    
    private int id;

    UmlsValueSet(int id) {
        this.id = id;
    }
    
    public int getId() {
        return id;
    }
}
