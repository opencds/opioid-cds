package org.opencds.opioid.valuesets;

public enum RxNavValueSet {
    OPIOIDS(1),
    OPIOID_CODEINE_COUGH_MEDS(2),
    OPIOIDS_INDICATING_END_OF_LIFE(3),
    OPIOIDS_ABUSED_IN_AMBULATORY_CARE(4),
    BENZODIAZEPINES(5),
    LONG_ACTING_OPIOIDS(6),
    NALOXONE(7),
    SHORT_ACTING_OPIOIDS(8);
    
    private int id;

    RxNavValueSet(int id) {
        this.id = id;
    }
    
    public int getId() {
        return id;
    }
    
}
