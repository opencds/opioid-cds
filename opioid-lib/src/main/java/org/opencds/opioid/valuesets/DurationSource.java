package org.opencds.opioid.valuesets;

public enum DurationSource {
    SIG,
    NOTE,
    DEFAULT_ESTIMATE;
}
