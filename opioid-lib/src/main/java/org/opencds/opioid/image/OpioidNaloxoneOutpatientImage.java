package org.opencds.opioid.image;

import org.apache.batik.svggen.SVGGraphics2D;
import org.opencds.opioid.util.SvgUtil;

import java.awt.Color;
import java.awt.Font;
import java.awt.Polygon;

public class OpioidNaloxoneOutpatientImage {
    private static final double CONSIDER = 50.0;
    private static final String CONSIDER_STRING = Integer.toString(Double.valueOf(CONSIDER).intValue());
    private static Color RED = new Color(195, 2, 0);
    private static Color YELLOW = new Color(251, 203, 0);
    private static Color GREY = new Color(136, 136, 136);
    private static Color BLACK = Color.BLACK;

    private static Font FONT = new Font(Font.SANS_SERIF, Font.PLAIN, 12);

    private static int[] triangleXs = new int[] {1, 9, 5};
    private static int[] triangleYs = new int[] {1, 1, 6};

    private static int[] triangleXsInv = new int[] {1, 9, 5};
    private static int[] triangleYsInv = new int[] {6, 6, 1};

    private static int imageWidth = 670;
    private static int imageHeight = 100;

    // where the bar begins (and padding on left, right and on top)
    private static int xoffset = 30;
    private static int yoffset = 40;

    // width of the full bar
    private static int xlength = 610;
    // height of the full bar
    private static int ylength = 18;

    public static String createImage(double value) {
        SVGGraphics2D g2d = SvgUtil.newSvg(imageWidth, imageHeight);

        // fixed
        g2d.setColor(BLACK);
        g2d.setFont(FONT);
        g2d.drawString("0", xoffset-4, yoffset-13);
        Polygon zeroTri = new Polygon(triangleXs, triangleYs, 3);
        zeroTri.translate(xoffset-5, yoffset-8);
        g2d.fillPolygon(zeroTri);

        double red = value - CONSIDER;
        if (red < 0) {
            red = 0;
        }

        double divisor = red + CONSIDER;
        double greyWid = (double) (CONSIDER/divisor);
        double redWid = (double) (red/divisor);

        // grey
        int greyPos = 0;
        int greyWidth = (int) Math.round(greyWid*xlength);
        g2d.setColor(GREY);
        g2d.fillRect(greyPos+xoffset, yoffset, greyWidth, ylength);

        // red
        int redPos = greyPos+greyWidth;
        int redWidth = 0;
        if (redWid > 0.0) {
            redPos = greyWidth;
            redWidth = (int) Math.round(redWid*xlength);
            g2d.setColor(RED);
            g2d.fillRect(redPos+xoffset, yoffset, redWidth, ylength);
        }
        g2d.setColor(BLACK);
        g2d.setFont(FONT);
        int redXoffset = redPos+xoffset-57;
        if (redXoffset+115 > imageWidth) {
            // the text is 115 pixels wide
            redXoffset = imageWidth-115;
        } else if (redXoffset < 0) {
            redXoffset = 2;
        }
        g2d.drawString("(consider naloxone)", redXoffset, yoffset-28);
        g2d.drawString(CONSIDER_STRING, redPos+xoffset-12, yoffset-13);
        Polygon avoidTri = new Polygon(triangleXs, triangleYs, 3);
        avoidTri.translate(redPos+xoffset-5, yoffset-8);
        g2d.fillPolygon(avoidTri);

        // current
        int curPos = (int) Math.round(value/divisor*xlength);
        g2d.setColor(BLACK);
        g2d.setFont(FONT);
        g2d.drawString("(current)", curPos+xoffset-23, yoffset+55);
        double scale = 8f;
        String total = String.format("%3.2f", value);
        int valueLen = total.length();
        g2d.drawString(total, (int) (curPos+xoffset+1-(valueLen/2*scale)), yoffset+41);
        Polygon curTri = new Polygon(triangleXsInv, triangleYsInv, 3);
        curTri.translate(curPos+xoffset-5, yoffset+20);
        g2d.fillPolygon(curTri);

        String image = SvgUtil.getContent(g2d);
        g2d.dispose();
        return image;
    }

}
