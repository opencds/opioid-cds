package org.opencds.opioid.image;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Path;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class ImageWriter {
    private static final Log log = LogFactory.getLog(ImageWriter.class);

    public static void write(String image, Path output) {
        File outputfile = output.toFile();
        try (FileOutputStream fos = new FileOutputStream(outputfile)){
            fos.write(image.getBytes(), 0, image.length());
        } catch (IOException e) {
            log.warn(e.getMessage(), e);
        }
    }
}
