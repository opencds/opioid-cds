package org.opencds.opioid.eval;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hl7.fhir.dstu2.model.CodeableConcept;
import org.hl7.fhir.dstu2.model.Condition;
import org.hl7.fhir.dstu2.model.Condition.ConditionVerificationStatus;
import org.hl7.fhir.dstu2.model.Medication;
import org.hl7.fhir.dstu2.model.MedicationOrder;
import org.hl7.fhir.dstu2.model.MedicationOrder.MedicationOrderStatus;
import org.opencds.common.utilities.StringUtility;
import org.opencds.hooks.model.response.Indicator;
import org.opencds.hooks.model.response.Link;
import org.opencds.hooks.model.response.Source;
import org.opencds.opioid.config.OpioidConfig;
import org.opencds.opioid.mme.Mme;
import org.opencds.opioid.process.CommonRuleProcessor;
import org.opencds.opioid.process.model.MedOrderAttributes;
import org.opencds.opioid.process.model.MedOrdersLists;
import org.opencds.opioid.results.EvalResults;
import org.opencds.opioid.util.UrlUtil;
import org.opencds.opioid.util.fhir.MatchingMedOrders;
import org.opencds.opioid.util.term.UmlsUtil;
import org.opencds.opioid.valuesets.RxNavValueSet;
import org.opencds.opioid.valuesets.UmlsValueSet;
import org.opencds.tools.terminology.rxnav.client.RxNavTerminologyClient;
import org.opencds.tools.terminology.umls.api.client.UmlsTerminologyClient;

public class NaloxoneEvaluation {
    private static Log log = LogFactory.getLog(NaloxoneEvaluation.class);

    private final OpioidConfig config;
    private final RxNavTerminologyClient rxNavClient;
    private final UmlsTerminologyClient umlsClient;
    private final CommonRuleProcessor processor;

    public NaloxoneEvaluation(RxNavTerminologyClient rxNavClient, UmlsTerminologyClient umlsClient,
            OpioidConfig opioidConfig) {
        this.config = opioidConfig;
        this.rxNavClient = rxNavClient;
        this.umlsClient = umlsClient;
        processor = new CommonRuleProcessor(rxNavClient, umlsClient, opioidConfig);
    }

    /**
     * Returns a HashMap with the following keys and associated objects:
     *      returnCard  Boolean.  True if a CDS Hooks card should be returned (action needed), False otherwise.  If False, only summary returned for debug purposes.
     *      summary     String.  One-sentence, <140-character summary message for display to the user inside of this card.
     *      detail      String.  Optional detailed information to display, represented in Markdown.  Provides guidance + MME table.
     *      indicator   Indicator.  "info" if returnCard == true, null otherwise.
     *      source      Source.     Source of guidance (CDC opioid guidance).
     *      links       List<Link>. Links to further CDC info.
     */
    public EvalResults getNaloxoneEvaluationResults(java.util.Date evalTime, String focalPersonId, URI serverUri,
            List<MedicationOrder> medOrders, List<Medication> meds, List<Condition> conditions) {
    	log.info("Running getNaloxoneEvaluationResults");

        MedOrdersLists medOrdersLists = new MedOrdersLists();
        MedOrderAttributes medOrderAttributes = new MedOrderAttributes();

        Source source = new Source();
        source.setLabel("CDC opioid Rx guideline -- recommendation #8");
        source.setUrl(UrlUtil.newURL("https://www.cdc.gov/mmwr/volumes/65/rr/rr6501e1.htm#8_starting_periodically_during_continuation"));

        List<Link> links = new ArrayList<Link>();

        EvalResults results = processor.conductCommonProcessingAndReturnEvalResultsIfShouldExit(evalTime, focalPersonId,
                medOrders, meds, conditions, source, links, medOrdersLists, medOrderAttributes);

        if (results != null) {
            return results;
        } else {
        	// check criterion 1: average MME >= 50
        	boolean averageMmeGe50 = false;

            Mme mme = new Mme(config.isIncludeAverageMmePerDay(),
                    medOrdersLists.getActiveOrDraftAbusedInAmbCareToInclude(), medOrderAttributes, evalTime,
                    config.getMmeShortLabel());

            double totalMme_ave = mme.getTotalMmeAvg();

            if (totalMme_ave >= 50)
            {
            	averageMmeGe50 = true;
            }

            // check criterion 2: on benzodiazepine
            boolean onBenzodiazepine = false;

            List<MedicationOrderStatus> allowedMedicationOrderStatuses = new ArrayList<MedicationOrderStatus>();
            allowedMedicationOrderStatuses.add(MedicationOrderStatus.ACTIVE);
            allowedMedicationOrderStatuses.add(MedicationOrderStatus.DRAFT);
            List<MedicationOrder> activeOrDraftBenzodiazepineMedOrders = MatchingMedOrders.get(
                    medOrders, meds, allowedMedicationOrderStatuses, focalPersonId, rxNavClient,
                    RxNavValueSet.BENZODIAZEPINES, config);

            if (activeOrDraftBenzodiazepineMedOrders.size() >= 1)
            {
            	onBenzodiazepine = true;
            }

            // check criterion 3: history of substance abuse (not checking for any clinical status, as past ones are also relevant).
            boolean hasHxOfSubstanceAbuse = false;

            for (Condition condition : conditions) {

            	ConditionVerificationStatus verificationStatus = condition.getVerificationStatus();
            	if ((verificationStatus == null) || (verificationStatus.equals(ConditionVerificationStatus.NULL)) ||
            		(verificationStatus.equals(ConditionVerificationStatus.CONFIRMED)) || (verificationStatus.equals(ConditionVerificationStatus.PROVISIONAL))
            		|| (verificationStatus.equals(ConditionVerificationStatus.UNKNOWN)))
            	{
                //String status = condition.getClinicalStatus();
                //if ((status != null) && (status.equalsIgnoreCase("active"))) {
                    // TODO: consider also checking for type in the future,
                    // especially as the values that are used are made consistent
                    // across EHR vendors.
            		// }
                    CodeableConcept code = condition.getCode();
                    if (UmlsUtil.isConceptInUmlsValueSet(code, umlsClient, UmlsValueSet.SUBSTANCE_ABUSE)) {
                    	hasHxOfSubstanceAbuse = true;
                    }
                }
            }

            // check exclusion criterion: on naloxone
            boolean onNaloxone = false;

            List<MedicationOrder> activeOrDraftNaloxoneMedOrders = MatchingMedOrders.get(medOrders, meds, allowedMedicationOrderStatuses, focalPersonId, rxNavClient, RxNavValueSet.NALOXONE, config);

            if (activeOrDraftNaloxoneMedOrders.size() >= 1)
            {
            	onNaloxone = true;
            }

            // create output
            boolean metInclusionCriteria = false;
            if (averageMmeGe50 || onBenzodiazepine || hasHxOfSubstanceAbuse)
        	{
            	metInclusionCriteria = true;
        	}

            ArrayList<String> riskFactors = new ArrayList<String>();
            if (averageMmeGe50)
            {
            	riskFactors.add("average " + config.getMmeShortLabel() + " >= 50 mg/day");
            }
            if (onBenzodiazepine)
            {
            	riskFactors.add("concurrent use of benzodiazepine");
            }
            if (hasHxOfSubstanceAbuse)
            {
            	riskFactors.add("history of alcohol or drug abuse");
            }

        	if (metInclusionCriteria)
            {
            	if (onNaloxone)
            	{
            		boolean returnCard = config.isReturnCardForDebug();
                    String summary = "At least one risk factor warranting consideration of naloxone, but already on naloxone -- no need for action.";
                    String detail = "";
                    Indicator indicator = null;
                    return EvalResults.buildEvalResults(returnCard, summary, detail, indicator, source, links);
            	}
            	else
            	{
            	    boolean returnCard = true;
            	    String summary = "Consider offering naloxone.";
            	    String detail = "__Consider offering naloxone.__ Risk factor(s) for opioid overdose: " + StringUtility.getArrayAsString(riskFactors, "", ", ") + ".";
            	    Indicator indicator = Indicator.WARNING;
                    return EvalResults.buildEvalResults(returnCard, summary, detail, indicator, source, links);
            	}
            }
            else
            {
                boolean returnCard = config.isReturnCardForDebug();
                String summary = "Patient does not appear to have a risk factor warranting consideration of naloxone -- no need for action.";
                String detail = "";
                Indicator indicator = null;
                return EvalResults.buildEvalResults(returnCard, summary, detail, indicator, source, links);
            }
        }
    }
}
