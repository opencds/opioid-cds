package org.opencds.opioid.eval;

import java.awt.image.RenderedImage;
import java.net.URI;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hl7.fhir.dstu2.model.Condition;
import org.hl7.fhir.dstu2.model.Medication;
import org.hl7.fhir.dstu2.model.MedicationOrder;
import org.hl7.fhir.dstu2.model.Patient;
import org.opencds.hooks.model.response.Indicator;
import org.opencds.hooks.model.response.Link;
import org.opencds.hooks.model.response.Source;
import org.opencds.opioid.CDCMessage;
import org.opencds.opioid.config.OpioidConfig;
import org.opencds.opioid.display.Content;
import org.opencds.opioid.display.HtmlBuilder;
import org.opencds.opioid.display.TableUtil;
import org.opencds.opioid.display.mme.MmeConstants;
import org.opencds.opioid.image.ImageWriter;
import org.opencds.opioid.image.OpioidMmeImage;
import org.opencds.opioid.mme.Mme;
import org.opencds.opioid.mme.MmeEntry;
import org.opencds.opioid.process.CommonRuleProcessor;
import org.opencds.opioid.process.model.MedOrderAttributes;
import org.opencds.opioid.process.model.MedOrdersLists;
import org.opencds.opioid.results.EvalResults;
import org.opencds.opioid.util.AgeUtil;
import org.opencds.opioid.util.DoubleUtil;
import org.opencds.opioid.util.UrlUtil;
import org.opencds.tools.terminology.rxnav.client.RxNavTerminologyClient;
import org.opencds.tools.terminology.umls.api.client.UmlsTerminologyClient;

public class MmeEvaluation {
    private static final Log log = LogFactory.getLog(MmeEvaluation.class);

    private final CommonRuleProcessor processor;
    private final OpioidConfig config;


    public MmeEvaluation(RxNavTerminologyClient rxNavClient, UmlsTerminologyClient umlsClient,
            OpioidConfig opioidConfig) {
        this.config = opioidConfig;
        processor = new CommonRuleProcessor(rxNavClient, umlsClient, config);
    }


    /**
     * Returns an {@link EvalResults}.
     *
     * @param evalTime
     * @param focalPersonId
     * @param serverUri
     * @param medOrders     No expectations on restrictions.  Only needs meds covering at least one day in past 90 days.
     * @param meds
     * @param conditions
     * @return
     */
    public EvalResults getMmeEvaluationResults(Date evalTime, String focalPersonId, URI serverUri, Patient patient,
                                               List<MedicationOrder> medOrders, List<Medication> meds,
                                               List<Condition> conditions) {

        log.info("Running getMmeEvaluationResults");

        MedOrdersLists medOrdersLists = new MedOrdersLists();
        MedOrderAttributes medOrderAttributes = new MedOrderAttributes();

        Indicator indicator = null;

        Source source = new Source();
        source.setLabel("CDC opioid Rx guideline -- recommendation #5");
        source.setUrl(UrlUtil.newURL("https://www.cdc.gov/mmwr/volumes/65/rr/rr6501e1.htm#5_When_opioids_are_started"));

        List<Link> links = new ArrayList<>();
        Link mmeConversionLink = new Link();
        mmeConversionLink.setLabel(config.getMmeShortLabel() + " conversion table");
        mmeConversionLink.setUrl(UrlUtil.newURL("https://www.cdc.gov/drugoverdose/pdf/calculating_total_daily_dose-a.pdf"));
        links.add(mmeConversionLink);

        /*
         * Are activeOrDraftOpioidMedOrdersAbusedInAmbulatoryCareToIncludeForPt and medOrderIdToNameValuePairMap intended to be reused?  These my have values
         */
        EvalResults results = processor.conductCommonProcessingAndReturnEvalResultsIfShouldExit(evalTime, focalPersonId,
                medOrders, meds, conditions, source, links, medOrdersLists, medOrderAttributes);

        if (results != null) {
            return results;
        } else {

            Mme mme = new Mme(config.isIncludeAverageMmePerDay(),
                    medOrdersLists.getActiveOrDraftAbusedInAmbCareToInclude(), medOrderAttributes,
                    evalTime, config.getMmeShortLabel());

            double totalMmeAvg = mme.getTotalMmeAvg();
            double totalMmeMax = mme.getTotalMmeMax();
            int numEntriesWithUnknownMmeAvg = mme.getNumEntriesWithUnknownMmeAvg();
            int numEntriesWithUnknownMmeMax = mme.getNumEntriesWithUnknownMmeMax();

            List<MmeEntry> sortedMmeEntries = mme.getMmeEntries_sorted();
//          ArrayList<MmeEntry> sortedPotentiallyDuplicateMmeEntries = mme.getPotentiallyDuplicateMmeEntries_sorted();

            Content content = TableUtil.getContent(config.isIncludeAverageMmePerDay(),
                    config.isUseDynamicTableFormat(), sortedMmeEntries, "Active Opioid Rx", config.getMmeShortLabel(),
                    config.getDefaultOpioidRxDaysSupplyEstimate());
            content.setTotal(totalMmeAvg);

            String image = OpioidMmeImage.createImage(totalMmeAvg);
            String imageId = UUID.randomUUID().toString() + ".svg";
            Path folder = null;
            if (config.getImageCache() == null) {
                log.warn("imageCache is not set; using default");
                folder = Paths.get(System.getProperty("user.home"), ".fhir", "image-cache");
            } else {
                folder = Paths.get(config.getImageCache());
            }
            folder.toFile().mkdirs();
            ImageWriter.write(image, Paths.get(folder.toString(), imageId));
            content.setOpioidImage(imageId);

            content.setImageCacheBaseUrl(serverUri.toString());
            content.setNewImage(config.getNewImage());
            content.setExpandImage(config.getExpandImage());
            content.setWarningImage(config.getWarningImage());
            /*
            String potentiallyDuplicateMmeTable = null;

            if (sortedPotentiallyDuplicateMmeEntries.size() > 0)
            {
                calulcateTotal = false;
                potentiallyDuplicateMmeTable = getMmeTable(sortedPotentiallyDuplicateMmeEntries, "Excluded Opioid Rx", mmeShortLabel, calulcateTotal, -1);
            }

            if (potentiallyDuplicateMmeTable != null)
            {
                mmeTablesStr += "\n\nThe following opioids were <b>excluded</b> from the " + mmeShortLabel + " calcluation due to presumed duplication with one of the opioids above.\n\n";
                mmeTablesStr += potentiallyDuplicateMmeTable;
            }
            */

            content.setSortedEntries(sortedMmeEntries);
            content.setDynamicDetails(config.isUseDynamicTableFormat());

            int numEntriesWithUnknownMme = 0;
            String stat = "";
            double totalMme = 0;

            if (config.isIncludeAverageMmePerDay()) {
                stat = "average";
                numEntriesWithUnknownMme = numEntriesWithUnknownMmeAvg;
                totalMme = totalMmeAvg;
            } else {
                stat = "max";
                numEntriesWithUnknownMme = numEntriesWithUnknownMmeMax;
                totalMme = totalMmeMax;
            }

            content.setSummaryData(stat, numEntriesWithUnknownMme, totalMme, config.getMmeLongLabel());


            boolean isUnder18Years = false;
            // patient shouldn't be null, but it could be.
            if (patient != null) {
                isUnder18Years = AgeUtil.underAge(patient.getBirthDate(), evalTime, 18);
            } else {
                log.warn("Age calculation could not be determined (patient resource is null.");
            }

            if (totalMme < 50) {
                boolean returnCard = config.isReturnCardForDebug();
                String summary = null;
                String detail = null;
                if (config.isExcludeIfNoDraftOpioidMedOrdersAbusedInAmbulatoryCare()) {
                    content.setCdcTextData(CDCMessage.AT_LEAST_ONE_NO_ABUSE, isUnder18Years);
                    detail = HtmlBuilder.build(content, new MmeConstants());
                } else {
                    // e.g., in context of SMART on FHIR app
                    summary = config.getMmeShortLabel() + "/day &lt; 50 (" + DoubleUtil.format(totalMme) + " mg).";
                    content.setCdcTextData(CDCMessage.NONE, isUnder18Years);
                    // content.setCdcText(CDCMessage.AT_LEAST_ONE_NO_ABUSE_SOF, isUnder18Years, config.getMmeShortLabel(), totalMme);
                    detail = HtmlBuilder.build(content, new MmeConstants());
                }
                indicator = Indicator.INFO;
                return EvalResults.buildEvalResults(returnCard, summary, detail, indicator, source, links);
            } else if (totalMme < 90) { // >= 50, < 90
                String summary = null;
                String detail = null;
                boolean returnCard = true;
                summary = config.getMmeShortLabel() + " &gt;= 50 mg/day";
                content.setCdcTextData(CDCMessage.REASSESS, isUnder18Years);
                detail = HtmlBuilder.build(content, new MmeConstants());
                indicator = Indicator.WARNING;
                return EvalResults.buildEvalResults(returnCard, summary, detail, indicator, source, links);
            } else { // >= 90
                String summary = null;
                String detail = null;
                boolean returnCard = true;
                summary = config.getMmeShortLabel() + " &gt;= 90 mg/day";
                content.setCdcTextData(CDCMessage.JUSTIFY, isUnder18Years);
                detail = HtmlBuilder.build(content, new MmeConstants());
                indicator = Indicator.WARNING;
                return EvalResults.buildEvalResults(returnCard, summary, detail, indicator, source, links);
            }
        }
    }

}
