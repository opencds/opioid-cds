package org.opencds.opioid.eval;

import java.net.URI;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hl7.fhir.dstu2.model.Condition;
import org.hl7.fhir.dstu2.model.Encounter;
import org.hl7.fhir.dstu2.model.Medication;
import org.hl7.fhir.dstu2.model.MedicationOrder;
import org.hl7.fhir.dstu2.model.MedicationOrder.MedicationOrderStatus;
import org.opencds.Utils;
import org.opencds.common.utilities.DateUtility;
import org.opencds.hooks.model.response.Indicator;
import org.opencds.hooks.model.response.Link;
import org.opencds.hooks.model.response.Source;
import org.opencds.opioid.config.OpioidConfig;
import org.opencds.opioid.process.CommonRuleProcessor;
import org.opencds.opioid.process.DaysCoveredByMedOrders;
import org.opencds.opioid.process.model.MedOrderAttributes;
import org.opencds.opioid.process.model.MedOrdersLists;
import org.opencds.opioid.results.EvalResults;
import org.opencds.opioid.util.UrlUtil;
import org.opencds.opioid.util.fhir.MatchingMedOrders;
import org.opencds.opioid.valuesets.RxNavValueSet;
import org.opencds.tools.terminology.rxnav.client.RxNavTerminologyClient;
import org.opencds.tools.terminology.umls.api.client.UmlsTerminologyClient;

public class ImmediateReleaseOpioidEvaluation {
    private static Log log = LogFactory.getLog(ImmediateReleaseOpioidEvaluation.class);

    private final OpioidConfig config;
    private final RxNavTerminologyClient rxNavClient;
    private CommonRuleProcessor processor;

    public ImmediateReleaseOpioidEvaluation(RxNavTerminologyClient rxNavClient, UmlsTerminologyClient umlsClient,
            OpioidConfig opioidConfig) {
        this.config = opioidConfig;
        this.rxNavClient = rxNavClient;
        this.processor = new CommonRuleProcessor(rxNavClient, umlsClient, opioidConfig);
    }

    /**
     * Returns a HashMap with the following keys and associated objects:
     * returnCard Boolean. True if a CDS Hooks card should be returned (action
     * needed), False otherwise. If False, only summary returned for debug
     * purposes. summary String. One-sentence, <140-character summary message
     * for display to the user inside of this card. detail String. Optional
     * detailed information to display, represented in Markdown. Provides
     * guidance + MME table. indicator Indicator. "info" if returnCard == true,
     * null otherwise. source Source. Source of guidance (CDC opioid guidance).
     * links List&lt;Link&gt;. Links to further CDC info.
     */
    public EvalResults getImmediateReleaseOpioidEvaluationResults(java.util.Date evalTime, String focalPersonId,
            URI serverUri, List<MedicationOrder> medOrders, List<Medication> meds, List<Encounter> encounters,
            List<Condition> conditions) {
        log.info("Running getImmediateReleaseOpioidEvaluationResults");

        MedOrdersLists medOrdersLists = new MedOrdersLists();
        MedOrderAttributes medOrderAttributes = new MedOrderAttributes();

        Source source = new Source();
        source.setLabel("CDC opioid Rx guideline -- recommendation #4");
        source.setUrl(UrlUtil.newURL(
                "https://www.cdc.gov/mmwr/volumes/65/rr/rr6501e1.htm#4_When_starting_opioid_therapy"));

        List<Link> links = new ArrayList<Link>();

        EvalResults results = processor.conductCommonProcessingAndReturnEvalResultsIfShouldExit(evalTime, focalPersonId,
                medOrders, meds, conditions, source, links, medOrdersLists, medOrderAttributes);

        if (results != null) {
            return results;
        } else {
            // At least one encounter in past year, no opioids in past 90 days
            DateUtility dateUtility = DateUtility.getInstance();

            Date evalTimeWithoutHms = dateUtility.getDateFromString(dateUtility.getDateAsString(evalTime, "yyyy-MM-dd"),
                    "yyyy-MM-dd");

            Date yesterdayWithoutHms = dateUtility.getDateAfterAddingTime(evalTimeWithoutHms, Calendar.DATE, -1);

            boolean useEndDatePreferDefault = config
                    .isCalculateCoverageDatesUsingPresumptionOfDefaultDaysSupplyUnlessExplicitlyStatedOtherwise();

            List<Date> daysCoveredByOpioidMedOrdersAbusedInAmbulatoryCare_last90Days = DaysCoveredByMedOrders.get(
                    yesterdayWithoutHms, 90, medOrdersLists.getActiveDraftCompletedOrStoppedAbusedInAmbCare(),
                    medOrderAttributes, useEndDatePreferDefault);

            int numDaysCoveredByOpioidMedOrdersAbusedInAmbulatoryCare_last90Days = daysCoveredByOpioidMedOrdersAbusedInAmbulatoryCare_last90Days
                    .size();

            Date earliestStartDateInclusive = dateUtility.getDateAfterAddingTime(evalTimeWithoutHms, Calendar.YEAR, -1);
            Date latestStartDateExclusive = evalTimeWithoutHms;

            // Was using Utils.getEncountersMatchingCriteria, but it was really only filtering by date
            long numEncsForPtPastYearExcludingEvalDate = encounters.stream()
                    .filter(e -> Utils.isReferencedPatientIdFocalPersonId(e.getPatient(), focalPersonId))
                    .filter(e -> e.getPeriod() != null && e.getPeriod().getStart() != null)
                    .filter(e -> e.getPeriod().getStart().before(earliestStartDateInclusive)
                            && e.getPeriod().getStart().before(latestStartDateExclusive))
                    .count();

            boolean meetsInclusionCriteria = false;
            if ((numDaysCoveredByOpioidMedOrdersAbusedInAmbulatoryCare_last90Days == 0)
                    && (numEncsForPtPastYearExcludingEvalDate >= 1)) {
                meetsInclusionCriteria = true;
            }

            List<MedicationOrderStatus> allowedMedicationOrderStatuses = new ArrayList<MedicationOrderStatus>();
            allowedMedicationOrderStatuses.add(MedicationOrderStatus.DRAFT);
            List<MedicationOrder> draftLongActingOpioidMedOrders = MatchingMedOrders.get(medOrders,
                    meds, allowedMedicationOrderStatuses, focalPersonId, rxNavClient,
                    RxNavValueSet.LONG_ACTING_OPIOIDS, config);

            if (draftLongActingOpioidMedOrders.size() >= 1) {
                if (meetsInclusionCriteria) {
                    String summary = "Recommend use of immediate-release opioids.";
                    String detail = "__Recommend use of immediate-release opioids when starting patients on opioids.__";

                    // always return a card
                    return EvalResults.buildEvalResults(true, summary, detail, Indicator.WARNING, source, links);
                } else {
                    boolean returnCard = config.isReturnCardForDebug();
                    String summary = "At least one draft long-acting opioid, but either opioid with primary care abuse potential prescribed in previous 90 days excluding today, or no encounter in past year excluding today -- no need for action.";
                    String detail = "";
                    return EvalResults.buildEvalResults(returnCard, summary, detail, null, source, links);
                }
            } else {
                boolean returnCard = config.isReturnCardForDebug();
                String summary = "No draft long-acting opioid -- no need for action.";
                String detail = "";
                return EvalResults.buildEvalResults(returnCard, summary, detail, null, source, links);
            }
        }
    }

}
