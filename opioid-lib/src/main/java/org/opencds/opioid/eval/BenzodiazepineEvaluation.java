package org.opencds.opioid.eval;

import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hl7.fhir.dstu2.model.Condition;
import org.hl7.fhir.dstu2.model.Medication;
import org.hl7.fhir.dstu2.model.MedicationOrder;
import org.hl7.fhir.dstu2.model.MedicationOrder.MedicationOrderStatus;
import org.opencds.hooks.model.response.Indicator;
import org.opencds.hooks.model.response.Link;
import org.opencds.hooks.model.response.Source;
import org.opencds.opioid.config.OpioidConfig;
import org.opencds.opioid.process.CommonRuleProcessor;
import org.opencds.opioid.process.model.MedOrderAttributes;
import org.opencds.opioid.process.model.MedOrdersLists;
import org.opencds.opioid.results.EvalResults;
import org.opencds.opioid.util.UrlUtil;
import org.opencds.opioid.util.fhir.MatchingMedOrders;
import org.opencds.opioid.valuesets.RxNavValueSet;
import org.opencds.tools.terminology.rxnav.client.RxNavTerminologyClient;
import org.opencds.tools.terminology.umls.api.client.UmlsTerminologyClient;

public class BenzodiazepineEvaluation {
    private static Log log = LogFactory.getLog(BenzodiazepineEvaluation.class);

    private final CommonRuleProcessor processor;
    private final OpioidConfig config;
    private final RxNavTerminologyClient rxNavClient;

    public BenzodiazepineEvaluation(RxNavTerminologyClient rxNavClient, UmlsTerminologyClient umlsClient,
            OpioidConfig opioidConfig) {
        this.config = opioidConfig;
        this.rxNavClient = rxNavClient;
        this.processor = new CommonRuleProcessor(rxNavClient, umlsClient, opioidConfig);
    }

    /**
     * Returns a HashMap with the following keys and associated objects:
     *      returnCard  Boolean.  True if a CDS Hooks card should be returned (action needed), False otherwise.  If False, only summary returned for debug purposes.
     *      summary     String.  One-sentence, <140-character summary message for display to the user inside of this card.
     *      detail      String.  Optional detailed information to display, represented in Markdown.  Provides guidance + MME table.
     *      indicator   Indicator.  "info" if returnCard == true, null otherwise.
     *      source      Source.     Source of guidance (CDC opioid guidance).
     *      links       List<Link>. Links to further CDC info.
     */
    public EvalResults getBenzodiazepineEvaluationResults(java.util.Date evalTime, String focalPersonId, URI serverUri,
            List<MedicationOrder> medOrders, List<Medication> meds, List<Condition> conditions) {
    	log.info("Running getBenzodiazepineEvaluationResults");

    	MedOrdersLists medOrdersLists = new MedOrdersLists();
    	MedOrderAttributes medOrderAttributes = new MedOrderAttributes();

        Source source = new Source();
        source.setLabel("CDC opioid Rx guideline -- recommendation #11");
        source.setUrl(UrlUtil.newURL("https://www.cdc.gov/mmwr/volumes/65/rr/rr6501e1.htm#11_Clinicians_should_avoid_prescribing"));

        List<Link> links = new ArrayList<Link>();

        EvalResults results = processor.conductCommonProcessingAndReturnEvalResultsIfShouldExit(evalTime, focalPersonId,
                medOrders, meds, conditions, source, links, medOrdersLists, medOrderAttributes);

        if (results != null) {
            return results;
        } else {
            // check for active/draft benzodiazepines
            medOrdersLists.addDraftBenzodiazepine(MatchingMedOrders.get(medOrders, meds,
                    Arrays.asList(MedicationOrderStatus.DRAFT), focalPersonId, rxNavClient,
                    RxNavValueSet.BENZODIAZEPINES, config));

            medOrdersLists.addActiveBenzodiazepine(MatchingMedOrders.get(medOrders, meds,
                    Arrays.asList(MedicationOrderStatus.ACTIVE), focalPersonId, rxNavClient,
                    RxNavValueSet.BENZODIAZEPINES, config));

            if (medOrdersLists.hasDraftBenzodiazepine() || medOrdersLists.hasDraftAbusedInAmbCare()) {
                if (medOrdersLists.hasActiveOrDraftAbusedInAmbCare()
                        && medOrdersLists.hasActiveOrDraftBenzodiazepine()) {
                    String summary = "Avoid co-prescribing opioid and benzodiazepine.";
                    String detail = "__Avoid co-prescribing opioid and benzodiazepine concurrently whenever possible.__";
                    // always return a card
                    return EvalResults.buildEvalResults(true, summary, detail, Indicator.WARNING, source, links);
                } else {
                    boolean returnCard = config.isReturnCardForDebug();
                    String summary = "Patient about to be prescribed opioid with ambulatory care abuse potential or benzodiazepine, but not on both -- no need for action.";
                    String detail = "";
                    return EvalResults.buildEvalResults(returnCard, summary, detail, null, source, links);
                }
            } else {
                boolean returnCard = config.isReturnCardForDebug();
                String summary = "Patient not about to be prescribed opioid with ambulatory care abuse potential or benzodiazepine -- no need for action.";
                String detail = "";
                return EvalResults.buildEvalResults(returnCard, summary, detail, null, source, links);
            }
        }
    }
}
