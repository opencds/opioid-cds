package org.opencds.opioid.eval;

import java.net.URI;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hl7.fhir.dstu2.model.Condition;
import org.hl7.fhir.dstu2.model.DateTimeType;
import org.hl7.fhir.dstu2.model.Encounter;
import org.hl7.fhir.dstu2.model.Medication;
import org.hl7.fhir.dstu2.model.MedicationOrder;
import org.hl7.fhir.dstu2.model.Period;
import org.hl7.fhir.dstu2.model.Procedure;
import org.hl7.fhir.dstu2.model.Procedure.ProcedureStatus;
import org.opencds.Utils;
import org.opencds.common.utilities.DateUtility;
import org.opencds.hooks.model.response.Indicator;
import org.opencds.hooks.model.response.Link;
import org.opencds.hooks.model.response.Source;
import org.opencds.opioid.config.OpioidConfig;
import org.opencds.opioid.process.CommonRuleProcessor;
import org.opencds.opioid.process.DaysCoveredByMedOrders;
import org.opencds.opioid.process.model.MedOrderAttributes;
import org.opencds.opioid.process.model.MedOrdersLists;
import org.opencds.opioid.results.EvalResults;
import org.opencds.opioid.util.UrlUtil;
import org.opencds.opioid.util.term.UmlsUtil;
import org.opencds.opioid.valuesets.UmlsValueSet;
import org.opencds.tools.terminology.rxnav.client.RxNavTerminologyClient;
import org.opencds.tools.terminology.umls.api.client.UmlsTerminologyClient;

public class PeriodicAssessmentEvaluation {
    private static Log log = LogFactory.getLog(PeriodicAssessmentEvaluation.class);

    private final OpioidConfig config;
    private final UmlsTerminologyClient umlsClient;
    private final CommonRuleProcessor processor;

    public PeriodicAssessmentEvaluation(RxNavTerminologyClient rxNavClient, UmlsTerminologyClient umlsClient, OpioidConfig opioidConfig) {
        this.config = opioidConfig;
        this.umlsClient = umlsClient;
        processor = new CommonRuleProcessor(rxNavClient, umlsClient, opioidConfig);
    }

    /**
     * Returns a HashMap with the following keys and associated objects:
     *      returnCard  Boolean.  True if a CDS Hooks card should be returned (action needed), False otherwise.  If False, only summary returned for debug purposes.
     *      summary     String.  One-sentence, <140-character summary message for display to the user inside of this card.
     *      detail      String.  Optional detailed information to display, represented in Markdown.  Provides guidance + MME table.
     *      indicator   Indicator.  "info" if returnCard == true, null otherwise.
     *      source      Source.     Source of guidance (CDC opioid guidance).
     *      links       List<Link>. Links to further CDC info.
     */
    public EvalResults getPeriodicAssessmentEvaluationResults(java.util.Date evalTime, String focalPersonId,
            URI serverUri, List<MedicationOrder> medOrders, List<Medication> meds, List<Encounter> encounters,
            List<Condition> conditions, List<Procedure> procedures) {
    	log.info("Running getPeriodicAssessmentEvaluationResults");

	    MedOrdersLists medOrderLists = new MedOrdersLists();
	    MedOrderAttributes medOrderAttributes = new MedOrderAttributes();

	    Source source = new Source();
	    source.setLabel("CDC opioid Rx guideline -- recommendation #7");
        source.setUrl(UrlUtil
                .newURL("https://www.cdc.gov/mmwr/volumes/65/rr/rr6501e1.htm#7_Clinicians_should_evaluate_benefits"));

        List<Link> links = new ArrayList<Link>();

        EvalResults results = processor.conductCommonProcessingAndReturnEvalResultsIfShouldExit(evalTime, focalPersonId,
                medOrders, meds, conditions, source, links, medOrderLists, medOrderAttributes);

        if (results != null) {
            return results;
        } else {
        	// Check opioid use requirement:
        	// (Opioid Rx for at least 7 of the past 10 days AND at least 1 encounter in past 12 months [excluding today]) OR (Opioid Rx for at least 21 of 30 days for each of the past 3 months)
        	DateUtility dateUtility = DateUtility.getInstance();

        	boolean useEndDatePreferDefault = config
                    .isCalculateCoverageDatesUsingPresumptionOfDefaultDaysSupplyUnlessExplicitlyStatedOtherwise();

        	Date thirtyDaysAgo = dateUtility.getDateAfterAddingTime(evalTime, Calendar.DATE, -30);
            Date sixtyDaysAgo = dateUtility.getDateAfterAddingTime(evalTime, Calendar.DATE, -60);
            List<Date> daysCoveredByOpioidMedOrdersAbusedInAmbulatoryCare_last10Days = DaysCoveredByMedOrders.get(
                    evalTime, 9, medOrderLists.getActiveDraftCompletedOrStoppedAbusedInAmbCare(),
                    medOrderAttributes, useEndDatePreferDefault);
            List<Date> daysCoveredByOpioidMedOrdersAbusedInAmbulatoryCare_last30Days = DaysCoveredByMedOrders.get(
                    evalTime, 29, medOrderLists.getActiveDraftCompletedOrStoppedAbusedInAmbCare(),
                    medOrderAttributes, useEndDatePreferDefault);
            List<Date> daysCoveredByOpioidMedOrdersAbusedInAmbulatoryCare_last31_60Days = DaysCoveredByMedOrders.get(
                    thirtyDaysAgo, 29, medOrderLists.getActiveDraftCompletedOrStoppedAbusedInAmbCare(),
                    medOrderAttributes, useEndDatePreferDefault);
            List<Date> daysCoveredByOpioidMedOrdersAbusedInAmbulatoryCare_last61_90Days = DaysCoveredByMedOrders.get(
                    sixtyDaysAgo, 29, medOrderLists.getActiveDraftCompletedOrStoppedAbusedInAmbCare(),
                    medOrderAttributes, useEndDatePreferDefault);
            int numDaysCoveredByOpioidMedOrdersAbusedInAmbulatoryCare_last10Days = daysCoveredByOpioidMedOrdersAbusedInAmbulatoryCare_last10Days
                    .size();
            int numDaysCoveredByOpioidMedOrdersAbusedInAmbulatoryCare_last30Days = daysCoveredByOpioidMedOrdersAbusedInAmbulatoryCare_last30Days
                    .size();
            int numDaysCoveredByOpioidMedOrdersAbusedInAmbulatoryCare_last31_60Days = daysCoveredByOpioidMedOrdersAbusedInAmbulatoryCare_last31_60Days
                    .size();
            int numDaysCoveredByOpioidMedOrdersAbusedInAmbulatoryCare_last61_90Days = daysCoveredByOpioidMedOrdersAbusedInAmbulatoryCare_last61_90Days
                    .size();

            Date evalTimeWithoutHms = dateUtility.getDateFromString(dateUtility.getDateAsString(evalTime, "yyyy-MM-dd"), "yyyy-MM-dd");
            Date earliestStartDateInclusive = dateUtility.getDateAfterAddingTime(evalTimeWithoutHms, Calendar.YEAR, -1);
            Date latestStartDateExclusive = evalTimeWithoutHms;

            // Was using Utils.getEncountersMatchingCriteria, but it was really only filtering by date
            long numEncsForPtPastYearExcludingEvalDate = encounters.stream()
                    .filter(e -> Utils.isReferencedPatientIdFocalPersonId(e.getPatient(), focalPersonId))
                    .filter(e -> e.getPeriod() != null && e.getPeriod().getStart() != null)
                    .filter(e -> e.getPeriod().getStart().before(earliestStartDateInclusive)
                            && e.getPeriod().getStart().before(latestStartDateExclusive))
                    .count();

            boolean meetsOpioidUseRequirement = false;
            if (
                ((numDaysCoveredByOpioidMedOrdersAbusedInAmbulatoryCare_last30Days >= 21)
                    && (numDaysCoveredByOpioidMedOrdersAbusedInAmbulatoryCare_last31_60Days >= 21)
                    && (numDaysCoveredByOpioidMedOrdersAbusedInAmbulatoryCare_last61_90Days >= 21))
                ||
                ((numDaysCoveredByOpioidMedOrdersAbusedInAmbulatoryCare_last10Days >= 7) && (numEncsForPtPastYearExcludingEvalDate >= 1))
               )
            {
            	meetsOpioidUseRequirement = true;
            }

        	// Check assessment requirement:
        	// No assessment of risk for opioid use (see below) within past 90 days (noted as Procedure) (inclusive time interval search from evalTimeWithoutHms - 90 days to evalTime)
            // Status allowed are completed or in-progress

            boolean assessmentCompletedOrInProgressInPast90Days = false;

            for(Procedure proc : procedures)
            {
            	if ((Utils.isReferencedPatientIdFocalPersonId(proc.getSubject(), focalPersonId)))
    			{
            		ProcedureStatus status = proc.getStatus();
            		if (status != null)
            		{
            			String statusCode = status.toCode();
            			if (statusCode.equals("completed") || statusCode.equals("in-progress")) // purposely avoiding STU version-specific enums
            			{
            				try
                    		{
            					Date date = null;
        	            		if (proc.hasPerformedDateTimeType())
        	            		{
        	            			DateTimeType performedDateTimeType = proc.getPerformedDateTimeType();
        	            			date = performedDateTimeType.getValue();
        	            		}
        	            		else if (proc.hasPerformedPeriod())
        	            		{
        	            			Period performedPeriod = proc.getPerformedPeriod();
        	            			if (performedPeriod != null)
        	            			{
        	            				date = performedPeriod.getStart();
        	            			}
        	            		}

        	            		if (date != null)
        	            		{
        	            			Date ninetyDaysAgo = dateUtility.getDateAfterAddingTime(evalTimeWithoutHms, Calendar.DATE, -90);
        	            			if ((! date.before(ninetyDaysAgo)) && (! date.after(evalTime)))
        	            			{
        	            				if (UmlsUtil.isConceptInUmlsValueSet(proc.getCode(), umlsClient, UmlsValueSet.OPIATE_THERAPY_RISK_ASSESSMENT))
        	            				{
        									assessmentCompletedOrInProgressInPast90Days = true;
	        	            			}
        	            			}
        	            		}
                    		}
                    		catch (Exception e) // unexpected
                    		{
                    			e.printStackTrace();
                    		}
            			}
            		}
    			}
            }

            String opioidUseRequirementDesc = "Opioid use requirement: (Opioid Rx for at least 7 of the past 10 days AND at least 1 encounter in past 12 months [excluding today]) OR (Opioid Rx for at least 21 of 30 days for each of the past 3 months)";
            if (meetsOpioidUseRequirement)
            {
            	if (assessmentCompletedOrInProgressInPast90Days)
            	{
            		boolean returnCard = config.isReturnCardForDebug();
                    String summary = "At least one draft medication order which is an opioid abused in primary care, and patient does meet opioid use requirement, but required assessment has been completed or is in progress over past 90 days -- no need for action.";
                    String detail = opioidUseRequirementDesc;
                    Indicator indicator = Indicator.INFO;
                    return EvalResults.buildEvalResults(returnCard, summary, detail, indicator, source, links);
            	}
            	else
            	{
            		boolean returnCard = true;
            		String summary = "Recommend evaluation of benefits and harms of opioid use.";
            		String detail = "__Recommend evaluation of benefits and harms of opioid use.__ Patients on opioid therapy should be evaluated for benefits and harms within 1-4 weeks of starting opioid therapy and every 3 months or more subsequently.";
            		Indicator indicator = Indicator.WARNING;
                    return EvalResults.buildEvalResults(returnCard, summary, detail, indicator, source, links);
            	}
            }
            else
            {
            	boolean returnCard = config.isReturnCardForDebug();
            	String summary = "At least one draft medication order which is an opioid abused in primary care, but patient does not meet opioid use requirement.";
            	String detail = opioidUseRequirementDesc;
            	Indicator indicator = Indicator.INFO;
                return EvalResults.buildEvalResults(returnCard, summary, detail, indicator, source, links);
            }
        }
    }
}
