package org.opencds.opioid.eval;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hl7.fhir.dstu2.model.CodeableConcept;
import org.hl7.fhir.dstu2.model.Condition;
import org.hl7.fhir.dstu2.model.DateTimeType;
import org.hl7.fhir.dstu2.model.Medication;
import org.hl7.fhir.dstu2.model.MedicationOrder;
import org.hl7.fhir.dstu2.model.Observation;
import org.hl7.fhir.dstu2.model.Observation.ObservationStatus;
import org.opencds.Utils;
import org.opencds.common.utilities.DateUtility;
import org.opencds.hooks.model.response.Indicator;
import org.opencds.hooks.model.response.Link;
import org.opencds.hooks.model.response.Source;
import org.opencds.opioid.config.OpioidConfig;
import org.opencds.opioid.process.CommonRuleProcessor;
import org.opencds.opioid.process.model.MedOrderAttributes;
import org.opencds.opioid.process.model.MedOrdersLists;
import org.opencds.opioid.results.EvalResults;
import org.opencds.opioid.util.term.UmlsUtil;
import org.opencds.opioid.valuesets.UmlsValueSet;
import org.opencds.tools.terminology.rxnav.client.RxNavTerminologyClient;
import org.opencds.tools.terminology.umls.api.client.UmlsTerminologyClient;

public class DrugTestingEvaluation {
    private static Log log = LogFactory.getLog(DrugTestingEvaluation.class);

    private final CommonRuleProcessor processor;
    private final OpioidConfig config;
    private final UmlsTerminologyClient umlsClient;

    public DrugTestingEvaluation(RxNavTerminologyClient rxNavClient, UmlsTerminologyClient umlsClient, OpioidConfig config) {
        this.config = config;
        this.umlsClient = umlsClient;
        this.processor = new CommonRuleProcessor(rxNavClient, umlsClient, config);
    }

    /**
     * Returns a HashMap with the following keys and associated objects:
     *      returnCard  Boolean.  True if a CDS Hooks card should be returned (action needed), False otherwise.  If False, only summary returned for debug purposes.
     *      summary     String.  One-sentence, <140-character summary message for display to the user inside of this card.
     *      detail      String.  Optional detailed information to display, represented in Markdown.  Provides guidance + MME table.
     *      indicator   Indicator.  "info" if returnCard == true, null otherwise.
     *      source      Source.     Source of guidance (CDC opioid guidance).
     *      links       List<Link>. Links to further CDC info.
     */
    public synchronized EvalResults getDrugTestingEvaluationResults(java.util.Date evalTime, String focalPersonId, URI serverUri,
            List<MedicationOrder> medOrders, List<Medication> meds, List<Condition> conditions, List<Observation> observations)
    {
        log.info("Running getDrugTestingEvaluationResults");

        MedOrderAttributes medOrderAttributes = new MedOrderAttributes();

        boolean returnCard = false;
        String summary = null;
        String detail = null;
        Indicator indicator = null;
        Source source = new Source();

        source.setLabel("CDC opioid Rx guideline -- recommendation #10");
        try
        {
            source.setUrl(new URL("https://www.cdc.gov/mmwr/volumes/65/rr/rr6501e1.htm#10_When_prescribing_opioids"));
        }
        catch (MalformedURLException e)
        {
            e.printStackTrace();
        }
        List<Link> links = new ArrayList<Link>();

        MedOrdersLists medOrdersLists = new MedOrdersLists();
        EvalResults results = processor.conductCommonProcessingAndReturnEvalResultsIfShouldExit(evalTime, focalPersonId,
                medOrders, meds, conditions, source, links, medOrdersLists, medOrderAttributes);

        if (results != null)
        {
            return results;
        }
        else
        {
            // check to see if urine opiate testing has been done in the past year
            boolean urineOpiateTestingDoneInPastYear = false;
            for (Observation observation : observations)
            {
                ObservationStatus status = observation.getStatus();
                if ((status != null) && ((status == ObservationStatus.FINAL) || (status == ObservationStatus.AMENDED) || (status == ObservationStatus.PRELIMINARY) || (status == ObservationStatus.REGISTERED)))
                {
                    // Not checking for category of laboratory as if the code and status match, there really can't be anything else
                    CodeableConcept code = observation.getCode();
                    if (
                            (Utils.isReferencedPatientIdFocalPersonId(observation.getSubject(), focalPersonId))
                       )
                    {
                    	if (UmlsUtil.isConceptInUmlsValueSet(observation.getCode(), umlsClient, UmlsValueSet.OPIATE_URINE_DRUG_TEST))
                    	{
                    		// TODO: consider supporting effective time that is a period rather than a DateTime.  However, at least in some EHRs, this is returned as a DateTime.
	                        try
	                        {
	                            DateTimeType effectiveDateTimeType = observation.getEffectiveDateTimeType();
	                            Date effectiveDate = new Date(effectiveDateTimeType.getTime());
	                            if (! effectiveDate.before(DateUtility.getInstance().getDateAfterAddingTime(evalTime, Calendar.YEAR, -1)))
	                            {
	                                urineOpiateTestingDoneInPastYear = true;
	                            }
	                        } catch (Exception e)
	                        {
	                            // ignore
	                        }
                    	}
                    }
                }
            }

            if (urineOpiateTestingDoneInPastYear)
            {
                returnCard = false;
                summary = "At least one draft medication order which is an opioid abused in primary care, but patient has had a urine opiate test in the past year -- no need for action.";
                detail = null;
                indicator = null;
                return EvalResults.buildEvalResults(returnCard, summary, detail, indicator, source, links);
            }
            else
            {
                returnCard = true;
                summary = "Recommend urine drug testing.";
                detail = "__Recommend urine drug testing.__ The CDC recommends annual urine drug testing in the context of opioid use for chronic pain. Make sure that opioids are positive in screen and that other unexpected drugs are not.";
                indicator = Indicator.WARNING;
                return EvalResults.buildEvalResults(returnCard, summary, detail, indicator, source, links);
            }
        }
    }
}
