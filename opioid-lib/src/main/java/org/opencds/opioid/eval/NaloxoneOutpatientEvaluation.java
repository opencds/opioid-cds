package org.opencds.opioid.eval;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hl7.fhir.dstu2.model.Condition;
import org.hl7.fhir.dstu2.model.Medication;
import org.hl7.fhir.dstu2.model.MedicationOrder;
import org.hl7.fhir.dstu2.model.MedicationOrder.MedicationOrderStatus;
import org.opencds.hooks.model.response.Indicator;
import org.opencds.hooks.model.response.Link;
import org.opencds.hooks.model.response.Source;
import org.opencds.opioid.CDCMessage;
import org.opencds.opioid.config.OpioidConfig;
import org.opencds.opioid.display.Content;
import org.opencds.opioid.display.HtmlBuilder;
import org.opencds.opioid.display.TableUtil;
import org.opencds.opioid.display.naloxoneoutpatient.NaloxoneOutpatientConstants;
import org.opencds.opioid.image.ImageWriter;
import org.opencds.opioid.image.OpioidNaloxoneOutpatientImage;
import org.opencds.opioid.mme.Mme;
import org.opencds.opioid.process.CommonRuleProcessor;
import org.opencds.opioid.process.model.MedOrderAttributes;
import org.opencds.opioid.process.model.MedOrdersLists;
import org.opencds.opioid.results.EvalResults;
import org.opencds.opioid.util.UrlUtil;
import org.opencds.opioid.util.fhir.MatchingMedOrders;
import org.opencds.opioid.valuesets.RxNavValueSet;
import org.opencds.tools.terminology.rxnav.client.RxNavTerminologyClient;
import org.opencds.tools.terminology.umls.api.client.UmlsTerminologyClient;

import java.net.URI;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class NaloxoneOutpatientEvaluation {
    private static Log log = LogFactory.getLog(NaloxoneOutpatientEvaluation.class);

    private final OpioidConfig config;
    private final RxNavTerminologyClient rxNavClient;
    private final UmlsTerminologyClient umlsClient;
    private final CommonRuleProcessor processor;

    public NaloxoneOutpatientEvaluation(RxNavTerminologyClient rxNavClient, UmlsTerminologyClient umlsClient,
                                        OpioidConfig opioidConfig) {
        this.config = opioidConfig;
        this.rxNavClient = rxNavClient;
        this.umlsClient = umlsClient;
        processor = new CommonRuleProcessor(rxNavClient, umlsClient, opioidConfig);
    }

    /**
     * Returns a HashMap with the following keys and associated objects:
     * returnCard Boolean. True if a CDS Hooks card should be returned (action
     * needed), False otherwise. If False, only summary returned for debug
     * purposes. summary String. One-sentence, <140-character summary message
     * for display to the user inside of this card. detail String. Optional
     * detailed information to display, represented in Markdown. Provides
     * guidance + MME table. indicator Indicator. "info" if returnCard == true,
     * null otherwise. source Source. Source of guidance (CDC opioid guidance).
     * links List<Link>. Links to further CDC info.
     */
    public EvalResults getNaloxoneOutpatientEvaluationResults(java.util.Date evalTime, String focalPersonId,
            URI serverUri, List<MedicationOrder> medOrders, List<Medication> meds, List<Condition> conditions) {
        log.info("Running getNaloxoneOutpatientEvaluationResults");

        MedOrdersLists medOrdersLists = new MedOrdersLists();
        MedOrderAttributes medOrderAttributes = new MedOrderAttributes();

        Source source = new Source();
        source.setLabel("CDC opioid Rx guideline -- recommendation #8");
        source.setUrl(UrlUtil.newURL(
                "https://www.cdc.gov/mmwr/volumes/65/rr/rr6501e1.htm#8_starting_periodically_during_continuation"));

        List<Link> links = new ArrayList<Link>();

        EvalResults results = processor.conductCommonProcessingAndReturnEvalResultsIfShouldExit(evalTime, focalPersonId,
                medOrders, meds, conditions, source, links, medOrdersLists, medOrderAttributes);

        if (results != null) {
            return results;
        } else {
            // check inclusion criterion: average MME >= 50
            boolean avgMmeGe50 = false;

            Mme mme = new Mme(config.isIncludeAverageMmePerDay(),
                    medOrdersLists.getActiveOrDraftAbusedInAmbCareToInclude(), medOrderAttributes, evalTime,
                    config.getMmeShortLabel());

            double totalMme_avg = mme.getTotalMmeAvg();

            if (totalMme_avg >= 50) {
                avgMmeGe50 = true;
            }

            // check exclusion criterion: on naloxone
            boolean onNaloxone = false;

            List<MedicationOrderStatus> allowedMedicationOrderStatuses = new ArrayList<MedicationOrderStatus>();
            allowedMedicationOrderStatuses.add(MedicationOrderStatus.ACTIVE);
            allowedMedicationOrderStatuses.add(MedicationOrderStatus.DRAFT);

            List<MedicationOrder> activeOrDraftNaloxoneMedOrders = MatchingMedOrders.get(medOrders, meds,
                    allowedMedicationOrderStatuses, focalPersonId, rxNavClient, RxNavValueSet.NALOXONE, config);

            if (activeOrDraftNaloxoneMedOrders.size() >= 1) {
                onNaloxone = true;
            }

            // create output
            Content content = TableUtil.getContent(config.isIncludeAverageMmePerDay(),
                    config.isUseDynamicTableFormat(), mme.getMmeEntries_sorted(), "Active Opioid Rx",
                    config.getMmeShortLabel(), config.getDefaultOpioidRxDaysSupplyEstimate());

            content.setTotal(totalMme_avg);

            String image = OpioidNaloxoneOutpatientImage.createImage(totalMme_avg);
            String imageId = UUID.randomUUID().toString() + ".svg";
            Path folder = null;
            if (config.getImageCache() == null) {
                log.warn("imageCache is not set; using default");
                folder = Paths.get(System.getProperty("user.home"), ".fhir", "image-cache");
            } else {
                folder = Paths.get(config.getImageCache());
            }
            folder.toFile().mkdirs();
            ImageWriter.write(image, Paths.get(folder.toString(), imageId));
            content.setOpioidImage(imageId);

            content.setImageCacheBaseUrl(serverUri.toString());
            content.setNewImage(config.getNewImage());
            content.setExpandImage(config.getExpandImage());
            content.setWarningImage(config.getWarningImage());

            content.setSortedEntries(mme.getMmeEntries_sorted());
            content.setDynamicDetails(config.isUseDynamicTableFormat());
            
            content.setSummaryData("average", mme.getNumEntriesWithUnknownMmeAvg(), totalMme_avg, config.getMmeLongLabel());
            
            if (avgMmeGe50) {
                if (onNaloxone) {
                    boolean returnCard = config.isReturnCardForDebug();
                    String summary = "Average OME >= 50 mg/day, but already on naloxone -- no need for action.";
                    content.setCdcTextData(CDCMessage.AT_LEAST_50_ALREADY_ON_NALOXONE, false);
                    String detail = HtmlBuilder.build(content, new NaloxoneOutpatientConstants());
                    Indicator indicator = Indicator.INFO;
                    return EvalResults.buildEvalResults(returnCard, summary, detail, indicator, source, links);
                } else {
                    boolean returnCard = true;
                    String summary = "Consider offering naloxone.";
                    content.setCdcTextData(CDCMessage.CONSIDER_NALOXONE, false);
                    content.setAdditionalSummary("Consider Naloxone.");
                    // "Consider offering naloxone. Max oral morphine equivalence (OME) is >= 50 mg/day, and no active naloxone prescription on record."
                    String detail = HtmlBuilder.build(content, new NaloxoneOutpatientConstants());
                    // TODO: add table of max OME
                    Indicator indicator = Indicator.WARNING;
                    return EvalResults.buildEvalResults(returnCard, summary, detail, indicator, source, links);
                }
            } else {
                boolean returnCard = config.isReturnCardForDebug();
                String summary = "Patient does not have average OME >= 50 mg/day -- no need for action.";
                content.setCdcTextData(CDCMessage.NONE, false);
                String detail = HtmlBuilder.build(content, new NaloxoneOutpatientConstants());
                Indicator indicator = Indicator.INFO;
                return EvalResults.buildEvalResults(returnCard, summary, detail, indicator, source, links);
            }
        }
    }
}
