package org.opencds.opioid.process;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hl7.fhir.dstu2.model.CodeableConcept;
import org.hl7.fhir.dstu2.model.MedicationOrder;
import org.hl7.fhir.dstu2.model.MedicationOrder.MedicationOrderDispenseRequestComponent;
import org.hl7.fhir.dstu2.model.MedicationOrder.MedicationOrderDosageInstructionComponent;
import org.hl7.fhir.dstu2.model.Period;
import org.hl7.fhir.dstu2.model.Range;
import org.hl7.fhir.dstu2.model.SimpleQuantity;
import org.hl7.fhir.dstu2.model.Timing;
import org.hl7.fhir.dstu2.model.Timing.TimingRepeatComponent;
import org.hl7.fhir.dstu2.model.Timing.UnitsOfTime;
import org.hl7.fhir.dstu2.model.Type;
import org.opencds.RxSig;
import org.opencds.Utils;
import org.opencds.opioid.process.model.MedOrderAttribute;
import org.opencds.opioid.util.DoubleUtil;
import org.opencds.opioid.util.StringUtil;
import org.opencds.opioid.util.fhir.MedOrderUtil;
import org.opencds.opioid.valuesets.DurationSource;
import org.opencds.tools.terminology.rxnav.api.model.RxNormConcept;
import org.opencds.tools.terminology.rxnav.api.model.RxNormSemanticClinicalDrugComponent;
import org.opencds.tools.terminology.rxnav.api.model.RxNormSemanticDrug;
import org.opencds.tools.terminology.rxnav.client.RxNavTerminologyClient;

public class MedOrderAttributeBuilder {
    private static final Log log = LogFactory.getLog(MedOrderAttributeBuilder.class);

    /**
     * Sets the attributes of interest for a med order.  Name -> Object type (description) are as follows:
     *  conversionFactor_ave --> Double (i.e., morphine equivalence; null or 0 if can't compute)
     *  conversionFactor_max --> Double (i.e., morphine equivalence; null or 0 if can't compute).  Same as ave always, except for methadone.
     *  dailyDoseStr_ave --> String (the message to show regarding how the average daily dose was computed, or if it couldn't be computed)
     *  dailyDoseStr_max --> String (the message to show regarding how the maximum daily dose was computed, or if it couldn't be computed)
     *  dispenseQuantity --> SimpleQuantity (not available for draft meds)
     *  drug --> RxNormSemanticDrug (the semantic drug associated with the med order)
     *  endDate --> Date (the date of the med order ending)
     *  endDateCalcMethod --> String (the method used to calculate the date)
     *  endDatePreferDefault --> Date (same as endDate, but where calculated with a preference to default days supply, unless explicitly stated otherwise -- i.e., does not try to infer from quantity and use frequency)
     *  expectedSupplyDuration --> Duration (if there is information in the structured data available on how long the supply is expected to last)
     *  isDraft --> Boolean (whether this is a draft med)
     *  isEndDateEstimated --> Boolean (whether the end date is estimated)
     *  isOralBuprenorphine --> Boolean
     *  isPrn --> Boolean (whether this is a PRN med)
     *  medName --> String (the display name of the medication from MedicationReference, or if not available there, the name from the drug RxNorm code)
     *  mmePerDay_ave --> Double (the average MMEs per day; null or 0 if can't compute)
     *  mmePerDay_max --> Double (the maximum MMEs per day; null or 0 if can't compute)
     *  mmePerDay_aveCalculatedUsingDefaultDaysSupplyEstimate --> Boolean
     *  numRepeatsAllowed --> Integer (the number of times refills may be dispensed)
     *  prescriber --> String (the name of the prescriber)
     *  rxDate --> Date (the date of the med order being prescribed)
     *  rxSig_note (may be null) --> RxSig (the Note on the med order, including all the parsed results from the note)
     *  rxSig_sig --> RxSig (the Sig of the med order, including all the parsed results from that sig)
     *  singleDoseMmeDescription --> String (e.g., "For 1 patch, OME = 63 mg.")
     *  startDate --> Date (the date of the med order starting)
     *
     * @param medOrder
     * @param medCodeableConcept
     * @param medOrderIdToNameValuePairMap
     * @param defaultOpioidRxDaysSupplyEstimate
     *
     * @return medOrderAttribute
     */
    public static MedOrderAttribute build(MedicationOrder medOrder, CodeableConcept medCodeableConcept,
            RxNavTerminologyClient rxNavClient, String mmeShortLabel, int defaultOpioidRxDaysSupplyEstimate) {
        if (medOrder == null) {
            return null;
        }

        MedOrderAttribute attr = new MedOrderAttribute();
        String drugRxCui = medCodeableConcept.getCoding().stream()
                .filter(coding -> coding.getSystem().equals("http://www.nlm.nih.gov/research/umls/rxnorm"))
                .map(coding -> coding.getCode()).findFirst().orElse(null);

        if (drugRxCui == null) {
            log.info("- RxCui unexpectedly null for med: " + medCodeableConcept);
            return null;
        }
        // drug
        RxNormSemanticDrug drug = rxNavClient.getDrug(drugRxCui);
        attr.setDrug(drug);

        // medName
        attr.setMedName(MedOrderUtil.getMedName(medOrder, medCodeableConcept, drug));

        // isDraft
        attr.setDraft(MedOrderUtil.isDraftMedOrder(medOrder));

        // rxPersonName
        attr.setPrescriber(MedOrderUtil.getPrescriberName(medOrder));

        // rxDate
        attr.setRxDate(medOrder.getDateWritten());

        // TODO: if needed, customize prescription text so it is
        // shorter (e.g., PO rather than oral)
        // TODO: if needed, consider more than 1 dosage instruction
        // (currently, will just take first)
        String dosageInstructionText = null;
        String doseStr = "";
        String frequencyStr = "";
        String periodStr = "";
        double doseValue = 0;
        String doseUnit = "";
        boolean isDoseValueFromRange = false;
        int frequency = 1; // default
        boolean isFrequencyFromRange = false;
        boolean isPeriodFromRange = false;
        boolean isNumTimePerDayFromRange = false;
        double numTimesPerDay = 1; // default
        boolean prn = false;

        if (medOrder.getDosageInstruction().size() == 0) {
            log.warn("medOrder " + medOrder.getId() + " does not have dosageInstructions");
        } else {
            MedicationOrderDosageInstructionComponent dosageInstruction = medOrder.getDosageInstruction()
                    .get(0);

            // For dosage, frequency, and period, use the one that would
            // lead to largest MME for calculation purposes,
            // but display the range


            // dosage
            Type dose = dosageInstruction.getDose();
            if (dose instanceof SimpleQuantity) {
                SimpleQuantity simpleQuantity = (SimpleQuantity) dose;
                doseValue = simpleQuantity.getValue().doubleValue();
                doseUnit = simpleQuantity.getUnit();
                if (doseUnit != null) {
                    doseUnit = doseUnit.toLowerCase();
                    if (doseUnit.equals("ug")) {
                        doseUnit = "mcg";
                    }
                }
                doseStr = DoubleUtil.format(doseValue) + " " + doseUnit;
            } else if (dose instanceof Range) {
                Range doseRange = (Range) dose;
                SimpleQuantity lowQuantity = doseRange.getLow();
                SimpleQuantity highQuantity = doseRange.getHigh();
                double lowDoseValue = lowQuantity.getValue().doubleValue();
                doseUnit = lowQuantity.getUnit();
                if (doseUnit != null) {
                    doseUnit = doseUnit.toLowerCase();
                }
                double highDoseValue = highQuantity.getValue().doubleValue();
                if (highDoseValue != lowDoseValue) {
                    isDoseValueFromRange = true;
                }

                doseStr = DoubleUtil.format(lowDoseValue) + "-"
                        + DoubleUtil.format(highDoseValue) + doseUnit;
                doseValue = highDoseValue;
            } else {
                doseValue = 1.0;
            }

            dosageInstructionText = dosageInstruction.getText();

            // timing
            Timing timing = dosageInstruction.getTiming();
            try {
                if (dosageInstruction.getAsNeededBooleanType().getValue().booleanValue() == true) {
                    prn = true;
                }
            } catch (Exception e) {
                // do nothing
            }

            if (timing != null) {
                TimingRepeatComponent repeat = timing.getRepeat();

                try {
                    Period boundsPeriod = repeat.getBoundsPeriod();
                    if (boundsPeriod != null) {
                        // startDate
                        if (boundsPeriod.hasStart()) {
                            attr.setStartDate(boundsPeriod.getStart());
                        }

                        // endDate
                        if (boundsPeriod.hasEnd()) {
                            attr.setEndDate(boundsPeriod.getEnd());
                            attr.setEndDateEstimated(Boolean.FALSE);
                        }
                    }
                } catch (Exception e) {
                    // do nothing
                }

                if (repeat != null) {
                    if (repeat.hasFrequency()) {
                        frequency = repeat.getFrequency();
                        frequencyStr = frequency + "";

                        if (repeat.hasFrequencyMax()) {
                            int frequencyMax = repeat.getFrequencyMax();

                            if ((frequencyMax > 0) && (frequencyMax != frequency)) {
                                isFrequencyFromRange = true;
                                frequencyStr = frequency + "-" + frequencyMax;
                                frequency = frequencyMax;
                            }
                        }
                    }

                    BigDecimal periodBigDecimal = null;
                    UnitsOfTime periodUnits = null;

                    if (repeat.hasPeriod()) {
                        periodBigDecimal = repeat.getPeriod();
                        periodUnits = repeat.getPeriodUnits();

                        if (repeat.hasPeriodMax()) {
                            BigDecimal periodMaxBigDecimal = repeat.getPeriodMax();
                            periodStr = DoubleUtil.format(periodBigDecimal.doubleValue()) + "-"
                                    + DoubleUtil.format(periodMaxBigDecimal.doubleValue())
                                    + periodUnits.toString();
                            if (periodBigDecimal.doubleValue() != periodBigDecimal.doubleValue()) {
                                isPeriodFromRange = true;
                            }
                        } else {
                            periodStr = DoubleUtil.format(periodBigDecimal.doubleValue());
                            if ((periodUnits != null) && (periodUnits.getDisplay() != null)) {
                                periodStr += periodUnits.getDisplay();
                            }
                        }
                    }

                    if ((frequency > 0) && (periodBigDecimal != null) && (periodUnits != null)) {
                        numTimesPerDay = Utils.getNumTimesPerDay(frequency, periodBigDecimal.doubleValue(), periodUnits);
                    }

                    if (isFrequencyFromRange || isPeriodFromRange) {
                        isNumTimePerDayFromRange = true;
                    }
                }
            }
        }

        // dispense quanitty
        MedicationOrderDispenseRequestComponent dispenseRequest = medOrder.getDispenseRequest();
        if (dispenseRequest != null) {
            // numRepeatsAllowed
            if (dispenseRequest.hasNumberOfRepeatsAllowed()) {
                attr.setNumRepeatsAllowed(Integer.valueOf(dispenseRequest.getNumberOfRepeatsAllowed()));
            }

            // dispenseQuantity, dispenseUnits
            if (dispenseRequest.hasQuantity()) {
                attr.setDispenseQuantity(dispenseRequest.getQuantity());
            }

            // expected supply duration
            if (dispenseRequest.hasExpectedSupplyDuration()) {
                attr.setExpectedSupplyDuration(dispenseRequest.getExpectedSupplyDuration());
            }
        }

        // sig
        String sig = null;
        if (dosageInstructionText != null) {
            sig = dosageInstructionText;
        } else {
            if (!Utils.isMedicationOrderMissingDoseQuantity(medOrder)) {
                sig = doseStr + " ";
                if (frequency > 1) {
                    sig += frequencyStr + "x ";
                }
                sig += "q" + periodStr;
                if (prn) {
                    sig += " PRN";
                }
            }
        }

        RxSig rxSig_sig = null;
        RxSig rxSig_note = null;

        if (sig != null) {
            rxSig_sig = new RxSig(sig);
            attr.setRxSigSig(rxSig_sig);
            if (rxSig_sig.isPrn()) {
                prn = true;
            }
        }

        // note
        String note = medOrder.getNote();

        if (note != null) {
            rxSig_note = new RxSig(note);
            attr.setRxSigNote(rxSig_note);
        }

        // create ave and max versions of numTimesPerDay, as they
        // may be updated when using ave/max daily frequency info
        // from Sig
        double numTimesPerDay_ave = numTimesPerDay;
        double numTimesPerDay_max = numTimesPerDay;

        boolean aveNumTimesPerDayTakenFromSig = false;
        boolean maxNumTimesPerDayTakenFromSig = false;

        // if ave daily frequency known, use that for the # times
        // per day (ave) and flag
        if ((rxSig_sig != null) && (rxSig_sig.getAverageDailyFrequency() != null)) {
            numTimesPerDay_ave = rxSig_sig.getAverageDailyFrequency().doubleValue();
            aveNumTimesPerDayTakenFromSig = true;
        }

        // if max daily frequency known, use that for the # times
        // per day (max) and flag
        if ((rxSig_sig != null) && (rxSig_sig.getMaxDailyFrequency() != null)) {
            numTimesPerDay_max = rxSig_sig.getMaxDailyFrequency().doubleValue();
            maxNumTimesPerDayTakenFromSig = true;
        }

        double numTimesPerDayToUseForEndDateEstimate = numTimesPerDay;
        if (maxNumTimesPerDayTakenFromSig) // use max # times per
                                           // day from sig if
                                           // avaialble, for end
                                           // date estimate
        {
            numTimesPerDayToUseForEndDateEstimate = numTimesPerDay_max;
        }
        if (aveNumTimesPerDayTakenFromSig) // overwrite with ave #
                                           // times per day from sig
                                           // if avaialble, for end
                                           // date estimate
        {
            numTimesPerDayToUseForEndDateEstimate = numTimesPerDay_ave;
        }

        // get end date estimate if needed
        if (attr.getEndDate() == null) {
//                    if (Utils.getAttributeValue(medOrderId, "endDate", medOrderIdToNameValuePairMap) == null) {
            Double numTimesPerDay_mayBeNull = null;
            if (!Utils.isMedicationOrderMissingDoseQuantity(medOrder)) {
                numTimesPerDay_mayBeNull = new Double(numTimesPerDayToUseForEndDateEstimate);
            }
            Map<String, Object> endDateAndMethod = Utils.estimateMedOrderEndDate(attr.getStartDate(),
                    attr.getNumRepeatsAllowed(), attr.getDispenseQuantity(), attr.getExpectedSupplyDuration(), rxSig_sig, rxSig_note,
                    defaultOpioidRxDaysSupplyEstimate, numTimesPerDay_mayBeNull, doseValue, doseUnit);
            Date endDate = (Date) endDateAndMethod.get("endDate");
            Date endDatePreferDefault = (Date) endDateAndMethod.get("endDatePreferDefault");
            String endDateCalcMethod = (String) endDateAndMethod.get("endDateCalcMethod");

            attr.setEndDate(endDate);
            attr.setEndDatePreferDefault(endDatePreferDefault);

            if (endDateCalcMethod != null) {
                String endDateCalcMethodDispStr = "";
                boolean isEndDateEstimated = false;

                if (endDateCalcMethod.equals("EndDateInSig")) {
                    endDateCalcMethodDispStr += "end date in sig";
                } else if (endDateCalcMethod.equals("EndDateInNote")) {
                    endDateCalcMethodDispStr += "end date in note to pharmacy";
                } else {

                    if (attr != null && attr.getNumRepeatsAllowed() != null && attr.getNumRepeatsAllowed() > 0) {
                        endDateCalcMethodDispStr = "# refills and ";
                    }

                    if (endDateCalcMethod.equals("DurationInSig")) {
                        endDateCalcMethodDispStr += "days supply in sig";
                    } else if (endDateCalcMethod.equals("DurationInNote")) {
                        endDateCalcMethodDispStr += "days supply in note to pharmacy";
                    } else if (endDateCalcMethod.equals("AverageDailyDoseInSig")) {
                        endDateCalcMethodDispStr += "dispense quantity and average daily dose in sig";
                        isEndDateEstimated = true;
                    } else if (endDateCalcMethod.equals("AverageDailyDoseInNote")) {
                        endDateCalcMethodDispStr += "dispense quantity and average daily dose in note to pharmacy";
                        isEndDateEstimated = true;
                    } else if (endDateCalcMethod.equals("MaxDailyDoseInSig")) {
                        endDateCalcMethodDispStr += "dispense quantity and max daily dose in sig";
                        isEndDateEstimated = true;
                    } else if (endDateCalcMethod.equals("MaxDailyDoseInNote")) {
                        endDateCalcMethodDispStr += "dispense quantity and max daily dose in note to pharmacy";
                        isEndDateEstimated = true;
                    } else if (endDateCalcMethod.equals("ExpectedSupplyDurationFromInput")) {
                        endDateCalcMethodDispStr += "dispense quantity and use frequency";
                        isEndDateEstimated = true;
                    } else if (endDateCalcMethod.equals("DispenseQtyAndUseFrequency")) {
                        endDateCalcMethodDispStr += "dispense quantity and use frequency";
                        isEndDateEstimated = true;
                    }
                    /*
                     * else if (endDateCalcMethod.equals(
                     * "DispenseQtyAndUseFrequencyFromSigWithoutCaution"
                     * )) { endDateCalcMethodDispStr +=
                     * "dispense quantity and use frequency";
                     * isEndDateEstimated = true; }
                     */
                    else if (endDateCalcMethod.equals("DefaultRxDaysSupplyEstimate")) {
                        endDateCalcMethodDispStr += "presumed " + defaultOpioidRxDaysSupplyEstimate
                                + "d supply";
                        isEndDateEstimated = true;
                    }
                }

                attr.setEndDateCalcMethod(endDateCalcMethodDispStr);
                attr.setEndDateEstimated(isEndDateEstimated);
            }
        }

        // prn
        attr.setPrn(prn);

        boolean isNumTimePerDayFromRangeOrPrn = false;
        if (isNumTimePerDayFromRange || prn) {
            isNumTimePerDayFromRangeOrPrn = true;
        }

        // dose form
        RxNormConcept doseFormRxNormConcept = drug.getDoseFormConcept();

        // dose form groups
        Set<String> doseFormGroupRxCuis = getDoseFormGroupRxCuis(drug);

            // scdc
        Set<RxNormSemanticClinicalDrugComponent> scdcs = drug.getSemanticClinicalDrugComponents();

        // NOTE ASSUMPTION: an opioid med only contains one opioid
        // ingredient (i.e., no opioid med that contains 2+ opioids
        // of different types, as would over-ride previous
        // attributes)
        for (RxNormSemanticClinicalDrugComponent scdc : scdcs) {
            Double strengthValueObj = scdc.getStrengthValue();
            double strengthValue = 0;
            if (strengthValueObj != null) {
                strengthValue = strengthValueObj.doubleValue();
            }
            String strengthUnit = scdc.getStrengthUnit();
            if (strengthUnit != null) {
                strengthUnit = strengthUnit.toLowerCase();
            }

            RxNormConcept ingredientRxNormConcept = scdc.getIngredientRxNormConcept();

            if ((ingredientRxNormConcept != null) && (strengthUnit != null)
                    && (doseFormRxNormConcept != null) && (doseFormGroupRxCuis.size() > 0)) {
                String doseFormRxCui = doseFormRxNormConcept.getRxCui();
                String doseFormName = doseFormRxNormConcept.getName();

                String ingredientRxCui = ingredientRxNormConcept.getRxCui();
                // if this is a non-surgical opioid scdc
                if (ingredientRxCui.equals("10689") || ingredientRxCui.equals("1819")
                        || ingredientRxCui.equals("1841") || ingredientRxCui.equals("23088")
                        || ingredientRxCui.equals("237005") || ingredientRxCui.equals("2670")
                        || ingredientRxCui.equals("3423") || ingredientRxCui.equals("4337")
                        || ingredientRxCui.equals("5489") || ingredientRxCui.equals("6378")
                        || ingredientRxCui.equals("6754") || ingredientRxCui.equals("6813")
                        || ingredientRxCui.equals("6814") || ingredientRxCui.equals("7052")
                        || ingredientRxCui.equals("7804") || ingredientRxCui.equals("7814")
                        || ingredientRxCui.equals("787390") || ingredientRxCui.equals("8001")
                        || ingredientRxCui.equals("8785") || ingredientRxCui.equals("7238")) {
                    String nonSurgicalOpioidIngredientName = ingredientRxNormConcept.getName();

                    // algorithm for daily dose:
                    // - in all cases: if dose or strength < 0.1 and
                    // unit starts with mg, convert to mcg

                    double dailyDose_ave = 0;
                    double dailyDose_max = 0;
                    String dailyDoseStr_ave = "Unable to calculate due to free-text sig.";
                    String dailyDoseStr_max = "Unable to calculate due to free-text sig.";

                    double conversionFactor_ave = 0;
                    double conversionFactor_max = 0;

                    double mmePerDay_ave = 0;
                    double mmePerDay_max = 0;

                    if ((strengthValue < 0.1) && (strengthUnit.startsWith("mg"))) {
                        strengthValue = strengthValue * 1000;
                        if (strengthUnit.equals("mg")) {
                            strengthUnit = "mcg";
                        } else {
                            strengthUnit = "mcg" + strengthUnit.substring(2);
                        }
                    }

                    if ((doseValue < 0.1) && (doseUnit.startsWith("mg"))) {
                        doseValue = doseValue * 1000;
                        if (doseUnit.equals("mg")) {
                            doseUnit = "mcg";
                        } else {
                            doseUnit = "mcg" + doseUnit.substring(2);
                        }
                    }

                    // create ave and max versions of dose units, as
                    // they may be updated when using ave/max daily
                    // dose info
                    String doseUnit_ave = doseUnit;
                    String doseUnit_max = doseUnit;

                    // get/set single-dose MME
                    String singleDoseMmeDescription = SingleDoseMmeDescription.get(doseFormRxCui,
                            ingredientRxCui, strengthValue, strengthUnit, mmeShortLabel, doseFormGroupRxCuis);
                    attr.setSingleDoseMmeDescription(singleDoseMmeDescription);

                    // if patch --> daily dose (both ave and max) =
                    // dose value (e.g, number patches with
                    // doseQuantity unit = "patch") * per-hour
                    // strength
                    // transdermal system / patch
                    if (doseFormRxCui.equals("316987")) {    // test 014
                        // any reliable sig info would have already
                        // been parsed in
                        if (Utils.isMedicationOrderMissingDoseQuantity(medOrder)) {
                            // Unable to calculate due to free-text
                            // sig.
                            dailyDose_ave = 0;
                            dailyDose_max = dailyDose_ave;
                            conversionFactor_ave = MedOrderConversionUtil.getConversionFactor(
                                    ingredientRxCui, doseFormRxCui, dailyDose_ave, strengthUnit, doseFormGroupRxCuis);
                            conversionFactor_max = conversionFactor_ave;
                            dailyDoseStr_ave = "Unable to calculate due to free-text sig.";
                            dailyDoseStr_max = dailyDoseStr_ave;
                        } else {
                            // Daily dose: Fentanyl patch: 1 * 0.1
                            // mg/hr = 0.1 mg/hr (note patches
                            // rarely, if ever, seem to be rx'd with
                            // a range of dosage)
                            // (** UNLESS dose units start with mg or mcg --> then that is the actual dose **)
                            if ((doseUnit != null) && (doseUnit.startsWith("mg") || doseUnit.startsWith("mcg")))  // test 015
                            {
                                dailyDose_ave = doseValue;
                                dailyDose_max = dailyDose_ave;
                                conversionFactor_ave = MedOrderConversionUtil.getConversionFactor(
                                        ingredientRxCui, doseFormRxCui, dailyDose_ave, doseUnit, doseFormGroupRxCuis);
                                conversionFactor_max = conversionFactor_ave;
                                dailyDoseStr_ave = nonSurgicalOpioidIngredientName + " patch: "
                                        + StringUtil.getLabelIfTrue("max ", isDoseValueFromRange)
                                        + DoubleUtil.format(dailyDose_ave) + " " + StringUtil.getStrengthUnitPrefixInLowerCase(doseUnit) + ".";
                                dailyDoseStr_max = dailyDoseStr_ave;
                            }
                            else // test 016
                            {
                                dailyDose_ave = doseValue * strengthValue;
                                dailyDose_max = dailyDose_ave;
                                conversionFactor_ave = MedOrderConversionUtil.getConversionFactor(
                                        ingredientRxCui, doseFormRxCui, dailyDose_ave, strengthUnit, doseFormGroupRxCuis);
                                conversionFactor_max = conversionFactor_ave;
                                dailyDoseStr_ave = nonSurgicalOpioidIngredientName + " patch: "
                                        + StringUtil.getLabelIfTrue("max ", isDoseValueFromRange)
                                        + DoubleUtil.format(doseValue) + " * "
                                        + DoubleUtil.format(strengthValue) + " " + strengthUnit + " = "
                                        + DoubleUtil.format(dailyDose_ave) + " " + strengthUnit + ".";
                                dailyDoseStr_max = dailyDoseStr_ave;
                            }
                        }
                    }
                    // IV
                    else if (doseFormGroupRxCuis.contains("1151126")) // Injectable drug; test 017
                    {
                        // Unable to calculate for injectable drug
                        dailyDose_ave = 0;
                        dailyDose_max = dailyDose_ave;
                        conversionFactor_ave = 0;
                        conversionFactor_max = conversionFactor_ave;
                        dailyDoseStr_ave = "Unable to calculate " + mmeShortLabel + " for injectable drug.";
                        dailyDoseStr_max = dailyDoseStr_ave;
                    }
                    // Implantable
                    else if (doseFormGroupRxCuis.contains("1151136")) // Implantable drug
                    {
                        // Unable to calculate for implantable drug
                        dailyDose_ave = 0;
                        dailyDose_max = dailyDose_ave;
                        conversionFactor_ave = 0;
                        conversionFactor_max = conversionFactor_ave;
                        dailyDoseStr_ave = "Unable to calculate " + mmeShortLabel + " for implantable drug.";
                        dailyDoseStr_max = dailyDoseStr_ave;
                    }
                    // other than transdermal system / patch or IV,
                    // which we can process
                    else if ((doseFormRxCui.equals("858080")) || (doseFormRxCui.equals("970789"))
                            || (doseFormRxCui.equals("316943")) || (doseFormRxCui.equals("316945"))
                            || (doseFormRxCui.equals("346163")) || (doseFormRxCui.equals("126542"))
                            || (doseFormRxCui.equals("316965")) || (doseFormRxCui.equals("316992"))
                            || (doseFormRxCui.equals("704866")) || (doseFormRxCui.equals("317541"))
                            || (doseFormRxCui.equals("317007")) || (doseFormRxCui.equals("316942"))
                            || (doseFormRxCui.equals("1535727")) || (doseFormRxCui.equals("316946"))
                            || (doseFormRxCui.equals("316968")) || (doseFormRxCui.equals("316969"))
                            || (doseFormRxCui.equals("1861409"))) {
                        // 858080 -- buccal film
                        // 970789 -- buccal tablet
                        // 316943 -- extended release oral capsule

                        // 316945 -- extended release oral tablet
                        // 346163 -- mucosal spray
                        // 126542 -- nasal spray

                        // 316965 -- oral capsule
                        // 316992 -- oral lozange
                        // 704866 -- oral strip

                        // 317541 -- oral tablet
                        // 317007 -- sublingual tablet

                        // 316942 -- Disintegrating Oral Tablet

                        // 1535727 -- Effervescent Oral Tablet
                        // 316946 -- Extended Release Suspension
                        // 316968 -- Oral Solution

                        // 316969 -- oral suspension
                        // 1861409 -- tablet for oral suspension
                        boolean dailyDoseCalculated_ave = false;

                        // set oral buprenorphine flag
                        // (buprenorphine and not nasal spray)
                        if ((ingredientRxCui.equals("1819")) && (!doseFormRxCui.equals("126542"))) {
                            attr.setOralBuprenorphine(true);
                        }

                        // if days supply known from sig (or if not
                        // there, in Rx note) --> daily dose (ave) =
                        // # dispense * strength / days supply
                        Integer durationFromSig = null;
                        String durationUnitsFromSig = null;
                        Integer durationFromNote = null;
                        String durationUnitsFromNote = null;

                        if (rxSig_sig != null) {
                            durationFromSig = rxSig_sig.getDuration();
                            durationUnitsFromSig = rxSig_sig.getDurationTimeUnits();
                        }

                        if (rxSig_note != null) {
                            durationFromNote = rxSig_note.getDuration();
                            durationUnitsFromNote = rxSig_note.getDurationTimeUnits();
                        }

                        if (((durationFromSig != null) && (durationUnitsFromSig != null))
                                || ((durationFromNote != null) && (durationUnitsFromNote != null))) {
                            boolean isDurationFromSig = false;
                            Integer duration = null;
                            String durationUnits = null;

                            if ((durationFromSig != null) && (durationUnitsFromSig != null)) {
                                isDurationFromSig = true;
                                duration = durationFromSig;
                                durationUnits = durationUnitsFromSig;
                            } else {
                                duration = durationFromNote;
                                durationUnits = durationUnitsFromNote;
                            }

                            int daysSupply = -1;
                            if (durationUnits.equals("d")) {
                                daysSupply = duration;
                            } else if (durationUnits.equals("mo")) {
                                // assumes 30 d in mo
                                daysSupply = duration.intValue() * 30;
                            } else if (durationUnits.equals("wk")) {
                                daysSupply = duration.intValue() * 7;
                            }

                            DurationSource durationSource = null;
                            if (isDurationFromSig) {
                                durationSource = DurationSource.SIG;
                            } else {
                                durationSource = DurationSource.NOTE;
                            }

                            if (daysSupply > 0) {
                                Map<String, Object> doseInfo = DailyDoseInfoFromDispenseQuantityAndDaysSupply.get(
                                        attr.getDispenseQuantity(), ingredientRxCui, nonSurgicalOpioidIngredientName,
                                        doseFormRxCui, doseFormName, strengthValue, strengthUnit,
                                        daysSupply, durationSource, doseFormGroupRxCuis);
                                if (doseInfo != null) {
                                    dailyDose_ave = ((Double) doseInfo.get("dailyDose")).doubleValue();
                                    dailyDoseStr_ave = (String) doseInfo.get("dailyDoseStr");
                                    conversionFactor_ave = ((Double) doseInfo.get("conversionFactor"))
                                            .doubleValue();
                                    dailyDoseCalculated_ave = true;
                                }
                            }
                        }

                        boolean aveDailyDoseFromSigUsedForDailyDose = false;
                        boolean maxDailyDoseFromSigUsedForDailyDose = false;

                        // if ave dosage per day known, populate
                        // that as the daily dose (ave) and flag
                        // source of dosage
                        if ((rxSig_sig != null) && (rxSig_sig.getAverageDailyDose() != null)) {
                            dailyDose_ave = rxSig_sig.getAverageDailyDose();
                            doseUnit_ave = rxSig_sig.getAverageDailyDoseUnits();
                            aveDailyDoseFromSigUsedForDailyDose = true;
                        }

                        // if max dosage per day known, populate
                        // that as the daily dose (max) and flag
                        // source of dosage
                        if ((rxSig_sig != null) &&(rxSig_sig.getMaxDailyDose() != null)) {
                            dailyDose_max = rxSig_sig.getMaxDailyDose();
                            doseUnit_max = rxSig_sig.getMaxDailyDoseUnits();
                            maxDailyDoseFromSigUsedForDailyDose = true;
                        }

                        // for average daily dose, if not already
                        // calculated, attempt to set if ave dosage
                        // per day or ave # times per day known from
                        // sig
                        if (!dailyDoseCalculated_ave) {
                            boolean isMaxVsAve = false;
                            double dailyDose = dailyDose_ave;
                            String doseUnitToUse = doseUnit_ave;
                            boolean dailyDoseFromSigUsedForDailyDose = aveDailyDoseFromSigUsedForDailyDose;
                            double numTimesPerDayToUse = numTimesPerDay_ave;
                            boolean numTimesPerDayTakenFromSig = aveNumTimesPerDayTakenFromSig;

                            Map<String, Object> doseInfo = DailyDoseInfoForNonPatchNonIvMed.get(isMaxVsAve, dailyDose,
                                    dailyDoseFromSigUsedForDailyDose, doseValue, doseUnitToUse, numTimesPerDayToUse,
                                    numTimesPerDayTakenFromSig, isNumTimePerDayFromRangeOrPrn, isDoseValueFromRange,
                                    nonSurgicalOpioidIngredientName, doseFormName, ingredientRxCui, doseFormRxCui,
                                    strengthValue, strengthUnit, doseFormGroupRxCuis);

                            if (doseInfo != null) {
                                dailyDose_ave = ((Double) doseInfo.get("dailyDose")).doubleValue();
                                dailyDoseStr_ave = (String) doseInfo.get("dailyDoseStr");
                                conversionFactor_ave = ((Double) doseInfo.get("conversionFactor")).doubleValue();
                                dailyDoseCalculated_ave = true;
                            }
                        }

                        // for max daily dose, attempt to set if max
                        // dosage per day known from sig or if
                        // structured information available

                        if ((rxSig_sig != null) && ((rxSig_sig.getMaxDailyDose() != null))
                                || (!Utils.isMedicationOrderMissingDoseQuantity(medOrder))) {
                            boolean isMaxVsAve = true;
                            double dailyDose = dailyDose_max;
                            String doseUnitToUse = doseUnit_max;
                            boolean dailyDoseFromSigUsedForDailyDose = maxDailyDoseFromSigUsedForDailyDose;
                            double numTimesPerDayToUse = numTimesPerDay_max;
                            boolean numTimesPerDayTakenFromSig = maxNumTimesPerDayTakenFromSig;

                            Map<String, Object> doseInfo = DailyDoseInfoForNonPatchNonIvMed.get(isMaxVsAve, dailyDose,
                                    dailyDoseFromSigUsedForDailyDose, doseValue, doseUnitToUse, numTimesPerDayToUse,
                                    numTimesPerDayTakenFromSig, isNumTimePerDayFromRangeOrPrn, isDoseValueFromRange,
                                    nonSurgicalOpioidIngredientName, doseFormName, ingredientRxCui, doseFormRxCui,
                                    strengthValue, strengthUnit, doseFormGroupRxCuis);

                            if (doseInfo != null) {
                                dailyDose_max = ((Double) doseInfo.get("dailyDose")).doubleValue();
                                dailyDoseStr_max = (String) doseInfo.get("dailyDoseStr");
                                conversionFactor_max = ((Double) doseInfo.get("conversionFactor")).doubleValue();
                            }
                        }

                        // if not yet calculated, attempt daily dose
                        // (ave) = # dispense * strength / default
                        // days supply (should generally work as
                        // long as dispense quantity available)
                        if (!dailyDoseCalculated_ave) {
                            Map<String, Object> doseInfo = DailyDoseInfoFromDispenseQuantityAndDaysSupply.get(
                                    attr.getDispenseQuantity(), ingredientRxCui, nonSurgicalOpioidIngredientName,
                                    doseFormRxCui, doseFormName, strengthValue, strengthUnit,
                                    defaultOpioidRxDaysSupplyEstimate, DurationSource.DEFAULT_ESTIMATE,
                                    doseFormGroupRxCuis);
                            if (doseInfo != null) {
                                dailyDose_ave = ((Double) doseInfo.get("dailyDose")).doubleValue();
                                dailyDoseStr_ave = (String) doseInfo.get("dailyDoseStr");
                                conversionFactor_ave = ((Double) doseInfo.get("conversionFactor")).doubleValue();
                                dailyDoseCalculated_ave = true;
                                attr.setMmePerDayAvgCalcUsingDefaultDaySupplyEst(true);
                            }
                        }
                    }

                    // reset if IV and got calculated through other attempts
                    // Injectable drug
                    if (doseFormGroupRxCuis.contains("1151126")) {
                        // Unable to calculate for injectable drug
                        dailyDose_ave = 0;
                        dailyDose_max = dailyDose_ave;
                        conversionFactor_ave = 0;
                        conversionFactor_max = conversionFactor_ave;
                        dailyDoseStr_ave = "Unable to calculate " + mmeShortLabel + " for injectable drug.";
                        dailyDoseStr_max = dailyDoseStr_ave;
                    } else if (doseFormGroupRxCuis.contains("1151136")) {
                        // Implantable drug
                        // Unable to calculate for implantable drug
                        dailyDose_ave = 0;
                        dailyDose_max = dailyDose_ave;
                        conversionFactor_ave = 0;
                        conversionFactor_max = conversionFactor_ave;
                        dailyDoseStr_ave = "Unable to calculate " + mmeShortLabel + " for implantable drug.";
                        dailyDoseStr_max = dailyDoseStr_ave;
                    }

                    mmePerDay_ave = dailyDose_ave * conversionFactor_ave;
                    mmePerDay_max = dailyDose_max * conversionFactor_max;

                    // in case where max OME/CF is available but average OME/CF isn't (should only happen in
                    // cases where dispense quantity is not available, such as some EHRs
                    // for draft meds, set average OME/CF to the max OME/CF and note what was done
                    if ((mmePerDay_ave <= 0) && (conversionFactor_ave <= 0) && (mmePerDay_max > 0)
                            && (conversionFactor_max > 0)) {
                        mmePerDay_ave = mmePerDay_max;
                        ;
                        conversionFactor_ave = conversionFactor_max;
                        dailyDoseStr_ave = "<span style=\"font-weight: 600;\">";

                        // in some EHRs, cannot get dispense quantity or Sig
                        // (which may indicate duration) for new Rxs, so can't
                        // calculate average MME
                        if ((attr.isDraft()) && (attr.getDispenseQuantity() == null)) {
                            dailyDoseStr_ave += " unavailable for new Rx; ";
                        }
                        dailyDoseStr_ave += "assuming same as max daily dose</span>";
                    }

                    // If conversion factors not set, and not IV/implantable or methadone, set them.  Daily dose is immaterial
                    // outside of methadone.  Patches should have already been set above.
                    if ((conversionFactor_ave == 0) && (! doseFormGroupRxCuis.contains("1151126")) && (! doseFormGroupRxCuis.contains("1151136")) && (! ingredientRxCui.equals("6813")))
                    {
                        conversionFactor_ave = MedOrderConversionUtil.getConversionFactor(
                                ingredientRxCui, doseFormRxCui, dailyDose_ave, strengthUnit, doseFormGroupRxCuis);
                    }
                    if ((conversionFactor_max == 0) && (! doseFormGroupRxCuis.contains("1151126")) && (! doseFormGroupRxCuis.contains("1151136")) && (! ingredientRxCui.equals("6813")))
                    {
                        conversionFactor_max = MedOrderConversionUtil.getConversionFactor(
                                ingredientRxCui, doseFormRxCui, dailyDose_max, strengthUnit, doseFormGroupRxCuis);
                    }

                    // add attributes
                    if (mmePerDay_ave > 0) {
                        attr.setMmePerDayAvg(mmePerDay_ave);
                    }

                    if (mmePerDay_max > 0) {
                        attr.setMmePerDayMax(mmePerDay_max);
                    }

                    if (conversionFactor_ave > 0) {
                        attr.setConversionFactorAvg(conversionFactor_ave);
                    }

                    if (conversionFactor_max > 0) {
                        attr.setConversionFactorMax(conversionFactor_max);
                    }

                    attr.setDailyDoseAvg(dailyDoseStr_ave);
                    attr.setDailyDoseMax(dailyDoseStr_max);
                }
            }
        }
        return attr;
    }

    private static Set<String> getDoseFormGroupRxCuis(RxNormSemanticDrug drug) {
        Set<String> doseFormGroupRxCuis = new HashSet<>();
        Set<RxNormConcept> doseFormGroupConcepts = drug.getDoseFormGroupConcepts();
        if (doseFormGroupConcepts != null) {
            for (RxNormConcept doseFormGroupConcept : doseFormGroupConcepts) {
                doseFormGroupRxCuis.add(doseFormGroupConcept.getRxCui());
            }
        }
        return doseFormGroupRxCuis;
    }



}
