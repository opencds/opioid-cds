package org.opencds.opioid.process;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.hl7.fhir.dstu2.model.SimpleQuantity;
import org.opencds.opioid.util.DoubleUtil;
import org.opencds.opioid.util.StringUtil;
import org.opencds.opioid.valuesets.DurationSource;

public class DailyDoseInfoFromDispenseQuantityAndDaysSupply {
    /**
     * Returns null if cannot process. Otherwise returns map with: dailyDose -->
     * Double dailyDoseStr --> String conversionFactor --> Double
     *
     * @param dispenseQuantity
     * @param ingredientRxCui
     * @param nonSurgicalOpioidIngredientName
     * @param doseFormRxCui
     * @param doseFormName
     * @param strengthValue
     * @param strengthUnit
     * @param daysSupply
     * @param durationSource
     * @param doseFormGroupRxCuis
     * @return
     */
    public static Map<String, Object> get(SimpleQuantity dispenseQuantity, String ingredientRxCui,
            String nonSurgicalOpioidIngredientName, String doseFormRxCui, String doseFormName, double strengthValue,
            String strengthUnit, int daysSupply, DurationSource durationSource, Set<String> doseFormGroupRxCuis) {
        if (dispenseQuantity != null) {
            String dispenseUnits = dispenseQuantity.getUnit();
            BigDecimal dispenseQty = dispenseQuantity.getValue();

            Map<String, Object> mapToReturn = new HashMap<String, Object>();

            String durationSourceStr = "";
            if (durationSource == DurationSource.SIG) {
                durationSourceStr = "from sig";
            } else if (durationSource == DurationSource.NOTE) {
                durationSourceStr = "from Rx note";
            } else if (durationSource == DurationSource.DEFAULT_ESTIMATE) {
                durationSourceStr = "<span style=\"font-weight: 700\">assumed</span>";
            }

            if ((dispenseUnits != null) && dispenseUnits.equalsIgnoreCase("ml") && (strengthUnit.endsWith("/ml")))  // test 018
            {
                // 240 ml dispense * 2mg/ml / 30d supply (from sig/from Rx note/**assumed**) = 16 mg
                double dailyDose = dispenseQty.doubleValue() * strengthValue / daysSupply;
                String dailyDoseStr = nonSurgicalOpioidIngredientName + " " + doseFormName + " "
                        + DoubleUtil.format(dispenseQty) + " " + dispenseUnits + " dispense * "
                        + DoubleUtil.format(strengthValue) + " " + strengthUnit + " / " + daysSupply
                        + "d supply (" + durationSourceStr + ") = " + DoubleUtil.format(dailyDose) + " " + StringUtil.getStrengthUnitPrefixInLowerCase(strengthUnit) + ".";
                double conversionFactor = MedOrderConversionUtil.getConversionFactor(ingredientRxCui, doseFormRxCui,
                        dailyDose, strengthUnit, doseFormGroupRxCuis);

                mapToReturn.put("dailyDose", new Double(dailyDose));
                mapToReturn.put("dailyDoseStr", dailyDoseStr);
                mapToReturn.put("conversionFactor", new Double(conversionFactor));
                return mapToReturn;
            }
            else if ((dispenseUnits != null) && (dispenseUnits.equalsIgnoreCase("mg"))) // test 019
            {
                // 240 mg dispense / 30d supply (from sig/from Rx note/**assumed**) = 8 mg
                double dailyDose = dispenseQty.doubleValue() / daysSupply;
                String dailyDoseStr = nonSurgicalOpioidIngredientName + " " + doseFormName + " "
                        + DoubleUtil.format(dispenseQty) + " " + dispenseUnits + " dispense / " + daysSupply
                        + "d supply (" + durationSourceStr + ") = " + DoubleUtil.format(dailyDose) + " " + StringUtil.getStrengthUnitPrefixInLowerCase(strengthUnit) + ".";
                double conversionFactor = MedOrderConversionUtil.getConversionFactor(ingredientRxCui, doseFormRxCui,
                        dailyDose, dispenseUnits, doseFormGroupRxCuis);

                mapToReturn.put("dailyDose", new Double(dailyDose));
                mapToReturn.put("dailyDoseStr", dailyDoseStr);
                mapToReturn.put("conversionFactor", new Double(conversionFactor));
                return mapToReturn;
            }
            else { // test 020

                // 120 dispense * 5 mg / 30d supply (from sig/from Rx
                // note/**assumed**) = 20 mg
                double dailyDose = dispenseQty.doubleValue() * strengthValue / daysSupply;
                String dailyDoseStr = nonSurgicalOpioidIngredientName + " " + doseFormName + " "
                        + DoubleUtil.format(dispenseQty) + " dispense * "
                        + DoubleUtil.format(strengthValue) + " " + strengthUnit + " / " + daysSupply
                        + "d supply (" + durationSourceStr + ") = " + DoubleUtil.format(dailyDose) + " " + StringUtil.getStrengthUnitPrefixInLowerCase(strengthUnit) + ".";
                double conversionFactor = MedOrderConversionUtil.getConversionFactor(ingredientRxCui, doseFormRxCui,
                        dailyDose, strengthUnit, doseFormGroupRxCuis);

                mapToReturn.put("dailyDose", new Double(dailyDose));
                mapToReturn.put("dailyDoseStr", dailyDoseStr);
                mapToReturn.put("conversionFactor", new Double(conversionFactor));
                return mapToReturn;
            }
        }
        return null;
    }


}
