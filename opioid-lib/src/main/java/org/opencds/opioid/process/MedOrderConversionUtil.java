package org.opencds.opioid.process;

import java.util.Set;

public class MedOrderConversionUtil {

    /**
     * Returns 0 if cannot convert.
     * 
     * @param opioidIngredientRxCui
     * @param doseFormRxCui
     * @param dailyDose
     *            Needed for methadone
     * @param doseOrStrengthUnitStartingWithMgOrMcg
     *            Needed to distinguish between mg and mcg-based conversion
     *            factors
     * @param doseFormGroupRxCuis
     * @return
     */
    public static double getConversionFactor(String opioidIngredientRxCui, String doseFormRxCui, double dailyDose,
            String doseOrStrengthUnitStartingWithMgOrMcg, Set<String> doseFormGroupRxCuis) {
        double conversionFactor = 0;

        // assume mg-based for setting default conversion factors

        if ((opioidIngredientRxCui != null) && (doseFormRxCui != null)
        		&& (! doseFormGroupRxCuis.contains("1151126")) // Injectable drug
        		&& (! doseFormGroupRxCuis.contains("1151136")) // Implantable drug
                && (doseOrStrengthUnitStartingWithMgOrMcg != null)
                && ((doseOrStrengthUnitStartingWithMgOrMcg.startsWith("mg"))
                        || (doseOrStrengthUnitStartingWithMgOrMcg.startsWith("mcg")))) {
            // buprenorphine
            if (opioidIngredientRxCui.equals("1819")) {
                // transdermal system / patch
                if (doseFormRxCui.equals("316987")) {
                    conversionFactor = 1.8 * 1000;
                } else {
                    conversionFactor = 30;
                }
            }
            // Butorphanol
            else if (opioidIngredientRxCui.equals("1841")) {
                conversionFactor = 7;
            }
            // Codeine
            else if (opioidIngredientRxCui.equals("2670")) {
                conversionFactor = 0.15;
            }
            // dihydrocodeine
            else if (opioidIngredientRxCui.equals("23088")) {
                conversionFactor = 0.25;
            }
            // Fentanyl
            else if (opioidIngredientRxCui.equals("4337")) {
                // transdermal system / patch
                if (doseFormRxCui.equals("316987")) {
                    conversionFactor = 2.4 * 1000;
                }
                // 970789 = Buccal Tablet; 317007 = Sublingual Tablet; 316992 =
                // Oral Lozenge
                else if ((doseFormRxCui.equals("970789")) || (doseFormRxCui.equals("317007"))
                        || (doseFormRxCui.equals("316992"))) {
                    conversionFactor = 0.13 * 1000;
                }
                // 858080 = Buccal Film; 346163 = Mucosal Spray
                else if ((doseFormRxCui.equals("858080")) || (doseFormRxCui.equals("346163"))) {
                    conversionFactor = 0.18 * 1000;
                }
                // 126542 = Nasal Spray
                else if ((doseFormRxCui.equals("126542"))) {
                    conversionFactor = 0.16 * 1000;
                }
            }
            // Hydrocodone
            else if (opioidIngredientRxCui.equals("5489")) {
                conversionFactor = 1;
            }
            // Hydromorphone
            else if (opioidIngredientRxCui.equals("3423")) {
                conversionFactor = 4;
            }
            // Levomethadyl
            else if (opioidIngredientRxCui.equals("237005")) {
                conversionFactor = 8;
            }
            // Levorphanol
            else if (opioidIngredientRxCui.equals("6378")) {
                conversionFactor = 11;
            }
            // Meperidine
            else if (opioidIngredientRxCui.equals("6754")) {
                conversionFactor = 0.1;
            }
            // Methadone
            else if (opioidIngredientRxCui.equals("6813")) {
                if (dailyDose > 60) {
                    conversionFactor = 12;
                } else if (dailyDose > 40) {
                    conversionFactor = 10;
                } else if (dailyDose > 20) {
                    conversionFactor = 8;
                } else {
                    conversionFactor = 4;
                }

                /*
                 * 1-20 mg/day 4 21-40 mg/day 8 41-60 mg/day 10 >= 61-80 mg/day
                 * 12
                 */
            }
            // Morphine
            else if (opioidIngredientRxCui.equals("7052")) {
                conversionFactor = 1;
            }
            // Oxycodone
            else if (opioidIngredientRxCui.equals("7804")) {
                conversionFactor = 1.5;
            }
            // Oxymorphone
            else if (opioidIngredientRxCui.equals("7814")) {
                conversionFactor = 3;
            }
            // Pentazocine
            else if (opioidIngredientRxCui.equals("8001")) {
                conversionFactor = 0.37;
            }
            // tapentadol
            else if (opioidIngredientRxCui.equals("787390")) {
                conversionFactor = 0.4;
            }
            // Tramadol
            else if (opioidIngredientRxCui.equals("10689")) {
                conversionFactor = 0.1;
            }

            if (doseOrStrengthUnitStartingWithMgOrMcg.startsWith("mcg")) {
                conversionFactor = conversionFactor / 1000;
            }
        }
        return conversionFactor;
    }
}
