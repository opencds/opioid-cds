package org.opencds.opioid.process.model;

import java.util.ArrayList;
import java.util.List;

import org.hl7.fhir.dstu2.model.MedicationOrder;

public class MedOrdersLists {
    private List<MedicationOrder> draftAbusedInAmbCare = new ArrayList<>();
    private List<MedicationOrder> draftBenzodiazepine = new ArrayList<>();
    private List<MedicationOrder> draftIndicatingEndOfLife = new ArrayList<>();

    private List<MedicationOrder> activeAbusedInAmbCare = new ArrayList<>();
    private List<MedicationOrder> activeBenzodiazepine = new ArrayList<>();
    private List<MedicationOrder> activeIndicatingEndOfLife = new ArrayList<>();

    private List<MedicationOrder> completedOrStoppedAbusedInAmbCare = new ArrayList<>();

    public boolean hasDraftAbusedInAmbCare() {
        return draftAbusedInAmbCare.size() > 0;
    }

    public List<MedicationOrder> getDraftAbusedInAmbCare() {
        return draftAbusedInAmbCare;
    }

    public void addDraftAbusedInAmbCare(MedicationOrder medOrder) {
        if (medOrder != null) {
            this.draftAbusedInAmbCare.add(medOrder);
        }
    }

    public void addDraftAbusedInAmbCare(List<MedicationOrder> medOrders) {
        if (medOrders != null) {
            this.draftAbusedInAmbCare.addAll(medOrders);
        }
    }

    /* */

    public boolean hasDraftBenzodiazepine() {
        return draftBenzodiazepine.size() > 0;
    }

    public List<MedicationOrder> getDraftBenzodiazepine() {
        return draftBenzodiazepine;
    }

    public void addDraftBenzodiazepine(MedicationOrder medOrder) {
        if (medOrder != null) {
            this.draftBenzodiazepine.add(medOrder);
        }
    }

    public void addDraftBenzodiazepine(List<MedicationOrder> medOrders) {
        if (medOrders != null) {
            this.draftBenzodiazepine.addAll(medOrders);
        }
    }


    /* */

    public boolean hasDraftIndicatingEndOfLife() {
        return draftIndicatingEndOfLife.size() > 0;
    }

    public List<MedicationOrder> getDraftIndicatingEndOfLife() {
        return draftIndicatingEndOfLife;
    }

    public void addDraftIndicatingEndOfLife(MedicationOrder medOrder) {
        if (medOrder != null) {
            this.draftIndicatingEndOfLife.add(medOrder);
        }
    }

    public void addDraftIndicatingEndOfLife(List<MedicationOrder> medOrders) {
        if (medOrders != null) {
            this.draftIndicatingEndOfLife.addAll(medOrders);
        }
    }

    /* */

    public boolean hasActiveAbusedInAmbCare() {
        return activeAbusedInAmbCare.size() > 0;
    }

    public List<MedicationOrder> getActiveAbusedInAmbCare() {
        return activeAbusedInAmbCare;
    }

    public void addActiveAbusedInAmbCare(MedicationOrder medOrder) {
        if (medOrder != null) {
            this.activeAbusedInAmbCare.add(medOrder);
        }
    }

    public void addActiveAbusedInAmbCare(List<MedicationOrder> medOrders) {
        if (medOrders != null) {
            this.activeAbusedInAmbCare.addAll(medOrders);
        }
    }

    /* */

    public boolean hasActiveBenzodiazepine() {
        return activeBenzodiazepine.size() > 0;
    }

    public List<MedicationOrder> getActiveBenzodiazepine() {
        return activeBenzodiazepine;
    }

    public void addActiveBenzodiazepine(MedicationOrder medOrder) {
        if (medOrder != null) {
            this.activeBenzodiazepine.add(medOrder);
        }
    }

    public void addActiveBenzodiazepine(List<MedicationOrder> medOrders) {
        if (medOrders != null) {
            this.activeBenzodiazepine.addAll(medOrders);
        }
    }

    /* */

    public boolean hasActiveIndicatingEndOfLife() {
        return activeIndicatingEndOfLife.size() > 0;
    }

    public List<MedicationOrder> getActiveIndicatingEndOfLife() {
        return activeIndicatingEndOfLife;
    }

    public void addActiveIndicatingEndOfLife(MedicationOrder medOrder) {
        if (medOrder != null) {
            this.activeIndicatingEndOfLife.add(medOrder);
        }
    }

    public void addActiveIndicatingEndOfLife(List<MedicationOrder> medOrders) {
        if (medOrders != null) {
            this.activeIndicatingEndOfLife.addAll(medOrders);
        }
    }

    /* */

    public boolean hasCompletedOrStoppedAbusedInAmbCare() {
        return completedOrStoppedAbusedInAmbCare.size() > 0;
    }

    public List<MedicationOrder> getCompletedOrStoppedAbusedInAmbCare() {
        return completedOrStoppedAbusedInAmbCare;
    }

    public void addCompletedOrStoppedAbusedInAmbCare(MedicationOrder medOrder) {
        if (medOrder != null) {
            this.completedOrStoppedAbusedInAmbCare.add(medOrder);
        }
    }

    public void addCompletedOrStoppedAbusedInAmbCare(List<MedicationOrder> medOrders) {
        if (medOrders != null) {
            this.completedOrStoppedAbusedInAmbCare.addAll(medOrders);
        }
    }


    /* */

    public boolean hasActiveOrDraftAbusedInAmbCare() {
        return (activeAbusedInAmbCare.size() > 0) || (draftAbusedInAmbCare.size() > 0);
    }

    public List<MedicationOrder> getActiveOrDraftAbusedInAmbCareToInclude() {
    	List<MedicationOrder> activeOrDraftAbusedInAmbCareToInclude = new ArrayList<>();
    	activeOrDraftAbusedInAmbCareToInclude.addAll(activeAbusedInAmbCare);
    	activeOrDraftAbusedInAmbCareToInclude.addAll(draftAbusedInAmbCare);
        return activeOrDraftAbusedInAmbCareToInclude;
    }

    /* */

    public boolean hasActiveOrDraftBenzodiazepine() {
        return (activeBenzodiazepine.size() > 0) || (draftBenzodiazepine.size() > 0);
    }

    public List<MedicationOrder> getActiveOrDraftBenzodiazepine() {
    	List<MedicationOrder> activeOrDraftBenzodiazepine = new ArrayList<>();
    	activeOrDraftBenzodiazepine.addAll(activeBenzodiazepine);
    	activeOrDraftBenzodiazepine.addAll(draftBenzodiazepine);
        return activeOrDraftBenzodiazepine;
    }


    /* */

    public boolean hasActiveOrDraftIndicatingEndOfLife() {
        return (activeIndicatingEndOfLife.size() > 0) || (draftIndicatingEndOfLife.size() > 0);
    }

    public List<MedicationOrder> getActiveOrDraftIndicatingEndOfLife() {
    	List<MedicationOrder> activeOrDraftIndicatingEndOfLife = new ArrayList<>();
    	activeOrDraftIndicatingEndOfLife.addAll(activeIndicatingEndOfLife);
    	activeOrDraftIndicatingEndOfLife.addAll(draftIndicatingEndOfLife);
        return activeOrDraftIndicatingEndOfLife;
    }

    /* */

    public boolean hasActiveDraftCompletedOrStoppedAbusedInAmbCare() {
        return (activeAbusedInAmbCare.size() > 0) || (draftAbusedInAmbCare.size() > 0) || (completedOrStoppedAbusedInAmbCare.size() > 0);
    }

    public List<MedicationOrder> getActiveDraftCompletedOrStoppedAbusedInAmbCare() {
    	List<MedicationOrder> activeDraftCompletedOrStoppedAbusedInAmbCareToInclude = new ArrayList<>();
    	activeDraftCompletedOrStoppedAbusedInAmbCareToInclude.addAll(activeAbusedInAmbCare);
    	activeDraftCompletedOrStoppedAbusedInAmbCareToInclude.addAll(draftAbusedInAmbCare);
    	activeDraftCompletedOrStoppedAbusedInAmbCareToInclude.addAll(completedOrStoppedAbusedInAmbCare);
        return activeDraftCompletedOrStoppedAbusedInAmbCareToInclude;
    }
}
