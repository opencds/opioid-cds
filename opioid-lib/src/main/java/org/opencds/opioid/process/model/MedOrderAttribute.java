package org.opencds.opioid.process.model;

import java.util.Date;

import org.hl7.fhir.dstu2.model.Duration;
import org.hl7.fhir.dstu2.model.SimpleQuantity;
import org.opencds.RxSig;
import org.opencds.tools.terminology.rxnav.api.model.RxNormSemanticDrug;

public class MedOrderAttribute {

    private RxNormSemanticDrug drug;
    private String medName;
    private Boolean draft;
    private String prescriber;
    private Date rxDate;
    private Date startDate;
    private Date endDate;
    private Boolean endDateEstimated;
    private Integer numRepeatsAllowed;
    private SimpleQuantity dispenseQuantity;
    private Duration expectedSupplyDuration;
    private RxSig rxSigSig;
    private RxSig rxSigNote;
    private Date endDatePreferDefault;
    private String endDateCalcMethod;
    private Boolean prn;
    private String singleDoseMmeDescription;
    private Double mmePerDayAvg;
    private Double mmePerDayMax;
    private Double conversionFactorAvg;
    private Double conversionFactorMax;
    private String dailyDoseStrAvg;
    private String dailyDoseMax;
    private Boolean oralBuprenorphine;
    private Boolean mmePerDayAvgCalcUsingDefaultDaySupplyEst;

    public RxNormSemanticDrug getDrug() {
        return drug;
    }

    public void setDrug(RxNormSemanticDrug drug) {
        this.drug = drug;
    }

    public String getMedName() {
        return medName;
    }

    public void setMedName(String medName) {
        this.medName = medName;
    }

    public Boolean isDraft() {
        if (draft == null) {
            return Boolean.FALSE;
        }
        return draft;
    }

    public void setDraft(Boolean draft) {
        this.draft = draft;
    }

    public String getPrescriber() {
        return prescriber;
    }

    public void setPrescriber(String prescriber) {
        this.prescriber = prescriber;
    }

    public Date getRxDate() {
        return rxDate;
    }

    public void setRxDate(Date rxDate) {
        this.rxDate = rxDate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Boolean isEndDateEstimated() {
        if (endDateEstimated == null) {
            return Boolean.FALSE;
        }
        return endDateEstimated;
    }

    public void setEndDateEstimated(Boolean endDateEstimated) {
        this.endDateEstimated = endDateEstimated;
    }

    public Integer getNumRepeatsAllowed() {
        return numRepeatsAllowed;
    }

    public void setNumRepeatsAllowed(Integer numRepeatsAllowed) {
        this.numRepeatsAllowed = numRepeatsAllowed;
    }

    public SimpleQuantity getDispenseQuantity() {
        return dispenseQuantity;
    }

    public void setDispenseQuantity(SimpleQuantity dispenseQuantity) {
        this.dispenseQuantity = dispenseQuantity;
    }

    public Duration getExpectedSupplyDuration() {
        return expectedSupplyDuration;
    }

    public void setExpectedSupplyDuration(Duration expectedSupplyDuration) {
        this.expectedSupplyDuration = expectedSupplyDuration;
    }

    public RxSig getRxSigSig() {
        return rxSigSig;
    }

    public void setRxSigSig(RxSig rxSigSig) {
        this.rxSigSig = rxSigSig;
    }

    public RxSig getRxSigNote() {
        return rxSigNote;
    }

    public void setRxSigNote(RxSig rxSigNote) {
        this.rxSigNote = rxSigNote;
    }

    public Date getEndDatePreferDefault() {
        return endDatePreferDefault;
    }

    public void setEndDatePreferDefault(Date endDatePreferDefault) {
        this.endDatePreferDefault = endDatePreferDefault;
    }

    public String getEndDateCalcMethod() {
        return endDateCalcMethod;
    }

    public void setEndDateCalcMethod(String endDateCalcMethod) {
        this.endDateCalcMethod = endDateCalcMethod;
    }

    public Boolean isPrn() {
        if (prn == null) {
            return Boolean.FALSE;
        }
        return prn;
    }

    public void setPrn(Boolean prn) {
        this.prn = prn;
    }

    public String getSingleDoseMmeDescription() {
        return singleDoseMmeDescription;
    }

    public void setSingleDoseMmeDescription(String singleDoseMmeDescription) {
        this.singleDoseMmeDescription = singleDoseMmeDescription;
    }

    public Double getMmePerDayAvg() {
        return mmePerDayAvg;
    }

    public void setMmePerDayAvg(Double mmePerDayAvg) {
        this.mmePerDayAvg = mmePerDayAvg;
    }

    public Double getMmePerDayMax() {
        return mmePerDayMax;
    }

    public void setMmePerDayMax(Double mmePerDayMax) {
        this.mmePerDayMax = mmePerDayMax;
    }

    public Double getConversionFactorAvg() {
        return conversionFactorAvg;
    }

    public void setConversionFactorAvg(Double conversionFactorAvg) {
        this.conversionFactorAvg = conversionFactorAvg;
    }

    public Double getConversionFactorMax() {
        return conversionFactorMax;
    }

    public void setConversionFactorMax(Double conversionFactorMax) {
        this.conversionFactorMax = conversionFactorMax;
    }

    public String getDailyDoseAvg() {
        return dailyDoseStrAvg;
    }

    public void setDailyDoseAvg(String dailyDoseStrAvg) {
        this.dailyDoseStrAvg = dailyDoseStrAvg;
    }

    public String getDailyDoseMax() {
        return dailyDoseMax;
    }

    public void setDailyDoseMax(String dailyDoseMax) {
        this.dailyDoseMax = dailyDoseMax;
    }

    public Boolean isOralBuprenorphine() {
        if (oralBuprenorphine == null) {
            return Boolean.FALSE;
        }
        return oralBuprenorphine;
    }

    public void setOralBuprenorphine(Boolean oralBuprenorphine) {
        this.oralBuprenorphine = oralBuprenorphine;
    }

    public Boolean getMmePerDayAvgCalcUsingDefaultDaySupplyEst() {
        if (mmePerDayAvgCalcUsingDefaultDaySupplyEst == null) {
            return Boolean.FALSE;
        }
        return mmePerDayAvgCalcUsingDefaultDaySupplyEst;
    }

    public void setMmePerDayAvgCalcUsingDefaultDaySupplyEst(
            Boolean mmePerDayAvgCalcUsingDefaultDaySupplyEst) {
        this.mmePerDayAvgCalcUsingDefaultDaySupplyEst = mmePerDayAvgCalcUsingDefaultDaySupplyEst;
    }



}
