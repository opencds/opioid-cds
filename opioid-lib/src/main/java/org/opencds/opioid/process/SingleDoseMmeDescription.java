package org.opencds.opioid.process;

import java.util.Set;

import org.opencds.opioid.util.DoubleUtil;

public class SingleDoseMmeDescription {

    /**
     * Returns null if cannot process. Otherwise returns String describing what
     * a single dose equates to (e.g., "For 1 patch, OME = 63 mg."). Note that
     * methadone has a complex string due to difference according to daily dose.
     * 
     * @param doseFormRxCui
     * @param ingredientRxCui
     * @param strengthValue
     * @param strengthUnit
     * @param mmeShortLabel
     * @@param doseFormGroupRxCuis
     * @return
     */
    public static String get(String doseFormRxCui, String ingredientRxCui,
            double strengthValue, String strengthUnit, String mmeShortLabel, Set<String> doseFormGroupRxCuis) {
        String strToReturn = null;
        if ((doseFormRxCui != null) && (ingredientRxCui != null) && (strengthUnit != null) && (mmeShortLabel != null) && (! doseFormGroupRxCuis.contains("1151126")) && (! doseFormGroupRxCuis.contains("1151136"))) {
            // unit to use for "For 1 XXX"
            String doseUnit = "dose";

            // transdermal system / patch
            if (doseFormRxCui.equals("316987")) {
                doseUnit = "patch";
            } else {
                if (strengthUnit.endsWith("ml")) {
                    doseUnit = "ml";
                } else if (strengthUnit.endsWith("actuat")) {
                    doseUnit = "dose";
                }
                // buccal film
                else if (doseFormRxCui.equals("858080")) {
                    doseUnit = "film";
                }
                // buccal tablet
                else if (doseFormRxCui.equals("970789")) {
                    doseUnit = "tablet";
                }
                // extended release oral capsule
                else if (doseFormRxCui.equals("316943")) {
                    doseUnit = "capsule";
                }
                // extended release oral tablet
                else if (doseFormRxCui.equals("316945")) {
                    doseUnit = "tablet";
                }
                // oral capsule
                else if (doseFormRxCui.equals("316965")) {
                    doseUnit = "capsule";
                }
                // oral lozange
                else if (doseFormRxCui.equals("316992")) {
                    doseUnit = "lozange";
                }
                // oral strip
                else if (doseFormRxCui.equals("704866")) {
                    doseUnit = "strip";
                }
                // oral tablet
                else if (doseFormRxCui.equals("317541")) {
                    doseUnit = "tablet";
                }
                // sublingual tablet
                else if (doseFormRxCui.equals("317007")) {
                    doseUnit = "tablet";
                }
                // Disintegrating Oral Tablet
                else if (doseFormRxCui.equals("316942")) {
                    doseUnit = "tablet";
                }
                // Effervescent Oral Tablet
                else if (doseFormRxCui.equals("1535727")) {
                    doseUnit = "tablet";
                }
                // tablet for oral suspension
                else if (doseFormRxCui.equals("1861409")) {
                    doseUnit = "tablet";
                }
            }

            // methadone
            if (ingredientRxCui.equals("6813")) {
                double conversionFactorFor0_20 = MedOrderConversionUtil.getConversionFactor(ingredientRxCui,
                        doseFormRxCui, 10.0, strengthUnit, doseFormGroupRxCuis);
                double conversionFactorFor21_40 = MedOrderConversionUtil.getConversionFactor(ingredientRxCui,
                        doseFormRxCui, 30.0, strengthUnit, doseFormGroupRxCuis);
                double conversionFactorFor41_60 = MedOrderConversionUtil.getConversionFactor(ingredientRxCui,
                        doseFormRxCui, 50.0, strengthUnit, doseFormGroupRxCuis);
                double conversionFactorFor61_plus = MedOrderConversionUtil.getConversionFactor(ingredientRxCui,
                        doseFormRxCui, 70.0, strengthUnit, doseFormGroupRxCuis);

                double mmeFor1DoseFor0_20 = strengthValue * conversionFactorFor0_20;
                double mmeFor1DoseFor21_40 = strengthValue * conversionFactorFor21_40;
                double mmeFor1DoseFor41_60 = strengthValue * conversionFactorFor41_60;
                double mmeFor1DoseFor61_plus = strengthValue * conversionFactorFor61_plus;

                strToReturn = "For 1 " + doseUnit + ", " + mmeShortLabel + " = "
                        + DoubleUtil.format(mmeFor1DoseFor0_20) + " mg (if 1-20 mg/d), "
                        + DoubleUtil.format(mmeFor1DoseFor21_40) + " mg (if 21-40 mg/d), "
                        + DoubleUtil.format(mmeFor1DoseFor41_60) + " mg (if 41-60 mg/d), or "
                        + DoubleUtil.format(mmeFor1DoseFor61_plus) + " mg (if >= 60 mg/d).";
            } else {
                double dailyDose = 1.0 * strengthValue;
                double conversionFactor = MedOrderConversionUtil.getConversionFactor(ingredientRxCui, doseFormRxCui,
                        dailyDose, strengthUnit, doseFormGroupRxCuis);
                double mmeFor1Dose = dailyDose * conversionFactor;

                strToReturn = "For 1 " + doseUnit + ", " + mmeShortLabel + " = "
                        + DoubleUtil.format(mmeFor1Dose) + " mg.";
            }
        }
        return strToReturn;
    }


}
