package org.opencds.opioid.process;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.opencds.opioid.util.DoubleUtil;
import org.opencds.opioid.util.StringUtil;

public class DailyDoseInfoForNonPatchNonIvMed {

    /**
     * Returns null if cannot process. Otherwise returns map with: dailyDose -->
     * Double dailyDoseStr --> String conversionFactor --> Double
     * 
     * @param isMaxVsAve
     * @param dailyDose
     * @param dailyDoseFromSigUsedForDailyDose
     * @param doseValue
     * @param doseUnit
     * @param numTimesPerDay
     * @param numTimesPerDayTakenFromSig
     * @param isNumTimePerDayFromRangeOrPrn
     * @param isDoseValueFromRange
     * @param nonSurgicalOpioidIngredientName
     * @param doseFormName
     * @param ingredientRxCui
     * @param doseFormRxCui
     * @param strengthValue
     * @param strengthUnit
     * @param doseFormGroupRxCuis
     * @return
     */
    public static Map<String, Object> get(boolean isMaxVsAve, double dailyDose,
            boolean dailyDoseFromSigUsedForDailyDose, double doseValue, String doseUnit, double numTimesPerDay,
            boolean numTimesPerDayTakenFromSig, boolean isNumTimePerDayFromRangeOrPrn, boolean isDoseValueFromRange,
            String nonSurgicalOpioidIngredientName, String doseFormName, String ingredientRxCui, String doseFormRxCui,
            double strengthValue, String strengthUnit, Set<String> doseFormGroupRxCuis) {
        if ((nonSurgicalOpioidIngredientName == null) || (doseFormName == null) || (ingredientRxCui == null)
                || (doseFormRxCui == null)) {
            // do nothing
        } else {
            Map<String, Object> mapToReturn = new HashMap<String, Object>();
            String dailyDoseStr = null;
            double conversionFactor = 0;

            String maxOrAveStr = "ave";
            if (isMaxVsAve) {
                maxOrAveStr = "max";
            }

            // if dose unit in actual mass units (mg or mcg -- when it's a
            // single med) --> daily dose = daily dose in sig or numTimesPerDay * dose; for ave,
            // calculate only if # times/day is actually from the sig
            if ((doseUnit != null) && ((doseUnit.equals("mg") || doseUnit.equals("mcg")))) { 
                if (dailyDoseFromSigUsedForDailyDose) {   // test 021 (mg); mcg not expected
                    // 20 mg (daily max/ave per sig)
                    dailyDoseStr = nonSurgicalOpioidIngredientName + " " + doseFormName + " "
                            + DoubleUtil.format(dailyDose) + " " + doseUnit + " (daily " + maxOrAveStr
                            + " per sig).";
                    conversionFactor = MedOrderConversionUtil.getConversionFactor(ingredientRxCui, doseFormRxCui,
                            dailyDose, doseUnit, doseFormGroupRxCuis);
                } else if (isMaxVsAve) {   // test 022 (unusual)
                    // (max) 4/d * 5 mg = 20 mg
                    dailyDose = numTimesPerDay * doseValue;
                    dailyDoseStr = nonSurgicalOpioidIngredientName + " " + doseFormName + " "
                            + StringUtil.getLabelIfTrue("max ", isNumTimePerDayFromRangeOrPrn)
                            + DoubleUtil.format(numTimesPerDay) + "/d * "
                            + StringUtil.getLabelIfTrue("max ", isDoseValueFromRange)
                            + DoubleUtil.format(doseValue) + " " + doseUnit + " = "
                            + DoubleUtil.format(dailyDose) + " " + doseUnit + ".";
                    conversionFactor = MedOrderConversionUtil.getConversionFactor(ingredientRxCui, doseFormRxCui,
                            dailyDose, doseUnit, doseFormGroupRxCuis);
                } else if ((!isMaxVsAve) && (numTimesPerDayTakenFromSig)) {  // test 023 (unusual)
                    // ave 4/d * 5 mg = 20 mg
                    dailyDose = numTimesPerDay * doseValue;
                    dailyDoseStr = nonSurgicalOpioidIngredientName + " " + doseFormName + " ave "
                            + DoubleUtil.format(numTimesPerDay) + "/d * "
                            + StringUtil.getLabelIfTrue("max ", isDoseValueFromRange)
                            + DoubleUtil.format(doseValue) + " " + doseUnit + " = "
                            + DoubleUtil.format(dailyDose) + " " + doseUnit + ".";
                    conversionFactor = MedOrderConversionUtil.getConversionFactor(ingredientRxCui, doseFormRxCui,
                            dailyDose, doseUnit, doseFormGroupRxCuis);
                }
            }
            // if doseQuantity is in actual volume units (mL) --> daily dose =
            // numTimesPerDay * dose * strength
            else if ((doseUnit != null) && (doseUnit.equals("ml"))) {
                // should be the case
                if (strengthUnit.endsWith("/ml")) {
                    if (dailyDoseFromSigUsedForDailyDose) { // test 024
                        double dailyDoseInMl = dailyDose;
                        dailyDose = dailyDoseInMl * strengthValue;

                        // 5 ml (daily max/ave per sig) * 4 mg/ml = 20 mg
                        dailyDoseStr = nonSurgicalOpioidIngredientName + " " + doseFormName + " "
                                + DoubleUtil.format(dailyDoseInMl) + " " + doseUnit + " (daily " + maxOrAveStr
                                + " per sig) * " + DoubleUtil.format(strengthValue) + " " + strengthUnit + " = "
                                + DoubleUtil.format(dailyDose) + " "
                                + StringUtil.getStrengthUnitPrefixInLowerCase(strengthUnit) + ".";
                        conversionFactor = MedOrderConversionUtil.getConversionFactor(ingredientRxCui, doseFormRxCui,
                                dailyDose, strengthUnit, doseFormGroupRxCuis);
                    } else if (isMaxVsAve) { // test 025
                        // (max) 2 ml * (max) 4/d * 4 mg/ml = 32 mg
                        dailyDose = doseValue * numTimesPerDay * strengthValue;
                        dailyDoseStr = nonSurgicalOpioidIngredientName + " " + doseFormName + " "
                                + StringUtil.getLabelIfTrue("max ", isDoseValueFromRange)
                                + DoubleUtil.format(doseValue) + " " + doseUnit + " * "
                                + StringUtil.getLabelIfTrue("max ", isNumTimePerDayFromRangeOrPrn)
                                + DoubleUtil.format(numTimesPerDay) + "/d * "
                                + DoubleUtil.format(strengthValue) + " " + strengthUnit + " = "
                                + DoubleUtil.format(dailyDose) + " "
                                + StringUtil.getStrengthUnitPrefixInLowerCase(strengthUnit) + ".";
                        conversionFactor = MedOrderConversionUtil.getConversionFactor(ingredientRxCui, doseFormRxCui,
                                dailyDose, strengthUnit, doseFormGroupRxCuis);
                    } else if ((!isMaxVsAve) && (numTimesPerDayTakenFromSig)) { // test 026
                        // (max) 2 ml * ave 4/d * 4 mg/ml = 32 mg
                        dailyDose = doseValue * numTimesPerDay * strengthValue;
                        dailyDoseStr = nonSurgicalOpioidIngredientName + " " + doseFormName + " "
                                + StringUtil.getLabelIfTrue("max ", isDoseValueFromRange)
                                + DoubleUtil.format(doseValue) + " " + doseUnit + " * ave "
                                + DoubleUtil.format(numTimesPerDay) + "/d * "
                                + DoubleUtil.format(strengthValue) + " " + strengthUnit + " = "
                                + DoubleUtil.format(dailyDose) + " "
                                + StringUtil.getStrengthUnitPrefixInLowerCase(strengthUnit) + ".";
                        conversionFactor = MedOrderConversionUtil.getConversionFactor(ingredientRxCui, doseFormRxCui,
                                dailyDose, strengthUnit, doseFormGroupRxCuis);
                    }
                }
                // should not generally happen
                else {
                    // keep default of being unable to calculate
                }
            }
            // if doseQuantity is not in actual units (e.g., 1 tab, 1 spray, or
            // 1 -- when it's a combo med with a unit of tablet, or it's
            // mg/actuat) --> daily dose = numTimesPerDay * dose value *
            // strength value (**AS LONG AS strenght unit is not /ml **)
            else {
                if (strengthUnit.endsWith("/ml"))  // test 027      // TODO: this one is rare, but in cases where it says, e.g., 1 spray (1mg) in Sig, could potentially use alternate dose to calculate.
                {
                    dailyDose = 0;
                    conversionFactor = 0;
                    dailyDoseStr = "Unable to calculate as ml per dose not available.";  
                }
                else
                {
                    if (dailyDoseFromSigUsedForDailyDose) { // test 028
                    // 5 (daily max/ave per sig) * 5mg = 25 mg
                    double dailyDoseInSig = dailyDose;
                    dailyDose = dailyDoseInSig * strengthValue;

                    dailyDoseStr = nonSurgicalOpioidIngredientName + " " + doseFormName + " "
                            + DoubleUtil.format(dailyDoseInSig)
                            + StringUtil.getStrWithPrecedingSpaceIfNotNull(doseUnit) + " (daily " + maxOrAveStr
                            + " per sig) * " + DoubleUtil.format(strengthValue) + " " + strengthUnit + " = "
                            + DoubleUtil.format(dailyDose) + " "
                            + StringUtil.getStrengthUnitPrefixInLowerCase(strengthUnit) + ".";
                    conversionFactor = MedOrderConversionUtil.getConversionFactor(ingredientRxCui, doseFormRxCui,
                            dailyDose, strengthUnit, doseFormGroupRxCuis);
                    } else if (isMaxVsAve) { // test 029
                    // (max) 2 * (max) 5/d * 5 mg = 50 mg
                    dailyDose = numTimesPerDay * doseValue * strengthValue;
                    dailyDoseStr = nonSurgicalOpioidIngredientName + " " + doseFormName + " "
                            + StringUtil.getLabelIfTrue("max ", isDoseValueFromRange)
                            + DoubleUtil.format(doseValue)
                            + StringUtil.getStrWithPrecedingSpaceIfNotNull(doseUnit) + " * "
                            + StringUtil.getLabelIfTrue("max ", isNumTimePerDayFromRangeOrPrn)
                            + DoubleUtil.format(numTimesPerDay) + "/d * "
                            + DoubleUtil.format(strengthValue) + " " + strengthUnit + " = "
                            + DoubleUtil.format(dailyDose) + " "
                            + StringUtil.getStrengthUnitPrefixInLowerCase(strengthUnit) + ".";
                    conversionFactor = MedOrderConversionUtil.getConversionFactor(ingredientRxCui, doseFormRxCui,
                            dailyDose, strengthUnit, doseFormGroupRxCuis);
                    } else if ((!isMaxVsAve) && (numTimesPerDayTakenFromSig)) { // test 030
                    // (max) 2 * ave 5/d * 5 mg = 50 mg
                    dailyDose = numTimesPerDay * doseValue * strengthValue;
                    dailyDoseStr = nonSurgicalOpioidIngredientName + " " + doseFormName + " "
                            + StringUtil.getLabelIfTrue("max ", isDoseValueFromRange)
                            + DoubleUtil.format(doseValue)
                            + StringUtil.getStrWithPrecedingSpaceIfNotNull(doseUnit) + " * ave "
                            + DoubleUtil.format(numTimesPerDay) + "/d * "
                            + DoubleUtil.format(strengthValue) + " " + strengthUnit + " = "
                            + DoubleUtil.format(dailyDose) + " "
                            + StringUtil.getStrengthUnitPrefixInLowerCase(strengthUnit) + ".";
                    conversionFactor = MedOrderConversionUtil.getConversionFactor(ingredientRxCui, doseFormRxCui,
                            dailyDose, strengthUnit, doseFormGroupRxCuis);
                    }
                }
            }

            if (dailyDoseStr != null) {
                mapToReturn.put("dailyDose", new Double(dailyDose));
                mapToReturn.put("dailyDoseStr", dailyDoseStr);
                mapToReturn.put("conversionFactor", new Double(conversionFactor));
                return mapToReturn;
            }
        }
        return null;
    }
    

}
