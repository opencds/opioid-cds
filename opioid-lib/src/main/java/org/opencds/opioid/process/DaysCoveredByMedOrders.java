package org.opencds.opioid.process;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.hl7.fhir.dstu2.model.MedicationOrder;
import org.hl7.fhir.dstu2.model.Period;
import org.hl7.fhir.dstu2.model.Timing;
import org.hl7.fhir.dstu2.model.MedicationOrder.MedicationOrderDosageInstructionComponent;
import org.hl7.fhir.dstu2.model.Timing.TimingRepeatComponent;
import org.opencds.common.utilities.DateUtility;
import org.opencds.opioid.process.model.MedOrderAttributes;

public class DaysCoveredByMedOrders {

    /**
     * Copied from Utils in fhir-util; adapted to use MedOrderAttributes.
     *
     * Returns all dates between (anchorDate - daysToLookBack) and anchorDate (inclusive) covered by one of the medication orders.
     * Returns in ascending order.
     * Uses medOrder.dosageInstructions.dosageInstruction.timing.repeat.boundsPeriod.start and end.  If either is null, secondarily uses
     * medOrderIdToNameValuePairMap_optional contents for "startDate" (Date) and "endDate" (Date), or "endDatePreferDefault" (Date) if so specified.
     * Start Date must be present.
     * If End Date still null after all the other processing, assumed to continue indefinitely.
     * @param anchorDate
     * @param daysToLookBack
     * @param medOrders
     * @param medOrderIdToNameValuePairMap_optional
     * @param useEndDatePreferDefault
     * @return
     */
    public static List<Date> get(Date anchorDate, int daysToLookBack, List<MedicationOrder> medOrders,
            MedOrderAttributes medOrderAttributes, boolean useEndDatePreferDefault) {
        Set<Date> daysCovered = new HashSet<>();

        DateUtility dateUtility = DateUtility.getInstance();

        if ((medOrders != null) && (anchorDate != null) && (daysToLookBack >= 0)) {
            System.out.println();
            List<Date> potentialDates = new ArrayList<>();
            Date anchorDateWithoutHrsMinSec = dateUtility.getDateFromString(dateUtility.getDateAsString(anchorDate, "yyyy-MM-dd"), "yyyy-MM-dd");
            potentialDates.add(anchorDate);

            for (int k = 0; k < daysToLookBack; k++) {
                Date potentialDate = dateUtility.getDateAfterAddingTime(anchorDateWithoutHrsMinSec, Calendar.DATE, -1 * (k + 1));
                potentialDates.add(potentialDate);
            }

            for (MedicationOrder medOrder : medOrders) {
                String medOrderId = medOrder.getId();

                List<MedicationOrderDosageInstructionComponent> dosageInstructionList = medOrder.getDosageInstruction();
                if (dosageInstructionList != null) {
                    for (MedicationOrderDosageInstructionComponent dosageInstruction : dosageInstructionList) {
                        Timing timing = dosageInstruction.getTiming();
                        if (timing != null) {
                            TimingRepeatComponent repeat = timing.getRepeat();
                            if (repeat != null) {
                                Period period = null;
                                try {
                                    period = repeat.getBoundsPeriod();
                                } catch (Exception e) {
                                    // do nothing
                                }

                                // TODO: consider support other types of bounds
                                if (period != null) {
                                    Date start = period.getStart();
                                    Date end = period.getEnd();

                                    if (end == null) {
                                        if (useEndDatePreferDefault) {
                                            end = medOrderAttributes.getAttribute(medOrderId,
                                                    attr -> attr.getEndDatePreferDefault());
                                        } else {
                                            end = medOrderAttributes.getAttribute(medOrderId,
                                                    attr -> attr.getEndDate());
                                        }
                                    }

                                    if (start == null) {
                                        start = medOrderAttributes.getAttribute(medOrderId,
                                                attr -> attr.getStartDate());
                                    }
                                    // extra processing if it's not null
                                    if (start != null) {
                                        Date startDateWithoutHrsMinSec = dateUtility.getDateFromString(
                                                dateUtility.getDateAsString(start, "yyyy-MM-dd"), "yyyy-MM-dd");

                                        Date endDateWithoutHrsMinSec = null;
                                        if (end != null) {
                                            endDateWithoutHrsMinSec = dateUtility.getDateFromString(
                                                    dateUtility.getDateAsString(end, "yyyy-MM-dd"), "yyyy-MM-dd");
                                        }

                                        for (Date potentialDate : potentialDates) {
                                            if (end == null) {
                                                if (!potentialDate.before(startDateWithoutHrsMinSec)) {
                                                    daysCovered.add(potentialDate);
                                                }
                                            } else {
                                                if ((!potentialDate.before(startDateWithoutHrsMinSec))
                                                        && (!potentialDate.after(endDateWithoutHrsMinSec))) {
                                                    daysCovered.add(potentialDate);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return daysCovered.stream().sorted().collect(Collectors.toList());
    }


}
