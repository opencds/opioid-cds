package org.opencds.opioid.process.model;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;

/**
 * Used to be a Map<String, Map<String, Object>>, where the key was the
 * medOrderId and the values are composed of assertions
 * 
 * @author phillip
 *
 */
public class MedOrderAttributes {
    private final Map<String, MedOrderAttribute> attributes;
    
    public MedOrderAttributes() {
        this.attributes = new ConcurrentHashMap<>();
    }
    
    public void setAttribute(String medOrderId, MedOrderAttribute attr) {
        attributes.put(medOrderId, attr);
    }
    
    public <T> T getAttribute(String medOrderId, Function<MedOrderAttribute, T> delegate) {
        MedOrderAttribute attr = attributes.get(medOrderId);
        if (attr != null) {
            return delegate.apply(attr);
        }
        return null;
    }

}
