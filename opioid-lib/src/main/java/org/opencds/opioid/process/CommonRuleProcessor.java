package org.opencds.opioid.process;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hl7.fhir.dstu2.model.CodeableConcept;
import org.hl7.fhir.dstu2.model.Condition;
import org.hl7.fhir.dstu2.model.Extension;
import org.hl7.fhir.dstu2.model.IdType;
import org.hl7.fhir.dstu2.model.Medication;
import org.hl7.fhir.dstu2.model.MedicationOrder;
import org.hl7.fhir.dstu2.model.MedicationOrder.MedicationOrderStatus;
import org.opencds.Utils;
import org.opencds.common.utilities.DateUtility;
import org.opencds.hooks.model.response.Indicator;
import org.opencds.hooks.model.response.Link;
import org.opencds.hooks.model.response.Source;
import org.opencds.opioid.config.OpioidConfig;
import org.opencds.opioid.config.valuesets.ChronicOpioidUseDeterminationMethod;
import org.opencds.opioid.process.model.MedOrderAttributes;
import org.opencds.opioid.process.model.MedOrdersLists;
import org.opencds.opioid.results.EvalResults;
import org.opencds.opioid.util.fhir.MedOrderUtil;
import org.opencds.opioid.util.term.RxNavUtil;
import org.opencds.opioid.util.term.UmlsUtil;
import org.opencds.opioid.valuesets.RxNavValueSet;
import org.opencds.opioid.valuesets.UmlsValueSet;
import org.opencds.tools.terminology.rxnav.client.RxNavTerminologyClient;
import org.opencds.tools.terminology.umls.api.client.UmlsTerminologyClient;

import com.ibm.icu.util.Calendar;

public class CommonRuleProcessor {
    private static final Log log = LogFactory.getLog(CommonRuleProcessor.class);
    private static final String DETAIL_EMPTY = "";
    private static final String SUMM_UNABLE_TO_PROC_EVALTIME = "Unable to process -- evalTime is null.";
    private static final String SUMM_NO_ABUSE_NO_NEED_FOR_ACTION = "No draft medication orders abused in ambuatory care -- no need for action.";
    private static final String UNABLE_TO_PROC_FOCAL_PERSON_ID = "Unable to process -- focalPersonId is null.";
    private static final String SUMM_AT_LEAST_ONE__ONE_EOL__NO_ACTION = "At least one draft medication order which is an opioid abused in primary care, but at least one draft or active medication order which is an opioid indicating end of life -- no need foraction.";
    private static final String SUMM_AT_LEAST_ONE__COND_EOL__NO_ACTION = "At least one draft medication order which is an opioid abused in primary care, but patient has an active condition indicative of end of life -- no need for action.";
    private static final String SUMM_AT_LEAST_ONE_IN_PRIM_CARE__LT80_OF_90 = "At least one draft medication order which is an opioid abused in primary care, but less than 80 of past 90 days covered by opioid.";
    private static final String SUMM_AT_LEAST_ONE_IN_PRIM_CARE__NO_LT_21_DAYS = "At least one draft medication order which is an opioid abused in primary care, but does not meet check for at least 21 days covered by opioid in each of past 3 months (measured in 30 day intervals).";

    private final RxNavTerminologyClient rxNavClient;
    private final UmlsTerminologyClient umlsClient;
    private final OpioidConfig config;

    public CommonRuleProcessor(RxNavTerminologyClient rxNavClient, UmlsTerminologyClient umlsClient,
            OpioidConfig config) {
        this.rxNavClient = rxNavClient;
        this.umlsClient = umlsClient;
        this.config = config;
    }

    /**
     * Conducts common processing from across various related guideline rules.
     * Generally returns null, which should be checked to proceed. If returns
     * non-null, then a reason for not proceeding further was found, and the
     * calling method should return the results provided.
     */
    public EvalResults conductCommonProcessingAndReturnEvalResultsIfShouldExit(Date evalTime, String focalPersonId,
            List<MedicationOrder> medOrders, List<Medication> meds, List<Condition> conditions, Source source,
            List<Link> links, MedOrdersLists medOrdersLists, MedOrderAttributes medOrderAttributes) {

        // local guideline link
        if ((config.getLocalGuidelineLabel() != null) && (config.getLocalGuidelineUrl() != null)) {
            Link localGuidelineLink = new Link();
            localGuidelineLink.setLabel(config.getLocalGuidelineLabel());

            try {
                localGuidelineLink.setUrl(new URL(config.getLocalGuidelineUrl()));
                links.add(localGuidelineLink);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }

        // baseline condition checking
        if (evalTime == null) {
            return EvalResults.buildEvalResults(config.isReturnCardForDebug(), SUMM_UNABLE_TO_PROC_EVALTIME, DETAIL_EMPTY,
                    Indicator.INFO, source, links);
        }

        if (focalPersonId == null) {
            return EvalResults.buildEvalResults(config.isReturnCardForDebug(), UNABLE_TO_PROC_FOCAL_PERSON_ID,
                    DETAIL_EMPTY, Indicator.INFO, source, links);
        }

        // clean up input data
        // - for all med orders
        // Is for patient
        // Is an opioid
        // - sort based on status and subtypes of interest (all opioids, opioid
        // abused in ambulatory care, opioid indicating end of life)
        // - do not do any checking based on time
        // TODO: in future, may want to restrict active and draft meds to those
        // that include evalTime (should not be needed, though)

        log.debug("medOrders: " + medOrders);
        cleanupMedOrders(medOrders, meds, focalPersonId, medOrdersLists);

        // identify if there are any active conditions indicating end of life
        // TODO: consider also checking for type in the future,
        // especially as the values that are used are made consistent
        // across EHR vendors.
        boolean atLeastOneActiveConditionIndicatingEndOfLife = conditions.stream().anyMatch(cond -> {
            return "active".equalsIgnoreCase(cond.getClinicalStatus()) && UmlsUtil
                    .isConceptInUmlsValueSet(cond.getCode(), umlsClient, UmlsValueSet.CONDITION_INDICATING_END_OF_LIFE);
        });

        if (config.isExcludeIfNoDraftOpioidMedOrdersAbusedInAmbulatoryCare() && !medOrdersLists.hasDraftAbusedInAmbCare()) {
            return EvalResults.buildEvalResults(config.isReturnCardForDebug(), SUMM_NO_ABUSE_NO_NEED_FOR_ACTION, DETAIL_EMPTY, Indicator.INFO, source,
                    links);
        } else if (config.isExcludeIfDraftOrActiveMedOrderIndicatingEndOfLife()
                && (medOrdersLists.hasDraftIndicatingEndOfLife() || medOrdersLists.hasActiveIndicatingEndOfLife())) {
            return EvalResults.buildEvalResults(config.isReturnCardForDebug(), SUMM_AT_LEAST_ONE__ONE_EOL__NO_ACTION,
                    DETAIL_EMPTY, Indicator.INFO, source, links);
        } else if (config.isExcludeIfActiveConditionIndicatingEndOfLife()
                && atLeastOneActiveConditionIndicatingEndOfLife) {
            return EvalResults.buildEvalResults(config.isReturnCardForDebug(), SUMM_AT_LEAST_ONE__COND_EOL__NO_ACTION,
                    DETAIL_EMPTY, Indicator.INFO, source, links);
        } else {
            // at least one draft medication order for opioid abused in
            // ambulatory care, (if checked) no draft or active opioid
            // indictiave of end of life, (if checked) no condition indicating
            // exclusion

            // proceed to get/set attributes for each med order of remaining
            // interest

            boolean proceedEvenIfRxSigStatesUseWithCaution = false;
            for (MedicationOrder medOrderOfInterest : medOrdersLists
                    .getActiveDraftCompletedOrStoppedAbusedInAmbCare()) {
                Utils.updateMedOrderDosageInstructionWithSigInfo(medOrderOfInterest,
                        proceedEvenIfRxSigStatesUseWithCaution);
                CodeableConcept medOfInterest = MedOrderUtil.getAssociatedMed(medOrderOfInterest, meds);
                if (medOfInterest != null) {
                    medOrderAttributes.setAttribute(medOrderOfInterest.getId(),
                            MedOrderAttributeBuilder.build(medOrderOfInterest, medOfInterest, rxNavClient,
                                    config.getMmeShortLabel(), config.getDefaultOpioidRxDaysSupplyEstimate()));
                }
            }

            // (if requested) check for chronic use of opioids abused in
            // ambulatory care
            boolean useEndDatePreferDefault = config
                    .isCalculateCoverageDatesUsingPresumptionOfDefaultDaysSupplyUnlessExplicitlyStatedOtherwise();
            DateUtility dateUtility = DateUtility.getInstance();
            if (config.getChronicOpioidUseDeterminationMethod() == ChronicOpioidUseDeterminationMethod.M_80D_OF_90D) {
                List<Date> daysCoveredByOpioidMedOrdersAbusedInAmbulatoryCare = DaysCoveredByMedOrders.get(
                        evalTime, 89, medOrdersLists.getActiveDraftCompletedOrStoppedAbusedInAmbCare(),
                        medOrderAttributes, useEndDatePreferDefault);
                int numDaysCoveredByOpioidMedOrdersAbusedInAmbulatoryCare = daysCoveredByOpioidMedOrdersAbusedInAmbulatoryCare
                        .size();

                if (numDaysCoveredByOpioidMedOrdersAbusedInAmbulatoryCare < 80) {
                    String detail = "# days covered = " + numDaysCoveredByOpioidMedOrdersAbusedInAmbulatoryCare
                            + ". <br><br> Days: ";
                    for (int k = 0; k < numDaysCoveredByOpioidMedOrdersAbusedInAmbulatoryCare; k++) {
                        detail += dateUtility.getDateAsString(daysCoveredByOpioidMedOrdersAbusedInAmbulatoryCare.get(k),
                                "yyyy-MM-dd");
                        if (k != numDaysCoveredByOpioidMedOrdersAbusedInAmbulatoryCare - 1) {
                            detail += ", ";
                        }
                    }

                    // FIXME - Indicator.WARNING or HARD_STOP?
                    return EvalResults.buildEvalResults(config.isReturnCardForDebug(), SUMM_AT_LEAST_ONE_IN_PRIM_CARE__LT80_OF_90, detail, null,
                            source, links);
                }
            } else if (config
                    .getChronicOpioidUseDeterminationMethod() == ChronicOpioidUseDeterminationMethod.THREE_CONSECUTIVE_21D_OF_30D) {
                Date thirtyDaysAgo = dateUtility.getDateAfterAddingTime(evalTime, Calendar.DATE, -30);
                Date sixtyDaysAgo = dateUtility.getDateAfterAddingTime(evalTime, Calendar.DATE, -60);
                List<Date> daysCoveredByOpioidMedOrdersAbusedInAmbulatoryCare_last30Days = DaysCoveredByMedOrders.get(
                        evalTime, 29, medOrdersLists.getActiveDraftCompletedOrStoppedAbusedInAmbCare(),
                        medOrderAttributes, useEndDatePreferDefault);
                List<Date> daysCoveredByOpioidMedOrdersAbusedInAmbulatoryCare_last31_60Days = DaysCoveredByMedOrders
                        .get(thirtyDaysAgo, 29,
                                medOrdersLists.getActiveDraftCompletedOrStoppedAbusedInAmbCare(),
                                medOrderAttributes, useEndDatePreferDefault);
                List<Date> daysCoveredByOpioidMedOrdersAbusedInAmbulatoryCare_last61_90Days = DaysCoveredByMedOrders
                        .get(sixtyDaysAgo, 29,
                                medOrdersLists.getActiveDraftCompletedOrStoppedAbusedInAmbCare(),
                                medOrderAttributes, useEndDatePreferDefault);
                int numDaysCoveredByOpioidMedOrdersAbusedInAmbulatoryCare_last30Days = daysCoveredByOpioidMedOrdersAbusedInAmbulatoryCare_last30Days
                        .size();
                int numDaysCoveredByOpioidMedOrdersAbusedInAmbulatoryCare_last31_60Days = daysCoveredByOpioidMedOrdersAbusedInAmbulatoryCare_last31_60Days
                        .size();
                int numDaysCoveredByOpioidMedOrdersAbusedInAmbulatoryCare_last61_90Days = daysCoveredByOpioidMedOrdersAbusedInAmbulatoryCare_last61_90Days
                        .size();

                if ((numDaysCoveredByOpioidMedOrdersAbusedInAmbulatoryCare_last30Days < 21)
                        || (numDaysCoveredByOpioidMedOrdersAbusedInAmbulatoryCare_last31_60Days < 21)
                        || (numDaysCoveredByOpioidMedOrdersAbusedInAmbulatoryCare_last61_90Days < 21)) {
                    String detail = "Number of days covered last 30 days (ending  "
                            + dateUtility.getDateAsString(evalTime, "yyyy-MM-dd") + ") = "
                            + numDaysCoveredByOpioidMedOrdersAbusedInAmbulatoryCare_last30Days + ".<br>";
                    detail += "Number of days covered last 31-60 days (ending  "
                            + dateUtility.getDateAsString(thirtyDaysAgo, "yyyy-MM-dd") + ") = "
                            + numDaysCoveredByOpioidMedOrdersAbusedInAmbulatoryCare_last31_60Days + ".<br>";
                    detail += "Number of days covered last 61-90 days (ending  "
                            + dateUtility.getDateAsString(sixtyDaysAgo, "yyyy-MM-dd") + ") = "
                            + numDaysCoveredByOpioidMedOrdersAbusedInAmbulatoryCare_last61_90Days + ".<br><br>";
                    detail += "Days in last 30 days: ";
                    for (int k = 0; k < numDaysCoveredByOpioidMedOrdersAbusedInAmbulatoryCare_last30Days; k++) {
                        detail += dateUtility.getDateAsString(
                                daysCoveredByOpioidMedOrdersAbusedInAmbulatoryCare_last30Days.get(k), "yyyy-MM-dd");
                        if (k != numDaysCoveredByOpioidMedOrdersAbusedInAmbulatoryCare_last30Days - 1) {
                            detail += ", ";
                        }
                    }
                    detail += ".  <br><br>Days in last 31 - 60 days: ";
                    for (int k = 0; k < numDaysCoveredByOpioidMedOrdersAbusedInAmbulatoryCare_last31_60Days; k++) {
                        detail += dateUtility.getDateAsString(
                                daysCoveredByOpioidMedOrdersAbusedInAmbulatoryCare_last31_60Days.get(k), "yyyy-MM-dd");
                        if (k != numDaysCoveredByOpioidMedOrdersAbusedInAmbulatoryCare_last31_60Days - 1) {
                            detail += ", ";
                        }
                    }
                    detail += ".  <br><br>Days in last 61 - 90 days: ";
                    for (int k = 0; k < numDaysCoveredByOpioidMedOrdersAbusedInAmbulatoryCare_last61_90Days; k++) {
                        detail += dateUtility.getDateAsString(
                                daysCoveredByOpioidMedOrdersAbusedInAmbulatoryCare_last61_90Days.get(k), "yyyy-MM-dd");
                        if (k != numDaysCoveredByOpioidMedOrdersAbusedInAmbulatoryCare_last61_90Days - 1) {
                            detail += ", ";
                        }
                    }

                    // FIXME - Indicator WARNING OR HARD_STOP?
                    return EvalResults.buildEvalResults(config.isReturnCardForDebug(), SUMM_AT_LEAST_ONE_IN_PRIM_CARE__NO_LT_21_DAYS, detail, null,
                            source, links);
                }
            }

            if (config.isExcludeIfRecentOpioidUse()) {
                List<Date> daysCoveredByOpioidMedOrdersAbusedInAmbulatoryCare_recent = DaysCoveredByMedOrders.get(
                        evalTime, config.getRecentOpioidUseLookbackDays() - 1,
                        medOrdersLists.getActiveDraftCompletedOrStoppedAbusedInAmbCare(), medOrderAttributes,
                        useEndDatePreferDefault);

                // if patient has taken opioids in the lookback period over a
                // small minimum allowance
                if (daysCoveredByOpioidMedOrdersAbusedInAmbulatoryCare_recent.size() >= config
                        .getRecentOpioidUseIfAtLeastXDaysInLookbackOnOpioids()) {

                    String summary = "At least one draft medication order which is an opioid abused in primary care, but patient excluded due to recent opioid use of at least "
                            + config.getRecentOpioidUseIfAtLeastXDaysInLookbackOnOpioids() + " days in  past "
                            + config.getRecentOpioidUseLookbackDays() + " days.";
                    String detail = "# days covered = "
                            + daysCoveredByOpioidMedOrdersAbusedInAmbulatoryCare_recent.size() + ". <br><br> Days: ";
                    for (int k = 0; k < daysCoveredByOpioidMedOrdersAbusedInAmbulatoryCare_recent.size(); k++) {
                        detail += dateUtility.getDateAsString(
                                daysCoveredByOpioidMedOrdersAbusedInAmbulatoryCare_recent.get(k), "yyyy-MM-dd");
                        if (k != daysCoveredByOpioidMedOrdersAbusedInAmbulatoryCare_recent.size() - 1) {
                            detail += ", ";
                        }
                    }

                    // indicator=null or Indicator.INFO?
                    return EvalResults.buildEvalResults(config.isReturnCardForDebug(), summary, detail, null, source,
                            links);
                }
            }
        }

        return null;
    }

    private void cleanupMedOrders(List<MedicationOrder> medOrders, List<Medication> meds, String focalPersonId, MedOrdersLists medOrdersLists) {
        if (medOrders == null) {
            return;
        }
        // Not a pure function
        medOrders.forEach(medOrder -> {
            // correct time zones if needed
            if (config.isReverseMistakenLabelingOfStartAndEndDatesWithUtcTimeZone()) {
                Utils.reverseMistakenLabelingOfMedOrderStartAndEndDatesWithUtcTimeZone(medOrder,
                        config.getCorrectTimeZone());
            }

            if (medOrder.getIdElement().getIdPart() == null) {
                log.warn("medOrder ID is null; generating...");
                medOrder.setIdElement(new IdType(UUID.randomUUID().toString()));
            }

            String medOrderId = medOrder.getIdElement().getIdPart();
            log.debug("medOrder ID: " + medOrderId);
            CodeableConcept associatedMed = MedOrderUtil.getAssociatedMed(medOrder, meds);

            if ((Utils.isReferencedPatientIdFocalPersonId(medOrder.getPatient(), focalPersonId))
                    && (medOrder.getStatus() != null) && (associatedMed != null)
                    && (RxNavUtil.isConceptInRxNavValueSet(associatedMed, rxNavClient, RxNavValueSet.OPIOIDS))) {
                boolean passesMedStatementCategoryRestriction = false;
                if (config.isRestrictToSpecifiedMedStatementCategories()) {
                    List<Extension> extensions = medOrder.getExtension();
                    for (Extension extension : extensions) {
                        String url = extension.getUrl();
                        if (url != null
                                && ("http://hl7.org/fhir/StructureDefinition/extension-MedicationStatement.category".equals(url)
                                || "http://hl7.org/fhir/3.0/StructureDefinition/extension-MedicationRequest.category".equals(url)
                        )) {
                            try {
                                CodeableConcept codeableConcept = (CodeableConcept) extension.getValue();
                                if (config.getMedStatementCategoriesAllowed() != null) {
                                    for (String medStatementCategoryAllowed : config
                                            .getMedStatementCategoriesAllowed()) {
                                        if (Utils.isCodingContains(codeableConcept,
                                                "http://hl7.org/fhir/medication-request-category",
                                                medStatementCategoryAllowed)) {
                                            passesMedStatementCategoryRestriction = true;
                                        }
                                    }
                                }
                            } catch (Exception e) {
                                // do nothing
                            }
                        }
                    }
                } else {
                    passesMedStatementCategoryRestriction = true;
                }

                if (passesMedStatementCategoryRestriction) {
                    MedicationOrderStatus medOrderStatus = medOrder.getStatus();
                    String debugLine = "> Med Order (" + medOrder.getId() + "). Status: " + medOrderStatus + ". ";
                    if (associatedMed.getId() == null) {
                        debugLine += "> Associated med found, but no ID as associated med created temporarily from code in medication order.";
                    } else {
                        debugLine += "> Associated Med ID: " + associatedMed.getId();
                    }
                    log.debug(debugLine);

                    if (medOrderStatus.equals(MedicationOrderStatus.DRAFT)) {
                        if (RxNavUtil.isConceptInRxNavValueSet(associatedMed, rxNavClient,
                                RxNavValueSet.OPIOIDS_ABUSED_IN_AMBULATORY_CARE)) {
                            medOrdersLists.addDraftAbusedInAmbCare(medOrder);
                        }
                        if (RxNavUtil.isConceptInRxNavValueSet(associatedMed, rxNavClient,
                                RxNavValueSet.OPIOIDS_INDICATING_END_OF_LIFE)) {
                            medOrdersLists.addDraftIndicatingEndOfLife(medOrder);
                        }
                    } else if (medOrderStatus.equals(MedicationOrderStatus.ACTIVE)) {
                        if (RxNavUtil.isConceptInRxNavValueSet(associatedMed, rxNavClient,
                                RxNavValueSet.OPIOIDS_ABUSED_IN_AMBULATORY_CARE)) {
                            medOrdersLists.addActiveAbusedInAmbCare(medOrder);
                        }
                        if (RxNavUtil.isConceptInRxNavValueSet(associatedMed, rxNavClient,
                                RxNavValueSet.OPIOIDS_INDICATING_END_OF_LIFE)) {
                            medOrdersLists.addActiveIndicatingEndOfLife(medOrder);
                        }
                    } else if (medOrderStatus.equals(MedicationOrderStatus.COMPLETED)
                            || medOrderStatus.equals(MedicationOrderStatus.STOPPED)) {
                        if (RxNavUtil.isConceptInRxNavValueSet(associatedMed, rxNavClient,
                                RxNavValueSet.OPIOIDS_ABUSED_IN_AMBULATORY_CARE)) {
                            medOrdersLists.addCompletedOrStoppedAbusedInAmbCare(medOrder);
                        }
                    }
                }
            }
            log.debug("draftOpioidMedOrdersAbusedInAmbulatoryCareForPt: " + medOrdersLists.getDraftAbusedInAmbCare());
            log.debug("");
        });

    }

}
