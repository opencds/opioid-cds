package org.opencds.opioid.mme;

import java.util.Comparator;
import java.util.Date;

import org.opencds.common.utilities.DateUtility;
import org.opencds.opioid.util.DoubleUtil;

import com.ibm.icu.util.Calendar;

public class MmeDuplicateEntryComparator implements Comparator<MmeEntry> {

    @Override
    public int compare(MmeEntry a, MmeEntry b) {
        Double mmePerDay_a = a.getMmePerDay_higher();
        Double mmePerDay_b = b.getMmePerDay_higher();
        Date startDate_a = a.getStartDate();
        Date startDate_b = b.getStartDate();
        Date evalTime = a.getEvalTime();

        if (DoubleUtil.isNotNullOr0(mmePerDay_a) && DoubleUtil.isNullOr0(mmePerDay_b)) {
            return -1;
        } else if (DoubleUtil.isNotNullOr0(mmePerDay_b) && DoubleUtil.isNullOr0(mmePerDay_a)) {
            return 1;
        } else {
            if (a.isDraft() && (!b.isDraft())) {
                return -1;
            } else if (b.isDraft() && (!a.isDraft())) {
                return 1;
            } else {
                if ((startDate_a != null) && (startDate_b == null)) {
                    return -1;
                } else if ((startDate_b != null) && (startDate_a == null)) {
                    return 1;
                } else {
                    DateUtility dateUtility = DateUtility.getInstance();

                    if ((evalTime != null) && (startDate_a != null) && (startDate_b != null)) {
                        Date startDateIgnoringTime_a = dateUtility.getDateFromString(
                                dateUtility.getDateAsString(startDate_a, "yyyy-MM-dd"), "yyyy-MM-dd");
                        Date startDateIgnoringTime_b = dateUtility.getDateFromString(
                                dateUtility.getDateAsString(startDate_b, "yyyy-MM-dd"), "yyyy-MM-dd");
                        Date evalTimeIgnoringTime = dateUtility
                                .getDateFromString(dateUtility.getDateAsString(evalTime, "yyyy-MM-dd"), "yyyy-MM-dd");

                        boolean startDateOnOrAfterEvalTime_a = false;
                        boolean startDateOnOrAfterEvalTime_b = false;

                        if (!startDateIgnoringTime_a.before(evalTimeIgnoringTime)) {
                            startDateOnOrAfterEvalTime_a = true;
                        }

                        if (!startDateIgnoringTime_b.before(evalTimeIgnoringTime)) {
                            startDateOnOrAfterEvalTime_b = true;
                        }

                        if (startDateOnOrAfterEvalTime_a && (!startDateOnOrAfterEvalTime_b)) {
                            return -1;
                        } else if (startDateOnOrAfterEvalTime_b && (!startDateOnOrAfterEvalTime_a)) {
                            return 1;
                        } else {
                            double daysDifWithEval_a = dateUtility.getApproximateTimeDifference(evalTimeIgnoringTime,
                                    startDateIgnoringTime_a, Calendar.DATE, true);
                            double daysDifWithEval_b = dateUtility.getApproximateTimeDifference(evalTimeIgnoringTime,
                                    startDateIgnoringTime_b, Calendar.DATE, true);

                            daysDifWithEval_a = Math.abs(daysDifWithEval_a);
                            daysDifWithEval_b = Math.abs(daysDifWithEval_b);

                            if (daysDifWithEval_a < daysDifWithEval_b) {
                                return -1;
                            } else if (daysDifWithEval_b < daysDifWithEval_a) {
                                return 1;
                            }
                        }
                    }

                    // if still not returned result / comparison not resolved
                    if (DoubleUtil.isNotNullOr0(mmePerDay_a) && DoubleUtil.isNotNullOr0(mmePerDay_b)) {
                        if (mmePerDay_a.doubleValue() > mmePerDay_b.doubleValue()) {
                            return -1;
                        } else if (mmePerDay_b.doubleValue() > mmePerDay_a.doubleValue()) {
                            return 1;
                        }
                    }

                    // if still not returned result / comparison not resolved
                    String a_comparisonString = a.getMedName()
                            + DateUtility.getInstance().getDateAsString(a.getStartDate(), "yyyy-MM-dd")
                            + a.getRxWarnings() + a.getRxDetails();
                    String b_comparisonString = b.getMedName()
                            + DateUtility.getInstance().getDateAsString(b.getStartDate(), "yyyy-MM-dd")
                            + b.getRxWarnings() + b.getRxDetails();

                    return a_comparisonString.compareTo(b_comparisonString);
                }
            }
        }
    }

}