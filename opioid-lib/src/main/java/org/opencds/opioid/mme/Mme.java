package org.opencds.opioid.mme;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hl7.fhir.dstu2.model.MedicationOrder;
import org.hl7.fhir.dstu2.model.SimpleQuantity;
import org.opencds.RxSig;
import org.opencds.common.utilities.DateUtility;
import org.opencds.opioid.process.model.MedOrderAttributes;
import org.opencds.opioid.util.DoubleUtil;
import org.opencds.tools.terminology.rxnav.api.model.RxNormSemanticDrug;

public class Mme
{
	/**
     *
     */
    private DateUtility myDateUtility;
	private double myTotalMmeAvg; // among those known
	private double myTotalMmeMax; // among those known
	private int myNumEntriesWithUnknownMmeAvg; // number of MmeEntries with unknown MME
	private int myNumEntriesWithUnknownMmeMax; // number of MmeEntries with unknown MME
	private List<MmeEntry> myMmeEntries;
	private List<MmeEntry> myPotentiallyDuplicateMmeEntries; // not included in myTotalMmeQuantity_ave/max or myNumEntriesWithUnknownMmeAvg/max

    public Mme(boolean includeAvgMmePerDay, List<MedicationOrder> medOrders, MedOrderAttributes medOrderAttributes, Date evalTime, String mmeShortLabel) {
        myTotalMmeAvg = 0;
        myTotalMmeMax = 0;
        myNumEntriesWithUnknownMmeAvg = 0;
        myNumEntriesWithUnknownMmeMax = 0;
        myMmeEntries = new ArrayList<MmeEntry>();
        myPotentiallyDuplicateMmeEntries = new ArrayList<>();
        myDateUtility = DateUtility.getInstance();
        Date evalTimeIgnoringTime = myDateUtility.getDateFromString(myDateUtility.getDateAsString(evalTime, "yyyy-MM-dd"), "yyyy-MM-dd");

        process(includeAvgMmePerDay, medOrders, medOrderAttributes, evalTime, evalTimeIgnoringTime, mmeShortLabel);
    }

    private void process(boolean includeAvgMmePerDay, List<MedicationOrder> medOrders,
            MedOrderAttributes medOrderAttributes, Date evalTime, Date evalTimeIgnoringTime,
            String mmeShortLabel)	{
		ArrayList<MmeEntry> unbinnedMmeEntries = new ArrayList<MmeEntry>(); // not yet sorted to themain vs. duplicate entries
		boolean atLeastOneOralBuprenorphine = false;

		if ((medOrders != null) && (medOrderAttributes != null))
		{
			for (MedicationOrder medOrder : medOrders)
			{
				MmeEntry mmeEntry = new MmeEntry();
				String medOrderId = medOrder.getId();

				if (medOrderId != null)
				{
					// drug
					mmeEntry.setDrug(medOrderAttributes.getAttribute(medOrderId, attr -> attr.getDrug()));

					// med name
					mmeEntry.setMedName(medOrderAttributes.getAttribute(medOrderId, attr -> attr.getMedName()));

					// eval time
					mmeEntry.setEvalTime(evalTime);

					// isDraft
					mmeEntry.setDraft(medOrderAttributes.getAttribute(medOrderId, attr -> attr.isDraft()));

					// start date
					mmeEntry.setStartDate(medOrderAttributes.getAttribute(medOrderId, attr -> attr.getStartDate()));

					// MME per day (ave and max)
					mmeEntry.setMmePerDay_ave(medOrderAttributes.getAttribute(medOrderId, attr -> attr.getMmePerDayAvg()));
					mmeEntry.setMmePerDay_max(medOrderAttributes.getAttribute(medOrderId, attr -> attr.getMmePerDayMax()));

					// atLeastOneOralBuprenorphine, isOralBuprenorphine
					mmeEntry.setIsOralBuprenorphine(medOrderAttributes.getAttribute(medOrderId, attr -> attr.isOralBuprenorphine()));

					// REMOVED -- warning -- MME estimated assuming XXd supply
					//Boolean mmePerDay_aveCalculatedUsingDefaultDaysSupplyEstimate = (Boolean) Utils.getAttributeValue(medOrderId, "mmePerDay_aveCalculatedUsingDefaultDaysSupplyEstimate", medOrderIdToNameValuePairMap);
					//if ((mmePerDay_aveCalculatedUsingDefaultDaysSupplyEstimate != null) && (mmePerDay_aveCalculatedUsingDefaultDaysSupplyEstimate.booleanValue() == true))
					//{
					//	mmeEntry.addRxWarning(mmeShortLabel + " estimated assuming " + DEFAULT_OPIOID_RX_DAYS_SUPPLY_ESTIMATE + "d supply");
					//}

					// warning -- may be expired
					Date endDate = medOrderAttributes.getAttribute(medOrderId, attr -> attr.getEndDate());
					Date aWeekFromNow = myDateUtility.getDateAfterAddingTime(evalTimeIgnoringTime, Calendar.DATE, +7);

                    if ((endDate != null) && (endDate.before(evalTimeIgnoringTime))) {
                        mmeEntry.addRxWarning("Verify taking; Rx may have expired");
                    } else if ((endDate != null) && (aWeekFromNow != null) && (endDate.before(aWeekFromNow))) {
                        mmeEntry.addRxWarning("May be expiring soon");
                    }

					// details
					RxSig rxSig_sig = medOrderAttributes.getAttribute(medOrderId, attr -> attr.getRxSigSig());
					RxSig rxSig_note = medOrderAttributes.getAttribute(medOrderId, attr -> attr.getRxSigNote());
					String rxSig_sig_originalSig = null;
					String rxSig_note_originalSig = null;

                    if (rxSig_sig != null) {
                        rxSig_sig_originalSig = rxSig_sig.getOriginalSig();
                    }

                    if (rxSig_note != null) {
                        rxSig_note_originalSig = rxSig_note.getOriginalSig();
                    }

					// Sig
                    if (rxSig_sig_originalSig != null) {
                        mmeEntry.addRxDetail("Sig: " + rxSig_sig_originalSig);
                    } else if (mmeEntry.isDraft()) {
                        mmeEntry.addRxDetail("Sig: unavailable for new Rx when entered as free text");
                    }

                    // Note to pharmacy
                    if (rxSig_note_originalSig != null) {
                        mmeEntry.addRxDetail("Note to pharmacy: " + rxSig_note_originalSig);
                    }

					// Morphine equivalence: 1.8x. (bold if MME not available for OME of interest) For 1 patch/tablet/etc., OME = 63 mg.
					Double conversionFactor_ave = medOrderAttributes.getAttribute(medOrderId, attr -> attr.getConversionFactorAvg());
					Double conversionFactor_max = medOrderAttributes.getAttribute(medOrderId, attr -> attr.getConversionFactorMax());

					String str = "Morphine equivalence: ";

                    if (!includeAvgMmePerDay) {
                        if (conversionFactor_max != null) {
                            str += DoubleUtil.format(conversionFactor_max) + "x.";
                        } else {
                            str += "not available.";
                        }
                    } else {
                        if (conversionFactor_ave != null) {
                            str += DoubleUtil.format(conversionFactor_ave) + "x.";
                        } else {
                            str += "not available.";
                        }
                    }

					String singleDoseMmeDescription = medOrderAttributes.getAttribute(medOrderId, attr -> attr.getSingleDoseMmeDescription());

					boolean mmeMissing = false;
                    if (DoubleUtil.isNullOr0(mmeEntry.getMmePerDay_max())
                            || (includeAvgMmePerDay && DoubleUtil.isNullOr0(mmeEntry.getMmePerDay_ave()))) {
                        mmeMissing = true;
                    }

                    if (singleDoseMmeDescription != null) {
                        str += " ";
                        if (mmeMissing) {
                            str += "<span style=\"font-weight: 600\">";
                        }
                        str += singleDoseMmeDescription;
                        if (mmeMissing) {
                            str += "</span>";
                        }
                    }

					mmeEntry.addRxDetail(str);

					// (if not new) (or new) Rx by John Smith, MD on XX/XX/17. Disp 360 tablet, Refill 0.
					// why (!x || x) ??
                    if (!mmeEntry.isDraft() || mmeEntry.isDraft()) {
						str = "";

						String prescriber = medOrderAttributes.getAttribute(medOrderId, attr -> attr.getPrescriber());
						Date rxDate = medOrderAttributes.getAttribute(medOrderId, attr -> attr.getRxDate());

                        if ((prescriber != null) && (rxDate != null)) {
                            str += "Rx by " + prescriber + " on "
                                    + myDateUtility.getDateAsString(rxDate, "MM/dd/yy") + ". ";
                        } else if (prescriber != null) {
                            str += "Rx by " + prescriber + ". ";
                        }

						SimpleQuantity dispenseQuantity = medOrderAttributes.getAttribute(medOrderId, attr -> attr.getDispenseQuantity());
						Integer numRepeatsAllowed = medOrderAttributes.getAttribute(medOrderId, attr -> attr.getNumRepeatsAllowed());

                        if ((dispenseQuantity != null) && (numRepeatsAllowed != null)) {
                            str += "Disp " + DoubleUtil.format(dispenseQuantity.getValue());

                            if (dispenseQuantity.hasUnit()) {
                                str += " " + dispenseQuantity.getUnit();
                            }

                            int refillCount = numRepeatsAllowed.intValue();
                            str += ", Refills " + refillCount + ".";
                        }

                        if (!str.equals("")) {
                            mmeEntry.addRxDetail(str);
                        }
					}

					// (if not new) (or new) Start date: XX/XX/17. End date (estimated): XX/XX/17. Based on (# refills and [if any refills])(days supply in Sig/days supply in note to pharmacy/dispense
                    // why (!x || x) ??
                    if (!mmeEntry.isDraft() || mmeEntry.isDraft()) {
						str = "";
                        if (mmeEntry.getStartDate() != null) {
                            str += "Start date: "
                                    + myDateUtility.getDateAsString(mmeEntry.getStartDate(), "MM/dd/yy") + ". ";
                        }

						if (endDate != null)
						{
							str += "End date";
							Boolean isEndDateEstimated = medOrderAttributes.getAttribute(medOrderId, attr -> attr.isEndDateEstimated());
                            if ((isEndDateEstimated != null) && (isEndDateEstimated.booleanValue() == true)) {
                                str += " (estimated)";
                            }
							str += ": " + myDateUtility.getDateAsString(endDate, "MM/dd/yy") + ". ";

							String endDateCalcMethod = medOrderAttributes.getAttribute(medOrderId, attr -> attr.getEndDateCalcMethod());
                            if (endDateCalcMethod != null) {
                                str += "Based on " + endDateCalcMethod + ".";
                            }

							mmeEntry.addRxDetail(str);
						}
					}

					// Daily dose (ave) and Daily dose (max): [various options].
					String dailyDoseStr_ave = medOrderAttributes.getAttribute(medOrderId, attr -> attr.getDailyDoseAvg());
					String dailyDoseStr_max = medOrderAttributes.getAttribute(medOrderId, attr -> attr.getDailyDoseMax());

					boolean dailyDoseStrIdentical = false;
					boolean alreadyProcessedDailyDoseStrMax = false;
                    if ((dailyDoseStr_ave != null) && (dailyDoseStr_max != null)
                            && (dailyDoseStr_ave.equals(dailyDoseStr_max))) {
                        dailyDoseStrIdentical = true;
                    }

                    if ((includeAvgMmePerDay) && (dailyDoseStr_ave != null)) {
                        if (dailyDoseStrIdentical) {
                            mmeEntry.addRxDetail("Daily dose (avg & max): " + dailyDoseStr_ave);
                            alreadyProcessedDailyDoseStrMax = true;
                        } else {
                            mmeEntry.addRxDetail("Daily dose (avg): " + dailyDoseStr_ave);
                        }
                    }

                    if ((dailyDoseStr_max != null) && (!alreadyProcessedDailyDoseStrMax)) {
                        mmeEntry.addRxDetail("Daily dose (max): " + dailyDoseStr_max);
                    }

					// if start date as on or after 1 year ago
					Date oneYearAgo = myDateUtility.getDateAfterAddingTime(evalTimeIgnoringTime, Calendar.YEAR, -1);
                    if (mmeEntry.getStartDate() != null && !mmeEntry.getStartDate().before(oneYearAgo)) {
                        unbinnedMmeEntries.add(mmeEntry);
                    }
				} else {
					System.err.println("Was unable to process med order: " + medOrder);
				}
			}

			// outside of individual med orders, identify potential duplicates
			Map<RxNormSemanticDrug, List<MmeEntry>> drugToMmeEntryListMap = new HashMap<RxNormSemanticDrug, List<MmeEntry>>();
			for (MmeEntry mmeEntry : unbinnedMmeEntries)
			{
				RxNormSemanticDrug drug = mmeEntry.getDrug();
				if (drug != null)
				{
					List<MmeEntry> mmeEntryListForDrug = drugToMmeEntryListMap.get(drug);
					if (mmeEntryListForDrug == null)
					{
						mmeEntryListForDrug = new ArrayList<MmeEntry>();
					}
					mmeEntryListForDrug.add(mmeEntry);
					drugToMmeEntryListMap.put(drug, mmeEntryListForDrug);
				}
			}

			Set<RxNormSemanticDrug> drugs = drugToMmeEntryListMap.keySet();
			for (RxNormSemanticDrug drug : drugs)
			{
				List<MmeEntry> mmeEntryListForDrug = drugToMmeEntryListMap.get(drug);
				if (mmeEntryListForDrug.size() > 1)
				{
					Collections.sort(mmeEntryListForDrug, new MmeDuplicateEntryComparator());
					for (int k = 0; k < mmeEntryListForDrug.size(); k++)
					{
						MmeEntry mmeEntry = mmeEntryListForDrug.get(k);
						if (k != 0)
						{
							mmeEntry.setIsPotentiallyDuplicate(true);
						}
						else
						{
							String warning = null;

							ArrayList<Date> duplicateStartDates = new ArrayList<Date>();
							for (int j = 1; j < mmeEntryListForDrug.size(); j++)
							{
								MmeEntry innerMmeEntry = mmeEntryListForDrug.get(j);
								Date startDate = innerMmeEntry.getStartDate();
								if (startDate != null)
								{
									duplicateStartDates.add(startDate);
								}
							}

							Collections.sort(duplicateStartDates);

							if (duplicateStartDates.size() == 1)
							{
								warning = "Not adding " + mmeShortLabel + " for presumed redundant Rx with start date of " + myDateUtility.getDateAsString(duplicateStartDates.get(0), "MM/dd/yy") + ".";
							}
							else if (duplicateStartDates.size() > 1)
							{
								warning = "Not adding " + mmeShortLabel + " for presumed redundant Rxs with start dates of ";
								for (int x = 0; x < duplicateStartDates.size(); x++)
								{
									warning += myDateUtility.getDateAsString(duplicateStartDates.get(x), "MM/dd/yy");
									if (x < duplicateStartDates.size() - 2)
									{
										warning += ", ";
									}
									else if (x == duplicateStartDates.size() - 2)
									{
										warning += " and ";
									}
								}
								warning += ".";
							}

							if (warning != null)
							{
								mmeEntry.addRxWarning(warning);
							}
						}
					}
				}
			}

			// outside of individual med orders, bin to potential duplicates vs. not.  Also calculate total MME quantity and # without known MME.
			for (MmeEntry mmeEntry : unbinnedMmeEntries)
			{
				boolean isPotentiallyDuplicate = mmeEntry.isPotentiallyDuplicate();
				if (isPotentiallyDuplicate)
				{
					myPotentiallyDuplicateMmeEntries.add(mmeEntry);
				}
				else // main table
				{
					// add MME info
					Double mmePerDay_ave = mmeEntry.getMmePerDay_ave();
					if (DoubleUtil.isNullOr0(mmePerDay_ave))
					{
						myNumEntriesWithUnknownMmeAvg++;
					}
					else
					{
						myTotalMmeAvg += mmePerDay_ave.doubleValue();
					}

					Double mmePerDay_max = mmeEntry.getMmePerDay_max();
					if (DoubleUtil.isNullOr0(mmePerDay_max))
					{
						myNumEntriesWithUnknownMmeMax++;
					}
					else
					{
						myTotalMmeMax += mmePerDay_max.doubleValue();
					}

					// warning re: oral buprenorphine
					if ((atLeastOneOralBuprenorphine) && (mmeEntry.isOralBuprenorphine() == false))
					{
						mmeEntry.addRxWarning("Concurrent oral buprenorphine  Rx.  Please consider goals of opioid prescribing.");
					}

					myMmeEntries.add(mmeEntry);
				}
			}
		}
	}

	public double getTotalMmeAvg() {
		return myTotalMmeAvg;
	}

	public double getTotalMmeMax() {
		return myTotalMmeMax;
	}

	public int getNumEntriesWithUnknownMmeAvg() {
		return myNumEntriesWithUnknownMmeAvg;
	}

	public int getNumEntriesWithUnknownMmeMax() {
		return myNumEntriesWithUnknownMmeMax;
	}

	public List<MmeEntry> getMmeEntries_sorted() {
		Collections.sort(myMmeEntries, new MmeEntryComparator());
		return myMmeEntries;
	}

	public List<MmeEntry> getPotentiallyDuplicateMmeEntries_sorted() {
		Collections.sort(myPotentiallyDuplicateMmeEntries, new MmeEntryComparator());
		return myPotentiallyDuplicateMmeEntries;
	}
}
