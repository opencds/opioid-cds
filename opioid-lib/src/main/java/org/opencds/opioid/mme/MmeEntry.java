package org.opencds.opioid.mme;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.opencds.tools.terminology.rxnav.api.model.RxNormSemanticDrug;

public class MmeEntry
{
	// Example output
	// Methadone Hydrochloride 10 MG Oral Tablet
	
	// WARNINGS
	// REMOVED -- (if applicable) MME estimated assuming XXd supply
	// (if applicable) Verify taking; Rx may have expired
	// (if applicable) (and not above) May be expiring soon
	// (if applicable) Concurrent oral buprenorphine Rx.  Please consider goals of opioid prescribing. (if there is another buprenorphine oral, and this is not a buprenorphine oral)
	//     - Priority for choosing the one to display (go down list of parameters and use those as tie-breakers if needed):
	//        - MME calculation avaialble
	//        - Draft status (vs. not)
	//   	  - Start date available (should be the case)
	//        		- Start date on or after evalTime (ignore time) 
	//        		- Start date closer to evalTime (ignore time)
	//        - MME quantity (larger = higher priority)
	//        - displayContents (medName + rxWarnings + rxDetails)	
	// (if applicable) Not adding OME for presumed redundant Rx(s) with start date(s) of XX/XX/XX, XX/XX/XX.
	
	// RELEVANT INFO
	// (if exists) Sig: Take 0.5 tablets by mouth Every 6 hours as needed for up to 180 days.  Max 2/day.
	// (if not exists and draft) Sig: unavailable for new Rx when entered as free text
	// (if exists) Note to pharmacy: DO NOT FILL UNTIL 12/19/17 EACH FILL must last 30 days,NRF 1/18/18, ERX
	// Morphine equivalence: 1.8x. (bold if MME not available for OME of interest) For 1 patch/tablet/etc., OME = 63 mg.
	// (if not new) (or new) Rx by John Smith, MD on XX/XX/17. Disp 360 tablet, Refills 0.  
	// (if not new) (or new) Start date: XX/XX/17.  End date (estimated): XX/XX/17. Based on (end date in sig/end date in note to pharmacy) (or) (# refills and [if any refills])(days supply in sig/days supply in note to pharmacy/disepense quantity and max daily dose in sig/dispense quantity and max daily dose in note to pharmacy/dispense quantity and use frequency/presumed XXd supply)
	//   Daily dose: [options below]. 

	// Patch
	// Daily dose (ave/max): Unable to calculate due to free-text sig.
	// Daily dose: Fentanyl patch: (max) 1 * 0.1 mg/hr = 0.1 mg/hr.
			
	// Injection
	// Daily dose: Unable to calculate OME for injectable drug.
			
	// Other		
	// Daily dose: 120 dispense * 5 mg / 30d supply (from sig/Rx note) = 20 mg
			
	// Daily dose: Unable to calculate due to free-text sig.
			
	// Daily dose: Oxycodone oral tablet 20 mg (daily max per sig).
	// Daily dose: Oxycodone oral tablet (max) 1 tablet * (max) 4/d * 5 mg = 20 mg.
			
	// Daily dose: Codeine elixir 5 ml (daily max per sig) * 4 mg/ml = 20 mg.
	// Daily dose: Codeine elixir (max) 2 ml * (max) 4/d * 4 mg/ml = 32 mg.
			
	// Daily dose: Oxycodone oral tablet 5 (daily max per sig) * 5mg = 25 mg.
	// Daily dose: Oxycodone oral tablet (max) 2 * (max) 5/d * 5 mg = 50 mg.
			
	// Daily dose: Oxycodone oral tablet 360 dispense * 10 mg / 180d supply = 20 mg
	
	// MME: XX mg or ?
	
	// hierarchy of calculation: (# dispense generally known)
	// For pills
	// 1. Days supply known from Sig (or if not there, in Rx note)  --> # dispense * strength / days supply
	// 2. Max/Average daily use known from Sig --> Max/average daily use
	// 3. Structured sig or fully parsed free-text sig --> Dosage * frequency
	// 4. If dispense quantity available and med strength allows for daily dose calculation given days supply, presume 30d supply (set by input parameter) to calculate OME 
	// 5. Unknown
	
	// For patches
	// 1. Structured sig or fully parsed free-text sig --> Dosage
	// 2. Unknown

	private Boolean myDraft;
	private Boolean myIsOralBuprenorphine;
	private RxNormSemanticDrug myDrug;
	private String myMedName; // e.g. Oxycodone-acetaminophen 5-325mg (Percocet) PO Tablet 
	private Double myMmePerDay_ave; // null or 0 if can't compute
	private Double myMmePerDay_max; // null or 0 if can't compute
	private List<String> myRxWarnings; // lines of the warnings to show for each medication
	private List<String> myRxDetails; // lines of the details to show for each medication
	private Date myStartDate;
	private Date myEvalTime;
	private Boolean myIsPotentiallyDuplicate; // potentially a duplicate med (i.e., other MmeEntr(ies) are likely the only ones that need to be shown  
	
	public MmeEntry() {
		myDraft = false;
		myIsPotentiallyDuplicate = false;
		myIsOralBuprenorphine = false;
		myRxWarnings = new ArrayList<String>();
		myRxDetails = new ArrayList<String>();
	}
	
	public boolean isDraft() {
	    if (myDraft == null) {
	        return false;
	    }
		return myDraft.booleanValue();
	}
	public void setDraft(Boolean draft) {
		this.myDraft = draft;
	}
	public boolean isOralBuprenorphine() {
	    if (myIsOralBuprenorphine == null) {
	        return false;
	    }
		return myIsOralBuprenorphine.booleanValue();
	}
	public void setIsOralBuprenorphine(Boolean isOralBuprenorphine) {
		this.myIsOralBuprenorphine = isOralBuprenorphine;
	}
	public RxNormSemanticDrug getDrug() {
		return myDrug;
	}
	public void setDrug(RxNormSemanticDrug drug) {
		this.myDrug = drug;
	}				
	public String getMedName() {
		return myMedName;
	}
	public void setMedName(String medName) {
		this.myMedName = medName;
	}
	public Double getMmePerDay_ave() {
		return myMmePerDay_ave;
	}
	public void setMmePerDay_ave(Double mmePerDay_ave) {
		this.myMmePerDay_ave = mmePerDay_ave;
	}		
	public Double getMmePerDay_max() {
		return myMmePerDay_max;
	}
	public void setMmePerDay_max(Double mmePerDay_max) {
		this.myMmePerDay_max = mmePerDay_max;
	}
	public Double getMmePerDay_lower() {
		if (myMmePerDay_ave == null)
		{
			return myMmePerDay_max;
		}
		else if (myMmePerDay_max == null)
		{
			return myMmePerDay_ave;				
		}			
		else
		{
			return new Double(Math.min(myMmePerDay_ave, myMmePerDay_max));
		}
	}
	public Double getMmePerDay_higher() {
		if (myMmePerDay_ave == null)
		{
			return myMmePerDay_max;
		}
		else if (myMmePerDay_max == null)
		{
			return myMmePerDay_ave;				
		}			
		else
		{
			return new Double(Math.max(myMmePerDay_ave, myMmePerDay_max));
		}
	}
	public List<String> getRxWarnings() {
		return myRxWarnings;
	}		
	public void addRxWarning(String rxWarning) {
		this.myRxWarnings.add(rxWarning);
	}
	public List<String> getRxDetails() {
		return myRxDetails;
	}		
	public void addRxDetail(String rxDetail) {
		this.myRxDetails.add(rxDetail);
	}		
	public Date getStartDate() {
		return myStartDate;
	}		
	public void setStartDate(Date startDate) {
		this.myStartDate = startDate;
	}		
	public Date getEvalTime() {
		return myEvalTime;
	}		
	public void setEvalTime(Date evalTime) {
		this.myEvalTime = evalTime;
	}
	public boolean isPotentiallyDuplicate() {
	    if (myIsPotentiallyDuplicate == null) {
	        return false;
	    }
		return myIsPotentiallyDuplicate.booleanValue();
	}
	public void setIsPotentiallyDuplicate(Boolean isPotentiallyDuplicate) {
		this.myIsPotentiallyDuplicate = isPotentiallyDuplicate;
	}
	public boolean hasSameAveAndMaxOmeContent()
	{
		if ((myMmePerDay_ave == null) && (myMmePerDay_max == null)) // if both null, they are the same
		{
			return true;
		}
		else if ((myMmePerDay_ave == null) || (myMmePerDay_max == null))// if at least one null, it means the other is not null, so they are not the same
		{
			return false;
		}
		return (myMmePerDay_ave.equals(myMmePerDay_max));
	}
	

	@Override
	public String toString() {
		return "MmeEntry [myDraft=" + myDraft + ", myIsOralBuprenorphine=" + myIsOralBuprenorphine + ", myDrug="
				+ myDrug + ", myMedName=" + myMedName + ", myMmePerDay_ave=" + myMmePerDay_ave + ", myMmePerDay_max=" + myMmePerDay_max + ", myRxWarnings="
				+ myRxWarnings + ", myRxDetails=" + myRxDetails + ", myStartDate=" + myStartDate + ", myEvalTime="
				+ myEvalTime + ", myIsPotentiallyDuplicate=" + myIsPotentiallyDuplicate + "]";
	}
}