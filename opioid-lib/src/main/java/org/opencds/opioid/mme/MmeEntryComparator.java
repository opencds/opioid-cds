package org.opencds.opioid.mme;

import java.util.Comparator;

import org.opencds.common.utilities.DateUtility;

public class MmeEntryComparator implements Comparator<MmeEntry>
{
	@Override
    public int compare(MmeEntry a, MmeEntry b) 
	{
		if (a.isDraft() && (! b.isDraft()))
		{
			return -1;
		}
		else if (b.isDraft() && (! a.isDraft()))
		{
			return 1;
		}
		else
		{
			String a_comparisonString = a.getMedName() + DateUtility.getInstance().getDateAsString(a.getStartDate(), "yyyy-MM-dd") + a.getRxWarnings() + a.getRxDetails();
			String b_comparisonString = b.getMedName() + DateUtility.getInstance().getDateAsString(b.getStartDate(), "yyyy-MM-dd") + b.getRxWarnings() + b.getRxDetails();
			
			if (a_comparisonString.compareTo(b_comparisonString) != 0)
			{
				return a_comparisonString.compareTo(b_comparisonString);
			}
			else
			{
				return (a.getMmePerDay_ave()).compareTo(b.getMmePerDay_ave());
			}						
		}		        
	}
}