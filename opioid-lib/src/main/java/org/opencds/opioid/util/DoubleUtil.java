package org.opencds.opioid.util;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;

public class DoubleUtil {

    private static NumberFormat decimalFormatter = new DecimalFormat("#########.##");

    public static String format(double value) {
        return decimalFormatter.format(value);
    }
    
    public static String format(BigDecimal value) {
        if (value == null) {
            return null;
        }
        return decimalFormatter.format(value.doubleValue());
    }
    
    public static boolean isNotNullOr0(Double dbl) {
        return (!isNullOr0(dbl));
    }

    public static boolean isNullOr0(Double dbl) {
        if (dbl == null || dbl.doubleValue() == 0) {
            return true;
        }
        return false;
    }

    public static String doubleFormat(double value) {
        return String.format("%3.2f", value);
    }

}
