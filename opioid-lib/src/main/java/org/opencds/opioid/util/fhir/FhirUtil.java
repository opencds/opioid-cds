package org.opencds.opioid.util.fhir;

import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hl7.fhir.dstu2.model.Bundle;
import org.hl7.fhir.dstu2.model.CodeableConcept;
import org.hl7.fhir.dstu2.model.Coding;
import org.hl7.fhir.dstu2.model.Resource;
import org.hl7.fhir.dstu2.model.ResourceType;

public class FhirUtil {
    private static final Log log = LogFactory.getLog(FhirUtil.class);

    public static String getSystemAndCode(CodeableConcept codeableConcept) {
        StringBuilder message = new StringBuilder();
        for (Coding coding : codeableConcept.getCoding()) {
            message.append(coding.getSystem()).append(" | ").append(coding.getCode()).append(", ");
        }
        if (message.length() == 0) {
            message.append("<no coding provided>");
        }
        return message.toString();
    }

    public static <R extends Resource> Predicate<R> resourceFilter(String id) {
        return (R res) -> {
            return res.getIdElement() != null && res.getIdElement().getIdPart() != null
                    && res.getIdElement().getIdPart().equals(id);
        };
    }

    public static <R extends Resource> List<R> findResources(Bundle contextMedOrders, ResourceType type) {
        if (contextMedOrders == null) {
            return Collections.emptyList();
        }
        return contextMedOrders.getEntry().stream().map(bec -> (R) bec.getResource()).filter(typeFilter(type))
                .collect(Collectors.toList());
    }

    public static Predicate<? super Resource> typeFilter(ResourceType type) {
        return (Resource r) -> type == r.getResourceType();
    }

}
