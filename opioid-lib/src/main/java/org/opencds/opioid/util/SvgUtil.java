package org.opencds.opioid.util;

import java.awt.Dimension;
import java.awt.RenderingHints;
import java.awt.RenderingHints.Key;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import org.apache.batik.dom.GenericDOMImplementation;
import org.apache.batik.svggen.SVGGraphics2D;
import org.apache.batik.svggen.SVGGraphics2DIOException;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;

public class SvgUtil {
    private static final String SVG_NS = "http://www.w3.org/2000/svg";
    private static final String SVG = "svg";
    
    public static SVGGraphics2D newSvg(int width, int height) {
        DOMImplementation domImpl = GenericDOMImplementation.getDOMImplementation();
        Document doc = domImpl.createDocument(SVG_NS, SVG, null);
        SVGGraphics2D g2d = new SVGGraphics2D(doc);
        g2d.setSVGCanvasSize(new Dimension(width, height));
        
        Map<Key, Object> hints = new HashMap<>();
        hints.put(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        hints.put(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        g2d.setRenderingHints(hints);
        
        return g2d;
    }
    
    public static String getContent(SVGGraphics2D g2d) {
        StringWriter writer = new StringWriter();
        try {
            g2d.stream(writer);
            return writer.toString();
        } catch (SVGGraphics2DIOException e) {
            throw new RuntimeException(e);
        }
    }
}
