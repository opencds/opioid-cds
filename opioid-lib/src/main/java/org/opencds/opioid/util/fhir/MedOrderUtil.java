package org.opencds.opioid.util.fhir;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hl7.fhir.dstu2.model.Bundle;
import org.hl7.fhir.dstu2.model.CodeableConcept;
import org.hl7.fhir.dstu2.model.Medication;
import org.hl7.fhir.dstu2.model.MedicationOrder;
import org.hl7.fhir.dstu2.model.Reference;
import org.hl7.fhir.dstu2.model.Resource;
import org.hl7.fhir.dstu2.model.ResourceType;
import org.hl7.fhir.dstu2.model.Type;
import org.hl7.fhir.dstu2.model.Bundle.BundleEntryComponent;
import org.hl7.fhir.dstu2.model.MedicationOrder.MedicationOrderStatus;
import org.opencds.tools.terminology.rxnav.api.model.RxNormSemanticDrug;

import ca.uhn.fhir.rest.client.api.IGenericClient;

public class MedOrderUtil {
    private static final Log log = LogFactory.getLog(MedOrderUtil.class);

    public static void searchAndAddMedOrdersIfNoneOtherThanDraft(IGenericClient fhirClient, String focalPersonId,
            List<MedicationOrder> medOrders) {
        boolean noMedOrdersBeyondDraft = true;
        Set<String> existingMedOrderIds = new HashSet<String>();
        if (medOrders != null) {
            for (MedicationOrder medOrder : medOrders) {
                existingMedOrderIds.add(medOrder.getId());
                if (medOrder.getStatus() != MedicationOrderStatus.DRAFT) {
                    noMedOrdersBeyondDraft = false;
                }
            }
        }

        if (noMedOrdersBeyondDraft) {
            Bundle response = null;
            try {
                if (fhirClient != null) {
                    // TODO: looks like this gets converted to
                    // MedicationOrder/_search?patient=16752. Look for different
                    // way to send.
                    response = fhirClient.search().byUrl("MedicationOrder?patient=16752").returnBundle(Bundle.class)
                            .execute();
                }
            } catch (Exception e) {
                log.warn("Encountered an error while connecting to FHIR server.");
                log.warn(e.getMessage());
                if (e.getCause() != null) {
                    log.warn(e.getCause().getMessage());
                }
                throw e;
            }
            if (response != null) {
                for (BundleEntryComponent entry : response.getEntry()) {
                    if (entry.getResource().getResourceType() == ResourceType.MedicationOrder) {
                        MedicationOrder medOrder = (MedicationOrder) entry.getResource();
                        String medOrderId = medOrder.getId();
                        if (!existingMedOrderIds.contains(medOrderId)) {
                            medOrders.add(medOrder);
                            existingMedOrderIds.add(medOrderId);
                        }
                    }
                }
            }
        }
    }

    /**
     * Swaps out <tt>medicationReference</tt>s for <tt>medicationCodeableConcept</tt>s
     * @param fhirClient
     * @param focalPersonId
     * @param medOrders
     * @param meds
     */
    public static void searchAndAddMedsIfReferencedByMedOrdersAndDoesNotExist(IGenericClient fhirClient, String focalPersonId,
            List<MedicationOrder> medOrders, List<Medication> meds) {
        if (medOrders == null) {
            return;
        }
        for (MedicationOrder medOrder : medOrders) {
            if (medOrder.getMedication() instanceof Reference) {
                // References
                String medIdFromOrder = ((Reference) medOrder.getMedication()).getReference();
                Medication med = meds.stream().filter((Medication testMed) -> {
                    return medIdFromOrder.equals(testMed.getIdElement().getIdPart());
                }).findFirst().orElse(null);
                if (med == null) {
                    if (fhirClient != null) {
                        try {
                            med = fhirClient.read().resource(Medication.class).withId(medIdFromOrder).execute();
                            medOrder.setMedication(med.getCode());
                        } catch (Exception e) {
                            log.warn("Encountered an error while connecting to FHIR server.", e);
                        }
                    }
                } else {
                    medOrder.setMedication(med.getCode());
                }
            } else if (medOrder.getMedication() instanceof CodeableConcept) {
                // CodeableConcepts
                log.debug("medOrder " + medOrder.getIdElement().getIdPart()
                        + " already has a med as codeableConcept");
            }
        }
    }

    public static CodeableConcept getAssociatedMed(MedicationOrder medOrder, List<Medication> meds) {
        CodeableConcept associatedMed = null;
        Type associated = medOrder.getMedication();
        if (associated instanceof CodeableConcept) {
            associatedMed = (CodeableConcept) associated;
            log.debug("> Associated Med (CodeableConcept): " + FhirUtil.getSystemAndCode(associatedMed));
        } else if (associated instanceof Reference) {
            Reference medRef = (Reference) associated;
            String id = medRef.getReferenceElement().getIdPart();
            Predicate<Resource> resourceFilter = FhirUtil.resourceFilter(id);
            Resource medRes = medOrder.getContained().parallelStream().filter(resourceFilter).findFirst().orElse(null);
            if (medRes != null) {
                associatedMed = ((Medication) medRes).getCode();
                log.debug("> Associated Med (contained Resource Reference): " + FhirUtil.getSystemAndCode(associatedMed));
            } else if (meds != null) {
                Medication med = meds.parallelStream().filter(resourceFilter).findFirst().orElse(null);
                if (med != null) {
                    associatedMed = med.getCode();
                    log.debug("> Associated Med (Reference): " + FhirUtil.getSystemAndCode(associatedMed));
                } else {
                    // TODO: query the FHIR server
                    log.debug("> Associated med not found");
                }
            }
        } else {
            log.warn("Associated Medication is neither a CodeableConcept nor a Reference");
        }
        return associatedMed;
    }

    public static String getMedName(MedicationOrder medOrder, CodeableConcept medCodeableConcept,
            RxNormSemanticDrug drug) {
        String medName = null;
        Type med = medOrder.getMedication();
        if (med instanceof Reference) {
            Reference reference = (Reference) med;
            if (reference != null) {
                String display = reference.getDisplay();
                if (display != null) {
                    medName = display;
                }
            }
        } else if (med instanceof CodeableConcept) {
                CodeableConcept cc = (CodeableConcept) med;
                medName = cc.getText();
        }

        if ((medName == null) && (medCodeableConcept != null)) {
            try {
                medName = medCodeableConcept.getText();
            } catch (Exception e) {
                // do nothing
            }
        }

        if (medName == null) {
            medName = drug.getRxNormConcept().getName();
        }
        return medName;
    }

    public static String getPrescriberName(MedicationOrder medOrder) {
        Reference prescriber = medOrder.getPrescriber();
        if ((prescriber != null) && (prescriber.getDisplay() != null)) {
            return prescriber.getDisplay();
        }
        return null;
    }

    public static boolean isDraftMedOrder(MedicationOrder medOrder) {
      if (medOrder.getStatus().equals(MedicationOrderStatus.DRAFT)) {
          return true;
      }
        return false;
    }



}
