package org.opencds.opioid.util.term;

import java.util.List;

import org.hl7.fhir.dstu2.model.CodeableConcept;
import org.hl7.fhir.dstu2.model.Coding;
import org.opencds.opioid.valuesets.UmlsValueSet;
import org.opencds.tools.terminology.api.config.CodeSystem;
import org.opencds.tools.terminology.umls.api.client.UmlsTerminologyClient;

public class UmlsUtil {

    public static boolean isConceptInUmlsValueSet(CodeableConcept concept, UmlsTerminologyClient umlsClient,
            UmlsValueSet valueSet) {
        List<Coding> l = concept.getCoding();
        for (Coding codingDt : l) {
            String codeSystem = codingDt.getSystem();

            // For some reason, some EHR FHIR APIs use the OID identifier for
            // ICD10CM. These EHR implementations are divergent from what is
            // recommended in the US Core FHIR profile
            // (http://hl7.org/fhir/us/core/ValueSet-us-core-condition-category.html)
            if ((CodeSystem.ICD_10_CM.getUrn().equalsIgnoreCase(codeSystem))
                    || (CodeSystem.ICD_10_CM.getOid().equalsIgnoreCase(codeSystem))) {
                if (umlsClient.isConceptInValueSet(CodeSystem.ICD_10_CM, codingDt.getCode(), Integer.toString(valueSet.getId()), null)) {
                    return true;
                }
            } else if (CodeSystem.ICD_9_CM.getUrn().equalsIgnoreCase(codeSystem)) {
                if (umlsClient.isConceptInValueSet(CodeSystem.ICD_9_CM, codingDt.getCode(), Integer.toString(valueSet.getId()), null)) {
                    return true;
                }
            } else if (CodeSystem.SNOMED_CT.getUrn().equalsIgnoreCase(codeSystem)) {
                if (umlsClient.isConceptInValueSet(CodeSystem.SNOMED_CT, codingDt.getCode(), Integer.toString(valueSet.getId()), null)) {
                    return true;
                }
            }
        }
        return false;
    }

}
