package org.opencds.opioid.util;

import java.net.MalformedURLException;
import java.net.URL;

public class UrlUtil {
    
    public static URL newURL(String string) {
        try {
            return new URL(string);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return null;
    }       

}
