package org.opencds.opioid.util.fhir;

import java.util.ArrayList;
import java.util.List;

import org.hl7.fhir.dstu2.model.CodeableConcept;
import org.hl7.fhir.dstu2.model.Extension;
import org.hl7.fhir.dstu2.model.Medication;
import org.hl7.fhir.dstu2.model.MedicationOrder;
import org.hl7.fhir.dstu2.model.MedicationOrder.MedicationOrderStatus;
import org.opencds.Utils;
import org.opencds.opioid.config.OpioidConfig;
import org.opencds.opioid.util.term.RxNavUtil;
import org.opencds.opioid.valuesets.RxNavValueSet;
import org.opencds.tools.terminology.rxnav.client.RxNavTerminologyClient;

public class MatchingMedOrders {

    public static List<MedicationOrder> get(List<MedicationOrder> medOrders,
            List<Medication> meds, List<MedicationOrderStatus> allowedMedicationOrderStatuses, String focalPersonId,
            RxNavTerminologyClient rxNavClient, RxNavValueSet rxNavValueSet, OpioidConfig opioidConfig) {
        List<MedicationOrder> returnList = new ArrayList<MedicationOrder>();

        if (medOrders != null) {
            for (MedicationOrder medOrder : medOrders) {
                CodeableConcept associatedMed = MedOrderUtil.getAssociatedMed(medOrder, meds);

                if ((Utils.isReferencedPatientIdFocalPersonId(medOrder.getPatient(), focalPersonId))
                        && (medOrder.getStatus() != null) && (associatedMed != null)
                        && (RxNavUtil.isConceptInRxNavValueSet(associatedMed, rxNavClient, rxNavValueSet))) {
                    boolean passesMedStatementCategoryRestriction = false;
                    if (opioidConfig.isRestrictToSpecifiedMedStatementCategories()) {
                        List<Extension> extensions = medOrder.getExtension();
                        for (Extension extension : extensions) {
                            String url = extension.getUrl();
                            if (url != null
                                    && ("http://hl7.org/fhir/StructureDefinition/extension-MedicationStatement.category".equals(url)
                                    || "http://hl7.org/fhir/3.0/StructureDefinition/extension-MedicationRequest.category".equals(url)
                            )) {
                                try {
                                    CodeableConcept codeableConcept = (CodeableConcept) extension.getValue();
                                    if (opioidConfig.getMedStatementCategoriesAllowed() != null) {
                                        for (String medStatementCategoryAllowed : opioidConfig
                                                .getMedStatementCategoriesAllowed()) {
                                            if (Utils.isCodingContains(codeableConcept,
                                                    "http://hl7.org/fhir/medication-request-category",
                                                    medStatementCategoryAllowed)) {
                                                passesMedStatementCategoryRestriction = true;
                                            }
                                        }
                                    }
                                } catch (Exception e) {
                                    // do nothing
                                }
                            }
                        }
                    } else {
                        passesMedStatementCategoryRestriction = true;
                    }

                    if (passesMedStatementCategoryRestriction) {
                        MedicationOrderStatus medOrderStatus = medOrder.getStatus();

                        if ((medOrderStatus != null) && (allowedMedicationOrderStatuses != null)
                                && (allowedMedicationOrderStatuses.contains(medOrderStatus))) {
                            returnList.add(medOrder);
                        }
                    }
                }
            }
        }

        return returnList;
    }

}
