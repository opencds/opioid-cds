package org.opencds.opioid.util.term;

import java.util.List;

import org.hl7.fhir.dstu2.model.CodeableConcept;
import org.hl7.fhir.dstu2.model.Coding;
import org.opencds.opioid.valuesets.RxNavValueSet;
import org.opencds.tools.terminology.rxnav.client.RxNavTerminologyClient;

public class RxNavUtil {

    private static final String RXNORM_URN = "http://www.nlm.nih.gov/research/umls/rxnorm";

    public static boolean isConceptInRxNavValueSet(CodeableConcept concept, RxNavTerminologyClient rxNavClient,
            RxNavValueSet valueSet) {
        List<Coding> l = concept.getCoding();
        for (Coding codingDt : l) {
            if (codingDt.getSystem().equalsIgnoreCase(RXNORM_URN)) {
                if (rxNavClient.isDrugInValueSet(codingDt.getCode(), valueSet.getId())) {
                    return true;
                }
            }
        }
        return false;
    }

}
