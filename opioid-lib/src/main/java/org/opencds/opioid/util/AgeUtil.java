package org.opencds.opioid.util;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class AgeUtil {
    private static final Log log = LogFactory.getLog(AgeUtil.class);

    /**
     * 
     * @param date
     *            date to compare
     * @param evalTime
     *            evaluation time
     * @param age
     *            age in years
     * @return
     */
    public static boolean underAge(Date date, Date evalTime, int age) {
        if (date == null) {
            log.warn("Could not calculate age; date from resource is null.");
            return false;
        }
        if (evalTime == null) {
            log.warn("Could not calculate age; evalTime is null.");
            return false;
        }
        LocalDate ldate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate evalDateMinusAge = evalTime.toInstant().atZone(ZoneId.systemDefault()).toLocalDate().minusYears(age);
        return evalDateMinusAge.isBefore(ldate);
    }
}
