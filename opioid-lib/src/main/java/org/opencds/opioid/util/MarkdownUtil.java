package org.opencds.opioid.util;

import java.util.Arrays;

import com.vladsch.flexmark.ext.gfm.strikethrough.StrikethroughExtension;
import com.vladsch.flexmark.ext.tables.TablesExtension;
import com.vladsch.flexmark.html.HtmlRenderer;
import com.vladsch.flexmark.parser.Parser;
import com.vladsch.flexmark.util.ast.Node;
import com.vladsch.flexmark.util.data.MutableDataSet;

public class MarkdownUtil {

    private Parser myParser;
    private HtmlRenderer myRenderer;

    public MarkdownUtil() {
        // from https://github.com/vsch/flexmark-java
        MutableDataSet options = new MutableDataSet();
        options.set(Parser.EXTENSIONS, Arrays.asList(TablesExtension.create(), StrikethroughExtension.create()));

        // uncomment to convert soft-breaks to hard breaks
        // options.set(HtmlRenderer.SOFT_BREAK, "<br />\n");
        myParser = Parser.builder(options).build();
        myRenderer = HtmlRenderer.builder(options).build();
    }

    public String getHtmlFromMarkDown(String markDown) {
        Node document = myParser.parse(markDown);
        return myRenderer.render(document);
    }

}
