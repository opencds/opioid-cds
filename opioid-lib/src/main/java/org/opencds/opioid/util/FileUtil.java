package org.opencds.opioid.util;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class FileUtil {
    private static final Log log = LogFactory.getLog(FileUtil.class);

    private static Pattern MULTISPACE = Pattern.compile("\\s+");
    private static String SPACE = " ";

    public static String getTemplateString(String resource) {
        log.error("RESOURCE: " + resource);
        listResources();
        URL url = FileUtil.class.getClassLoader().getResource(resource);
        log.error("URL: " + url);
        String lines = "";
        if (url.getProtocol().equals("jar")) {
            Map<String, String> env = new HashMap<>();
            env.put("create", "true");
            /*
             * FIXME: this is prone to FileSystemAlreadyExistsException
             *  this appears to arise when two calls to get the same filesystem
             *  are made simultaneously (e.g., via MmeConstants and
             *  NaloxoneInpatientConstants).  ZipFileSystemProvider.newFileSystem
             *  synchronizes access but it seems there may be a race condition
             *  where the FileSystem is already loaded when the second call hits
             *  that particular piece of code.
             *
             * TODO: We encountered a similar issue with EhrMappingTool, where the
             *  fix was to leverage a BufferedReader (read: InputStream).
             */
            try (FileSystem fs = FileSystems.newFileSystem(url.toURI(), env)) {
                lines = Files.lines(fs.getPath(url.getPath().split("!")[1]))
                        .map(s -> s.replaceAll(MULTISPACE.pattern(), SPACE)).collect(Collectors.joining());
            } catch (IOException | URISyntaxException | ArrayIndexOutOfBoundsException e) {
                log.error("Error reading template resource (jar): " + resource, e);
            }
        } else {
            try {
                Path path = Paths.get(url.toURI());
                lines = Files.lines(path).map(s -> s.replaceAll(MULTISPACE.pattern(), SPACE))
                        .collect(Collectors.joining());
            } catch (IOException | URISyntaxException e) {
                log.error("Error reading template resource (fs): " + resource, e);
            }
        }
        if (lines == null) {
            log.error("Error reading template resource (unknown): " + resource);
            return "";
        }
        return lines;
    }

    private static void listResources() {
        URL url = FileUtil.class.getClassLoader().getResource("/");
        log.error("URL: " + url);
    }

}
