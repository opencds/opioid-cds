package org.opencds.opioid.util;

public class StringUtil {
    public static String getStrWithPrecedingSpaceIfNotNull(String str) {
        return str == null ? "" : " " + str;
    }

    public static String getLabelIfTrue(String label, boolean condition) {
        return condition == true ? label : "";
    }

    /**
     * Gets strength before "/", or if no "/", itself; but in lower case.
     * 
     * @param strengthUnit
     * @return
     */
    public static String getStrengthUnitPrefixInLowerCase(String strengthUnit) {
        String stringToReturn = strengthUnit.toLowerCase();

        int dividerIndex = stringToReturn.indexOf("/");
        if (dividerIndex != -1) {
            stringToReturn = stringToReturn.substring(0, dividerIndex);
        }

        return stringToReturn;
    }
    
    public static boolean toBoolean(String value) {
        if (value != null) {
            return Boolean.valueOf(value.trim()).booleanValue();
        }
        return false;
    }
    
    public static String emptyIfNull(String value) {
        if (value == null) {
            return "";
        }
        return value;
    }

}
