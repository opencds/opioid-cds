package org.opencds.opioid;

import java.net.URI;
import java.util.Date;
import java.util.List;

import ca.uhn.fhir.context.FhirVersionEnum;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hl7.fhir.dstu2.model.Condition;
import org.hl7.fhir.dstu2.model.Encounter;
import org.hl7.fhir.dstu2.model.Medication;
import org.hl7.fhir.dstu2.model.MedicationOrder;
import org.hl7.fhir.dstu2.model.Observation;
import org.hl7.fhir.dstu2.model.Patient;
import org.hl7.fhir.dstu2.model.Practitioner;
import org.hl7.fhir.dstu2.model.Procedure;
import org.opencds.hooks.lib.fhir.FhirContextFactory;
import org.opencds.hooks.model.request.CdsRequest;
import org.opencds.hooks.model.response.Card;
import org.opencds.hooks.model.response.CdsResponse;
import org.opencds.opioid.config.OpioidConfig;
import org.opencds.opioid.eval.BenzodiazepineEvaluation;
import org.opencds.opioid.eval.DrugTestingEvaluation;
import org.opencds.opioid.eval.ImmediateReleaseOpioidEvaluation;
import org.opencds.opioid.eval.MmeEvaluation;
import org.opencds.opioid.eval.NaloxoneEvaluation;
import org.opencds.opioid.eval.NaloxoneInpatientEvaluation;
import org.opencds.opioid.eval.PeriodicAssessmentEvaluation;
import org.opencds.opioid.results.EvalResults;
import org.opencds.opioid.util.fhir.MedOrderUtil;
import org.opencds.tools.terminology.rxnav.client.RxNavTerminologyClient;
import org.opencds.tools.terminology.umls.api.client.UmlsTerminologyClient;

import ca.uhn.fhir.rest.client.api.IGenericClient;

public class UtilsOpioid {
    private static final Log log = LogFactory.getLog(UtilsOpioid.class);

    private static UtilsOpioid _instance = new UtilsOpioid();

    private UtilsOpioid() {
    }

    public static UtilsOpioid getInstance() {
        return _instance;
    }

    public CdsResponse getMmeEvaluationResultsAsCdsResponse(Date evalTime, URI serverUri, String focalPersonId,
            RxNavTerminologyClient rxNavClient, UmlsTerminologyClient umlsClient, CdsRequest request, Patient patient,
            List<MedicationOrder> medOrders, List<Medication> meds, List<Condition> conditions,
            List<Practitioner> practitioners, List<Encounter> encounters, OpioidConfig opioidConfig) {
        MmeEvaluation evaluation = new MmeEvaluation(rxNavClient, umlsClient, opioidConfig);

        IGenericClient fhirClient = null;

        // This is a potentially time-consuming step, so only conducting if
        // needed
        // TODO: refactor the FHIR client so it is pushed in if needed, so it
        // can be created once then re-used
        if (opioidConfig.isQueryForMedOrders() || opioidConfig.isQueryForMeds()) {
            fhirClient = request.getFhirClient(FhirContextFactory.get(FhirVersionEnum.DSTU2_HL7ORG));
        }

        if (opioidConfig.isQueryForMedOrders()) {
            MedOrderUtil.searchAndAddMedOrdersIfNoneOtherThanDraft(fhirClient, focalPersonId, medOrders);
        }

        if (opioidConfig.isQueryForMeds()) {
            MedOrderUtil.searchAndAddMedsIfReferencedByMedOrdersAndDoesNotExist(fhirClient, focalPersonId, medOrders,
                    meds);
        }

        EvalResults results = evaluation.getMmeEvaluationResults(evalTime, focalPersonId, serverUri, patient, medOrders,
                meds, conditions);

        CdsResponse cdsResponse = results.getCdsResponse();
        if (!results.isReturnCard()) {
            log.info("Not returning card.");
            List<Card> cards = cdsResponse.getCards();
            cards.forEach(card -> {
                log.info("Summary: " + card.getSummary());
                log.info("Detail: " + card.getDetail());
            });
            return new CdsResponse();
        }
        return cdsResponse;
    }

    /*
     * FIXME: convert DrugTesting to a model similar to MME.
     */
    public CdsResponse getDrugTestingEvaluationResultsAsCdsResponse(Date evalTime, URI serverUri, String focalPersonId,
            RxNavTerminologyClient rxNavClient, UmlsTerminologyClient umlsClient, List<MedicationOrder> medOrders,
            List<Medication> meds, List<Condition> conditions, List<Observation> observations,
            OpioidConfig opioidConfig) {
        DrugTestingEvaluation evaluation = new DrugTestingEvaluation(rxNavClient, umlsClient, opioidConfig);
        EvalResults results = evaluation.getDrugTestingEvaluationResults(evalTime, focalPersonId, serverUri, medOrders,
                meds, conditions, observations);

        CdsResponse cdsResponse = results.getCdsResponse();
        if (!results.isReturnCard()) {
            log.info("Not returning card.");
            List<Card> cards = cdsResponse.getCards();
            cards.forEach(card -> {
                log.info("Summary: " + card.getSummary());
                log.info("Detail: " + card.getDetail());
            });
            return new CdsResponse();
        }
        return cdsResponse;
    }

    /*
     * FIXME: convert PeriodicAssessment to a model similar to MME.
     */
    public CdsResponse getPeriodicAssessmentEvaluationResultsAsCdsResponse(Date evalTime, URI serverUri, String focalPersonId,
            RxNavTerminologyClient rxNavClient, UmlsTerminologyClient umlsClient, List<MedicationOrder> medOrders,
            List<Medication> meds, List<Practitioner> practitioners, List<Encounter> encounters,
            List<Condition> conditions, List<Procedure> procedures, OpioidConfig opioidConfig) {
        PeriodicAssessmentEvaluation evaluation = new PeriodicAssessmentEvaluation(rxNavClient, umlsClient,
                opioidConfig);
        EvalResults results = evaluation.getPeriodicAssessmentEvaluationResults(evalTime, focalPersonId, serverUri, medOrders,
                meds, encounters, conditions, procedures);

        CdsResponse cdsResponse = results.getCdsResponse();
        if (!results.isReturnCard()) {
            log.info("Not returning card.");
            List<Card> cards = cdsResponse.getCards();
            cards.forEach(card -> {
                log.info("Summary: " + card.getSummary());
                log.info("Detail: " + card.getDetail());
            });
            return new CdsResponse();
        }
        return cdsResponse;
    }

    /*
     * FIXME: convert to a model similar to MME.
     */
    public CdsResponse getNaloxoneEvaluationResultsAsCdsResponse(Date evalTime, URI serverUri, String focalPersonId,
            RxNavTerminologyClient rxNavClient, UmlsTerminologyClient umlsClient, List<MedicationOrder> medOrders,
            List<Medication> meds, List<Practitioner> practitioners, List<Encounter> encounters,
            List<Condition> conditions, OpioidConfig opioidConfig) {
        NaloxoneEvaluation evaluation = new NaloxoneEvaluation(rxNavClient, umlsClient, opioidConfig);
        EvalResults results = evaluation.getNaloxoneEvaluationResults(evalTime, focalPersonId, serverUri, medOrders, meds,
                conditions);

        CdsResponse cdsResponse = results.getCdsResponse();
        if (!results.isReturnCard()) {
            log.info("Not returning card.");
            List<Card> cards = cdsResponse.getCards();
            cards.forEach(card -> {
                log.info("Summary: " + card.getSummary());
                log.info("Detail: " + card.getDetail());
            });
            return new CdsResponse();
        }
        return cdsResponse;
    }

    /*
     * FIXME: convert to a model similar to MME.
     */
    public CdsResponse getNaloxoneInpatientEvaluationResultsAsCdsResponse(Date evalTime, URI serverUri,
            String focalPersonId, RxNavTerminologyClient rxNavClient, UmlsTerminologyClient umlsClient,
            List<MedicationOrder> medOrders, List<Medication> meds, List<Condition> conditions,
            List<Practitioner> practitioners, List<Encounter> encounters, OpioidConfig opioidConfig) {
        NaloxoneInpatientEvaluation evaluation = new NaloxoneInpatientEvaluation(rxNavClient, umlsClient, opioidConfig);
        EvalResults results = evaluation.getNaloxoneInpatientEvaluationResults(evalTime, focalPersonId, serverUri, medOrders,
                meds, conditions);

        CdsResponse cdsResponse = results.getCdsResponse();
        if (!results.isReturnCard()) {
            log.info("Not returning card.");
            List<Card> cards = cdsResponse.getCards();
            cards.forEach(card -> {
                log.info("Summary: " + card.getSummary());
                log.info("Detail: " + card.getDetail());
            });
            return new CdsResponse();
        }
        return cdsResponse;
    }

    /*
     * FIXME: convert to a model similar to MME.
     */
    public CdsResponse getBenzodiazepineEvaluationResultsAsCdsResponse(Date evalTime, URI serverUri, String focalPersonId,
            RxNavTerminologyClient rxNavClient, UmlsTerminologyClient umlsClient, List<MedicationOrder> medOrders,
            List<Medication> meds, List<Practitioner> practitioners, List<Encounter> encounters,
            List<Condition> conditions, OpioidConfig opioidConfig) {
        BenzodiazepineEvaluation evaluation = new BenzodiazepineEvaluation(rxNavClient, umlsClient, opioidConfig);
        EvalResults results = evaluation.getBenzodiazepineEvaluationResults(evalTime, focalPersonId, serverUri, medOrders, meds,
                conditions);

        CdsResponse cdsResponse = results.getCdsResponse();
        if (!results.isReturnCard()) {
            log.info("Not returning card.");
            List<Card> cards = cdsResponse.getCards();
            cards.forEach(card -> {
                log.info("Summary: " + card.getSummary());
                log.info("Detail: " + card.getDetail());
            });
            return new CdsResponse();
        }
        return cdsResponse;
    }

    /*
     * FIXME: convert to a model similar to MME.
     */
    public CdsResponse getImmediateReleaseOpioidEvaluationResultsAsCdsResponse(Date evalTime, URI serverUri, String focalPersonId,
            RxNavTerminologyClient rxNavClient, UmlsTerminologyClient umlsClient, List<MedicationOrder> medOrders,
            List<Medication> meds, List<Practitioner> practitioners, List<Encounter> encounters,
            List<Condition> conditions, OpioidConfig opioidConfig) {
        ImmediateReleaseOpioidEvaluation evaluation = new ImmediateReleaseOpioidEvaluation(rxNavClient, umlsClient,
                opioidConfig);
        EvalResults results = evaluation.getImmediateReleaseOpioidEvaluationResults(evalTime, focalPersonId, serverUri, medOrders,
                meds, encounters, conditions);

        CdsResponse cdsResponse = results.getCdsResponse();
        if (!results.isReturnCard()) {
            log.info("Not returning card.");
            List<Card> cards = cdsResponse.getCards();
            cards.forEach(card -> {
                log.info("Summary: " + card.getSummary());
                log.info("Detail: " + card.getDetail());
            });
            return new CdsResponse();
        }
        return cdsResponse;
    }
}
