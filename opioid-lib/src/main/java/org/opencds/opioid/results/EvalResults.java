package org.opencds.opioid.results;

import java.util.List;

import org.opencds.hooks.model.response.Card;
import org.opencds.hooks.model.response.CdsResponse;
import org.opencds.hooks.model.response.Indicator;
import org.opencds.hooks.model.response.Link;
import org.opencds.hooks.model.response.Source;

public class EvalResults {

    private boolean returnCard;
    private CdsResponse response;

    private EvalResults() {}
    
    /**
     * Builder for an {@link EvalResults} instance that contains the {@link CdsResponse} with the following objects and whether to return the card:
     * <ul>
     * <li>
     *      returnCard  boolean.  True if a CDS Hooks card should be returned (action needed), False otherwise.  If False, only summary returned for debug purposes.
     * </li>
     * <li>response ({@link CdsResponse}) containing a single {@link Card} with the following content:
     *  <ul>
     *      <li>summary     String.  One-sentence, <140-character summary message for display to the user inside of this card.</li>
     *      <li>detail      String.  Optional detailed information to display, represented in Markdown.  Provides guidance + MME table.</li>
     *      <li>indicator   Indicator.  "info" if returnCard == true, null otherwise.</li>
     *      <li>source      Source.     Source of guidance (CDC opioid guidance).</li>
     *      <li>links       List<Link>. Links to further CDC info.</li>
     * </li>
     * </ul>
     * @param returnCard
     * @param summary
     * @param detail
     * @param indicator
     * @param source
     * @param links
     * 
     * @param source
     */
    public static EvalResults buildEvalResults(boolean returnCard, String summary, String detail,
            Indicator indicator, Source source, List<Link> links) {
        EvalResults results = new EvalResults();
        results.setReturnCard(returnCard);
        CdsResponse response = new CdsResponse();
        Card card = new Card();
        card.setSummary(summary);
        card.setDetail(detail);
        card.setIndicator(indicator);
        card.setSource(source);
        card.setLinks(links);
        response.addCard(card);
        results.setCdsResponse(response);
        return results;
    }

    public void setReturnCard(boolean returnCard) {
        this.returnCard = returnCard;
    }

    public CdsResponse getCdsResponse() {
        return response;
    }

    public boolean isReturnCard() {
        return returnCard;
    }

    public void setCdsResponse(CdsResponse response) {
        this.response = response;
    }

}
