package org.opencds.opioid.display;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.IntStream;

import org.opencds.common.utilities.DateUtility;
import org.opencds.opioid.mme.MmeEntry;
import org.opencds.opioid.util.DoubleUtil;

public class TableUtil {

    @Deprecated
    public static String getMmeTable(boolean includeAverageMmePerDay, boolean useDynamicTableFormat, List<MmeEntry> sortedMmeEntryList,
            String tableMainColumnLabel, String mmeShortLabel, boolean calculateTotal, double totalMme_ave,
            double totalMme_max, int numEntriesWithUnknownMme_ave, int numEntriesWithUnknownMme_max, int defaultOpioidRxDaysSupplyEstimate) {
        String pad = " style=\"padding: 2px;\"";

        boolean aveAndMmeEntriesIdentical = true;
        for (MmeEntry mmeEntry : sortedMmeEntryList) {
            if (!mmeEntry.hasSameAveAndMaxOmeContent()) {
                aveAndMmeEntriesIdentical = false;
            }
        }

        //String mmeDetails = "<table border" + pad + ">\n" + "<tr>\n" + "  <th" + pad + ">" + tableMainColumnLabel
        String mmeDetails = "<table border=\"1\"" + pad + ">\n" + "<tr>\n" + "  <th" + pad + ">" + tableMainColumnLabel
                + "</th> \n" + "  <th" + pad + ">" + "Start Date" + "</th> \n";

        if (!includeAverageMmePerDay) {
            mmeDetails += "  <th" + pad + ">Max<br>" + mmeShortLabel + "/day*</th> \n";
        } else {
            if (aveAndMmeEntriesIdentical) {
                mmeDetails += "  <th" + pad + ">Avg. & Max<br>" + mmeShortLabel + "/day*</th> \n";
            } else {
                mmeDetails += "  <th" + pad + ">Avg.<br>" + mmeShortLabel + "/day*</th> \n" + "  <th" + pad + ">Max<br>"
                        + mmeShortLabel + "/day*</th> \n";
            }
        }

        mmeDetails += "</tr> \n";

        for (int j = 0; j < sortedMmeEntryList.size(); j++) {
        	MmeEntry mmeEntry = sortedMmeEntryList.get(j);
            mmeDetails += "<tr> \n";
            mmeDetails += "  <td" + pad + " align=\"left\">";
            if (mmeEntry.isDraft()) {
                mmeDetails += "<span style=\"font-weight: 600\">[ New ] ";
            } else {
                mmeDetails += "<span style=\\\"font-weight: 600\\\">";
            }

            mmeDetails += mmeEntry.getMedName() + "</span><br>";

            List<String> warnings = mmeEntry.getRxWarnings();
            for (int k = 0; k < warnings.size(); k++) {
                String warning = warnings.get(k);
                mmeDetails += "<b>*** " + warning + " ***</b><br>";
            }

            List<String> details = mmeEntry.getRxDetails();
            List<String> details_primary = new ArrayList<String>();
            List<String> details_secondary = new ArrayList<String>();
            if (useDynamicTableFormat)
            {
            	for (String detail : details)
            	{
            		if (detail.startsWith("Sig:") || detail.contains("<b>"))
    				{
            			details_primary.add(detail);    			
    				}
            		else
            		{
            			details_secondary.add(detail);
            		}
            	}
            	
            	for (int k = 0; k < details_primary.size(); k++) {
                    String detail_primary = details_primary.get(k);
                    mmeDetails += "&gt; " + detail_primary;
                    if (k < details_primary.size() - 1) {
                        mmeDetails += "<br>";
                    }
                    else {
                    	mmeDetails += "\n";
                    }                    	
                }
            	
            	if (details_secondary.size() > 0)
            	{
            		//mmeDetails += "<input id=\"toggle" + j + "\" type = \"checkbox\" unchecked = \"\"/>\n"
            		mmeDetails += "<input id=\"toggle" + j + "\" type = \"checkbox\"/>\n"
            				+ "<label for=\"toggle" + j + "\">Click for Details</label>\n"
            				+ "<div id=\"expand" + j + "\">";
            		
            		for (int k = 0; k < details_secondary.size(); k++) {
                        String detail_secondary = details_secondary.get(k);
                        mmeDetails += "&gt; " + detail_secondary;
                        if (k < details_secondary.size() - 1) {
                            mmeDetails += "<br>";
                        }
                        else {
                        	mmeDetails += "</div>\n";
                        }                    	
                    }
            	}            	    			
            }
            else
            {
            	for (int k = 0; k < details.size(); k++) {
                    String detail = details.get(k);
                    mmeDetails += "&gt; " + detail;
                    if (k < details.size() - 1) {
                        mmeDetails += "<br>";
                    }
                }            	
            }            

            mmeDetails += "</td> \n";

            mmeDetails += "  <td" + pad + " align=\"center\">";
            Date startDate = mmeEntry.getStartDate();
            String startDateStr = "";
            if (startDate != null) // should be the case
            {
                startDateStr = DateUtility.getInstance().getDateAsString(mmeEntry.getStartDate(), "MM/dd/yy");
            }

            mmeDetails += startDateStr + "</td> \n";

            String mmeStr_ave = "?";
            String mmeStr_max = "?";

            Double mme_ave = mmeEntry.getMmePerDay_ave();
            Double mme_max = mmeEntry.getMmePerDay_max();

            if (mme_ave != null) {
                mmeStr_ave = DoubleUtil.format(mme_ave) + " mg";
            }

            if (mme_max != null) {
                mmeStr_max = DoubleUtil.format(mme_max) + " mg";
            }

            if (!includeAverageMmePerDay) {
                mmeDetails += "  <td" + pad + ">" + mmeStr_max + "</td> \n";
            } else {
                if (aveAndMmeEntriesIdentical) {
                    mmeDetails += "  <td" + pad + " align=\"center\">" + mmeStr_ave + "</td> \n";
                } else {
                    mmeDetails += "  <td" + pad + " align=\"center\">" + mmeStr_ave + "</td> \n" + "  <td" + pad
                            + " align=\"center\">" + mmeStr_max + "</td> \n";
                }
            }

            mmeDetails += "</tr> \n";
        }

        if (calculateTotal) {
            mmeDetails += "<tr> \n" + "  <td colspan=\"2\" " + pad + " align=\"center\"><b>Total</b></td> \n";
            if (!includeAverageMmePerDay) {
                mmeDetails += "  <td" + pad + " align=\"center\"><b>"
                        + DoubleUtil.format(totalMme_max) + " mg"
                        + getUnknownMmeRxCountStr(numEntriesWithUnknownMme_max) + "</b></td> \n";
            } else {
                if (aveAndMmeEntriesIdentical) {
                    mmeDetails += "  <td" + pad + " align=\"center\"><b>"
                            + DoubleUtil.format(totalMme_ave) + " mg"
                            + getUnknownMmeRxCountStr(numEntriesWithUnknownMme_ave) + "</b></td> \n";
                } else {
                    mmeDetails += "  <td" + pad + " align=\"center\"><b>"
                            + DoubleUtil.format(totalMme_ave) + " mg"
                            + getUnknownMmeRxCountStr(numEntriesWithUnknownMme_ave) + "</b></td> \n" + "  <td" + pad
                            + " align=\"center\"><b>" + DoubleUtil.format(totalMme_max) + " mg"
                            + getUnknownMmeRxCountStr(numEntriesWithUnknownMme_max) + "</b></td> \n";
                }
            }
            mmeDetails += "</tr> \n";
        }

        mmeDetails += "</table>\n"
        		//+ "*Average " + mmeShortLabel + " calculated as (quantity dispensed)/(days supply). " + defaultOpioidRxDaysSupplyEstimate + "d supply assumed unless otherwised noted in Sig or note to pharmacy.<br>"
        		//+ "*Max " + mmeShortLabel + " = max amount patient may take on a given day according to Sig, even if patient runs out of med early.\n";;
        		+ "<table border=\"0\"><tr><td>*Ave " + mmeShortLabel + " = (qty dispensed)/(days supply). " + defaultOpioidRxDaysSupplyEstimate + "d supply assumed unless otherwised noted in Sig or note to pharmacy.<br>"
        		+ "*Max " + mmeShortLabel + " = max amount patient may take on a given day according to Sig, even if patient runs out of med early.</td></tr></table>\n";
        
        return mmeDetails;
    }

    private static String getUnknownMmeRxCountStr(int numEntriesWithUnknownMme) {
        if (numEntriesWithUnknownMme < 1) {
            return "";
        } else {
            return "<br>+" + numEntriesWithUnknownMme + " Rx";
        }
    }

    public static Content getContent(boolean includeAverageMmePerDay, boolean useDynamicTableFormat,
            List<MmeEntry> sortedMmeEntries, String tableMainColumnLabel, String mmeShortLabel,
            int defaultOpioidRxDaysSupplyEstimate) {
        Content content = new Content();
        boolean avgAndMaxEntriesIdentical = true;
        for (MmeEntry mmeEntry : sortedMmeEntries) {
            if (!mmeEntry.hasSameAveAndMaxOmeContent()) {
                avgAndMaxEntriesIdentical = false;
            }
        }

        content.setMainColumnName(tableMainColumnLabel);
        if (!includeAverageMmePerDay) {
            content.setMaxAvg("Max ");
        } else {
            // if (avgAndMaxEntriesIdentical) {
            // content.setMaxAvg("Avg & Max ");
            // } else {
            content.setMaxAvg("Avg ");
            // }
        }
        content.setShortLabel(mmeShortLabel);

        List<Double> totals = new ArrayList<>();
        IntStream.range(0, sortedMmeEntries.size()).forEachOrdered(i -> {
            MmeEntry mmeEntry = sortedMmeEntries.get(i);
            MedRow medRow = new MedRow();
            if (mmeEntry.isDraft()) {
                medRow.setNewContent(true);
            }
            medRow.setMedName(mmeEntry.getMedName());
            mmeEntry.getRxWarnings().stream().forEachOrdered(warning -> medRow.addWarning(warning));

            List<String> details = mmeEntry.getRxDetails();
            List<String> detailsPrimary = new ArrayList<String>();
            List<String> detailsSecondary = new ArrayList<String>();

            if (useDynamicTableFormat) {
                for (String detail : details) {
                    if (detail.startsWith("Sig:") || detail.contains("<b>")) {
                        detailsPrimary.add(detail);
                    } else {
                        detailsSecondary.add(detail);
                    }
                }

                detailsPrimary.stream().forEachOrdered(detail -> medRow.addExpansionContent(detail));
                detailsSecondary.stream().forEachOrdered(detail -> medRow.addExpansionContent(detail));
                medRow.setId(i);
            }
            // FIXME: We are leaving out non-dynamic tables

            String mmeAvgMax = "?";

            Double mme_ave = mmeEntry.getMmePerDay_ave();
            Double mme_max = mmeEntry.getMmePerDay_max();

            if (includeAverageMmePerDay && mme_ave != null) {
                mmeAvgMax = DoubleUtil.format(mme_ave) + " mg";
            } else if (mme_max != null) {
                mmeAvgMax = DoubleUtil.format(mme_max) + " mg";
            }

            totals.add(mme_ave);
            medRow.setMedQuantity(mmeAvgMax);
            content.addMedRow(medRow);
        });

        double sum = totals.stream().filter(a -> a != null).reduce(0.0, (a, total) -> a + total);

        // total
        // FIXME always returning total
        content.setCurrentValue(Double.toString(sum));

        return content;
    }
}
