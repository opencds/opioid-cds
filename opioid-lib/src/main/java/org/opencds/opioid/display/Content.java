package org.opencds.opioid.display;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.opencds.opioid.CDCMessage;
import org.opencds.opioid.mme.MmeEntry;

public class Content {
    private CDCMessage cdcMessage;
    private String cdcText;
    private String currentValue;
    private String mainColumnName;
    private String maxAvg;
    private String shortLabel;
    private List<MedRow> medRows = new ArrayList<>();
    private double total;
    private String opioidImage;
    private String imageCacheBaseUrl;
    private String newImage;
    private String expandImage;
    private String warningImage;
    private List<MmeEntry> sortedEntries;
    private boolean dynamicDetails;
    private String stat;
    private int numEntriesWithUnknownMme;
    private double totalMme;
    private String longLabel;
    private boolean underAge;
    private String additionalSummary;
    
    public void setSummaryData(String stat, int numEntriesWithUnknownMme, double totalMme, String longLabel) {
        this.stat = stat;
        this.numEntriesWithUnknownMme = numEntriesWithUnknownMme;
        this.totalMme = totalMme;
        this.longLabel = longLabel;
    }
    
    public CDCMessage getCdcMessage() {
        return cdcMessage;
    }

    public String getCdcText() {
        return cdcText;
    }

    public void setCdcTextData(CDCMessage cdcMessage, boolean isUnderAge) {
        this.cdcMessage = cdcMessage;
        this.underAge = isUnderAge;
//        this.cdcText = MmeConstants.getCdcMessage(cdcMessage, isUnderAge, shortLabel, totalMme);
    }

    public String getCurrentValue() {
        return currentValue;
    }

    public void setCurrentValue(String currentValue) {
        this.currentValue = currentValue;
    }

    public List<MedRow> getMedRows() {
        return medRows;
    }

    public String getMainColumnName() {
        return mainColumnName;
    }

    public void setMainColumnName(String mainColumnName) {
        this.mainColumnName = mainColumnName;
    }

    public String getMaxAvg() {
        return maxAvg;
    }

    public void setMaxAvg(String maxAvg) {
        this.maxAvg = maxAvg;
    }

    public String getShortLabel() {
        return shortLabel;
    }

    public void setShortLabel(String shortLabel) {
        this.shortLabel = shortLabel;
    }

    public void setMedRows(List<MedRow> medRows) {
        this.medRows = medRows;
    }

    public void addMedRow(MedRow medRow) {
        medRows.add(medRow);
    }

    public double getTotal() {
        return total;
    }
    
    public void setTotal(double total) {
        this.total = total;
    }

    public String getOpioidImage() {
        return opioidImage;
    }

    public void setOpioidImage(String opioidImage) {
        this.opioidImage = opioidImage;
    }
    
    public String getImageCacheBaseUrl() {
        return imageCacheBaseUrl;
    }
    
    public void setImageCacheBaseUrl(String url) {
        this.imageCacheBaseUrl = url;
    }

    public String getNewImage() {
        return newImage;
    }

    public void setNewImage(String newImage) {
        this.newImage = newImage;
    }

    public String getExpandImage() {
        return expandImage;
    }

    public void setExpandImage(String expandImage) {
        this.expandImage = expandImage;
    }

    public String getWarningImage() {
        return warningImage;
    }

    public void setWarningImage(String warningImage) {
        this.warningImage = warningImage;
    }
    
    public List<MmeEntry> getSortedEntries() {
        if (sortedEntries == null) {
            sortedEntries = Collections.emptyList();
        }
        return sortedEntries;
    }

    public void setSortedEntries(List<MmeEntry> sortedEntries) {
        this.sortedEntries = sortedEntries;
    }
    
    public boolean isDynamicDetails() {
        return dynamicDetails;
    }

    public void setDynamicDetails(boolean dynamicDetails) {
        this.dynamicDetails = dynamicDetails;
    }
    
    public String getStat() {
        return stat;
    }
    
    public int getNumEntriesWithUnknownMme() {
        return numEntriesWithUnknownMme;
    }
    
    public double getTotalMme() {
        return totalMme;
    }
    
    public String getLongLabel() {
        return longLabel;
    }

    public boolean isUnderAge() {
        return underAge;
    }

    public String getAdditionalSummary() {
        return additionalSummary;
    }
    
    public void setAdditionalSummary(String additionalSummary) {
        this.additionalSummary = additionalSummary;
    }

}
