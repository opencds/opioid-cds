package org.opencds.opioid.display.mme;

import java.util.HashMap;
import java.util.Map;

import org.opencds.opioid.CDCMessage;
import org.opencds.opioid.display.Constants;
import org.opencds.opioid.util.FileUtil;

public class MmeConstants implements Constants {

    private static final Map<CDCMessage, String> CDC_TEXT_TEMPLATES = new HashMap<>();
    static {
        CDC_TEXT_TEMPLATES.put(CDCMessage.AT_LEAST_ONE_NO_ABUSE,
                "At least one draft medication order which is an opioid abused in ambulatory care to consider, but {{shortLabel}}/day &lt; 50 ({{totalMme}} mg). No need for action.");
        CDC_TEXT_TEMPLATES.put(CDCMessage.AT_LEAST_ONE_NO_ABUSE_SOF, "{{shortLabel}}/day &lt; 50 ({{totalMme}} mg).");
        CDC_TEXT_TEMPLATES.put(CDCMessage.REASSESS,
                " {{forAdults}} CDC recommends reassessing evidence of individual benefits and risks when increasing dosage to &gt;= 50 {{shortLabel}}/day.");
        CDC_TEXT_TEMPLATES.put(CDCMessage.JUSTIFY,
                " {{forAdults}} CDC recommends reassessing evidence of individual benefits and risks when increasing dosage to &gt;= 50 {{shortLabel}}/day, and avoid increasing dosage to >= 90 {{shortLabel}}/day or carefully justifying such a decision.");
        CDC_TEXT_TEMPLATES.put(CDCMessage.NONE,
                " ");
    }

    private static final Map<CDCMessage, String> TOTAL_STYLE_TEMPLATES = new HashMap<>();
    static {
        TOTAL_STYLE_TEMPLATES.put(CDCMessage.AT_LEAST_ONE_NO_ABUSE, "");
        TOTAL_STYLE_TEMPLATES.put(CDCMessage.AT_LEAST_ONE_NO_ABUSE_SOF, "");
        TOTAL_STYLE_TEMPLATES.put(CDCMessage.REASSESS, "background-color: #FBCB00;");
        TOTAL_STYLE_TEMPLATES.put(CDCMessage.JUSTIFY, "color: #C30200 !important;");
        TOTAL_STYLE_TEMPLATES.put(CDCMessage.NONE, "");
    }

    private static final String NEW_CONTENT_IMG_TEMPLATE = "<span class=\"ome-image-new\" style=\"font-weight: bold\"><img src=\"{{newImage}}\" style=\"height: 12px; width: auto;\" border=\"0\"/> New</span>";
    private static final String WARNING_IMG_TEMPLATE = "<img src=\"{{warningImage}}\" style=\"height: 12px; width: auto;\" border=\"0\"/> ";

    private static final String SUMMARY_TEMPLATE = "Patient's {{stat}} {{longLabel}} ({{shortLabel}}) is <b>{{atLeast}} {{totalMme}}</b> mg/day{{notCounting}}. {{additionalSummary}}";
    private static final String NOT_COUNTING_CLAUSE_TEMPLATE = " not counting {{numEntriesWithUnknownMme}} Rx{{plurality}} where {{shortLabel}} could not be calculated";

    
    private static final String STYLE_CONTENT_TEMPLATE = FileUtil.getTemplateString("common/style.css");
    private static final String STYLE_EXPAND_TEMPLATE = FileUtil.getTemplateString("common/expand.css");
    private static final String TABLE_ROW_TEMPLATE = FileUtil.getTemplateString("common/tableRow.html");
    private static final String HTML_TEMPLATE = FileUtil.getTemplateString("mme/template.html");
    
    @Override
    public String getSummaryTemplate() {
        return SUMMARY_TEMPLATE;
    }
    
    @Override
    public String getCdcTextTemplate(CDCMessage cdcMessage) {
        if (cdcMessage == null) {
            return "";
        }
        return CDC_TEXT_TEMPLATES.getOrDefault(cdcMessage, "");
    }
    
    @Override
    public String getNotCountingClauseTemplate() {
        return NOT_COUNTING_CLAUSE_TEMPLATE;
    }
    
    @Override
    public String getStyleContentTemplate() {
        return STYLE_CONTENT_TEMPLATE;
    }
    
    @Override
    public String getStyleExpandTemplate() {
        return STYLE_EXPAND_TEMPLATE;
    }
    
    @Override
    public String getTableRowTemplate() {
        return TABLE_ROW_TEMPLATE;
    }
    
    @Override
    public String getNewContentImageTemplate() {
        return NEW_CONTENT_IMG_TEMPLATE;
    }
    
    @Override
    public String getWarningImageTemplate() {
        return WARNING_IMG_TEMPLATE;
    }
    
    @Override
    public String getTotalStyleTemplate(CDCMessage cdcMessage) {
        if (cdcMessage == null) {
            return "";
        }
        return TOTAL_STYLE_TEMPLATES.getOrDefault(cdcMessage, "");
    }
    
    @Override
    public String getHtmlTemplate() {
        return HTML_TEMPLATE;
    }

}
