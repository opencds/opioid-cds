package org.opencds.opioid.display;

import org.opencds.opioid.CDCMessage;

public interface Constants {

    String getSummaryTemplate();

    String getCdcTextTemplate(CDCMessage cdcMessage);
    
    String getNotCountingClauseTemplate();

    String getStyleContentTemplate();

    String getStyleExpandTemplate();

    String getTableRowTemplate();

    String getNewContentImageTemplate();
    
    String getWarningImageTemplate();
    
    String getTotalStyleTemplate(CDCMessage cdcMessage);
    
    String getHtmlTemplate();

}
