package org.opencds.opioid.display;

import java.util.ArrayList;
import java.util.List;

public class MedRow {
    private int id;
    private boolean newContent;
    private String medName;
    private List<String> warnings = new ArrayList<>();
    private List<String> expansionContent = new ArrayList<>();
    private String medQuantity;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isNewContent() {
        return newContent;
    }

    public void setNewContent(boolean newContent) {
        this.newContent = newContent;
    }

    public String getMedName() {
        return medName;
    }

    public void setMedName(String medName) {
        this.medName = medName;
    }

    public List<String> getWarnings() {
        return warnings;
    }

    public void setWarnings(List<String> warnings) {
        this.warnings = warnings;
    }

    public void addWarning(String warning) {
        warnings.add(warning);
    }

    public List<String> getExpansionContent() {
        return expansionContent;
    }

    public void setExpansionContent(List<String> expansionContent) {
        this.expansionContent = expansionContent;
    }

    public void addExpansionContent(String content) {
        expansionContent.add(content);
    }

    public String getMedQuantity() {
        return medQuantity;
    }

    public void setMedQuantity(String medQuantity) {
        this.medQuantity = medQuantity;
    }

}
