package org.opencds.opioid.display;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opencds.opioid.CDCMessage;
import org.opencds.opioid.util.DoubleUtil;
import org.opencds.opioid.util.StringUtil;

public class HtmlBuilder {
    private static final Log log = LogFactory.getLog(HtmlBuilder.class);
    private static final String STYLE_PFX = "<style>\n";
    private static final String STYLE_SFX = "\n</style>\n";

    private static final String FOR_ADULTS = "For adults,";

    private static final String HTTP = "http";

    private static final String SLASH = "/";

    public static String build(Content content, Constants constants) {
        StringBuilder medRows = new StringBuilder();
        String newImage = getImageLocation(content.getImageCacheBaseUrl(), content.getNewImage());
        String expandImage = getImageLocation(content.getImageCacheBaseUrl(), content.getExpandImage());
        String warningImage = getImageLocation(content.getImageCacheBaseUrl(), content.getWarningImage());
        String opioidImage = getImageLocation(content.getImageCacheBaseUrl(), content.getOpioidImage());

        content.getMedRows().stream().forEachOrdered(medRow -> medRows.append(getTableRow(constants, medRow)));
        return Template.create(constants.getHtmlTemplate()).replaceAll(Patterns.STYLE, StringUtil.emptyIfNull(buildStyle(content, constants)))
                .replaceAll(Patterns.TOTAL_STYLE, constants.getTotalStyleTemplate(content.getCdcMessage()))
                .replaceAll(Patterns.SUMMARY, StringUtil.emptyIfNull(getSummary(content, constants)))
                .replaceAll(Patterns.CDC, StringUtil.emptyIfNull(getCdcMessage(content, constants)))
                .replaceAll(Patterns.CURRENT_VALUE, StringUtil.emptyIfNull(content.getCurrentValue()))
                .replaceAll(Patterns.MAIN_COLUMN_NAME, StringUtil.emptyIfNull(content.getMainColumnName()))
                .replaceAll(Patterns.MAX_AVG, StringUtil.emptyIfNull(content.getMaxAvg()))
                .replaceAll(Patterns.SHORT_LABEL, StringUtil.emptyIfNull(content.getShortLabel()))
                .replaceAll(Patterns.TABLE_ROWS, StringUtil.emptyIfNull(medRows.toString()))
                .replaceAll(Patterns.TOTAL, DoubleUtil.doubleFormat(content.getTotal()))
                .replaceAll(Patterns.OPIOID_IMAGE, opioidImage)
                .replaceAll(Patterns.TOTAL, DoubleUtil.doubleFormat(content.getTotal()))
                .replaceAll(Patterns.NEW_IMAGE, newImage).replaceAll(Patterns.EXPAND_IMAGE, expandImage)
                .replaceAll(Patterns.WARNING_IMAGE, warningImage).value();
    }

    private static String buildStyle(Content content, Constants constants) {
        StringBuilder style = new StringBuilder("");
        if (content.isDynamicDetails()) {
            style.append(STYLE_PFX);
            String expansions = "";
            for (int k = 0; k < content.getSortedEntries().size(); k++) {
                expansions += getExpansionToggle(constants, k);
            }
            style.append(getStyleContent(content, constants, expansions));
            style.append(STYLE_SFX);
        }
        return style.toString();
    }

    private static String getStyleContent(Content content, Constants constants, String expansionsCss) {
        return Template.create(constants.getStyleContentTemplate())
                .replaceAll(Patterns.EXPAND_CSS, StringUtil.emptyIfNull(expansionsCss)).value();
    }

    private static String getExpansionToggle(Constants constants, int k) {
        String id = Integer.toString(k);
        return Template.create(constants.getStyleExpandTemplate()).replaceAll(Patterns.EXPAND_ID, StringUtil.emptyIfNull(id))
                .value();
    }

    private static String getSummary(Content content, Constants constants) {
        String atLeast = "";
        String notCounting = "";
            
        if (content.getNumEntriesWithUnknownMme() > 0) {
            atLeast = "at least ";
            String plurality = "";
            if (content.getNumEntriesWithUnknownMme() > 1) {
                plurality = "s";
            }
            notCounting = getNotCountingClause(constants, content.getNumEntriesWithUnknownMme(), plurality, content.getShortLabel());
        }
        
        return Template.create(constants.getSummaryTemplate()).replaceAll(Patterns.STAT, StringUtil.emptyIfNull(content.getStat()))
                .replaceAll(Patterns.LONG_LABEL, StringUtil.emptyIfNull(content.getLongLabel()))
                .replaceAll(Patterns.SHORT_LABEL, StringUtil.emptyIfNull(content.getShortLabel()))
                .replaceAll(Patterns.AT_LEAST, StringUtil.emptyIfNull(atLeast))
                .replaceAll(Patterns.TOTAL_MME, DoubleUtil.doubleFormat(content.getTotalMme()))
                .replaceAll(Patterns.NOT_COUNTING, StringUtil.emptyIfNull(notCounting))
                .replaceAll(Patterns.ADDITIONAL_SUMMARY, StringUtil.emptyIfNull(content.getAdditionalSummary())).value();
    }

    private static String getTableRow(Constants constants, MedRow medRow) {
        String newContent = "";
        if (medRow.isNewContent()) {
            newContent = constants.getNewContentImageTemplate();
        }
        StringBuilder expansion = new StringBuilder();
        medRow.getExpansionContent().stream()
                .forEachOrdered(ec -> expansion.append("<p class=\"ome-warn\">").append(ec + "</p>"));
        StringBuilder warnings = new StringBuilder();
        medRow.getWarnings().stream().forEachOrdered(
                warn -> warnings.append("<p class=\"ome-warn\">").append(constants.getWarningImageTemplate()).append(warn).append("</p>"));
        return Template.create(constants.getTableRowTemplate()).replaceAll(Patterns.NEW_CONTENT, StringUtil.emptyIfNull(newContent))
                .replaceAll(Patterns.MED_NAME, StringUtil.emptyIfNull(medRow.getMedName()))
                .replaceAll(Patterns.WARNINGS, StringUtil.emptyIfNull(warnings.toString()))
                .replaceAll(Patterns.EXPAND_ID, StringUtil.emptyIfNull(Integer.toString(medRow.getId())))
                .replaceAll(Patterns.EXPAND, StringUtil.emptyIfNull(expansion.toString()))
                .replaceAll(Patterns.MED_QTY, StringUtil.emptyIfNull(medRow.getMedQuantity())).value();
    }

    private static String getNotCountingClause(Constants constants, int numEntriesWithUnknownMme, String plurality, String shortLabel) {
        return Template.create(constants.getNotCountingClauseTemplate())
                .replaceAll(Patterns.NUM_ENTRIES_WITH_UNKNOWN_MME, Integer.toString(numEntriesWithUnknownMme))
                .replaceAll(Patterns.PLURALITY, StringUtil.emptyIfNull(plurality))
                .replaceAll(Patterns.SHORT_LABEL, StringUtil.emptyIfNull(shortLabel)).value();
    }

    private static String getCdcMessage(Content content, Constants constants) {
        if (content.getCdcMessage() == null || content.getCdcMessage() == CDCMessage.NONE) {
            log.warn("No CDC Message.");
            return "";
        }
        return Template.create(constants.getCdcTextTemplate(content.getCdcMessage()))
                .replaceAll(Patterns.FOR_ADULTS_PAT, StringUtil.emptyIfNull(content.isUnderAge() ? FOR_ADULTS : ""))
                .replaceAll(Patterns.SHORT_LABEL, StringUtil.emptyIfNull(content.getShortLabel()))
                .replaceAll(Patterns.TOTAL_MME, DoubleUtil.doubleFormat(content.getTotalMme())).value();
    }

    private static String getImageLocation(String baseUrl, String image) {
        if (baseUrl == null || image == null) {
            return "";
        }
        if (image.startsWith(HTTP)) {
            return image;
        } else {
            if (baseUrl.endsWith(SLASH)) {
                return baseUrl + image;
            } else {
                return baseUrl + SLASH + image;
            }
        }
    }

}
