package org.opencds.opioid.display;

import java.util.regex.Pattern;

public class Patterns {
    // for STYLE_CONTENT_TEMPLATE
    public static final Pattern EXPAND_CSS = Pattern.compile(("\\{\\{expandCss\\}\\}"));
    public static final Pattern TOTAL_STYLE = Pattern.compile(("\\{\\{totalStyle\\}\\}"));

    // for STYLE_EXPAND_TEMPLATE
    public static final Pattern EXPAND_ID = Pattern.compile(("\\{\\{id\\}\\}"));

    // for TABLE_ROW_TEMPLATE
    public static final Pattern NEW_CONTENT = Pattern.compile(("\\{\\{newContent\\}\\}"));
    public static final Pattern MED_NAME = Pattern.compile(("\\{\\{medName\\}\\}"));
    public static final Pattern WARNINGS = Pattern.compile(("\\{\\{warnings\\}\\}"));
    public static final Pattern EXPAND = Pattern.compile(("\\{\\{expand\\}\\}"));
    public static final Pattern MED_QTY = Pattern.compile(("\\{\\{medQuantity\\}\\}"));

    // for HTML_TEMPLATE
    public static final Pattern STYLE = Pattern.compile(("\\{\\{style\\}\\}"));
    public static final Pattern SUMMARY = Pattern.compile(("\\{\\{summary\\}\\}"));
    public static final Pattern CDC = Pattern.compile(("\\{\\{CDC\\}\\}"));
    public static final Pattern CURRENT_VALUE = Pattern.compile(("\\{\\{currentValue\\}\\}"));
    public static final Pattern MAIN_COLUMN_NAME = Pattern.compile(("\\{\\{mainColumnName\\}\\}"));
    public static final Pattern MAX_AVG = Pattern.compile(("\\{\\{maxAvg\\}\\}"));
    public static final Pattern SHORT_LABEL = Pattern.compile(("\\{\\{shortLabel\\}\\}"));
    public static final Pattern TABLE_ROWS = Pattern.compile(("\\{\\{tableRows\\}\\}"));
    public static final Pattern TOTAL = Pattern.compile(("\\{\\{total\\}\\}"));
    public static final Pattern OPIOID_IMAGE = Pattern.compile(("\\{\\{opioidImage\\}\\}"));
    public static final Pattern NEW_IMAGE = Pattern.compile(("\\{\\{newImage\\}\\}"));
    public static final Pattern EXPAND_IMAGE = Pattern.compile(("\\{\\{expandImage\\}\\}"));
    public static final Pattern WARNING_IMAGE = Pattern.compile(("\\{\\{warningImage\\}\\}"));

    // for SUMMARY_TEMPLATE
    public static final Pattern NUM_ENTRIES_WITH_UNKNOWN_MME = Pattern
            .compile(("\\{\\{numEntriesWithUnknownMme\\}\\}"));
    public static final Pattern PLURALITY = Pattern.compile(("\\{\\{plurality\\}\\}"));
    public static final Pattern LONG_LABEL = Pattern.compile(("\\{\\{longLabel\\}\\}"));
    public static final Pattern STAT = Pattern.compile(("\\{\\{stat\\}\\}"));
    public static final Pattern AT_LEAST = Pattern.compile(("\\{\\{atLeast\\}\\}"));
    public static final Pattern TOTAL_MME = Pattern.compile(("\\{\\{totalMme\\}\\}"));
    public static final Pattern NOT_COUNTING = Pattern.compile(("\\{\\{notCounting\\}\\}"));
    public static final Pattern ADDITIONAL_SUMMARY = Pattern.compile("\\{\\{additionalSummary\\}\\}");

    // for CDC Text
    public static final Pattern FOR_ADULTS_PAT = Pattern.compile(("\\{\\{forAdults\\}\\}"));

}
