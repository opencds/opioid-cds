package org.opencds.opioid.display;

import java.util.regex.Pattern;

/**
 * Mutates the value.
 * 
 * @author phillip
 *
 */
public class Template {
    private String value;
    
    private Template(String value) {
        this.value = value;
    }
    
    public static Template create(String value) {
        return new Template(value);
    }
    
    public Template replaceAll(Pattern pattern, String replacement) {
        value = pattern.matcher(value).replaceAll(replacement);
        return this;
    }

    public String value() {
        return value;
    }
}
