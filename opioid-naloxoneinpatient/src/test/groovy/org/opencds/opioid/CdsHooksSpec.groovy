package org.opencds.opioid

import org.hl7.fhir.instance.model.Bundle
import org.hl7.fhir.instance.model.Medication
import org.hl7.fhir.instance.model.MedicationOrder
import org.hl7.fhir.instance.model.Patient
import org.hl7.fhir.instance.model.Practitioner
import org.hl7.fhir.instance.model.Reference
import org.hl7.fhir.instance.model.Bundle.BundleEntryComponent
import org.opencds.hooks.model.context.HookContext
import org.opencds.hooks.model.context.WritableHookContext
import org.opencds.hooks.model.dstu2.util.Dstu2JsonUtil
import org.opencds.hooks.model.request.CdsRequest
import org.opencds.hooks.model.request.WritableCdsRequest
import org.opencds.hooks.model.util.JsonUtil

import ca.uhn.fhir.context.FhirContext
import spock.lang.Ignore
import spock.lang.Shared
import spock.lang.Specification

class CdsHooksSpec extends Specification {

    @Shared
    JsonUtil jsonUtil = new Dstu2JsonUtil()
    @Shared
    FhirContext ctx = FhirContext.forDstu2Hl7Org()

    @Ignore
    def 'test build request'() {
        when:
        // add in filename
        CdsRequest req = buildRequest(new File('/Users/phillip/tmp-phi/Test PHI/00'))
        
        then:
        println jsonUtil.toJson(req)
        true
    }

    private static final String PATIENT = 'Patient.json'
    private static final String MED_ORDER_BUNDLE = 'MedicationOrder_bundle.json'
    private static final String REFERENCED_MEDS = 'referenced_resources' + File.separator + 'Medication'
    private static final String REFERENCED_PRACTITIONER = 'referenced_resources' + File.separator + 'Practitioner'

    private CdsRequest buildRequest(File baseLocation) {
        CdsRequest req = new WritableCdsRequest()
        req.hookInstance = 'd1577c69-dfbe-44ad-ba6d-3e05e953b2ea'
        req.fhirServer = new URL('http://localhost:8080/fhir-wrapper/fhirServer')
        req.hook = 'encounter-view'
        req.user = 'Practitioner/phillip'
        Patient patient = ctx.newJsonParser().parseResource(Patient.class, new File(baseLocation, PATIENT).text)
        req.addPrefetchResource('patient', patient)
        
        // replace the patient in MedicationOrder below
        
        MedicationOrder medOrder = ctx.newJsonParser().parseResource(MedicationOrder.class, new File('src/test/resources/medorder_buprenorphine_patch_draft.json').text)
        medOrder.setPatient(new Reference(patient.getId()))
        println("MedOrder Patient: " + medOrder.getPatient().getReference())
        
        HookContext context = new WritableHookContext()
        context.add('patientId', patient.getIdElement().getIdPart())
        context.add('medications', new Bundle().addEntry(new BundleEntryComponent().setResource(medOrder)))
        req.context = context
        Bundle medOrderBundle = new Bundle();
        new File(baseLocation, REFERENCED_MEDS).eachFile {File medFile ->
            Medication med = ctx.newJsonParser().parseResource(Medication.class, medFile.text)
        }
        i = 0
        new File(baseLocation, REFERENCED_PRACTITIONER).eachFile {File practFile ->
            Practitioner pract = ctx.newJsonParser().parseResource(Practitioner.class, practFile.text)
            medOrderBundle.addEntry(new BundleEntryComponent().setResource(pract))
        }
        Bundle medBundle = ctx.newJsonParser().parseResource(Bundle.class, new File(baseLocation, MED_ORDER_BUNDLE).text)
        medBundle.entry.eachWithIndex {BundleEntryComponent comp, index ->
            medOrderBundle.addEntry(comp)
        }
        req.addPrefetchResource('medOrders', medOrderBundle)
        return req
    }


}
