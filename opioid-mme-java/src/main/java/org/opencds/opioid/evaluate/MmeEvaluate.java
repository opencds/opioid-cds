package org.opencds.opioid.evaluate;

import ca.uhn.fhir.context.FhirVersionEnum;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hl7.fhir.dstu2.model.Bundle;
import org.hl7.fhir.dstu2.model.Condition;
import org.hl7.fhir.dstu2.model.Medication;
import org.hl7.fhir.dstu2.model.MedicationOrder;
import org.hl7.fhir.dstu2.model.OperationOutcome;
import org.hl7.fhir.dstu2.model.Patient;
import org.hl7.fhir.dstu2.model.Resource;
import org.hl7.fhir.dstu2.model.ResourceType;
import org.opencds.hooks.engine.api.CdsHooksEvaluationContext;
import org.opencds.hooks.engine.api.CdsHooksExecutionEngine;
import org.opencds.hooks.lib.fhir.FhirContextFactory;
import org.opencds.hooks.model.dstu2.request.prefetch.Dstu2PrefetchHelper;
import org.opencds.hooks.model.request.CdsRequest;
import org.opencds.hooks.model.request.prefetch.PrefetchResult;
import org.opencds.hooks.model.response.Card;
import org.opencds.hooks.model.response.CdsResponse;
import org.opencds.hooks.model.response.Indicator;
import org.opencds.opioid.config.OpioidConfig;
import org.opencds.opioid.eval.MmeEvaluation;
import org.opencds.opioid.results.EvalResults;
import org.opencds.opioid.util.fhir.FhirUtil;
import org.opencds.opioid.util.fhir.MedOrderUtil;
import org.opencds.tools.terminology.rxnav.client.RxNavTerminologyClient;
import org.opencds.tools.terminology.umls.api.client.UmlsTerminologyClient;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MmeEvaluate implements CdsHooksExecutionEngine {
    private static final Log log = LogFactory.getLog(MmeEvaluate.class);
    private static final Dstu2PrefetchHelper prefetchHelper = new Dstu2PrefetchHelper();

    private static final String OPIOID_CONFIG_GLOBAL = "opioidConfig";
    private static final String RXNAV_CLIENT = "RxNavTerminologyClient";
    private static final String UMLS_CLIENT = "UmlsTerminologyClient";

    private static final String CONTEXT_PATIENT_ID = "patientId";
    private static final String CONTEXT_MEDICATIONS = "medications";

    @Override
    public CdsResponse evaluate(CdsRequest request, CdsHooksEvaluationContext evaluationContext) {
//            RxNavTerminologyClient rxNavClient, UmlsTerminologyClient umlsClient, OpioidConfig config) {
        log.debug(request);

        RxNavTerminologyClient rxNavClient = (RxNavTerminologyClient) evaluationContext.getGlobals().getOrDefault(RXNAV_CLIENT, new RxNavTerminologyClient());
        UmlsTerminologyClient umlsClient = (UmlsTerminologyClient) evaluationContext.getGlobals().getOrDefault(UMLS_CLIENT, new UmlsTerminologyClient());
        OpioidConfig config = (OpioidConfig) evaluationContext.getGlobals().getOrDefault(OPIOID_CONFIG_GLOBAL,
                new OpioidConfig()); // default, if not provided

        MmeEvaluation evaluation = new MmeEvaluation(rxNavClient, umlsClient, config);

        IGenericClient fhirClient = null;

        Bundle contextMedOrders = request.getContext().get(CONTEXT_MEDICATIONS, Bundle.class);
        List<MedicationOrder> medOrders = new ArrayList<>();
        medOrders.addAll(FhirUtil.findResources(contextMedOrders, ResourceType.MedicationOrder));
        List<Medication> meds = new ArrayList<>();
        meds.addAll(FhirUtil.findResources(contextMedOrders, ResourceType.Medication));

        List<Condition> conditions = new ArrayList<>();
        PrefetchResult<OperationOutcome, Resource> prefetchResource = request.getPrefetchResource("medOrders", prefetchHelper);
        if (prefetchResource != null && prefetchResource.hasResourceOfType(Bundle.class)) {
            Bundle medOrdersPrefetchBundle = prefetchResource.getResource(Bundle.class);
            medOrders.addAll(FhirUtil.findResources(medOrdersPrefetchBundle, ResourceType.MedicationOrder));
            meds.addAll(FhirUtil.findResources(medOrdersPrefetchBundle, ResourceType.Medication));
        }
        prefetchResource = request.getPrefetchResource("conditions", prefetchHelper);
        if (prefetchResource != null && prefetchResource.hasResourceOfType(Bundle.class)) {
            Bundle conditionsBundle = prefetchResource.getResource(Bundle.class);
            conditions.addAll(FhirUtil.findResources(conditionsBundle, ResourceType.Condition));
        }

        Patient patient = null;
        PrefetchResult<OperationOutcome, Resource> patientResult = request.getPrefetchResource("patient", prefetchHelper);
        if (patientResult != null && patientResult.hasResourceOfType(Patient.class)) {
            patient = patientResult.getResource(Patient.class);
        }

        // This is a potentially time-consuming step, so only conducting if
        // needed
        // TODO: refactor the FHIR client so it is pushed in if needed, so it
        // can be created once then re-used
        if (config.isQueryForMedOrders() || config.isQueryForMeds()) {
            fhirClient = request.getFhirClient(FhirContextFactory.get(FhirVersionEnum.DSTU2_HL7ORG));
        }

        try {
            String focalPersonId = request.getContext().get(CONTEXT_PATIENT_ID, String.class);
            if (config.isQueryForMedOrders()) {
                MedOrderUtil.searchAndAddMedOrdersIfNoneOtherThanDraft(fhirClient, focalPersonId, medOrders);
            }

            if (config.isQueryForMeds()) {
                MedOrderUtil.searchAndAddMedsIfReferencedByMedOrdersAndDoesNotExist(fhirClient, focalPersonId, medOrders,
                        meds);
            }

            EvalResults results = evaluation.getMmeEvaluationResults(evaluationContext.getEvalTime(), focalPersonId, evaluationContext.getServerBaseUri(), patient, medOrders,
                    meds, conditions);

            CdsResponse cdsResponse = results.getCdsResponse();
            if (!results.isReturnCard()) {
                log.info("Not returning card.");
                List<Card> cards = cdsResponse.getCards();
                cards.forEach(card -> {
                    log.info("Summary: " + card.getSummary());
                    log.info("Detail: " + card.getDetail());
                });
                return new CdsResponse();
            }
            return cdsResponse;
        } catch (Exception e) {
            CdsResponse response = new CdsResponse();
            Card card = new Card();
            card.setSummary(e.getClass().getSimpleName() + " encountered during execution");
            card.setIndicator(Indicator.CRITICAL);
            card.setDetail(e.getMessage());
            response.setCards(Collections.singletonList(card));
            return response;
        }
    }
}
