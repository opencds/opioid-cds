package org.opencds.opioid

import java.nio.file.Paths
import java.text.SimpleDateFormat

import org.kie.api.KieServices
import org.kie.api.builder.KieModule
import org.kie.api.builder.KieRepository
import org.kie.api.command.Command
import org.kie.api.io.Resource
import org.kie.api.runtime.ExecutionResults
import org.kie.api.runtime.KieContainer
import org.kie.api.runtime.StatelessKieSession
import org.kie.internal.command.CommandFactory
import org.opencds.hooks.evaluation.service.ResourceListBuilder
import org.opencds.hooks.lib.JsonUtil
import org.opencds.hooks.model.request.CdsRequest
import org.opencds.hooks.model.response.CdsResponse
import org.opencds.plugin.PluginContext
import org.opencds.plugin.PluginDataCache
import org.opencds.plugin.SupportingData
import org.opencds.plugin.SupportingDataPackage
import org.opencds.plugin.SupportingDataPackageSupplier
import org.opencds.plugin.PluginContext.PreProcessPluginContext
import org.opencds.plugin.opioid.OpioidTerminologyPreProcessPlugin
import org.opencds.plugin.support.PluginDataCacheImpl
import org.opencds.tools.terminology.api.store.DbConnection
import org.opencds.tools.terminology.rxnav.client.RxNavTerminologyClient
import org.opencds.tools.terminology.umls.api.client.UmlsTerminologyClient

import spock.lang.Ignore
import spock.lang.Shared
import spock.lang.Specification

/**
 * Created by salvador on 5/3/17.
 */
@Ignore
class EvaluateFunctionalSpec extends Specification {

    private static final String EVAL_TIME = "evalTime"
    private static final String CLIENT_LANG = "clientLanguage"
    private static final String CLIENT_TZ_OFFSET = "clientTimeZoneOffset"
    private static final String FOCAL_PERSON_ID = "focalPersonId"
    private static final String ASSERTIONS = "assertions"
    private static final String NAMED_OBJECTS = "namedObjects"
    private static final String FOCAL_PATIENT_RESOURCE = "focalPatientResource";
    private static final String IDENTIFIER_ASSIGNER = "OpenCDS";
    private static final String OPIOID_GLOBALS = "opioidGlobals";
    private static final String RXNAV_CLIENT = "RxNavTerminologyClient";
    private static final String UMLS_CLIENT = "UmlsTerminologyClient";
    
	static Map<String, String> globals;
	
    @Shared
    String url = "";
    @Shared
    String repositoryLocation = "local"; // remote or local
    @Shared
    String user = "";
    @Shared
    String pass = "";
    @Shared
    String kmScopingEntityId = "org.opencds.opioid"
    @Shared
    String kmBusinessId_immediatereleaseopioid = "opioid-cds-immediatereleaseopioid"
    @Shared
    String kmVersion = "1.0"
    @Shared
    KieContainer kieContainer
    @Shared
    JsonUtil jsonUtil

    String primaryProcess_immediatereleaseopioid = "opioidcds.immediatereleaseopioid.PrimaryProcess"
    List<Command> cmds = new ArrayList<Command<?>>()
    String clientLanguage
    String clientTimeZoneOffset
    Set<String> assertions = []
    Map<String, Object> namedObjects = [:]
    Map<String, Object> opioidGlobals = [:]
    String focalPersonId

    /** **************************************************/
    
	// evalTime: 5/1/17
	
	def "001"() {

        given:
        def payload = "test-payload.json"

        when:
		println ""
        println "Running test: " + payload
        def result = exec_immediatereleaseopioid(payload)
		println result
		println ""

        then:
        //JSONAssert.assertEquals(expected("test-output.json"), result, true)
        result instanceof CdsResponse
    }
	
	def "002"() {
		
		given:
		def payload = "test-payload-contained-draft.json"

		when:
		println ""
		println "Running test: " + payload
		def result = exec_immediatereleaseopioid(payload)
		println result
		println ""

		then:
		//JSONAssert.assertEquals(expected("test-output.json"), result, true)
		result instanceof CdsResponse	
	}
	
	def "003"() {
		
		given:
		def payload = "test-payload-code-draft.json"

		when:
		println ""
		println "Running test: " + payload
		def result = exec_immediatereleaseopioid(payload)
		println result
		println ""

		then:
		//JSONAssert.assertEquals(expected("test-output.json"), result, true)
		result instanceof CdsResponse
	}
	
	def "004"() {
		
		given:
		def payload = "test-payload-no-dosageInstructionText.json"

		when:
		println ""
		println "Running test: " + payload
		def result = exec_immediatereleaseopioid(payload)
		println result
		println ""

		then:
		//JSONAssert.assertEquals(expected("test-output.json"), result, true)
		result instanceof CdsResponse
	}
	
	def "005"() {
		
		given:
		def payload = "test-payload-lowExpectedDaysSupply.json"

		when:
		println ""
		println "Running test: " + payload
		def result = exec_immediatereleaseopioid(payload)
		println result
		println ""

		then:
		//JSONAssert.assertEquals(expected("test-output.json"), result, true)
		result instanceof CdsResponse
	}
	
	def "006"() {
		
		given:
		def payload = "test-payload-prescriber-and-dispense.json"

		when:
		println ""
		println "Running test: " + payload
		def result = exec_immediatereleaseopioid(payload)
		println result
		println ""

		then:
		//JSONAssert.assertEquals(expected("test-output.json"), result, true)
		result instanceof CdsResponse
	}
	
	def "007"() {
		
		given:
		def payload = "test-payload-pancreatic-cancer-active.json"

		when:
		println ""
		println "Running test: " + payload
		def result = exec_immediatereleaseopioid(payload)
		println result
		println ""

		then:
		//JSONAssert.assertEquals(expected("test-output.json"), result, true)
		result instanceof CdsResponse
	}
	
	def "008"() {
		
		given:
		def payload = "test-payload-pancreatic-cancer-remission.json"

		when:
		println ""
		println "Running test: " + payload
		def result = exec_immediatereleaseopioid(payload)
		println result
		println ""

		then:
		//JSONAssert.assertEquals(expected("test-output.json"), result, true)
		result instanceof CdsResponse
	}
	
	def "009"() {
		
		given:
		def payload = "test-payload-diabetes-active.json"

		when:
		println ""
		println "Running test: " + payload
		def result = exec_immediatereleaseopioid(payload)
		println result
		println ""

		then:
		//JSONAssert.assertEquals(expected("test-output.json"), result, true)
		result instanceof CdsResponse
	}
	
	def "010"() {
		
		given:
		def payload = "test-payload-urine-opiate-testing.json"

		when:
		println ""
		println "Running test: " + payload
		def result = exec_immediatereleaseopioid(payload)
		println result
		println ""

		then:
		//JSONAssert.assertEquals(expected("test-output.json"), result, true)
		result instanceof CdsResponse
	}
	
	def "011"() {
		
		given:
		def payload = "test-payload-from-epic.json"

		when:
		println ""
		println "Running test: " + payload
		def result = exec_immediatereleaseopioid(payload)
		println result
		println ""

		then:
		//JSONAssert.assertEquals(expected("test-output.json"), result, true)
		result instanceof CdsResponse
	}
	
    /** **************************************************/

    def setupSpec() {

        jsonUtil = new JsonUtil()

		KieServices ks = KieServices.Factory.get()
		kieContainer = null
		
		InputStream inputStream = Paths.get('target').toFile().listFiles().find {f -> f.name =~ /jar-with-dependencies/}?.newInputStream()
		Resource resource = ks.getResources().newInputStreamResource(inputStream)
		KieRepository kr = ks.getRepository()
		KieModule kieModule = kr.addKieModule(resource)
		kieContainer = ks.newKieContainer(kieModule.getReleaseId())
		
		try {
			OpioidTerminologyPreProcessPlugin plugin = new OpioidTerminologyPreProcessPlugin()
			Map<Class<?>, List<?>> afl = new HashMap<>();
			Map<String, String> no = new HashMap<>();
			globals = new HashMap<>()
			Map<String, SupportingData> sds = ['OPIOID_DB':
				SupportingData.create(
				'OPIOID_DB',
				'',
				'',
				'src/test/resources/OpioidManagementTerminologyKnowledge.accdb',
				'accdb',
				new Date(),
				[
					get:{
						[
							getFile:{ SupportingData sd ->
								File file = new File('src/test/resources/OpioidManagementTerminologyKnowledge.accdb')
								println "\n\n\t\tFile: " + file.getAbsolutePath() + "\n\n"
								file
							}, getBytes: {null}] as SupportingDataPackage
					}] as SupportingDataPackageSupplier)];
			PluginDataCache cache = new PluginDataCacheImpl()
			PreProcessPluginContext context = PluginContext.createPreProcessPluginContext(afl, no, globals, sds, cache);
            RxNavTerminologyClient rxNavClient = new RxNavTerminologyClient(new DbConnection(new File('/Users/phillip/tmp/terminology/LocalDataStore_RxNav_OpioidCds.accdb')))
            UmlsTerminologyClient umlsClient = new UmlsTerminologyClient(new DbConnection(new File('/Users/phillip/tmp/terminology/LocalDataStore_UMLS_OpioidCds.accdb')))
            globals.put(RXNAV_CLIENT, rxNavClient)
            globals.put(UMLS_CLIENT, umlsClient)
			plugin.execute(context)
			assert globals
			} catch (Throwable e) {
				e.printStackTrace()
				throw e
			}
    }

    def exec_immediatereleaseopioid(def fileName) {

        def commonFolder = "src/test/resources/"

        String json = new File(commonFolder + fileName).text
        def request = jsonUtil.fromJson(json, CdsRequest.class)

        ResourceListBuilder resourceListBuilder = new ResourceListBuilder();
        Map<Class<?>, List<?>> allFactLists = resourceListBuilder.buildAllFactLists(request);

		if (allFactLists != null) {
            for (Map.Entry<Class<?>, List<?>> factListEntry : allFactLists.entrySet()) {
                if (factListEntry.getValue().size() > 0) {
                    //if ("FocalPersonId".equals(factListEntry.getKey().getSimpleName()) && getFocalPersonId() == null) {
                    //    cmds.add(CommandFactory.newSetGlobal(FOCAL_PERSON_ID, factListEntry.getValue().get(0).toString()));
                    //}
                    cmds.add(CommandFactory.newInsertElements((List<?>) factListEntry.getValue(), factListEntry
                            .getKey().getSimpleName(), true, null));
                }
            }
        }

        //Date evalTime = request.getEvalTime() != null ? request.getEvalTime() : new Date()
		Date evalTime = request.getEvalTime() != null ? request.getEvalTime() : (new SimpleDateFormat("yyyy-MM-dd")).parse("2017-05-01")  
		
		//focalPersonId = "pat_id_1"
		focalPersonId = request.getPatient()
		
        cmds.add(CommandFactory.newSetGlobal(EVAL_TIME, evalTime))
        cmds.add(CommandFactory.newSetGlobal(CLIENT_LANG, clientLanguage))
        cmds.add(CommandFactory.newSetGlobal(CLIENT_TZ_OFFSET, clientTimeZoneOffset))
        cmds.add(CommandFactory.newSetGlobal(ASSERTIONS, assertions))
        cmds.add(CommandFactory.newSetGlobal(NAMED_OBJECTS, namedObjects))
        cmds.add(CommandFactory.newSetGlobal(FOCAL_PERSON_ID, focalPersonId))
        cmds.add(CommandFactory.newSetGlobal(OPIOID_GLOBALS, globals.opioidGlobals))
        cmds.add(CommandFactory.newSetGlobal(RXNAV_CLIENT, globals.get(RXNAV_CLIENT)))
        cmds.add(CommandFactory.newSetGlobal(UMLS_CLIENT, globals.get(UMLS_CLIENT)))
        cmds.add(CommandFactory.newStartProcess(primaryProcess_immediatereleaseopioid))

        StatelessKieSession statelessKnowledgeSession = kieContainer.newStatelessKieSession()

        ExecutionResults results = statelessKnowledgeSession.execute(CommandFactory.newBatchExecution(cmds))

        namedObjects.get("CdsResponse")
    }
	
    def expected(String fileName) {
        return new File("src/test/resources/${fileName}").text
    }
}