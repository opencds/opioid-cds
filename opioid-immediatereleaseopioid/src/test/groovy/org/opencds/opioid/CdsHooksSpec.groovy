package org.opencds.opioid

import org.hl7.fhir.instance.model.Patient
import org.hl7.fhir.instance.model.Practitioner
import org.hl7.fhir.instance.model.Reference
import org.hl7.fhir.instance.model.Bundle.BundleEntryComponent

import java.io.File

import org.hl7.fhir.instance.model.Bundle
import org.hl7.fhir.instance.model.Medication
import org.hl7.fhir.instance.model.MedicationOrder
import org.opencds.hooks.lib.JsonUtil
import org.opencds.hooks.model.context.MedicationPrescribeContext
import org.opencds.hooks.model.request.CdsRequest

import ca.uhn.fhir.context.FhirContext
import spock.lang.Ignore
import spock.lang.Shared
import spock.lang.Specification

class CdsHooksSpec extends Specification {

    @Shared
    JsonUtil jsonUtil = new JsonUtil()
    @Shared
    FhirContext ctx = FhirContext.forDstu2Hl7Org()

//    @Ignore
    def 'test build request'() {
        when:
        // add in filename
        CdsRequest req = buildRequest(new File('/Users/phillip/tmp-phi/Test PHI/00'))
        
        then:
        println jsonUtil.toJson(req)
        true
    }

    private static final String PATIENT = 'Patient.json'
    private static final String MED_ORDER_BUNDLE = 'MedicationOrder_bundle.json'
    private static final String REFERENCED_MEDS = 'referenced_resources' + File.separator + 'Medication'
    private static final String REFERENCED_PRACTITIONER = 'referenced_resources' + File.separator + 'Practitioner'

    private CdsRequest buildRequest(File baseLocation) {
        CdsRequest req = new CdsRequest()
        req.hookInstance = 'd1577c69-dfbe-44ad-ba6d-3e05e953b2ea'
        req.fhirServer = new URL('http://localhost:8080/fhir-wrapper/fhirServer')
        req.hook = 'encounter-view'
        req.user = 'Practitioner/phillip'
        Patient patient = ctx.newJsonParser().parseResource(Patient.class, new File(baseLocation, PATIENT).text)
        
        // replace the patient in MedicationOrder below
        
        MedicationOrder medOrder = ctx.newJsonParser().parseResource(MedicationOrder.class, new File('src/test/resources/medorder_buprenorphine_patch_draft.json').text)
        medOrder.setPatient(new Reference(patient.getId()))
        println("MedOrder Patient: " + medOrder.getPatient().getReference())
        
        req.context = new MedicationPrescribeContext(patient.getIdElement().getIdPart(), null, [medOrder])
        req.prefetch = new HashMap<>()
        int i = 0
        new File(baseLocation, REFERENCED_MEDS).eachFile {File medFile ->
            Medication med = ctx.newJsonParser().parseResource(Medication.class, medFile.text)
            req.prefetch.put("med$i".toString(), med)
        }
        i = 0
        new File(baseLocation, REFERENCED_PRACTITIONER).eachFile {File practFile ->
            Practitioner med = ctx.newJsonParser().parseResource(Practitioner.class, practFile.text)
            req.prefetch.put("pract$i".toString(), med)
        }
        Bundle medBundle = ctx.newJsonParser().parseResource(Bundle.class, new File(baseLocation, MED_ORDER_BUNDLE).text)
        medBundle.entry.eachWithIndex {BundleEntryComponent comp, index ->
            req.prefetch.put("order$index".toString(), comp.getResource())
        }
        return req
    }


}
