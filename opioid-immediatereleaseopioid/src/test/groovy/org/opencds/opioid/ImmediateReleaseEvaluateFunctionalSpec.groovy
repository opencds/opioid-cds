package org.opencds.opioid

import java.nio.file.Path
import java.nio.file.Paths
import java.sql.Connection
import java.sql.DriverManager
import java.sql.SQLException
import java.text.SimpleDateFormat
import java.util.function.Supplier

import org.hl7.fhir.instance.model.Bundle
import org.hl7.fhir.instance.model.Medication
import org.hl7.fhir.instance.model.MedicationOrder
import org.hl7.fhir.instance.model.Patient
import org.hl7.fhir.instance.model.Practitioner
import org.hl7.fhir.instance.model.Reference
import org.hl7.fhir.instance.model.Bundle.BundleEntryComponent
import org.kie.api.KieBase
import org.kie.api.KieServices
import org.kie.api.builder.KieModule
import org.kie.api.builder.KieRepository
import org.kie.api.command.Command
import org.kie.api.io.Resource
import org.kie.api.runtime.ExecutionResults
import org.kie.api.runtime.StatelessKieSession
import org.kie.internal.command.CommandFactory
import org.opencds.hooks.evaluation.service.ResourceListBuilder
import org.opencds.hooks.lib.JsonUtil
import org.opencds.hooks.model.context.MedicationPrescribeContext
import org.opencds.hooks.model.fhir.CdsHooksFhirClientFactory
import org.opencds.hooks.model.request.CdsRequest
import org.opencds.hooks.model.response.CdsResponse
import org.opencds.opioid.config.OpioidConfig
import org.opencds.opioid.config.valuesets.ChronicOpioidUseDeterminationMethod
import org.opencds.tools.terminology.api.store.DbConnection
import org.opencds.tools.terminology.rxnav.client.RxNavTerminologyClient
import org.opencds.tools.terminology.umls.api.client.UmlsTerminologyClient

import ca.uhn.fhir.context.FhirContext
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll

/**
 * Created by salvador on 5/3/17.
 */
class ImmediateReleaseEvaluateFunctionalSpec extends Specification {

    private static final String EVAL_TIME = "evalTime"
    private static final String CLIENT_LANG = "clientLanguage"
    private static final String CLIENT_TZ_OFFSET = "clientTimeZoneOffset"
    private static final String FOCAL_PERSON_ID = "focalPersonId"
    private static final String ASSERTIONS = "assertions"
    private static final String NAMED_OBJECTS = "namedObjects"
    private static final String FOCAL_PATIENT_RESOURCE = "focalPatientResource";
    private static final String IDENTIFIER_ASSIGNER = "OpenCDS";
    private static final String RXNAV_CLIENT = "RxNavTerminologyClient";
    private static final String UMLS_CLIENT = "UmlsTerminologyClient";
    private static final String RXNAV_GLOBALS = "RxNavGlobals";
    private static final String UMLS_GLOBALS = "UMLSGlobals";
    private static final String IMMEDIATERELEASEOPIOID_CONFIG_GLOBAL = "opioidConfig";
    
    static Map<String, String> globals;
    
    // Add base location for folders containing data
    static Path baseLocation = Paths.get('/Users/phillip/tmp-phi/Test PHI')
    static Path termBaseLocation = Paths.get('/Users/phillip/tmp/terminology')


    @Shared
    String url = "";
    @Shared
    String repositoryLocation = "local"; // remote or local
    @Shared
    String user = "";
    @Shared
    String pass = "";
    @Shared
    String kmScopingEntityId = "org.opencds.opioid"
    @Shared
    String kmBusinessId_immediatereleaseopioid = "opioid-cds-immediatereleaseopioid"
    @Shared
    String kmVersion = "1.0"
    @Shared
    KieBase kieContainer
    @Shared
    JsonUtil jsonUtil
    @Shared
    FhirContext ctx = FhirContext.forDstu2Hl7Org()
    @Shared
    Map<String, Double> timings = [:] as TreeMap

    Map<String, Object> opioidGlobals = [:]

    /** **************************************************/

    // evalTime: 5/1/17

    @Unroll
//    @Ignore
    def "001"() {

        when:
        println ""
        println "Running test: " + id
        File file = Paths.get(baseLocation.toString(), id).toFile()
        long t0 = System.nanoTime()
        CdsRequest request = buildRequest(file)
        timings.put("Request Build for $id", (System.nanoTime()-t0)/1e6)
        t0 = System.nanoTime()
        CdsResponse response = exec_immediatereleaseopioid(request)
        timings.put("Rule exec for $id", (System.nanoTime()-t0)/1e6)
        println response
        println ""

        then:
        response instanceof CdsResponse
        response.cards.size() == numCards
        //        response.cards[0].details.contains(someText)

        where:
        num | id | numCards //| someText
        0 | '00' | 1 //| 'this is the text'
        1 | '01' | 1
        2 | '02' | 1
        3 | '03' | 1
        4 | '04' | 1
        5 | '05' | 1
        6 | '06' | 1
        7 | '07' | 1
        8 | '08' | 1
        9 | '09' | 1
        10 | '10' | 1
        11 | '11' | 1
        12 | '12' | 1
        13 | '13' | 1
        14 | '14' | 1
        15 | '15' | 1
        16 | '16' | 1
        17 | '17' | 1
        18 | '18' | 1
        19 | '19' | 1
        20 | '20' | 1
        21 | '21' | 1
        22 | '22' | 1
        23 | '23' | 1
        24 | '24' | 1
        25 | '25' | 1
        26 | '26' | 1
        27 | '27' | 1
        28 | '28' | 1
        29 | '29' | 1
        30 | '30' | 1
        31 | '31' | 1
        32 | '32' | 1
        33 | '33' | 1
        34 | '34' | 1
        35 | '35' | 1
        36 | '36' | 1
    }

    def "002"() {

        when:
        println ""
        println "Running test: " + id
        File file = Paths.get('src','test','resources', id).toFile()
        long t0 = System.nanoTime()
        JsonUtil jsonUtil = new JsonUtil()
        CdsRequest request = jsonUtil.fromJson(file.text, CdsRequest)
        timings.put("Request Build for $id", (System.nanoTime()-t0)/1e6)
        t0 = System.nanoTime()
        CdsResponse response = exec_immediatereleaseopioid(request)
        timings.put("Rule exec for $id", (System.nanoTime()-t0)/1e6)
        println response
        println ""

        then:
        response instanceof CdsResponse
        response.cards.size() == numCards
        //        response.cards[0].details.contains(someText)
        where:
        num | id | numCards //| someText
        0 | 'dosage-instruction-oobe.json' | 1 //| 'this is the text'

    }


    private static final String PATIENT = 'Patient.json'
    private static final String MED_ORDER_BUNDLE = 'MedicationOrder_bundle.json'
    private static final String REFERENCED_MEDS = 'referenced_resources' + File.separator + 'Medication'
    private static final String REFERENCED_PRACTITIONER = 'referenced_resources' + File.separator + 'Practitioner'

    private CdsRequest buildRequest(File baseLocation) {
        CdsRequest req = new CdsRequest()
        req.hookInstance = 'd1577c69-dfbe-44ad-ba6d-3e05e953b2ea'
        req.fhirServer = new URL('http://localhost:8080/fhir-wrapper/fhirServer')
        req.hook = 'medication-prescribe'
        req.user = 'Practitioner/phillip'
        Patient patient = ctx.newJsonParser().parseResource(Patient.class, new File(baseLocation, PATIENT).text)

        // replace the patient in MedicationOrder below

        MedicationOrder medOrder = ctx.newJsonParser().parseResource(MedicationOrder.class, Paths.get('src','test','resources','medorder_buprenorphine_patch_draft.json').toFile().text)
        medOrder.setPatient(new Reference(patient.getId()))
        println("MedOrder Patient: " + medOrder.getPatient().getReference())

        req.context = new MedicationPrescribeContext(patient.getIdElement().getIdPart(), null, [medOrder])
        req.prefetch = new HashMap<>()
        int i = 0
        new File(baseLocation, REFERENCED_MEDS).eachFile {File medFile ->
            Medication med = ctx.newJsonParser().parseResource(Medication.class, medFile.text)
            req.prefetch.put("med$i".toString(), med)
        }
        i = 0
        new File(baseLocation, REFERENCED_PRACTITIONER).eachFile {File practFile ->
            Practitioner med = ctx.newJsonParser().parseResource(Practitioner.class, practFile.text)
            req.prefetch.put("pract$i".toString(), med)
        }
        Bundle medBundle = ctx.newJsonParser().parseResource(Bundle.class, new File(baseLocation, MED_ORDER_BUNDLE).text)
        medBundle.entry.eachWithIndex {BundleEntryComponent comp, index ->
            req.prefetch.put("order$index".toString(), comp.getResource())
        }
        return req
    }

    /** **************************************************/

    def setupSpec() {

        jsonUtil = new JsonUtil()

        KieServices ks = KieServices.Factory.get()
        FileInputStream fis = new FileInputStream('target/org.opencds.opioid^opioid-immediatereleaseopioid^1.0.kiebase');
        ObjectInputStream ois = new ObjectInputStream(fis);
        ks.getResources().newInputStreamResource(ois)
        kieContainer = (KieBase)ois.readObject();
        ois.close();

        try {
            globals = new HashMap<>()
            RxNavTerminologyClient rxNavClient = new RxNavTerminologyClient(new DbConnection(getConnection(Paths.get(termBaseLocation.toString(), 'LocalDataStore_RxNav_OpioidCds.accdb').toFile())))
            UmlsTerminologyClient umlsClient = new UmlsTerminologyClient(new DbConnection(getConnection(Paths.get(termBaseLocation.toString(), 'LocalDataStore_UMLS_OpioidCds.accdb').toFile())))
            globals.put(RXNAV_GLOBALS, [(RXNAV_CLIENT): rxNavClient])
            globals.put(UMLS_GLOBALS, [(UMLS_CLIENT): umlsClient])
            println "Globals------------------------------> " + globals
            assert globals
        } catch (Throwable e) {
            e.printStackTrace()
            throw e
        }
    }

    def cleanupSpec() {
        timings.each {k, v ->
            println "$k : $v ms"
        }
    }
    
    private Supplier<Connection> getConnection(File location) throws ClassNotFoundException {
        Class.forName("net.ucanaccess.jdbc.UcanaccessDriver");
        return [get: {
            try {
                return DriverManager.getConnection("jdbc:ucanaccess://" + location.getAbsolutePath() + ";memory=false");
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }] as Supplier<Connection>;
    }
    
    def exec_immediatereleaseopioid(CdsRequest request) {
        String primaryProcess_immediatereleaseopioid = "opioidcds.immediatereleaseopioid.PrimaryProcess"
        String clientLanguage
        String clientTimeZoneOffset
        Set<String> assertions = []
        Map<String, Object> namedObjects = [:]
        String focalPersonId
    
        ResourceListBuilder resourceListBuilder = new ResourceListBuilder();
        CdsHooksFhirClientFactory fhirClientFactory = new CdsHooksFhirClientFactory(request.getFhirServer(), request.getFhirAuthorization());
        Map<Class<?>, List<?>> allFactLists = resourceListBuilder.buildAllFactLists(request, fhirClientFactory);
        List<Command> cmds = new ArrayList<Command<?>>()

        if (allFactLists != null) {
            for (Map.Entry<Class<?>, List<?>> factListEntry : allFactLists.entrySet()) {
                if (factListEntry.getValue().size() > 0) {
                    //if ("FocalPersonId".equals(factListEntry.getKey().getSimpleName()) && getFocalPersonId() == null) {
                    //    cmds.add(CommandFactory.newSetGlobal(FOCAL_PERSON_ID, factListEntry.getValue().get(0).toString()));
                    //}
                    cmds.add(CommandFactory.newInsertElements((List<?>) factListEntry.getValue(), factListEntry
                            .getKey().getSimpleName(), true, null));
                }
            }
        }

        //Date evalTime = request.getEvalTime() != null ? request.getEvalTime() : new Date()
        Date evalTime = request.getEvalTime() != null ? request.getEvalTime() : (new SimpleDateFormat("yyyy-MM-dd")).parse("2018-01-12")

        //focalPersonId = "pat_id_1"
        focalPersonId = ((MedicationPrescribeContext)request.getContext()).getPatientId()

        OpioidConfig config = OpioidConfig.build(new Properties());
        config.setReturnCardForDebug(true)
        config.setNewImage('metaphorDetail - new.svg')
        config.setExpandImage('metaphorDetail - expand.svg')
        config.setWarningImage('caution-sign-svgrepo-com.svg')
        config.setChronicOpioidUseDeterminationMethod(ChronicOpioidUseDeterminationMethod.NONE)
        cmds.add(CommandFactory.newSetGlobal(EVAL_TIME, evalTime))
        cmds.add(CommandFactory.newSetGlobal(CLIENT_LANG, clientLanguage))
        cmds.add(CommandFactory.newSetGlobal(CLIENT_TZ_OFFSET, clientTimeZoneOffset))
        cmds.add(CommandFactory.newSetGlobal(ASSERTIONS, assertions))
        cmds.add(CommandFactory.newSetGlobal(NAMED_OBJECTS, namedObjects))
        cmds.add(CommandFactory.newSetGlobal(FOCAL_PERSON_ID, focalPersonId))
        cmds.add(CommandFactory.newSetGlobal(RXNAV_GLOBALS, globals.get(RXNAV_GLOBALS)))
        cmds.add(CommandFactory.newSetGlobal(UMLS_GLOBALS, globals.get(UMLS_GLOBALS)))
        cmds.add(CommandFactory.newSetGlobal(IMMEDIATERELEASEOPIOID_CONFIG_GLOBAL, config))
        cmds.add(CommandFactory.newStartProcess(primaryProcess_immediatereleaseopioid))

        StatelessKieSession statelessKnowledgeSession = kieContainer.newStatelessKieSession()

        ExecutionResults results = statelessKnowledgeSession.execute(CommandFactory.newBatchExecution(cmds))

        namedObjects.get("CdsResponse")
    }

    def expected(String fileName) {
        return new File("src/test/resources/${fileName}").text
    }
}
